	const_def 2 ; object constants

Birdon_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_BIRDON
	return

Birdon_MapEvents:
	db 0, 0 ; filler

	db 9 ; warp events
	warp_event  3,  4, BIRDON, 1 ; Mart
	warp_event  4,  9, BIRDON, 2 ; House 1
	warp_event  3, 13, BIRDON, 3 ; Wallpaper House
	warp_event  8,  5, BIRDON, 4 ; Gate
	warp_event  9,  5, BIRDON, 5 ; Gate
	warp_event  9, 13, BIRDON, 6 ; House 2
	warp_event 15,  4, BIRDON_POKECENTER_1F, 1
	warp_event 14, 15, BIRDON, 8 ; Gym
	warp_event 15, 15, BIRDON, 9 ; Gym

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

