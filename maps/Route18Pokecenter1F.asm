	const_def 2 ; object constants
	const ROUTE18POKECENTER1F_NURSE

Route18Pokecenter1F_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint
	setflag ENGINE_FLYPOINT_ROUTE_18
	return

Route18Pokecenter1FNurseScript:
	jumpstd pokecenternurse

Route18Pokecenter1F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  3,  7, ROUTE_18, 1
	warp_event  4,  7, ROUTE_18, 1
	warp_event  0,  7, POKECENTER_2F, 1

	db 0 ; coord events

	db 0 ; bg events

	db 1 ; object events
	object_event  3,  1, SPRITE_NURSE, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, Route18Pokecenter1FNurseScript, -1
