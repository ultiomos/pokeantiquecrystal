import sys, os.path

def Main(m, s):
	if m:
		o = "y"
		if os.path.isfile(sys.argv[1]+".blk"):
			o = input(sys.argv[1]+".blk already exists. Overwrite? [y/n] ")
		if o.lower() == "y":
			width = int(sys.argv[2])
			height = int(sys.argv[3])
			with open(sys.argv[1]+".blk", "wb") as f:
				f.write(b"\x00"*width*height)
	if s:
		o = "y"
		if os.path.isfile(sys.argv[1]+".asm"):
			o = input(sys.argv[1]+".asm already exists. Overwrite? [y/n] ")
		if o.lower() == "y":
			with open(sys.argv[1]+".asm", "w") as f:
				f.write("\tconst_def 2 ; object constants\n\n")
				f.write(sys.argv[1]+"_MapScripts:\n")
				f.write("\tdb 0 ; scene scripts\n\n")
				f.write("\tdb 0 ; callbacks\n\n")
				f.write(sys.argv[1]+"_MapEvents:\n")
				f.write("\tdb 0, 0 ; filler\n\n")
				f.write("\tdb 0 ; warp events\n\n")
				f.write("\tdb 0 ; coord events\n\n")
				f.write("\tdb 0 ; bg events\n\n")
				f.write("\tdb 0 ; object events\n\n")
	
if __name__ == "__main__":
	if len(sys.argv) <= 3:
		print("usage: python "+sys.argv[0]+" <map_name> <width> <height>")
	else:
		domap = False
		doscr = False
		if "-m" in sys.argv:
			domap = True
		elif "-s" in sys.argv:
			doscr = True
		else:
			domap = True
			doscr = True
		Main(domap, doscr)
