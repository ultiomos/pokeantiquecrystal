	const_def 2 ; object constants

FastShipDeck_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

FastShipDeck_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event 16,  6, FAST_SHIP_EXT, 6
	warp_event 16,  7, FAST_SHIP_EXT, 7

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

