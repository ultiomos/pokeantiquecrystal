	const_def 2 ; object constants

OldCity_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint

.FlyPoint:
	setflag ENGINE_FLYPOINT_OLD
	return
	
OldCity_MapEvents:
	db 0, 0 ; filler

	db 14 ; warp events
	warp_event  3, 26, OLD_MART, 1
	warp_event 26, 14, OLD_GYM, 1
	warp_event 27, 14, OLD_GYM, 2
	warp_event 27, 28, OLD_POKECENTER_1F, 1
	warp_event 11, 17, SPROUT_TOWER, 1
	warp_event 12, 17, SPROUT_TOWER, 2
	warp_event 18, 30, ROUTE_1_OLD_CITY_GATE, 1
	warp_event 19, 30, ROUTE_1_OLD_CITY_GATE, 2
	warp_event  4, 14, OLD_MUSEUM, 1
	warp_event  5, 14, OLD_MUSEUM, 1
	warp_event  3, 31, OLD_KURTS_HOUSE, 1
	warp_event 30, 22, OLD_BILLS_HOUSE, 1
	warp_event 10, 26, OLD_HOUSE, 1
	warp_event 22, 26, OLD_SCHOOL, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events
