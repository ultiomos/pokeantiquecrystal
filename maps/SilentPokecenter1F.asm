	const_def 2 ; object constants
	const SILENTPOKECENTER1F_NURSE

SilentPokecenter1F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

SilentPokecenter1FNurseScript:
	checkevent EVENT_GOT_A_POKEMON_FROM_OAK
	iffalse .MachineBroken
	jumpstd pokecenternurse
	
.MachineBroken:
	jumptext SilentPokecenter1FMachineBrokenText
	
SilentPokecenter1FMachineBrokenText:
	text "I'm sorry, but this"
	line "machine isn't work-"
	cont "ing right now."
	
	para "Please come back a"
	line "little later."
	done

SilentPokecenter1F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  3,  7, SILENT_HILL, 3
	warp_event  4,  7, SILENT_HILL, 3
	warp_event  0,  7, POKECENTER_2F, 1

	db 0 ; coord events

	db 0 ; bg events

	db 1 ; object events
	object_event  3,  1, SPRITE_NURSE, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentPokecenter1FNurseScript, -1
