	const_def 2 ; object constants

FountTown_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_FOUNT
	return
	
FountBounceFromMenu::
	ld hl, FountWaterfallScript.JustBounce
	call QueueScript
	ret
	
FountWaterfallScript:
	opentext
	writetext FountWaterfallText
	waitbutton
	callasm .CheckForBounce
	iffalse .NoBounce
	writetext FountWantToBounceText
	yesorno
	iffalse .NoBounce
	closetext
.JustBounce:
	callasm _GetPartyNick
	textbox FountUsedBounceText
	waitsfx
	playsound SFX_WATER_GUN
	applymovement PLAYER, FountBounceUpWaterfall
	special FadeOutPalettes
	pause 15
	warpfacing UP, RUINS_OF_ALPH_ENTRANCE, 4, 9
	end
	
.NoBounce:
	closetext
	end
	
.CheckForBounce:
	ld d, BOUNCE
	farcall CheckPartyMove
	jr c, .CheckFailed
	ld de, ENGINE_BURSTBADGE
	call .CheckBadge
	jr c, .CheckFailed
	ld a, $1
	ld [wScriptVar], a
	ret
	
.CheckBadge:
	ld b, CHECK_FLAG
	farcall EngineFlagAction
	ld a, c
	and a
	ret nz
	scf
	ret
	
.CheckFailed:
	xor a
	ld [wScriptVar], a
	ret
	
_GetPartyNick:
	farcall GetPartyNick
	ret
	
FountBounceUpWaterfall:
	step UP
	jump_step UP
	step_end
	
FountWaterfallText:
	text "There's a small"
	line "waterfall."
	done
	
FountWantToBounceText:
	text "Do you want to use"
	line "Bounce?"
	done
	
FountUsedBounceText:
	text_from_ram wStringBuffer2
	text " used"
	line "Bounce!"
	done

FountTown_MapEvents:
	db 0, 0 ; filler

	db 8 ; warp events
	warp_event  4,  3, FOUNT_TOWN, 1 ; Rocket House
	warp_event  3,  7, FOUNT_TOWN, 2 ; House 1
	warp_event  3, 12, FOUNT_POKECENTER_1F, 1
	warp_event 16, 13, FOUNT_TOWN, 4 ; Lab
	warp_event 15,  4, FOUNT_TOWN, 5 ; Mart
	warp_event 10,  6, FOUNT_TOWN, 6 ; Ruins, doesn't go anywhere
	warp_event 10, 15, FOUNT_TOWN, 7 ; Gate
	warp_event 11, 15, FOUNT_TOWN, 8 ; Gate

	db 0 ; coord events

	db 1 ; bg events
	bg_event 10,  8, BGEVENT_UP, FountWaterfallScript

	db 0 ; object events

