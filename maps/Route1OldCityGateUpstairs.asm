	const_def 2
	const ROUTE1OLDGATEUPSTAIRS_LASS
	const ROUTE1OLDGATEUPSTAIRS_GIRL
	
Route1OldCityGateUpstairs_MapScripts:
	db 0
	db 0
	
Route1OldCityGateUpstairs_MapEvents:
	db 0, 0
	
	db 1 ; warp events
	warp_event  5,  0, ROUTE_1_OLD_CITY_GATE, 5
	
	db 0 ; coord events
	
	db 2 ; bg events
	bg_event  1,  0, BGEVENT_READ, BGEvent
	bg_event  3,  0, BGEVENT_READ, BGEvent
	
	db 2 ; object events
	object_event  3,  3, SPRITE_LASS, SPRITEMOVEDATA_WANDER, 2, 2, -1, -1, PAL_NPC_GREEN, OBJECTTYPE_SCRIPT, 0, ObjectEvent, -1
	object_event  6,  4, SPRITE_TWIN, SPRITEMOVEDATA_SPINRANDOM_SLOW, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, -1
