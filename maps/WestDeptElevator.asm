	const_def 2 ; object constants

WestDeptElevator_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks
	
WestDeptElevatorScript:
	opentext
	elevator WestDeptElevatorData
	closetext
	iffalse .Done
	pause 5
	playsound SFX_ELEVATOR
	earthquake 60
	waitsfx
.Done:
	end
	
WestDeptElevatorData:
	db 5
	elevfloor FLOOR_1F, 4, WEST_DEPT_1F
	elevfloor FLOOR_2F, 3, WEST_DEPT_2F
	elevfloor FLOOR_3F, 3, WEST_DEPT_3F
	elevfloor FLOOR_4F, 3, WEST_DEPT_4F
	elevfloor FLOOR_5F, 3, WEST_DEPT_5F
	db -1

WestDeptElevator_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  1,  3, WEST_DEPT_1F, -1
	warp_event  2,  3, WEST_DEPT_1F, -1

	db 0 ; coord events

	db 1 ; bg events
	bg_event  3,  0, BGEVENT_READ, WestDeptElevatorScript

	db 0 ; object events

