	const_def 2 ; object constants
	const SILENT_TEACHER
	const SILENT_FISHER
	const SILENT_SILVER
	const SILENT_GREEN
	const SILENT_MOM

SilentHill_MapScripts:
	db 4 ; scene scripts
	scene_script .DummyScene0 ; SCENE_DEFAULT
	scene_script .DummyScene1 ; SCENE_SILENT_MET_RIVAL
	scene_script .DummyScene2 ; SCENE_SILENT_HAS_STARTER
	scene_script .DummyScene3 ; SCENE_SILENT_CAN_LEAVE

	db 2 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	callback MAPCALLBACK_TILES, .Rock

.DummyScene0:
	end

.DummyScene1:
	end

.DummyScene2:
	end

.DummyScene3:
	end

.FlyPoint:
	setflag ENGINE_FLYPOINT_SILENT
	clearevent EVENT_FIRST_TIME_BANKING_WITH_MOM
	return
	
.Rock:
	checkevent EVENT_SILENT_BOULDER_REMOVED
	iftrue .dontPlace
	changeblock 18, 6, $6B
.dontPlace
	return
	
SilentGreenStopsYou1:
	appear SILENT_GREEN
	opentext
	writetext SilentGreenText1
	waitbutton
	closetext
	playmusic MUSIC_PROF_OAK
	turnobject PLAYER, RIGHT
	applymovement SILENT_GREEN, Movement_GreenApproachTop_NBT
	opentext
	writetext SilentGreenText2
	waitbutton
	closetext
	follow SILENT_GREEN, PLAYER
	applymovement SILENT_GREEN, Movement_GreenReturnTop_NBT
	stopfollow
	jump SilentGreenFinish
SilentGreenStopsYou2:
	appear SILENT_GREEN
	opentext
	writetext SilentGreenText1
	waitbutton
	closetext
	playmusic MUSIC_PROF_OAK
	turnobject PLAYER, RIGHT
	applymovement SILENT_GREEN, Movement_GreenApproachBottom_NBT
	opentext
	writetext SilentGreenText2
	waitbutton
	closetext
	follow SILENT_GREEN, PLAYER
	applymovement SILENT_GREEN, Movement_GreenReturnBottom_NBT
	stopfollow
SilentGreenFinish:
	disappear SILENT_GREEN
	applymovement PLAYER, Movement_PlayerFollowFinish_NBT
	clearevent EVENT_SILENTLAB_OAK
	setmapscene OAKS_LAB, SCENE_SILENT_LAB_MEET_OAK
	special FadeOutPalettes
	pause 15
	warpfacing UP, OAKS_LAB, 4, 15
	end

SilentTeacherScript:
	checkflag ENGINE_BAG
	iffalse .noBag
	jumptextfaceplayer Text_CoolBackpack
	
.noBag:
	jumptextfaceplayer SilentHillNoBackpackText

SilentTechnologyGuy:
	jumptextfaceplayer Text_Technology

SilentTownSign:
	jumptext SilentSignText

SilentPlayersHouseSign:
	jumptext SilentPlayersHouseSignText

SilentOaksLabSign:
	jumptext SilentOaksLabSignText

SilentRivalsHouseSign:
	jumptext SilentRivalsHouseSignText
	
Silent_SilverEvent1
	applymovement SILENT_SILVER, Movement_SilverApproches_NBT
	turnobject PLAYER, RIGHT
	opentext
	writetext Text_Silver1
	waitbutton
	closetext
	special NameMom
	writetext Text_Silver2
	waitbutton
	closetext
	applymovement SILENT_SILVER, Movement_SilverLeaves_NBT
	disappear SILENT_SILVER
	setscene SCENE_SILENT_MET_RIVAL
	end
	
SilentMomStopsYou1:
	appear SILENT_MOM
	applymovement SILENT_MOM, Movement_MomExits
	showemote EMOTE_SHOCK, SILENT_MOM, 15
	textbox SilentMomWaitText
	applymovement SILENT_MOM, Movement_MomApproaches1
	turnobject PLAYER, RIGHT
	textbox SilentMomPhoneText
	setflag ENGINE_PHONE_CARD
	addcellnum PHONE_MOM
	opentext
	writetext SilentGotPhoneText
	playsound SFX_ITEM
	waitbutton
	closetext
	textbox SilentMomGoodLuckText
	applymovement SILENT_MOM, Movement_MomLeaves1
	disappear SILENT_MOM
	setscene SCENE_SILENT_CAN_LEAVE
	end

SilentMomStopsYou2:
	appear SILENT_MOM
	applymovement SILENT_MOM, Movement_MomExits
	showemote EMOTE_SHOCK, SILENT_MOM, 15
	textbox SilentMomWaitText
	applymovement SILENT_MOM, Movement_MomApproaches2
	turnobject PLAYER, RIGHT
	textbox SilentMomPhoneText
	setflag ENGINE_PHONE_CARD
	addcellnum PHONE_MOM
	opentext
	writetext SilentGotPhoneText
	playsound SFX_ITEM
	waitbutton
	closetext
	textbox SilentMomGoodLuckText
	applymovement SILENT_MOM, Movement_MomLeaves2
	disappear SILENT_MOM
	setscene SCENE_SILENT_CAN_LEAVE
	end
	
SilentBoulderCheck:
	conditional_event EVENT_SILENT_BOULDER_REMOVED, SilentBoulderScript
SilentBoulderScript:
	opentext
	writetext SilentTheresABoulderText
	waitbutton
	checkitem EXPLOSIVES
	iffalse .end
	writetext SilentWantToUseExplosivesText
	yesorno
	iffalse .end
	writetext SilentPlacedExplosivesText
	waitbutton
	closetext
	pause 30
	showemote EMOTE_SHOCK, PLAYER, 15
	applymovement PLAYER, SilentRunFromExplosives
	playsound SFX_EMBER
	earthquake 30
	turnobject SILENT_TEACHER, DOWN
	showemote EMOTE_SHOCK, SILENT_TEACHER, 15
	waitsfx
	setevent EVENT_SILENT_BOULDER_REMOVED
	changeblock 18, 6, $01
	opentext
	writetext SilentWhatHappenedText
	waitbutton
	reloadmappart
	closetext
	end
.end:
	closetext
	end

SilentRunFromExplosives:
	big_step LEFT
	big_step LEFT
	big_step LEFT
	big_step LEFT
	big_step LEFT
	big_step LEFT
	big_step LEFT
	big_step LEFT
	big_step LEFT
	step_end
	
Movement_MomExits:
	step DOWN
	step_end
	
Movement_MomApproaches2:
	step DOWN
Movement_MomApproaches1:
	step DOWN
	step DOWN
	step DOWN
	step LEFT
	step LEFT
	step LEFT
	step LEFT
	step_end
	
Movement_MomLeaves2:
	step UP
Movement_MomLeaves1:
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step UP
	step UP
	step UP
	step UP
	step_end

Movement_SilverApproches_NBT:
	big_step UP
	big_step UP
	big_step UP
	step UP
	step UP
	turn_head LEFT
	step_end
	
Movement_SilverLeaves_NBT:
	step DOWN
	big_step DOWN
	big_step DOWN
	big_step DOWN
	big_step DOWN
	big_step DOWN
	step_end
	
Movement_GreenApproachTop_NBT:
	step LEFT
	step LEFT
	step LEFT
	step UP
	step LEFT
	step LEFT
	step_end
	
Movement_GreenApproachBottom_NBT:
	step LEFT
	step LEFT
	step LEFT
	step LEFT
	step LEFT
	step_end
	
Movement_GreenReturnTop_NBT:
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step UP
	step_end
	
Movement_GreenReturnBottom_NBT:
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step DOWN
	step DOWN
	step DOWN
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step UP
	step_end
	
Movement_PlayerFollowFinish_NBT:
	step UP
	step_end
	
SilentHillNoBackpackText:
	text "Hey, <PLAYER>."
	line "Did you hear about"
	para "Silph's newest"
	line "backpack model?"
	done
	
SilentMomWaitText:
	text "<MOM>: <PLAYER>,"
	line "wait!"
	done
	
SilentMomPhoneText:
	text "PLACEHOLDER"
	line "Here's a phone."
	done
	
SilentMomGoodLuckText:
	text "PLACEHOLDER"
	line "Good luck."
	done
	
SilentGotPhoneText:
	text "<PLAYER> got the"
	line "Phone Card!"
	done

Text_CoolBackpack:
	text "Your backpack is"
	line "so cool!"
	
	para "Where did you get"
	line "it?"
	done

Text_Technology:
	text "Technology is"
	line "incredible!"
	
	para "Now you can edit"
	line "your favorite"
	cont "video games to"
	cont "make them even"
	cont "more fun!"
	done

Text_Silver1:
	text "<RIVAL>"
	line "PLACEHOLDER"
	done
	
Text_Silver2:
	text "<RIVAL>"
	line "PLACEHOLDER"
	done

SilentSignText:
	text "Silent Hill"

	para "A Quiet Beginning"
	line "to a Roaring"
	cont "Adventure"
	done

SilentPlayersHouseSignText:
	text "<PLAYER>'s House"
	done

SilentOaksLabSignText:
	text "Oak #MON Lab"
	line "Silent Hill Branch"
	done

SilentRivalsHouseSignText:
	text "<RIVAL>'s House"
	done
	
SilentGreenText1:
	text "Hey! Wait!"
	line "Hold on a minute!"
	done
	
SilentGreenText2:
	text "Don't you know"
	line "anything?"
	
	para "Wild #MON will"
	line "attack in tall"
	cont "grass."
	
	para "You need a #MON"
	line "of your own to"
	cont "fight back."
	
	para "<...>"
	
	para "Oh! Are you<.>"
	
	para "Why don't you come"
	line "with me?"
	done
	
SilentTheresABoulderText:
	text "There's a large"
	line "boulder blocking"
	cont "the path."
	done
	
SilentWantToUseExplosivesText:
	text "Want to use the"
	line "Explosives?"
	done
	
SilentPlacedExplosivesText:
	text "<PLAYER> placed the"
	line "explosives by the"
	cont "boulder."
	done
	
SilentWhatHappenedText:
	text "Oh! What happened?"
	line "Are you okay,"
	cont "<PLAYER>?"
	done

SilentHill_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event 14, 11, OAKS_LAB, 1
	warp_event  5,  4, PLAYERS_HOUSE_1F, 1
	warp_event 13,  4, SILENT_POKECENTER_1F, 1
	warp_event  3, 12, SILVERS_HOUSE, 1
	warp_event 15, 11, OAKS_LAB, 2

	db 5 ; coord events
	coord_event  0,  8, SCENE_SILENT_MET_RIVAL, SilentGreenStopsYou1
	coord_event  0,  9, SCENE_SILENT_MET_RIVAL, SilentGreenStopsYou2
	coord_event  5,  5, SCENE_DEFAULT, Silent_SilverEvent1
	coord_event  0,  8, SCENE_SILENT_HAS_STARTER, SilentMomStopsYou1
	coord_event  0,  9, SCENE_SILENT_HAS_STARTER, SilentMomStopsYou2

	db 6 ; bg events
	bg_event 16,  5, BGEVENT_READ, SilentTownSign
	bg_event  8,  4, BGEVENT_READ, SilentPlayersHouseSign
	bg_event 10, 11, BGEVENT_READ, SilentOaksLabSign
	bg_event  6, 12, BGEVENT_READ, SilentRivalsHouseSign
	bg_event 18,  6, BGEVENT_IFNOTSET, SilentBoulderCheck
	bg_event 18,  7, BGEVENT_IFNOTSET, SilentBoulderCheck

	db 5 ; object events
	object_event  8,  5, SPRITE_TEACHER, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 0, 1, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentTeacherScript, -1
	object_event 10, 13, SPRITE_FISHER, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 0, 1, -1, -1, PAL_NPC_GREEN, OBJECTTYPE_SCRIPT, 0, SilentTechnologyGuy, -1
	object_event  6, 10, SPRITE_SILVER, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, EVENT_SILENT_HILL_SILVER
	object_event  6,  9, SPRITE_BLUE, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, EVENT_SILENT_HILL_GREEN
	object_event  5,  4, SPRITE_MOM, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, EVENT_SILENT_HILL_MOM
