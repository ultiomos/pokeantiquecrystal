	const_def 2 ; object constants
	const HITEKKMART_CLERK

HitekkMart_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

HitekkMartClerkScript:
	opentext
	pokemart MARTTYPE_STANDARD, MART_INDIGO_PLATEAU
	closetext
	end

HitekkMart_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  4,  7, HITEKK_CITY, 8
	warp_event  5,  7, HITEKK_CITY, 8
	db 0 ; coord events

	db 0 ; bg events

	db 1 ; object events
	object_event  1,  3, SPRITE_CLERK, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, HitekkMartClerkScript, -1
