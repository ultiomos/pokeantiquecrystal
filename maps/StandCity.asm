	const_def 2 ; object constants

StandCity_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_STAND
	return

StandCity_MapEvents:
	db 0, 0 ; filler

	db 11 ; warp events
	warp_event  6, 17, STAND_CITY, 1 ; Safari Gate
	warp_event 16, 21, STAND_CITY, 2 ; Office
	warp_event 24, 17, STAND_CITY, 3 ; Lab
	warp_event 17, 31, STAND_CITY, 4 ; Rocket House
	warp_event 26, 29, STAND_CITY, 5 ; House 1
	warp_event 33, 20, STAND_POKECENTER_1F, 1
	warp_event 35, 26, STAND_CITY, 7 ; Mart
	warp_event 34, 31, STAND_CITY, 8 ; Gym
	warp_event 35, 31, STAND_CITY, 9 ; Gym
	warp_event 28, 13, STAND_CITY, 10; Gate
	warp_event 29, 13, STAND_CITY, 11; Gate

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

