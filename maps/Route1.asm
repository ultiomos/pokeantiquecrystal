	const_def 2 ; object constants

Route1_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks
	
Route1Sign1:
	jumptext Route1Sign1Text

Route1Sign2:
	jumptext Route1Sign2Text
	
Route1Sign1Text:
	text "Route 1"

	para "Old City -"
	line "Silent Hill"
	done

Route1Sign2Text:
	text "Quiet Hill"

	para "Look out for wild"
	line "#MON!"
	done

Route1_MapEvents:
	db 0, 0 ; filler

	db 6 ; warp events
	warp_event 28, 26, QUIET_HILL, 1
	warp_event 28, 27, QUIET_HILL, 2
	warp_event  8, 25, QUIET_HILL, 3
	warp_event  9, 25, QUIET_HILL, 4
	warp_event  8,  5, ROUTE_1_OLD_CITY_GATE, 3
	warp_event  9,  5, ROUTE_1_OLD_CITY_GATE, 4

	db 0 ; coord events

	db 2 ; bg events
	bg_event 20,  8, BGEVENT_READ, Route1Sign1
	bg_event 12,  7, BGEVENT_READ, Route1Sign2

	db 0 ; object events
