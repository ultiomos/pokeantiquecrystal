	const_def 2 ; object constants
	const SILENTLAB_OAK
	const SILENTLAB_GREEN
	const SILENTLAB_SILVER
	const SILENTLAB_AIDE1
	const SILENTLAB_AIDE2
	const SILENTLAB_DAISY
	const SILENTLAB_DEX1
	const SILENTLAB_DEX2

OaksLab_MapScripts:
	db 6 ; scene scripts
	scene_script .DummyScene0 ; SCENE_DEFAULT
	scene_script .MeetOak     ; SCENE_SILENT_LAB_MEET_OAK
	scene_script .GotPoke     ; SCENE_SILENT_LAB_GOT_POKE
	scene_script .DummyScene3 ; SCENE_SILENT_LAB_GOT_DEX
	scene_script .DummyScene4 ; SCENE_SILENT_LAB_FOUGHT_RIVAL
	scene_script .DummyScene5 ; SCENE_SILENT_LAB_NOTHING

	db 2 ; callbacks
	callback MAPCALLBACK_OBJECTS, .MoveSpritesCallback
	callback MAPCALLBACK_TILES, .ChangeDoorTile
	
.ChangeDoorTile:
	checkevent EVENT_SILENTLAB_DOOR_LOCKED
	iftrue .DoorLocked
	changeblock 4, 0, $09
.DoorLocked
	return
	
.MeetOak:
	appear SILENTLAB_GREEN
	jump .WalkUpToOak

.GotPoke:
	disappear SILENTLAB_OAK
	disappear SILENTLAB_SILVER
	appear SILENTLAB_AIDE1
	appear SILENTLAB_AIDE2
	appear SILENTLAB_DAISY
	clearevent EVENT_SILENTLAB_AIDE
	jump SilentLab_GotStarterScript
	end

.DummyScene0:
	end

.DummyScene3:
	end
	
.DummyScene4:
	end

.DummyScene5:
	end

.MoveSpritesCallback:
	checkscene
	ifequal SCENE_SILENT_LAB_GOT_POKE, .MoveGreenOakSilver
	ifequal SCENE_SILENT_LAB_NOTHING, .MoveGreen
	return
	
.MoveGreenOakSilver:
	moveobject SILENTLAB_SILVER, 4, 0
	moveobject SILENTLAB_OAK, 4, 0
.MoveGreen:
	moveobject SILENTLAB_GREEN, 1, 3
	return

.WalkUpToOak:
	follow SILENTLAB_GREEN, PLAYER
	applymovement SILENTLAB_GREEN, SilentLab_GreenWalkUp_NBT
	stopfollow
	turnobject PLAYER, UP
	turnobject SILENTLAB_SILVER, UP
	jump SilentLab_OakScript

SilentLab_OakScript:
	opentext
	checkflag ENGINE_PLAYER_IS_FEMALE
	iftrue .femaleText
	writetext SilentLab_OakText1Male
	jump .continue
.femaleText:
	writetext SilentLab_OakText1Female
.continue:
	waitbutton
	writetext SilentLab_OakText2
	waitbutton
	closetext
	turnobject SILENTLAB_SILVER, RIGHT
	pause 10
	opentext
	writetext SilentLab_OakText3
	waitbutton
	turnobject SILENTLAB_SILVER, UP
	closetext
	opentext
	writetext SilentLab_OakText4
	waitbutton
	writetext SilentLab_OakText5
	yesorno
	iftrue .saidYes
.butThouMust:
	writetext SilentLab_OakText6
	yesorno
	iffalse .butThouMust
.saidYes:
	writetext SilentLab_OakText7
	waitbutton
	closetext
	turnobject SILENTLAB_SILVER, RIGHT
	opentext
	writetext SilentLab_OakText8
	waitbutton
	closetext
	turnobject SILENTLAB_SILVER, UP
	opentext
	writetext SilentLab_OakText9
	waitbutton
	closetext
	applymovement SILENTLAB_OAK, SilentLab_OakEnterDoor
	disappear SILENTLAB_OAK
	applymovement SILENTLAB_SILVER, SilentLab_SilverEnterDoor
	disappear SILENTLAB_SILVER
	applymovement PLAYER, SilentLab_PlayerEnterDoor
	special FadeOutPalettes
	pause 15
	warpfacing UP, OAKS_LAB_BACK, 4, 7
	end
	
SilentLab_SilverScript:
	faceplayer
	opentext
	checkevent EVENT_GOT_DEX
	iffalse .Waiting
	writetext SilentLab_SilverMinesStrongerText
	jump .cont
.Waiting:
	writetext SilentLab_SilverWaitingText
.cont
	waitbutton
	closetext
	end
	
SilentLab_HintPoster:
	opentext
	checkevent EVENT_TEMPORARY_UNTIL_MAP_RELOAD_1
	iftrue .altmsg
	setevent EVENT_TEMPORARY_UNTIL_MAP_RELOAD_1
	writetext SilentLab_HintText1
	waitbutton
	closetext
	end
.altmsg:
	writetext SilentLab_HintText2
	waitbutton
	closetext
	clearevent EVENT_TEMPORARY_UNTIL_MAP_RELOAD_1
	end
	
SilentLab_PC:
	jumptext SilentLab_PCText
	
SilentLab_Door:
	conditional_event EVENT_SILENTLAB_DOOR_LOCKED, .Script
.Script:
	jumptext SilentLab_DoorText
	
SilentLab_GotStarterScript:
	applymovement PLAYER, SilentLab_PlayerExitDoor
	appear SILENTLAB_SILVER
	applymovement SILENTLAB_SILVER, SilentLab_SilverExitDoor
	opentext
	writetext SilentLab_OakText10
	waitbutton
	closetext
	turnobject SILENTLAB_GREEN, UP
	opentext
	writetext SilentLab_OakText11
	waitbutton
	closetext
	appear SILENTLAB_OAK
	applymovement SILENTLAB_OAK, SilentLab_OakExitDoor
	turnobject SILENTLAB_GREEN, RIGHT
	turnobject PLAYER, RIGHT
	opentext
	writetext SilentLab_OakText12
	waitbutton
	closetext
	disappear SILENTLAB_DEX1
	disappear SILENTLAB_DEX2
	setevent EVENT_GOT_DEX
	setflag ENGINE_POKEDEX
	opentext
	writetext SilentLab_GotPokedexText
	playsound SFX_DEX_FANFARE_80_109
	waitsfx
	waitbutton
	closetext
	opentext
	writetext SilentLab_OakText13
	waitbutton
	closetext
	opentext
	writetext SilentLab_OakText14
	waitbutton
	closetext
	setscene SCENE_SILENT_LAB_GOT_DEX
	setevent EVENT_SILENTLAB_DOOR_LOCKED
	end
	
SilentLab_PokedexScript:
	jumptext SilentLab_PokedexText
	
SilentLab_OakObjScript:
	jumptextfaceplayer SilentLab_OakWorldIsWaitingText
	
SilentLab_GreenScript:
	jumptextfaceplayer SilentLab_GreenDexIsToughText
	
SilentLab_TriggerBattle1:
	applymovement SILENTLAB_SILVER, SilentLab_SilverBattleApproach1
	turnobject PLAYER, RIGHT
	jump SilentLab_BattleCont1
	
SilentLab_TriggerBattle2:
	applymovement SILENTLAB_SILVER, SilentLab_SilverBattleApproach2
	turnobject PLAYER, LEFT
	jump SilentLab_BattleCont1
	
SilentLab_BattleCont1:
	opentext
	writetext SilentLab_SilverBattleIntroText
	waitbutton
	closetext
	checkevent EVENT_GOT_CHIKORITA_FROM_OAK
	iftrue .Flambear
	checkevent EVENT_GOT_FLAMBEAR_FROM_OAK
	iftrue .Cruz
	loadtrainer RIVAL1, RIVAL1_1_CHIKORITA
	jump SilentLab_BattleCont2
	
.Flambear:
	loadtrainer RIVAL1, RIVAL1_1_FLAMBEAR
	jump SilentLab_BattleCont2
	
.Cruz:
	loadtrainer RIVAL1, RIVAL1_1_CRUZ
	jump SilentLab_BattleCont2
	
SilentLab_BattleCont2:
	writecode VAR_BATTLETYPE, BATTLETYPE_CANLOSE
	winlosstext SilentLab_SilverWinText, SilentLab_SilverLossText
	startbattle
	reloadmap
	opentext
	iftrue .Won
	writetext SilentLab_SilverAfterWinText
	jump SilentLab_BattleCont3
	
.Won:
	writetext SilentLab_SilverAfterLossText
	jump SilentLab_BattleCont3
	
SilentLab_BattleCont3:
	waitbutton
	closetext
	applymovement SILENTLAB_SILVER, SilentLab_SilverBattleLeave
	disappear SILENTLAB_SILVER
	special HealParty
	setscene SCENE_SILENT_LAB_FOUGHT_RIVAL
	end
	
SilentLab_DaisyCoord1:
	applymovement SILENTLAB_DAISY, SilentLab_DaisyApproach1
	scall SilentLab_DaisyTalk
	applymovement SILENTLAB_DAISY, SilentLab_DaisyReturn1
	end
	
SilentLab_DaisyCoord2:
	applymovement SILENTLAB_DAISY, SilentLab_DaisyApproach2
	scall SilentLab_DaisyTalk
	applymovement SILENTLAB_DAISY, SilentLab_DaisyReturn2
	end

SilentLab_DaisyTalk:
	opentext
	writetext SilentLab_DaisyHelpText1
	waitbutton
	closetext
	opentext
	writetext SilentLab_GotBackpackText
	playsound SFX_ITEM
	waitsfx
	waitbutton
	closetext
	setflag ENGINE_BAG
	opentext
	writetext SilentLab_DaisyHelpText2
	buttonsound
	itemtotext POKE_BALL, MEM_BUFFER_1
	scall .getBalls
	giveitem POKE_BALL, 6
	itemnotify
	closetext
	opentext
	writetext SilentLab_DaisyHelpText3
	waitbutton
	closetext
	setscene SCENE_SILENT_LAB_NOTHING
	setmapscene SILENT_HILL, SCENE_SILENT_HAS_STARTER
	clearevent EVENT_PLAYERS_HOUSE_MOM
	end
	
.getBalls:
	jumpstd receiveitem
	end

SilentLab_AideScript:
	jumptextfaceplayer SilentLab_AideText
	
SilentLab_DaisyScript:
	jumptextfaceplayer SilentLab_DaisyText
	
SilentLab_PlayerExitDoor:
	step DOWN
	step DOWN
	step DOWN
	step LEFT
	step LEFT
	step_end
	
SilentLab_SilverExitDoor:
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step LEFT
	step LEFT
	turn_head RIGHT
	step_end

SilentLab_GreenWalkUp_NBT:
	step UP
	step UP
	step UP
	step UP
	step UP
	step UP
	step UP
	step UP
	step UP
	step UP
	step RIGHT
	turn_head UP
	step_end
	
SilentLab_OakEnterDoor:
	step UP
	step UP
	step_end
	
SilentLab_SilverEnterDoor:
	big_step UP
	big_step UP
	big_step RIGHT
	big_step UP
	big_step UP
	step_end
	
SilentLab_PlayerEnterDoor:
	step UP
	step UP
	step UP
	step UP
	step_end
	
SilentLab_OakExitDoor:
	step DOWN
	step DOWN
	step_end
	
SilentLab_SilverBattleApproach1:
	step DOWN
	step RIGHT
	step RIGHT
	step DOWN
	step DOWN
	step DOWN
	turn_head LEFT
	step_end

SilentLab_SilverBattleApproach2:
	step DOWN
	step RIGHT
	step DOWN
	step DOWN
	step DOWN
	turn_head RIGHT
	step_end
	
SilentLab_SilverBattleLeave:
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step_end
	
SilentLab_DaisyApproach1:
	step RIGHT
	step RIGHT
	step UP
	step_end
	
SilentLab_DaisyApproach2:
	step RIGHT
	step RIGHT
	step RIGHT
	step UP
	step_end
	
SilentLab_DaisyReturn1:
	step DOWN
	step LEFT
	step LEFT
	step_end
	
SilentLab_DaisyReturn2:
	step DOWN
	step LEFT
	step LEFT
	step LEFT
	step_end

SilentLab_SilverWaitingText:
	text "<RIVAL>: Oh, <PLAYER>!"
	line "I thought I was"
	cont "going to be late,"
	cont "but it looks like"
	cont "nobody is here."
	done
	
SilentLab_HintText1:
	text "Save your game"
	line "regularly!"
	done

SilentLab_HintText2:
	text "Press Start to"
	line "open the Menu!"
	done
	
SilentLab_PCText:
	text "There's an e-mail"
	line "on the screen."
	
	para "Professor Oak!"
	line "There's been a lot"
	cont "of talk about you"
	cont "ever since you"
	cont "went missing."
	
	para "The team has not"
	line "been able to find"
	cont "the #mon you"
	cont "asked about."
	
	para "I don't think we're"
	line "any closer to a"
	cont "discovery, either."
	
	para "Are you sure it's"
	line "real?"
	
	para "Your assitant"
	done
	
SilentLab_DoorText:
	text "It's locked."
	done
	
SilentLab_OakText1Male:
	text "Green: Gramps! I"
	line "brought him!"
	done
	
SilentLab_OakText1Female:
	text "Green: Gramps! I"
	line "brought her!"
	done
	
SilentLab_OakText2:
	text "Oak: Very good."
	done

SilentLab_OakText3:
	text "<RIVAL>: The man"
	line "who sent the e-"
	cont "mail was this old"
	cont "man<.>"
	done

SilentLab_OakText4:
	text "Oh! Excuse me."
	line "You're Professor"
	cont "Oak? It's good to"
	cont "meet you."
	done

SilentLab_OakText5:
	text "That's right! I am"
	line "Professor Oak."
	cont "??? PLACEHOLDER"
	cont "??? Moonrunes are"
	cont "??? confusing."
	
	para "Will you listen to"
	line "my strange story?"
	done

SilentLab_OakText6:
	text "I see<.>"
	para "??? PLACEHOLDER"
	cont "??? Google trans-"
	cont "??? late doesn't"
	cont "??? help much"
	
	para "Will you listen to"
	line "my story?"
	done

SilentLab_OakText7:
	text "About a year ago,"
	line "while I was still"
	cont "in Kanto, I met a"
	cont "kid, much like the"
	cont "two of you."
	
	para "I entrusted that"
	line "kid with a #dex"
	cont "and tasked him"
	cont "with completing"
	cont "it."
	
	para "And he did it!"
	
	para "He succeeded in"
	line "finding 150 diff-"
	cont "erent kinds of"
	cont "#mon!"
	
	para "But<...>"
	para "You see<...>"
	para "The world is wide."
	line "New #mon have"
	cont "been discovered in"
	cont "this area!"
	
	para "And so, I moved my"
	line "research to Silent"
	cont "Hill. If you"
	cont "change where you"
	cont "are, you are sure"
	cont "to encounter new"
	cont "kinds of #mon."
	
	para "<...><...><...>"
	line "<...><...><...>"
	
	para "I will continue my"
	line "research here and"
	cont "I have my grand-"
	cont "children to help"
	cont "me, as well as"
	cont "other assistants,"
	cont "but more aid is"
	cont "needed!"
	
	para "<PLAYER>! <RIVAL>!"
	para "Lend me your power"
	line "so I can study"
	cont "#mon!"
	done

SilentLab_OakText8:
	text "<RIVAL>: <PLAYER>,"
	line "this could be"
	cont "interesting!"
	done

SilentLab_OakText9:
	text "Oak: You two! Come"
	line "with me."
	done
	
SilentLab_OakText10:
	text "Green: A year ago,"
	line "my goal was to be-"
	cont "come the top #-"
	cont "mon Trainer in the"
	cont "world."
	
	para "That is, until one"
	line "kid put me in my"
	cont "place."
	
	para "But I'm glad. It's"
	line "thanks to him that"
	cont "I could follow my"
	cont "heart and assist"
	cont "my gramps with his"
	cont "#mon research."
	done
	
SilentLab_OakText11:
	text "Green: Here! This"
	line "device is called a"
	cont "#dex. It's a"
	cont "high-tech encyclo-"
	cont "pedia that auto-"
	cont "matically records"
	cont "the data of each"
	cont "and every #mon"
	cont "that you see."
	done

SilentLab_OakText12:
	text "Oak: <PLAYER>!"
	line "<RIVAL>!"
	
	para "I will entrust"
	line "each of you with a"
	cont "#dex!"
	done

SilentLab_OakText13:
	text "Oak: Having a com-"
	line "plete encyclopedia"
	cont "of every #mon<.>"
	cont "That was my dream!"
	
	para "But new #mon"
	line "are being found"
	cont "day by day! I'm"
	cont "getting old, so I"
	cont "don't have the time"
	cont "to fulfill my"
	cont "dream myself."
	
	para "That's where you"
	line "come in! I want"
	cont "you to fill it in"
	cont "my place!"
	
	para "Now, young #mon"
	line "Trainers, Fly!"
	cont "Your names will go"
	cont "down in history!"
	done

SilentLab_OakText14:
	text "<RIVAL>: Yeah!"
	line "Leave it to me,"
	cont "Professor!"
	done

SilentLab_GotPokedexText:
	text "<PLAYER> received"
	line "a #dex from"
	cont "Professor Oak!"
	done
	
SilentLab_SilverMinesStrongerText:
	text "<RIVAL>: The #-"
	line "min that I chose"
	cont "is definitely"
	cont "stronger!"
	done

SilentLab_GreenDexIsToughText:
	text "Green: I used to"
	line "work on filling up"
	cont "the #dex. It's"
	cont "hard work<.>"
	
	para "Good luck!"
	done

SilentLab_OakWorldIsWaitingText:
	text "Oak: The world of"
	line "#mon is waiting"
	cont "for you, <PLAYER>."
	
	para "Now go!"
	done

SilentLab_SilverBattleIntroText:
	text "<RIVAL>: <PLAYER>!"
	line "Since we got these"
	cont "#mon from the"
	cont "Professor<.>"
	
	para "I think a little"
	line "#mon battle is"
	cont "in order!"
	done

SilentLab_SilverWinText:
	text "<RIVAL>: Huh? I"
	line "wonder if my #-"
	cont "mon is any good<.>"
	done

SilentLab_SilverLossText:
	text "<RIVAL>: Yeah! I"
	line "totally chose a"
	cont "tough #mon!"
	done

SilentLab_SilverAfterWinText:
	text "<RIVAL>: Darn<.>"
	line "Next time I defi-"
	cont "nitely won't lose!"
	done

SilentLab_SilverAfterLossText:
	text "<RIVAL>: I'll beat"
	line "you next time we"
	cont "battle, too!"
	done

SilentLab_AideText:
	text "I'm one of Prof."
	line "Oak's aides."
	
	para "I have a feeling"
	line "that I'll be seeing"
	cont "you again."
	done

SilentLab_DaisyHelpText1:
	text "Daisy: That man"
	line "who brought you<.>"
	
	para "That's my little"
	line "brother!"
	
	para "I'm also one of"
	line "Prof. Oak's grand-"
	cont "children. He's a"
	cont "fantastic resear-"
	cont "cher and I'm happy"
	cont "to be able to help"
	cont "him."
	
	para "I can't do much for"
	line "you, but I do have"
	cont "this. It's the"
	cont "latest adventure"
	cont "backpack!"
	done

SilentLab_DaisyHelpText2:
	text "Daisy: That back-"
	line "pack has several"
	cont "different pockets"
	cont "holding different"
	cont "kinds of items."
	
	para "I'll also give you"
	line "some # Balls."
	cont "Here!"
	done

SilentLab_DaisyHelpText3:
	text "Daisy: You know,"
	line "<PLAYER><.>"
	
	para "Before you leave"
	line "town, you should"
	cont "say goodbye to"
	cont "your mother. She'll"
	cont "be worried about"
	cont "you, otherwise."
	
	para "Good luck!"
	done

SilentLab_GotBackpackText:
	text "<PLAYER> got the"
	line "#mon Backpack!"
	done

SilentLab_DaisyText:
	text "Daisy: Good luck,"
	line "<PLAYER>!"
	done

SilentLab_PokedexText:
	text "What is this? It's"
	line "pages are blank."
	done

OaksLab_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  3, 15, SILENT_HILL, 1
	warp_event  4, 15, SILENT_HILL, 5
	warp_event  4,  0, OAKS_LAB_BACK, 2

	db 5 ; coord events
	coord_event  3,  8, SCENE_SILENT_LAB_GOT_DEX, SilentLab_TriggerBattle1
	coord_event  4,  8, SCENE_SILENT_LAB_GOT_DEX, SilentLab_TriggerBattle2
	coord_event  3, 11, SCENE_SILENT_LAB_FOUGHT_RIVAL, SilentLab_DaisyCoord1
	coord_event  4, 11, SCENE_SILENT_LAB_FOUGHT_RIVAL, SilentLab_DaisyCoord2
	coord_event  4,  1, SCENE_SILENT_LAB_GOT_POKE, SilentLab_GotStarterScript

	db 3 ; bg events
	bg_event  2,  0, BGEVENT_READ, SilentLab_HintPoster
	bg_event  6,  1, BGEVENT_UP, SilentLab_PC
	bg_event  4,  0, BGEVENT_IFSET, SilentLab_Door
	
	db 8 ; object events
	object_event  4,  2, SPRITE_OAK, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLab_OakObjScript, EVENT_SILENTLAB_OAK
	object_event  4, 14, SPRITE_BLUE, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLab_GreenScript, EVENT_SILENTLAB_GREEN
	object_event  3,  4, SPRITE_SILVER, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLab_SilverScript, EVENT_SILENTLAB_STARTUP_DONE
	object_event  1,  8, SPRITE_SCIENTIST, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 0, 1, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, SilentLab_AideScript, EVENT_SILENTLAB_AIDE
	object_event  6, 12, SPRITE_SCIENTIST, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, SilentLab_AideScript, EVENT_SILENTLAB_AIDE
	object_event  1, 13, SPRITE_DAISY, SPRITEMOVEDATA_SPINRANDOM_SLOW, 0, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, SilentLab_DaisyScript, EVENT_SILENTLAB_AIDE
	object_event  0,  1, SPRITE_POKEDEX, SPRITEMOVEDATA_STILL, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLab_PokedexScript, EVENT_GOT_DEX
	object_event  1,  1, SPRITE_POKEDEX, SPRITEMOVEDATA_STILL, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLab_PokedexScript, EVENT_GOT_DEX
