	const_def 2 ; object constants

SproutTower_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks
	
SproutTower_MapEvents:
	db 0, 0 ; filler

	db 10 ; warp events
	warp_event  3,  7, OLD_CITY, 5
	warp_event  4,  7, OLD_CITY, 6
	warp_event  0,  1, SPROUT_TOWER, 4
	warp_event  0, 13, SPROUT_TOWER, 3
	warp_event  7, 19, SPROUT_TOWER, 6
	warp_event 21,  7, SPROUT_TOWER, 5
	warp_event 14,  1, SPROUT_TOWER, 8
	warp_event 14, 13, SPROUT_TOWER, 7
	warp_event 21, 19, SPROUT_TOWER, 10
	warp_event 33,  5, SPROUT_TOWER, 9

	db 0 ; coord events

	db 0 ; bg events

	db 6 ; object events
