	const_def 2 ; object constants

NorthTown_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_NORTH
	return

NorthTown_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event 15,  5, NORTH_TOWN, 1 ; Cave
	warp_event  9,  7, NORTH_TOWN, 2 ; House 1
	warp_event  9, 11, NORTH_TOWN, 3 ; House 2
	warp_event 19,  8, NORTH_TOWN, 4 ; Mart
	warp_event 17, 12, NORTH_POKECENTER_1F, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

