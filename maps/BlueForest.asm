	const_def 2 ; object constants

BlueForest_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_BLUEFOREST
	return

BlueForest_MapEvents:
	db 0, 0 ; filler

	db 9 ; warp events
	warp_event  5,  5, BLUE_FOREST, 1 ; Cave
	warp_event  9,  9, BLUE_FOREST, 2 ; Agatha's House
	warp_event  3, 22, BLUE_FOREST, 3 ; House 1
	warp_event 13, 18, BLUE_POKECENTER_1F, 1
	warp_event 19, 13, BLUE_FOREST, 5 ; House 2
	warp_event 25,  6, BLUE_FOREST, 6 ; Mart
	warp_event 27, 11, BLUE_FOREST, 7 ; House 3
	warp_event 26, 21, BLUE_FOREST, 8 ; Gym
	warp_event 27, 21, BLUE_FOREST, 9 ; Gym

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

