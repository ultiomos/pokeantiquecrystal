	const_def 2 ; object constants

Route12WestCityGate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route12WestCityGate_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event  4,  0, ROUTE_12, 1
	warp_event  5,  0, ROUTE_12, 2
	warp_event  4,  7, WEST_CITY, 3
	warp_event  5,  7, WEST_CITY, 4
	warp_event  1,  0, ROUTE_12_WEST_GATE_UPSTAIRS, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

