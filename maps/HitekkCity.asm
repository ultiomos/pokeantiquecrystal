	const_def 2 ; object constants

HitekkCity_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_HITEKK
	return
	
HitekkCity_MapEvents:
	db 0, 0 ; filler

	db 13 ; warp events
	warp_event 22,  5, HITEKK_CITY, 1 ; Bridge Gate
	warp_event 23,  5, HITEKK_CITY, 2 ; Bridge Gate
	warp_event 10, 11, HITEKK_CITY, 3 ; Gym
	warp_event 11, 11, HITEKK_CITY, 4 ; Gym
	warp_event 31, 10, HITEKK_POKECENTER_1F, 1
	warp_event  7, 17, HITEKK_CITY, 6 ; Fishing Guru's House
	warp_event 15, 17, HITEKK_CITY, 7 ; Sailor's House
	warp_event 31, 16, HITEKK_MART, 1
	warp_event 33, 20, HITEKK_CITY, 9 ; Imposter Oak's House
	warp_event  6, 27, HITEKK_CITY, 10 ; Museum
	warp_event  7, 27, HITEKK_CITY, 11 ; Museum
	warp_event 22, 31, HITEKK_PORT, 1
	warp_event 23, 31, HITEKK_PORT, 2

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

