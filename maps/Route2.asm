	const_def 2 ; object constants

Route2_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route2_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  6,  5, ROUTE_2_WEST_GATE, 3
	warp_event 15,  4, ROUTE_2_GAME_HOUSE, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events
