	const_def 2 ; object constants

SugarIsland_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_SUGAR
	return

SugarIsland_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event  5,  5, SUGAR_ISLAND, 1 ; Wierd House
	warp_event  5, 10, SUGAR_ISLAND, 2 ; Mart
	warp_event  9, 10, SUGAR_POKECENTER_1F, 1
	warp_event 15,  9, SUGAR_ISLAND, 4 ; Fishing Guru
	warp_event 13,  5, SUGAR_ISLAND, 5 ; Cave

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

