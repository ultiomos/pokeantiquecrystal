	const_def 2 ; object constants

WestRadio1F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

WestRadio1F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  2,  7, WEST_CITY, 5
	warp_event  3,  7, WEST_CITY, 6
	warp_event  7,  0, WEST_RADIO_2F, 2

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

