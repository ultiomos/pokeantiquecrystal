	const_def 2 ; object constants

RouteK22Gate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

RouteK22Gate_MapEvents:
	db 0, 0 ; filler

	db 6 ; warp events
	warp_event  0, 17, ROUTE_19, 1
	warp_event  1, 17, ROUTE_19, 1
	warp_event 28, 17, ROUTE_K22, 1
	warp_event 29, 17, ROUTE_K22, 1
	warp_event 20,  0, ROUTE_K22_GATE, 5 ; Route K23
	warp_event 21,  0, ROUTE_K22_GATE, 6 ; Route K23

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

