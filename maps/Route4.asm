	const_def 2 ; object constants

Route4_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route4_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event 10,  9, ROUTE_4_SOUTH_GATE, 3
	warp_event 11,  9, ROUTE_4_SOUTH_GATE, 4

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

