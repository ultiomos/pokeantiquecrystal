	const_def 2 ; object constants

WestRadio2F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

WestRadio2F_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  0,  0, WEST_RADIO_3F, 1
	warp_event  7,  0, WEST_RADIO_1F, 3

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

