	const_def 2 ; object constants

Route14to15Gate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route14to15Gate_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event  4,  0, ROUTE_14, 1
	warp_event  5,  0, ROUTE_14, 2
	warp_event  4,  7, ROUTE_15, 1
	warp_event  5,  7, ROUTE_15, 2
	warp_event  1,  0, ROUTE_14_TO_15_GATE_UPSTAIRS, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

