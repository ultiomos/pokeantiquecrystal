	const_def 2 ; object constants

WestDept4F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

WestDept4F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event 12, 0, WEST_DEPT_5F, 1
	warp_event 15, 0, WEST_DEPT_3F, 2
	warp_event  2,  0, WEST_DEPT_ELEVATOR, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

