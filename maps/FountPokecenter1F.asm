	const_def 2 ; object constants
	const FOUNTPOKECENTER1F_NURSE

FountPokecenter1F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

FountPokecenter1FNurseScript:
	jumpstd pokecenternurse

FountPokecenter1F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  3,  7, FOUNT_TOWN, 3
	warp_event  4,  7, FOUNT_TOWN, 3
	warp_event  0,  7, POKECENTER_2F, 1

	db 0 ; coord events

	db 0 ; bg events

	db 1 ; object events
	object_event  3,  1, SPRITE_NURSE, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, FountPokecenter1FNurseScript, -1
