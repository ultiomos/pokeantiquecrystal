	const_def 2 ; object constants

Route2GameHouse_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route2GameHouse_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  4,  7, ROUTE_2, 2
	warp_event  5,  7, ROUTE_2, 2

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

