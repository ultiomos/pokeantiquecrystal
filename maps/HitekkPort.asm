	const_def 2 ; object constants
	const HITEKK_PORT_BOAT_GUARD

HitekkPort_MapScripts:
	db 2 ; scene scripts
	scene_script .DummyScene0 ; SCENE_DEFAULT
	scene_script .Exit        ; SCENE_HIGH_TECH_PORT_EXIT

	db 0 ; callbacks
	
.DummyScene0:
	end
	
.Exit:
	applymovement PLAYER, HitekkPortPlayerExit
	setscene SCENE_DEFAULT
	end
	
HitekkPortBoatGuardScript:
	end
	
HitekkPortBoatScript:
	turnobject HITEKK_PORT_BOAT_GUARD, RIGHT
	questionbox HitekkPortWantToBoardText
	iffalse .dontBoard
	textbox HitekkPortHaveTicketText
	checkitem SHIP_TICKET
	iffalse .noTicket
	textbox HitekkPortGoAheadText
	setmapscene FAST_SHIP_EXT, SCENE_FAST_SHIP_EXT_ENTER
	clearevent EVENT_FAST_SHIP_HAS_ARRIVED
	writebyte 1
	copyvartobyte wFastShipID
	follow PLAYER, HITEKK_PORT_BOAT_GUARD
	applymovement PLAYER, HitekkPortPlayerApproachShip
	stopfollow
	pause 15
	applymovement PLAYER, HitekkPortPlayerEnterShip
	special FadeOutPalettes
	pause 15
	warp FAST_SHIP_EXT, 9, 1
	end
	
.noTicket:
	textbox HitekkPortNoTicketText
.dontBoard:
	applymovement PLAYER, HitekkPortPlayerMoveUp
	end
	
HitekkPortPlayerExit:
	step UP
	step UP
	step UP
	step UP
	step UP
	step UP
	step UP
	step_end

HitekkPortPlayerMoveUp:
	step UP
	step_end
	
HitekkPortPlayerApproachShip:
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	turn_head UP
	step_end
	
HitekkPortPlayerEnterShip:
	step DOWN
	step_end

HitekkPortWantToBoardText:
	text "PLACEHOLDER"
	line "Want to board?"
	done
	
HitekkPortHaveTicketText:
	text "PLACEHOLDER"
	line "Where's ticket?"
	done
	
HitekkPortGoAheadText:
	text "PLACEHOLDER"
	line "Go ahead."
	done
	
HitekkPortNoTicketText:
	text "PLACEHOLDER"
	line "You have no ticket"
	done
	
HitekkPort_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event 10,  4, HITEKK_CITY, 12
	warp_event 11,  4, HITEKK_CITY, 13

	db 1 ; coord events
	coord_event  7, 21, SCENE_DEFAULT, HitekkPortBoatScript

	db 0 ; bg events

	db 1 ; object events
	object_event  6, 21, SPRITE_SAILOR, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, HitekkPortBoatGuardScript, -1

