	const_def 2 ; object constants
	const PALLET_DUMMY_BLAINE
	const PALLET_DUMMY_PERSON_1
	const PALLET_DUMMY_PERSON_2
	const PALLET_DUMMY_PERSON_3

PalletDummy_MapScripts:
	db 1 ; scene scripts
	scene_script .DefScene ; SCENE_DEFAULT

	db 0 ; callbacks
	
.DefScene
	applymovement PALLET_DUMMY_BLAINE, PalletDummyBlaineMovement1
	textbox PalletDummyItsOverText
	pause 15
	applymovement PALLET_DUMMY_BLAINE, PalletDummyBlaineMovement2
	textbox PalletDummyThanksText
	pause 15
	special FadeOutPalettes
	pause 15
	warpfacing RIGHT, PALLET_TOWN, 6, 7
	end
	
PalletDummyBlaineMovement1:
	step UP
	step UP
	step UP
	step UP
	step_end
	
PalletDummyBlaineMovement2:
	step UP
	step LEFT
	step_end
	
PalletDummyItsOverText:
	text "Blaine: Everything"
	line "on the Island is"
	para "currently being"
	line "handled, now."
	done

PalletDummyThanksText:
	text "Thanks for your"
	line "help. What's your"
	cont "name?"
	
	para "<...><...><...><...>"
	line "<...><...><...><...>"
	
	para "Okay, <PLAYER>. I'll"
	line "take these people"
	cont "to somewhere safe."
	
	para "Maybe we'll meet"
	line "again."
	done

PalletDummy_MapEvents:
	db 0, 0 ; filler

	db 0 ; warp events

	db 0 ; coord events

	db 0 ; bg events

	db 4 ; object events
	object_event  8, 12, SPRITE_BLAINE, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, -1
	object_event  8,  5, SPRITE_TEACHER, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, -1
	object_event 10,  7, SPRITE_YOUNGSTER, SPRITEMOVEDATA_STANDING_LEFT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, -1
	object_event  7,  8, SPRITE_COOLTRAINER_F, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, -1
