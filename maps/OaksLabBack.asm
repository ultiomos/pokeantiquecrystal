	const_def 2
	const SILENTLABBACK_OAK
	const SILENTLABBACK_SILVER
	const SILENTLABBACK_BALL1
	const SILENTLABBACK_BALL2
	const SILENTLABBACK_BALL3
	
OaksLabBack_MapScripts:
	db 3 ; scene scripts
	scene_script .FirstEntry ; SCENE_DEFAULT
	scene_script .DummyScene1 ; SCENE_SILENTLABBACK_GETTING_POKE
	scene_script .DummyScene2 ; SCENE_SILENTLABBACK_GOT_POKE
	
	db 0 ; callbacks
	
.FirstEntry:
	priorityjump SilentLabBack_PokeIntroScript
	
.DummyScene1:
	end
	
.DummyScene2:
	end
	
SilentLabBack_PokeIntroScript:
	applymovement PLAYER, SilentLabBack_PlayerApproachPokes
	turnobject SILENTLABBACK_SILVER, UP
	opentext
	writetext SilentLabBack_OakText1
	waitbutton
	closetext
	opentext
	writetext SilentLabBack_OakText2
	waitbutton
	closetext
	opentext
	writetext SilentLabBack_OakText3
	waitbutton
	closetext
	setscene SCENE_SILENTLABBACK_GETTING_POKE
	end
	
SilentLabBack_OakScript:
	checkevent EVENT_GOT_A_POKEMON_FROM_OAK
	iftrue .AfterText
	jumptextfaceplayer SilentLabBack_OakText
.AfterText:
	jumptextfaceplayer SilentLabBack_OakAfterGetText
	
SilentLabBack_SilverScript:
	checkevent EVENT_GOT_A_POKEMON_FROM_OAK
	iftrue .AfterText
	jumptextfaceplayer SilentLabBack_SilverText
.AfterText:
	jumptextfaceplayer SilentLabBack_SilverAfterGetText

SilentLabBack_FlambearBallScript:
	checkevent EVENT_GOT_A_POKEMON_FROM_OAK
	iftrue SilentLabBack_AlreadyHavePoke
	turnobject SILENTLABBACK_OAK, DOWN
	refreshscreen
	pokepic FLAMBEAR
	cry FLAMBEAR
	waitbutton
	closepokepic
	opentext
	writetext SilentLabBack_FlambearBallText
	yesorno
	iffalse SilentLabBack_DidntChooseStarter
	disappear SILENTLABBACK_BALL1
	setevent EVENT_GOT_FLAMBEAR_FROM_OAK
	writetext SilentLabBack_EnergeticText
	buttonsound
	waitsfx
	pokenamemem FLAMBEAR, MEM_BUFFER_0
	writetext SilentLabBack_GotStarterText
	playsound SFX_CAUGHT_MON
	waitsfx
	buttonsound
	givepoke FLAMBEAR, 5, BERRY
	closetext
	applymovement SILENTLABBACK_SILVER, SilentLabBack_SilverMoveToCruz
	opentext
	writetext SilentLabBack_RivalTakeText
	waitbutton
	pokenamemem CRUZ, MEM_BUFFER_0
	disappear SILENTLABBACK_BALL2
	jump SilentLabBack_FinishStarter
	
SilentLabBack_CruzBallScript:
	checkevent EVENT_GOT_A_POKEMON_FROM_OAK
	iftrue SilentLabBack_AlreadyHavePoke
	turnobject SILENTLABBACK_OAK, DOWN
	refreshscreen
	pokepic CRUZ
	cry CRUZ
	waitbutton
	closepokepic
	opentext
	writetext SilentLabBack_CruzBallText
	yesorno
	iffalse SilentLabBack_DidntChooseStarter
	disappear SILENTLABBACK_BALL2
	setevent EVENT_GOT_CRUZ_FROM_OAK
	writetext SilentLabBack_EnergeticText
	buttonsound
	waitsfx
	pokenamemem CRUZ, MEM_BUFFER_0
	writetext SilentLabBack_GotStarterText
	playsound SFX_CAUGHT_MON
	waitsfx
	buttonsound
	givepoke CRUZ, 5, BERRY
	closetext
	applymovement SILENTLABBACK_SILVER, SilentLabBack_SilverMoveToChikorita
	opentext
	writetext SilentLabBack_RivalTakeText
	waitbutton
	pokenamemem CHIKORITA, MEM_BUFFER_0
	disappear SILENTLABBACK_BALL3
	jump SilentLabBack_FinishStarter
	
SilentLabBack_ChikoritaBallScript:
	checkevent EVENT_GOT_A_POKEMON_FROM_OAK
	iftrue SilentLabBack_AlreadyHavePoke
	turnobject SILENTLABBACK_OAK, DOWN
	refreshscreen
	pokepic CHIKORITA
	cry CHIKORITA
	waitbutton
	closepokepic
	opentext
	writetext SilentLabBack_ChikoritaBallText
	yesorno
	iffalse SilentLabBack_DidntChooseStarter
	disappear SILENTLABBACK_BALL3
	setevent EVENT_GOT_CHIKORITA_FROM_OAK
	writetext SilentLabBack_EnergeticText
	buttonsound
	waitsfx
	pokenamemem CHIKORITA, MEM_BUFFER_0
	writetext SilentLabBack_GotStarterText
	playsound SFX_CAUGHT_MON
	waitsfx
	buttonsound
	givepoke CHIKORITA, 5, BERRY
	closetext
	applymovement SILENTLABBACK_SILVER, SilentLabBack_SilverMoveToFlambear
	opentext
	writetext SilentLabBack_RivalTakeText
	waitbutton
	pokenamemem FLAMBEAR, MEM_BUFFER_0
	disappear SILENTLABBACK_BALL1
	jump SilentLabBack_FinishStarter

SilentLabBack_DidntChooseStarter:
	writetext SilentLabBack_DidntChooseText
	waitbutton
	closetext
	end
	
SilentLabBack_FinishStarter:
	writetext SilentLabBack_RivalGotStarterText
	playsound SFX_CAUGHT_MON
	waitsfx
	buttonsound
	closetext
	setevent EVENT_GOT_A_POKEMON_FROM_OAK
	setscene SCENE_SILENTLABBACK_GOT_POKE
	setmapscene OAKS_LAB, SCENE_SILENT_LAB_GOT_POKE
	end
	
SilentLabBack_AlreadyHavePoke:
	jumptext SilentLabBack_DontBeGreedyText

SilentLabBack_CantLeave:
	opentext
	writetext SilentLabBack_CantLeaveText
	waitbutton
	closetext
	applymovement PLAYER, SilentLabBack_MoveUp
	end
	
SilentLabBack_PlayerApproachPokes:
	step UP
	step UP
	step UP
	step_end
	
SilentLabBack_SilverMoveToChikorita:
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step UP
	step_end
	
SilentLabBack_SilverMoveToFlambear:
	step RIGHT
	step RIGHT
	step UP
	step_end
	
SilentLabBack_SilverMoveToCruz:
	step RIGHT
	step RIGHT
	step RIGHT
	step UP
	step_end
	
SilentLabBack_MoveUp:
	step UP
	step_end
	
SilentLabBack_OakText1:
	text "Oak: Here I have"
	line "three #mon."
	cont "Aha!"
	
	para "You can each have"
	line "one of them."
	cont "Come on!"
	done
	
SilentLabBack_OakText2:
	text "<RIVAL>: Oh, me"
	line "too! Give me one,"
	cont "too!"
	done

SilentLabBack_OakText3:
	text "Oak: Now, don't"
	line "panic, <RIVAL>."
	
	para "Take whichever one"
	line "you like!"
	done

SilentLabBack_OakText:
	text "Oak: Go ahead,"
	line "<PLAYER>. Which"
	cont "#mon do you"
	cont "want?"
	done

SilentLabBack_OakAfterGetText:
	text "Oak: Now, even if"
	line "a wild #mon"
	cont "attacks, you can"
	cont "make it to the"
	cont "next town!"
	done

SilentLabBack_SilverText:
	text "<RIVAL>: It's al-"
	line "right, <PLAYER>."
	cont "You can choose"
	cont "first."
	done

SilentLabBack_SilverAfterGetText:
	text "<RIVAL>: <PLAYER>,"
	line "your #mon is"
	cont "great! But mine's"
	cont "a little better,"
	cont "don't you think?"
	done

SilentLabBack_FlambearBallText:
	text "Oak: Ho! So, you"
	line "want the Fire"
	cont "#mon Flambear?"
	done

SilentLabBack_CruzBallText:
	text "Oak: Hm. So, you"
	line "want the Water"
	cont "#mon Cruz?"
	done

SilentLabBack_ChikoritaBallText:
	text "Oak: Ah! So, you"
	line "want the Grass"
	cont "#mon Chikorita?"
	done

SilentLabBack_DidntChooseText:
	text "Then which do you"
	line "want?"
	done

SilentLabBack_EnergeticText:
	text "This #mon is"
	line "really energetic!"
	done

SilentLabBack_GotStarterText:
	text "<PLAYER> received"
	line "@"
	text_from_ram wStringBuffer3
	text " from"
	cont "Prof. Oak!"
	done

SilentLabBack_RivalTakeText:
	text "<RIVAL>: Then, I'll"
	line "take this one!"
	done

SilentLabBack_RivalGotStarterText:
	text "<RIVAL> received"
	line "@"
	text_from_ram wStringBuffer3
	text " from"
	cont "Prof. Oak!"
	done

SilentLabBack_DontBeGreedyText:
	text "Oak: Don't be"
	line "greedy!"
	done
	
SilentLabBack_CantLeaveText:
	text "Where are you"
	line "going?"
	done

OaksLabBack_MapEvents:
	db 0, 0
	
	db 2 ; warp events
	warp_event  3,  7, OAKS_LAB, 3
	warp_event  4,  7, OAKS_LAB, 3
	
	db 2 ; coord events
	coord_event  3,  7, SCENE_SILENTLABBACK_GETTING_POKE, SilentLabBack_CantLeave
	coord_event  4,  7, SCENE_SILENTLABBACK_GETTING_POKE, SilentLabBack_CantLeave
	
	db 0 ; bg events
	
	db 5 ; object events
	object_event  4,  2, SPRITE_OAK, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLabBack_OakScript, -1
	object_event  3,  4, SPRITE_SILVER, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLabBack_SilverScript, -1
	object_event  5,  2, SPRITE_POKE_BALL, SPRITEMOVEDATA_STILL, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLabBack_FlambearBallScript, EVENT_FLAMBEAR_BALL
	object_event  6,  2, SPRITE_POKE_BALL, SPRITEMOVEDATA_STILL, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLabBack_CruzBallScript, EVENT_CRUZ_BALL
	object_event  7,  2, SPRITE_POKE_BALL, SPRITEMOVEDATA_STILL, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, SilentLabBack_ChikoritaBallScript, EVENT_CHIKORITA_BALL
