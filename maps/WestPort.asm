; wFastShipID
; 0 = West      - High Tech
; 1 = High Tech - West
; 2 = West      - ? 1
; 3 = ? 1       - West
; 4 = West      - ? 2
; 5 = ? 2       - West

	const_def 2 ; object constants
	const WEST_PORT_BOAT_GUARD1
	const WEST_PORT_BOAT_GUARD2
	const WEST_PORT_BOAT_GUARD3

WestPort_MapScripts:
	db 4 ; scene scripts
	scene_script .DummyScene0 ; SCENE_DEFAULT
	scene_script .Exit1       ; SCENE_EXIT_SHIP_1
	scene_script .Exit2       ; SCENE_EXIT_SHIP_2
	scene_script .Exit3       ; SCENE_EXIT_SHIP_3

	db 0 ; callbacks
	
.DummyScene0:
	end
	
.Exit1:
	applymovement PLAYER, WestPortExitShip1
	setscene SCENE_DEFAULT
	end

.Exit2:
	applymovement PLAYER, WestPortExitShip2
	setscene SCENE_DEFAULT
	end
	
.Exit3:
	applymovement PLAYER, WestPortExitShip3
	setscene SCENE_DEFAULT
	end

WestPortBoatGuard1Script:
	end
	
WestPortBoat1Script:
	turnobject WEST_PORT_BOAT_GUARD1, RIGHT
	opentext
	writetext WestPortWantToBoardBoat1Text
	yesorno
	closetext
	iffalse WestPortNoBoard
	textbox WestPortHaveTicketText
	checkitem SHIP_TICKET
	iffalse WestPortNoTicket
	textbox WestPortGoAheadText
	setmapscene FAST_SHIP_EXT, SCENE_FAST_SHIP_EXT_ENTER
	clearevent EVENT_FAST_SHIP_HAS_ARRIVED
	writebyte 0
	copyvartobyte wFastShipID
	follow PLAYER, WEST_PORT_BOAT_GUARD1
	applymovement PLAYER, WestPortPlayerApproachShip1
	stopfollow
	pause 15
	applymovement PLAYER, WestPortPlayerEnterShip
	special FadeOutPalettes
	pause 15
	warp FAST_SHIP_EXT, 9, 1
	end
	
WestPortNoTicket:
	textbox WestPortNoTicketText
WestPortNoBoard:
	applymovement PLAYER, WestPortPlayerMoveUp
	end
	
WestPortExitShip1:
	step UP
	step UP
	step UP
	step UP
	step UP
	step_end
	
WestPortExitShip2:
	step UP
	step UP
	step UP
	step UP
	step UP
	step UP
	step UP
	step_end
	
WestPortExitShip3:
	step UP
	step UP
	step UP
	step UP
	step_end
	
WestPortPlayerMoveUp:
	step UP
	step_end
	
WestPortPlayerApproachShip1:
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	turn_head UP
	step_end
	
WestPortPlayerEnterShip:
	step DOWN
	step_end
	
WestPortWantToBoardBoat1Text:
	text "PLACEHOLDER"
	line "Would you like to"
	cont "board boat num. 1?"
	done
	
WestPortHaveTicketText:
	text "PLACEHOLDER"
	line "Where's ticket?"
	done
	
WestPortGoAheadText:
	text "PLACEHOLDER"
	line "Go ahead."
	done
	
WestPortNoTicketText:
	text "PLACEHOLDER"
	line "You have no ticket"
	done

WestPort_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event  5, 11, WEST_PORT, 1
	warp_event 13, 17, WEST_PORT, 2
	warp_event 21, 29, WEST_PORT, 3
	warp_event 33,  6, WEST_CITY, 14
	warp_event 33,  7, WEST_CITY, 15

	db 3 ; coord events
	coord_event  5,  7, SCENE_DEFAULT, WestPortBoat1Script
	coord_event  0,  0, SCENE_DEFAULT, WestPortBoat1Script
	coord_event  0,  0, SCENE_DEFAULT, WestPortBoat1Script

	db 0 ; bg events

	db 3 ; object events
	object_event  4,  7, SPRITE_SAILOR, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestPortBoatGuard1Script, -1
	object_event 12, 11, SPRITE_SAILOR, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, -1
	object_event 20, 26, SPRITE_SAILOR, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, -1

