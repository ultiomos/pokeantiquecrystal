	const_def 2 ; object constants

Route9SouthCityGate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route9SouthCityGate_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event  0,  7, SOUTH_CITY, 7
	warp_event  1,  7, SOUTH_CITY, 7
	warp_event  8,  7, ROUTE_9, 1
	warp_event  9,  7, ROUTE_9, 1
	warp_event  1,  0, ROUTE_9_SOUTH_GATE_UPSTAIRS, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

