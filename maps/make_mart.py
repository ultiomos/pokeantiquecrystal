import sys, os.path

def Main():
	nam = sys.argv[1] + "Mart"
	o = "y"
	if os.path.isfile(nam+".asm"):
		o = input(nam+".asm already exists. Overwrite? [y/n] ")
	if o.lower() == "y":
		with open(nam+".asm", "w") as f:
			f.write("\tconst_def 2 ; object constants\n")
			f.write("\tconst "+nam.upper()+"_CLERK\n\n")
			f.write(nam+"_MapScripts:\n")
			f.write("\tdb 0 ; scene scripts\n\n")
			f.write("\tdb 0 ; callbacks\n\n")
			f.write(nam+"ClerkScript:\n")
			f.write("\topentext\n")
			f.write("\tpokemart MARTTYPE_STANDARD, MART_INDIGO_PLATEAU\n")
			f.write("\tclosetext\n")
			f.write("\tend\n\n")
			f.write(nam+"_MapEvents:\n")
			f.write("\tdb 0, 0 ; filler\n\n")
			f.write("\tdb 2 ; warp events\n")
			f.write("\twarp_event  4,  7, "+sys.argv[2]+", "+sys.argv[3]+"\n")
			f.write("\twarp_event  5,  7, "+sys.argv[2]+", "+sys.argv[3]+"\n")
			f.write("\tdb 0 ; coord events\n\n")
			f.write("\tdb 0 ; bg events\n\n")
			f.write("\tdb 1 ; object events\n")
			f.write("\tobject_event  1,  3, SPRITE_CLERK, SPRITEMOVEDATA"
				"_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, "
				+nam+"ClerkScript, -1\n")
	
if __name__ == "__main__":
	if len(sys.argv) <= 3:
		print("usage: python "+sys.argv[0]+" <base_map> <parent_map_constant> <parent_warp_number>")
		print("Makes a .asm file for a PMC")
	else:
		Main()

