	const_def 2 ; object constants

Route9_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route9_MapEvents:
	db 0, 0 ; filler

	db 1 ; warp events
	warp_event  6,  9, ROUTE_9_SOUTH_GATE, 3

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

