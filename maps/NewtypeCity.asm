	const_def 2 ; object constants

NewtypeCity_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_NEWTYPE
	return

NewtypeCity_MapEvents:
	db 0, 0 ; filler

	db 13 ; warp events
	warp_event  7,  8, NEWTYPE_POKECENTER_1F, 1
	warp_event  7, 14, NEWTYPE_CITY, 2 ; Mart
	warp_event  5, 23, NEWTYPE_CITY, 3 ; Restaraunt
	warp_event 11, 28, NEWTYPE_CITY, 4 ; House 1
	warp_event 18,  5, NEWTYPE_CITY, 5 ; Gate
	warp_event 19,  5, NEWTYPE_CITY, 6 ; Gate
	warp_event 23, 13, NEWTYPE_CITY, 7 ; Sailor's House
	warp_event 23, 22, NEWTYPE_CITY, 8 ; House 2
	warp_event 30,  9, NEWTYPE_CITY, 9 ; Gym
	warp_event 31,  9, NEWTYPE_CITY, 10; Gym
	warp_event 33, 15, NEWTYPE_CITY, 11; Dojo
	warp_event 34, 15, NEWTYPE_CITY, 12; Dojo
	warp_event 35, 30, NEWTYPE_CITY, 13; House 3

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

