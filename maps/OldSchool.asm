	const_def 2 ; object constants

OldSchool_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

OldSchool_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  3, 15, OLD_CITY, 14
	warp_event  4, 15, OLD_CITY, 14

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

