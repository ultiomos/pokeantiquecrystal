	const_def 2 ; object constants

Route1OldCityGate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks
	
Route1OldCityGate_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event  4,  0, OLD_CITY, 7
	warp_event  5,  0, OLD_CITY, 8
	warp_event  4,  7, ROUTE_1, 5
	warp_event  5,  7, ROUTE_1, 6
	warp_event  1,  0, ROUTE_1_OLD_CITY_GATE_UPSTAIRS, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events
