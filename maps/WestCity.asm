	const_def 2 ; object constants

WestCity_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_WEST
	return

WestCity_MapEvents:
	db 0, 0 ; filler

	db 15 ; warp events
	warp_event 13,  5, WEST_DEPT_1F, 1
	warp_event 14,  5, WEST_DEPT_1F, 2
	warp_event 22,  5, ROUTE_12_WEST_GATE, 3
	warp_event 23,  5, ROUTE_12_WEST_GATE, 4
	warp_event 31,  7, WEST_RADIO_1F, 1
	warp_event 32,  7, WEST_RADIO_1F, 2
	warp_event 18, 12, WEST_ROCKET_HOUSE, 1
	warp_event 25, 14, WEST_POKECENTER_1F, 1
	warp_event 35, 15, ROUTE_2_WEST_GATE, 1
	warp_event 14, 19, WEST_CITY, 10 ; Gym
	warp_event 15, 19, WEST_CITY, 11 ; Gym
	warp_event 26, 19, WEST_HOUSE_1, 1
	warp_event 32, 19, WEST_HOUSE_2, 1
	warp_event  4,  8, WEST_PORT, 4
	warp_event  4,  9, WEST_PORT, 5

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

