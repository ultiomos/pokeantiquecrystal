	const_def 2 ; object constants

FastShipInt1_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks
	
FastShipInt1PlayerBedScript:
	questionbox FastShipInt1SleepText
	iffalse .end
	ezheal
	checkevent EVENT_FAST_SHIP_HAS_ARRIVED
	iftrue .end
	setevent EVENT_FAST_SHIP_HAS_ARRIVED
	playsound SFX_ELEVATOR_END
	pause 30
	copybytetovar wFastShipID
	ifequal 0, .HighTech
	ifequal 2, .Unknown1
	ifequal 4, .Unknown2
	textbox FastShipInt1ArrivedInWest
	jump .end
.HighTech:
	textbox FastShipInt1ArrivedInHighTech
	jump .end
.Unknown1:
	textbox FastShipInt1ArrivedInUnk1
	jump .end
.Unknown2:
	textbox FastShipInt1ArrivedInUnk2
.end
	end
	
FastShipInt1SleepText:
	text "PLACEHOLDER"
	line "Want to rest?"
	done
	
FastShipInt1ArrivedInWest:
	text "PLACEHOLDER"
	line "Arrived in West."
	done
	
FastShipInt1ArrivedInHighTech:
	text "PLACEHOLDER"
	line "Arrived in HiTech."
	done
	
FastShipInt1ArrivedInUnk1:
	text "PLACEHOLDER"
	line "Arrived in Unk1."
	done
	
FastShipInt1ArrivedInUnk2:
	text "PLACEHOLDER"
	line "Arrived in Unk2."
	done
	

FastShipInt1_MapEvents:
	db 0, 0 ; filler

	db 8 ; warp events
	warp_event  3,  1, FAST_SHIP_EXT, 8
	warp_event  3, 20, FAST_SHIP_EXT, 12
	warp_event 17,  1, FAST_SHIP_EXT, 9
	warp_event 17, 20, FAST_SHIP_EXT, 13
	warp_event 31,  1, FAST_SHIP_EXT, 10
	warp_event 31, 20, FAST_SHIP_EXT, 14
	warp_event 45,  1, FAST_SHIP_EXT, 11
	warp_event 45, 20, FAST_SHIP_EXT, 15

	db 0 ; coord events

	db 2 ; bg events
	bg_event  9,  6, BGEVENT_READ, FastShipInt1PlayerBedScript
	bg_event  9,  7, BGEVENT_READ, FastShipInt1PlayerBedScript

	db 0 ; object events

