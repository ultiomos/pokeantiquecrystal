	const_def 2 ; object constants
	const PLAYERSHOUSE1F_MOM
	const PLAYERSHOUSE1F_KEN

PlayersHouse1F_MapScripts:
	db 2 ; scene scripts
	scene_script .DummyScene0 ; SCENE_DEFAULT
	scene_script .DummyScene1 ; SCENE_FINISHED

	db 0 ; callbacks

.DummyScene0:
	end

.DummyScene1:
	end
	
PlayersHouse1F_KenStop:
	checkevent EVENT_PLAYERS_HOUSE_SET_TIME
	iftrue .AlreadySetTime
	textbox PlayersHouse1F_KenText1
	setflag ENGINE_POKEGEAR
.TimeSetLoop:
	opentext
	callasm SetTimeFromEvent
	special SetDayOfWeek
	writetext PlayersHouse1F_DSTText
	yesorno
	iffalse .WrongDay
	special InitialSetDSTFlag
	yesorno
	iffalse .TimeSetLoop
	jump .TimeSetDone
.WrongDay:
	special InitialClearDSTFlag
	yesorno
	iffalse .TimeSetLoop
.TimeSetDone
	setevent EVENT_PLAYERS_HOUSE_SET_TIME
	textbox PlayersHouse1F_KenText2
.AlreadySetTime:
	textbox PlayersHouse1F_KenText3
	checkmapscene PLAYERS_HOUSE_2F
	iffalse .GoCheckMail
	textbox PlayersHouse1F_KenText4
	applymovement PLAYERSHOUSE1F_KEN, PlayersHouse1F_KenLeave
	setscene SCENE_FINISHED
	disappear PLAYERSHOUSE1F_KEN
	end
.GoCheckMail:
	applymovement PLAYER, PlayersHouse1F_ReturnToRoom
	special FadeOutPalettes
	pause 15
	warp PLAYERS_HOUSE_2F, 7, 0
	dontrestartmapmusic
	end
	
	
PlayersHouse1F_MomScript:
	checkmapscene SILENT_HILL
	ifequal SCENE_SILENT_HAS_STARTER, .leaving
	jumptextfaceplayer PlayersHouse1F_MomGoodLuckText
	
.leaving
	faceplayer
	textbox PlayersHouse1F_MomLeavingText
	setflag ENGINE_PHONE_CARD
	addcellnum PHONE_MOM
	opentext
	writetext PlayersHouse1F_GotPhoneText
	playsound SFX_ITEM
	waitbutton
	closetext
	textbox PlayersHouse1F_MomGoodLuckText
	setmapscene SILENT_HILL, SCENE_SILENT_CAN_LEAVE
	end

TVScript:
	jumptext TVText

StoveScript:
	jumptext StoveText

SinkScript:
	jumptext SinkText

FridgeScript:
	jumptext FridgeText
	
PlayersHouse1F_KenLeave:
	step DOWN
	step DOWN
	step DOWN
	step LEFT
	step LEFT
	step DOWN
	step_end
	
PlayersHouse1F_ReturnToRoom:
	step UP
	step_end
	
PlayersHouse1F_MomLeavingText:
	text "PLACEHOLDER"
	line "You're leaving?"
	
	para "PLACEHOLDER"
	line "Have a phone."
	done
	
PlayersHouse1F_MomGoodLuckText:
	text "PLACEHOLDER"
	line "Good luck."
	done
	
PlayersHouse1F_GotPhoneText:
	text "<PLAYER> got the"
	line "Phone Card!"
	done
	
PlayersHouse1F_DSTText:
	text "Is it Daylight"
	line "Saving Time now?"
	done
	
PlayersHouse1F_KenText1:
	text "Ken: Hey, that"
	line "watch on your arm<.>"
	
	para "You finally got a"
	line "#Gear! That's"
	cont "great!"
	
	para "But since you only"
	line "just got it, it's"
	cont "just got a clock,"
	cont "yeah?"
	
	para "Oh, you haven't set"
	line "the time. You"
	cont "should do it now."
	done
	
PlayersHouse1F_KenText2:
	text "Swing by my place"
	line "later and I'll help"
	cont "install a map on"
	cont "it!"
	
	para "It'll be tough to"
	line "ask my mom for the"
	cont "cash to buy a"
	cont "card, though."
	done
	
PlayersHouse1F_KenText3:
	text "Ken: That's right!"
	line "There's an e-mail"
	cont "on your computer."
	
	para "You should prob-"
	line "ably read it"
	cont "before you go."
	done
	
PlayersHouse1F_KenText4:
	text "Oh, you already"
	line "saw it? Ok."
	
	para "See you later,"
	line "<PLAYER>!"
	done

StoveText:
	text "Mom's specialty!"

	para "Cinnabar Volcano"
	line "Burger!"
	done

SinkText:
	text "The sink is spot-"
	line "less. Mom likes it"
	cont "clean."
	done

FridgeText:
	text "Let's see what's"
	line "in the fridge…"

	para "Fresh Water and"
	line "tasty Lemonade!"
	done

TVText:
	text "There's a movie on"
	line "TV: Stars dot the"

	para "sky as two boys"
	line "ride on a train…"

	para "I'd better get"
	line "rolling too!"
	done

PlayersHouse1F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  6,  7, SILENT_HILL, 2
	warp_event  7,  7, SILENT_HILL, 2
	warp_event  9,  0, PLAYERS_HOUSE_2F, 1

	db 1 ; coord events
	coord_event  9,  1, SCENE_DEFAULT, PlayersHouse1F_KenStop

	db 4 ; bg events
	bg_event  0,  1, BGEVENT_READ, StoveScript
	bg_event  1,  1, BGEVENT_READ, SinkScript
	bg_event  2,  1, BGEVENT_READ, FridgeScript
	bg_event  4,  1, BGEVENT_READ, TVScript

	db 2 ; object events
	object_event  7,  4, SPRITE_MOM, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, PlayersHouse1F_MomScript, EVENT_PLAYERS_HOUSE_MOM
	object_event  9,  2, SPRITE_ROCKER, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, EVENT_PLAYERS_HOUSE_1F_KEN
