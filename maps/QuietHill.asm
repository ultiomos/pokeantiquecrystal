	const_def 2 ; object constants

QuietHill_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

QuietHill_MapEvents:
	db 0, 0 ; filler

	db 4 ; warp events
	warp_event 49, 30, ROUTE_1, 1
	warp_event 49, 31, ROUTE_1, 2
	warp_event 6,  0, ROUTE_1, 3
	warp_event 7,  0, ROUTE_1, 4

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events
	
