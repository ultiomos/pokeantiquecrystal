ROOFL        EQU $17
ROOFC        EQU $11
ROOFR        EQU $02
BUILDINGL    EQU $18
BUILDINGDOOR EQU $19
BUILDINGR    EQU $1a
PMCSIGN      EQU $06
MARTSIGN     EQU $03
GYML         EQU $cb
GYMC         EQU $49
GYMR         EQU $cf
BUILDINGLD   EQU $05
CAVEENTRANCE EQU $d7
SIGNPOST     EQU $84

PMC_LEFT     EQU 12
PMC_TOP      EQU 16
MART_LEFT    EQU  4
MART_TOP     EQU  8
GYM_LEFT     EQU 18
GYM_TOP      EQU 20
LAB_LEFT     EQU  4
LAB_TOP      EQU 22
SIGN_LEFT    EQU 12
SIGN_TOP     EQU 24

	const_def 2 ; object constants
	const CINNABAR_BLAINE
	const CINNABAR_PREERUPT_PERSON_1
	const CINNABAR_PREERUPT_PERSON_2
	const CINNABAR_PREERUPT_PERSON_3

CinnabarIsland_MapScripts:
	db 2 ; scene scripts
	scene_script .DummyScene0 ; SCENE_DEFAULT
	scene_script .DummyScene1 ; SCENE_DEFAULT

	db 2 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	callback MAPCALLBACK_TILES, .SetBaseMap

.DummyScene0:
	end

.DummyScene1:
	end
	
.FlyPoint
	checkevent EVENT_CINNABAR_RES_POKECENTER
	iffalse .NoPMC
	setflag ENGINE_FLYPOINT_CINNABAR
.NoPMC
	return
	
.SetBaseMap:
	checkevent EVENT_CINNABAR_VOLCANO_ERUPTED
	iftrue .Erupted
	changemap Bank(CinnabarPreErupt_Blocks), CinnabarPreErupt_Blocks
	return
.Erupted
.SetupBuildingsAndCave:
	checkevent EVENT_CINNABAR_RES_POKECENTER
	iffalse .skipPMC
	scall CinnabarPlacePMCandSign
.skipPMC
	checkevent EVENT_CINNABAR_RES_MART
	iffalse .skipMart
	scall CinnabarPlaceMart
.skipMart
	checkevent EVENT_CINNABAR_RES_GYM
	iffalse .skipGym
	scall CinnabarPlaceGym
.skipGym
	checkevent EVENT_CINNABAR_RES_LAB
	iffalse .skipLab
	scall CinnabarPlaceLab
.skipLab
	checkevent EVENT_CINNABAR_VOLCANO_OPENED
	iffalse .skipCave
	changeblock 26, 20, CAVEENTRANCE
.skipCave
	return
	
	
CinnabarPlacePMCandSign:
	changeblock PMC_LEFT,   PMC_TOP,   ROOFL
	changeblock PMC_LEFT+2, PMC_TOP,   ROOFR
	changeblock PMC_LEFT,   PMC_TOP+2, BUILDINGLD
	changeblock PMC_LEFT+2, PMC_TOP+2, PMCSIGN
	changeblock SIGN_LEFT,  SIGN_TOP,  SIGNPOST
	end
	
CinnabarPlaceMart:
	changeblock MART_LEFT,   MART_TOP,   ROOFL
	changeblock MART_LEFT+2, MART_TOP,   ROOFR
	changeblock MART_LEFT,   MART_TOP+2, BUILDINGLD
	changeblock MART_LEFT+2, MART_TOP+2, MARTSIGN
	end
	
CinnabarPlaceGym:
	changeblock GYM_LEFT,   GYM_TOP,   ROOFL
	changeblock GYM_LEFT+2, GYM_TOP,   ROOFC
	changeblock GYM_LEFT+4, GYM_TOP,   ROOFR
	changeblock GYM_LEFT,   GYM_TOP+2, GYML
	changeblock GYM_LEFT+2, GYM_TOP+2, GYMC
	changeblock GYM_LEFT+4, GYM_TOP+2, GYMR
	changeblock GYM_LEFT-2, GYM_TOP+2, SIGNPOST
	end
	
CinnabarPlaceLab:
	changeblock LAB_LEFT,   LAB_TOP,   ROOFL
	changeblock LAB_LEFT+2, LAB_TOP,   ROOFC
	changeblock LAB_LEFT+4, LAB_TOP,   ROOFR
	changeblock LAB_LEFT,   LAB_TOP+2, BUILDINGL
	changeblock LAB_LEFT+2, LAB_TOP+2, BUILDINGDOOR
	changeblock LAB_LEFT+4, LAB_TOP+2, BUILDINGR
	changeblock LAB_LEFT+4, LAB_TOP+4, SIGNPOST
	end
	
CinnabarCaveEntranceCheck:
	conditional_event EVENT_CINNABAR_VOLCANO_OPENED, .script
.script
	opentext
	writetext CinnabarWallText
	waitbutton
	callasm .CheckForBoom
	iffalse .NoBoom
	ifequal EXPLOSION, .UseExplosionAsk
	writetext CinnabarWantToSelfdestructText
	jump .yesnobox
.UseExplosionAsk
	writetext CinnabarWantToExplosionText
.yesnobox
	yesorno
	iffalse .no
	closetext
	callasm _GetPartyNick
	callasm .CheckForBoom
	ifequal EXPLOSION, .Explosion
	textbox CinnabarUsedSelfdestructText
	jump .cont
.Explosion
	textbox CinnabarUsedExplosionText
.cont
	waitsfx
	scall .BoomSFX
	waitsfx
	setevent EVENT_CINNABAR_VOLCANO_OPENED
	changeblock 26, 20, CAVEENTRANCE
	reloadmappart
	textbox CinnabarCaveOpenedText
	end
	
.no
	closetext
	end
	
.NoBoom:
	textbox CinnabarMaybeABoomText
	end
	
.CheckForBoom:
	ld d, SELFDESTRUCT
	farcall CheckPartyMove
	jr nc, .GotSelfdestruct
	ld d, EXPLOSION
	farcall CheckPartyMove
	jr c, .none
	ld a, EXPLOSION
	jr .setVar
.none
	xor a
	jr .setVar
.GotSelfdestruct
	ld a, SELFDESTRUCT
.setVar
	ld [wScriptVar], a
	ret
	
.BoomSFX
rept 5
	playsound SFX_EGG_BOMB
	special FlashScreen
	pause 3
	playsound SFX_EGG_BOMB
	special UnflashScreen
	pause 3
endr
	end
	
VOLCANO_X  EQU 22
VOLCANO_Y  EQU 22

VOLCANO_NW EQU $cc
VOLCANO_N  EQU $cd
VOLCANO_NE EQU $ce
VOLCANO_W  EQU $d0
VOLCANO_C  EQU $d1
VOLCANO_E  EQU $d2
VOLCANO_SW EQU $d4
VOLCANO_S  EQU $d5
VOLCANO_SE EQU $d6
	
CinnabarVolcanoEruptLeftScript:
	applymovement PLAYER, CinnabarEnterMovement1
	jump CinnabarVolcanoEruptCont
CinnabarVolcanoEruptRightScript:
	applymovement PLAYER, CinnabarEnterMovement2
CinnabarVolcanoEruptCont:
	; Volcano starts at 22, 22
	playmusic MUSIC_NONE
	playsound SFX_EMBER
	earthquake 60
	textbox CinnabarWhatWasThatText
	playsound SFX_PLACE_PUZZLE_PIECE_DOWN
	changeblock 22, 22, $d1
	reloadmappart
	waitsfx
	showemote EMOTE_SHOCK, PLAYER, 15
	playsound SFX_PLACE_PUZZLE_PIECE_DOWN
	changeblock 20, 18, $cc
	changeblock 22, 18, $cd
	changeblock 24, 18, $ce
	changeblock 20, 20, $d0
	changeblock 22, 20, $d1
	changeblock 24, 20, $d2
	changeblock 20, 22, $d4
	changeblock 22, 22, $d5
	changeblock 24, 22, $d6
	reloadmappart
	waitsfx
	playmusic MUSIC_ZINNIA_BATTLE
	appear CINNABAR_BLAINE
	applymovement CINNABAR_BLAINE, CinnabarBlaineMovement1
	showemote EMOTE_SHOCK, CINNABAR_BLAINE, 15
	applymovement CINNABAR_BLAINE, CinnabarBlaineMovement2
	turnobject PLAYER, DOWN
	textbox CinnabarBlaineAreYouATrainerText
	applymovement PLAYER, CinnabarPlayerRescue1
	turnobject CINNABAR_PREERUPT_PERSON_2, UP
	pause 15
	applymovement PLAYER, CinnabarPlayerRescue2
	turnobject CINNABAR_PREERUPT_PERSON_1, RIGHT
	pause 15
	applymovement PLAYER, CinnabarPlayerRescue2
	turnobject CINNABAR_PREERUPT_PERSON_3, RIGHT
	pause 15
	special FadeOutPalettes
	pause 15
	setevent EVENT_CINNABAR_VOLCANO_ERUPTED
	setevent EVENT_CINNABAR_BLAINE
	setscene SCENE_FINISHED
	warpfacing RIGHT, PALLET_DUMMY, 6, 7
	end
	
CinnabarTownSignCheck:
	conditional_event EVENT_CINNABAR_RES_POKECENTER, .script
.script
	jumptext CinnabarTownSignText
	
CinnabarPMCSignCheck:
	conditional_event EVENT_CINNABAR_RES_POKECENTER, .script
.script
	jumpstd pokecentersign
	
CinnabarMartSignCheck:
	conditional_event EVENT_CINNABAR_RES_MART, .script
.script
	jumpstd martsign
	
CinnabarLabSignCheck:
	conditional_event EVENT_CINNABAR_RES_LAB, .script
.script
	jumptext CinnabarLabSignText
	
CinnabarGymSignCheck:
	conditional_event EVENT_CINNABAR_RES_GYM, .script
.script
	jumptext CinnabarGymSignText
	
CinnabarEnterMovement1:
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step_end
	
CinnabarEnterMovement2:
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step_end
	
CinnabarBlaineMovement1:
	step DOWN
	step DOWN
	step DOWN
	turn_head RIGHT
	step_end
	
CinnabarBlaineMovement2:
	step RIGHT
	turn_head UP
	step_end
	
CinnabarPlayerRescue1:
	big_step LEFT
	big_step DOWN
	big_step DOWN
	big_step DOWN
	step_end
	
CinnabarPlayerRescue2:
	big_step UP
	big_step UP
	big_step LEFT
	big_step LEFT
	big_step LEFT
	big_step LEFT
	big_step LEFT
	step_end
	
CinnabarTownSignText:
	text "Cinnabar Island"
	para "The fiery town,"
	line "reborn from ash!"
	done

CinnabarLabSignText:
	text "Cinnabar #mon"
	line "Research Lab"
	done
	
CinnabarGymSignText:
	text "Cinnabar Island"
	line "#mon Gym"
	
	para "Leader: Blaine"
	para "The hot-headed"
	line "quiz master!"
	done
	
CinnabarWallText:
	text "There's a weak part"
	line "here on the wall."
	done
	
CinnabarWantToSelfdestructText:
	text "Do you want to use"
	line "Self-Destruct?"
	done
	
CinnabarWantToExplosionText:
	text "Do you want to use"
	line "Explosion?"
	done
	
CinnabarUsedSelfdestructText:
	text_from_ram wStringBuffer2
	text " used"
	line "Self-Destruct!"
	done
	
CinnabarUsedExplosionText:
	text_from_ram wStringBuffer2
	text " used"
	line "Explosion!"
	done
	
CinnabarCaveOpenedText:
	text "The wall crumbled"
	line "away!"
	done
	
CinnabarMaybeABoomText:
	text "Maybe an Explosion"
	line "could get through."
	done
	
CinnabarWhatWasThatText:
	text "What was that?"
	done
	
CinnabarBlaineAreYouATrainerText:
	text "You! Are you a"
	line "#mon Trainer?"
	
	para "Help evacuate the"
	line "island. The Vol-"
	cont "cano's erupting!"
	done

CinnabarIsland_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event GYM_LEFT +4, GYM_TOP +3, CINNABAR_ISLAND, 1 ; Gym
	warp_event PMC_LEFT +1, PMC_TOP +3, CINNABAR_ISLAND, 2 ; PMC
	warp_event MART_LEFT+1, MART_TOP+3, CINNABAR_ISLAND, 3 ; Mart
	warp_event LAB_LEFT +2, LAB_TOP +3, CINNABAR_ISLAND, 4 ; Lab
	warp_event 27, 21, CINNABAR_ISLAND, 5 ; Cave

	db 2 ; coord events
	coord_event 10, 19, SCENE_DEFAULT, CinnabarVolcanoEruptLeftScript
	coord_event 11, 19, SCENE_DEFAULT, CinnabarVolcanoEruptRightScript

	db 6 ; bg events
	bg_event 27, 21, BGEVENT_IFNOTSET, CinnabarCaveEntranceCheck
	bg_event SIGN_LEFT+1, SIGN_TOP+1, BGEVENT_IFSET, CinnabarTownSignCheck
	bg_event PMC_LEFT+2, PMC_TOP+3, BGEVENT_IFSET, CinnabarPMCSignCheck
	bg_event MART_LEFT+2, MART_TOP+3, BGEVENT_IFSET, CinnabarMartSignCheck
	bg_event GYM_LEFT-1, GYM_TOP+3, BGEVENT_IFSET, CinnabarGymSignCheck
	bg_event LAB_LEFT+5, LAB_TOP+5, BGEVENT_IFSET, CinnabarLabSignCheck

	db 4 ; object events
	object_event 18, 21, SPRITE_BLAINE, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, EVENT_CINNABAR_BLAINE
	object_event 12, 24, SPRITE_TEACHER, SPRITEMOVEDATA_SPINRANDOM_SLOW, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, EVENT_CINNABAR_VOLCANO_ERUPTED
	object_event 18, 27, SPRITE_YOUNGSTER, SPRITEMOVEDATA_SPINRANDOM_SLOW, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, EVENT_CINNABAR_VOLCANO_ERUPTED
	object_event  7, 22, SPRITE_COOLTRAINER_F, SPRITEMOVEDATA_SPINRANDOM_SLOW, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, ObjectEvent, EVENT_CINNABAR_VOLCANO_ERUPTED
