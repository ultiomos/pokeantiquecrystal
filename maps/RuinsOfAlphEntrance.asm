	const_def 2 ; object constants

RuinsOfAlphEntrance_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

RuinsOfAlphEntrance_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  4,  9, FOUNT_TOWN, 6
	warp_event  5,  9, FOUNT_TOWN, 6

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

