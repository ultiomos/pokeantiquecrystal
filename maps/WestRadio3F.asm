	const_def 2 ; object constants

WestRadio3F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

WestRadio3F_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  0,  0, WEST_RADIO_2F, 1
	warp_event  7,  0, WEST_RADIO_4F, 2

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

