	const_def 2 ; object constants
	const FASTSHIPEXT_DOORMAN

FastShipExt_MapScripts:
	db 2 ; scene scripts
	scene_script .DummyScene0 ; SCENE_DEFAULT
	scene_script .Enter       ; SCENE_FAST_SHIP_EXT_ENTER

	db 0 ; callbacks
	
.DummyScene0:
	end
	
.Enter:
	applymovement FASTSHIPEXT_DOORMAN, FastShipExtDoormanStepAside
	applymovement PLAYER, FastShipExtPlayerEnter
	applymovement FASTSHIPEXT_DOORMAN, FastShipExtDoormanReturn
	setscene SCENE_DEFAULT
	end
	
FastShipExtDoormanScript:
	faceplayer
	checkevent EVENT_FAST_SHIP_HAS_ARRIVED
	iffalse .atSea
	questionbox FastShipExtWantToLeave
	iffalse .end
	checkcode VAR_FACING
	ifequal UP, .FacingUp
	applymovement PLAYER, FastShipExtPlayerLeaveFacingRight
.FacingUp
	applymovement FASTSHIPEXT_DOORMAN, FastShipExtDoormanStepAside
	applymovement PLAYER, FastShipExtPlayerLeave
	special FadeOutPalettes
	copybytetovar wFastShipID
	pause 15
	ifequal 0, .HighTech
	ifequal 2, .Unknown1
	ifequal 4, .Unknown2
	setmapscene WEST_PORT, SCENE_EXIT_SHIP_1
	warpfacing UP, WEST_PORT, 5, 11
	end
	
.HighTech:
	setmapscene HITEKK_PORT, SCENE_HIGH_TECH_PORT_EXIT
	warpfacing UP, HITEKK_PORT, 7, 27
	end
	
.Unknown1:
	setmapscene WEST_PORT, SCENE_EXIT_SHIP_2
	warpfacing UP, WEST_PORT, 5, 11
	end
	
.Unknown2:
	setmapscene WEST_PORT, SCENE_EXIT_SHIP_3
	warpfacing UP, WEST_PORT, 5, 11
	end
	
.atSea:
	textbox FastShipExtAtSeaText
	turnobject FASTSHIPEXT_DOORMAN, DOWN
.end
	end
	
FastShipExtDoormanStepAside:
	step LEFT
	turn_head RIGHT
	step_end
	
FastShipExtDoormanReturn:
	step RIGHT
	turn_head DOWN
	step_end
	
FastShipExtPlayerEnter:
	step DOWN
	step DOWN
	step_end
	
FastShipExtPlayerLeave:
	step UP
	step UP
	step_end
	
FastShipExtPlayerLeaveFacingRight:
	step DOWN
	step RIGHT
	turn_head UP
	step_end
	
FastShipExtAtSeaText:
	text "PLACEHOLDER"
	line "We're at sea."
	done
	
FastShipExtWantToLeave:
	text "PLACEHOLDER"
	line "Want to leave?"
	done

FastShipExt_MapEvents:
	db 0, 0 ; filler

	db 27 ; warp events
	warp_event  9,  1, FAST_SHIP_EXT, 1
	warp_event 22, 14, FAST_SHIP_EXT, 3
	warp_event 23, 42, FAST_SHIP_EXT, 2
	warp_event  2, 14, FAST_SHIP_EXT, 5
	warp_event  2, 30, FAST_SHIP_EXT, 4
	warp_event  2, 40, FAST_SHIP_DECK, 1
	warp_event  2, 41, FAST_SHIP_DECK, 2
	warp_event  7,  8, FAST_SHIP_INT_1, 1
	warp_event 11,  8, FAST_SHIP_INT_1, 3
	warp_event 15,  8, FAST_SHIP_INT_1, 5
	warp_event 19,  8, FAST_SHIP_INT_1, 7
	warp_event  7, 15, FAST_SHIP_INT_1, 2
	warp_event 11, 15, FAST_SHIP_INT_1, 4
	warp_event 15, 15, FAST_SHIP_INT_1, 6
	warp_event 19, 15, FAST_SHIP_INT_1, 8
	warp_event  7, 23, FAST_SHIP_EXT, 16
	warp_event 11, 23, FAST_SHIP_EXT, 17
	warp_event 15, 23, FAST_SHIP_EXT, 18
	warp_event 19, 23, FAST_SHIP_EXT, 19
	warp_event  7, 26, FAST_SHIP_EXT, 20
	warp_event 11, 26, FAST_SHIP_EXT, 21
	warp_event 15, 26, FAST_SHIP_EXT, 22
	warp_event 19, 26, FAST_SHIP_EXT, 23
	warp_event  7, 31, FAST_SHIP_EXT, 24
	warp_event 11, 31, FAST_SHIP_EXT, 25
	warp_event 15, 31, FAST_SHIP_EXT, 26
	warp_event 19, 31, FAST_SHIP_EXT, 27

	db 0 ; coord events

	db 0 ; bg events

	db 1 ; object events
	object_event  9,  2, SPRITE_SAILOR, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, FastShipExtDoormanScript, -1
