	const_def 2 ; object constants

SouthCity_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .FlyPoint
	
.FlyPoint:
	setflag ENGINE_FLYPOINT_SOUTH
	return

SouthCity_MapEvents:
	db 0, 0 ; filler

	db 10 ; warp events
	warp_event 17, 11, SOUTH_CITY, 1  ; Cave
	warp_event 19, 22, SOUTH_CITY, 2  ; Mart
	warp_event 26, 10, SOUTH_CITY, 3  ; North House
	warp_event 30,  5, SOUTH_CITY, 4  ; North Gate
	warp_event 31,  5, SOUTH_CITY, 5  ; North Gate
	warp_event 33, 14, SOUTH_POKECENTER_1F, 1
	warp_event 35, 19, ROUTE_9_SOUTH_GATE, 2
	warp_event 33, 23, SOUTH_CITY, 8  ; South House
	warp_event 30, 30, ROUTE_4_SOUTH_GATE, 1
	warp_event 31, 30, ROUTE_4_SOUTH_GATE, 2

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

