	const_def 2 ; object constants

RouteK22_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

RouteK22_MapEvents:
	db 0, 0 ; filler

	db 1 ; warp events
	warp_event 12,  5, ROUTE_K22_GATE, 4

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

