	const_def 2 ; object constants

Route2WestCityGate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route2WestCityGate_MapEvents:
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event  0,  7, WEST_CITY, 9
	warp_event  1,  7, WEST_CITY, 9
	warp_event  8,  7, ROUTE_2, 1
	warp_event  9,  7, ROUTE_2, 1
	warp_event  1,  0, ROUTE_2_WEST_GATE_UPSTAIRS, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

