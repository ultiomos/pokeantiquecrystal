	const_def 2 ; object constants

WestDept1F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

WestDept1F_MapEvents:
	db 0, 0 ; filler

	db 4 ; warp events
	warp_event 13,  7, WEST_CITY, 1
	warp_event 14,  7, WEST_CITY, 2
	warp_event 15,  0, WEST_DEPT_2F, 2
	warp_event  2,  0, WEST_DEPT_ELEVATOR, 1

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

