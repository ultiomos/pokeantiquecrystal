	const_def 2 ; object constants

Route15_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route15_MapEvents:
	db 0, 0 ; filler

	db 7 ; warp events
	warp_event  8,  5, ROUTE_14_TO_15_GATE, 3
	warp_event  9,  5, ROUTE_14_TO_15_GATE, 4
	warp_event  9, 10, ROUTE_15_POKECENTER_1F, 1
	warp_event 14, 12, ROUTE_15, 6
	warp_event 14, 13, ROUTE_15, 7
	warp_event 21,  8, ROUTE_15, 4
	warp_event 21,  9, ROUTE_15, 5

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events

