import sys, re
import pygame

def Main():
	nam = sys.argv[1]
	sym = "Tileset"+nam
	lnam = "_".join([s.lower() for s in re.findall("[A-Z][^A-Z]*", nam)])
	cnam = "TILESET_"+lnam.upper()
	with open("constants/tileset_constants.asm", "r+") as f:
		lins = f.read().split("\n")
		ti = 0
		li = 0
		looking = False
		for l in lins:
			if l[:7] == "\tconst ":
				looking = True
				ti += 1
			else:
				if looking:
					break
			li += 1
		t_string = "\tconst "+cnam+" "*(29-len(cnam))+"; "+hex(ti+1)[2:].zfill(2)
		lins.insert(li, t_string)
		f.seek(0)
		for l in lins:
			f.write(l+"\n")
	with open("data/tilesets.asm", "a") as f:
		f.write("\ttileset "+sym+"\n")
	with open("gfx/tilesets.asm", "a") as f:
		f.write("\n")
		f.write(sym+"Coll:\n")
		f.write("INCLUDE \"data/tilesets/"+lnam+"_collision.asm\"\n\n")
		f.write(sym+"Meta:\n")
		f.write("INCBIN \"data/tilesets/"+lnam+"_metatiles.bin\"\n\n")
		f.write(sym+"GFX:\n")
		f.write("INCBIN \"gfx/tilesets/"+lnam+".2bpp.lz\"\n")
	with open("engine/tilesets/tileset_anims.asm", "r+") as f:
		lins = f.read().split("\n")
		li = lins.index("DoneTileAnimation: ; fc2fb")
		lins.insert(li, "")
		lins.insert(li, "\tdw NULL,  DoneTileAnimation")
		lins.insert(li, "\tdw NULL,  WaitTileAnimation")
		lins.insert(li, "\tdw NULL,  WaitTileAnimation")
		lins.insert(li, "\tdw NULL,  WaitTileAnimation")
		lins.insert(li, "\tdw NULL,  WaitTileAnimation")
		lins.insert(li, sym+"Anim:")
		f.seek(0)
		for l in lins:
			f.write(l+"\n")
	with open("gfx/tileset_palette_maps.asm", "a") as f:
		f.write("\n")
		f.write(sym+"PalMap:\n")
		f.write("INCLUDE \"gfx/tilesets/"+lnam+"_palette_map.asm\"\n")
	with open("gfx/tilesets/"+lnam+"_palette_map.asm", "w") as f:
		for _ in range(12):
			f.write("\ttilepal 0, GRAY, GRAY, GRAY, GRAY, GRAY, GRAY, GRAY, GRAY\n")
		f.write("\nrept 16\n\tdb $ff\nendr\n\n")
		for _ in range(12):
			f.write("\ttilepal 1, GRAY, GRAY, GRAY, GRAY, GRAY, GRAY, GRAY, GRAY\n")
	with open("data/tilesets/"+lnam+"_collision.asm", "w") as f:
		for i in range(int(sys.argv[2])):
			f.write("\ttilecoll 0, 0, 0, 0 ; "+hex(i)[2:].zfill(2)+"\n")
	with open("data/tilesets/"+lnam+"_metatiles.bin", "wb") as f:
		f.write(b"\x00"*0x10*int(sys.argv[2]))
	blank_surf = pygame.Surface((128, 96))
	blank_surf.fill(pygame.Color(255,255,255))
	pygame.image.save(blank_surf, "gfx/tilesets/"+lnam+".png")
	
if __name__ == "__main__":
	if len(sys.argv) <= 2:
		print("usage: python "+sys.argv[0]+" <tilesetname> <blocksize>")
		print("Modifies and creates the following files to add a new tileset")
		print("All names are formatted like the disassemblies other sets")
		print("    Modifies:")
		print("        constants/tileset_constants.asm")
		print("        data/tilesets.asm")
		print("        gfx/tilesets.asm")
		print("        engine/tilesets/tileset_anims.asm")
		print("        gfx/tileset_palette_maps.asm")
		print("    Creates:")
		print("        data/tilesets/<name>_collision.asm")
		print("        gfx/tilesets/<name>_palette_map.asm")
		print("        data/tilesets/<name>_metatiles.bin")
		print("        gfx/tilesets/<name>.png")
	else:
		Main()

