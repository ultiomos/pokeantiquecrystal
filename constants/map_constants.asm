newgroup: MACRO
const_value = const_value + 1
	enum_start 1
ENDM

map_const: MACRO
;\1: map id
;\2: height: in blocks
;\3: width: in blocks
GROUP_\1 EQU const_value
	enum MAP_\1
\1_WIDTH EQU \2
\1_HEIGHT EQU \3
ENDM

; map group ids
; `newgroup` indexes are for:
; - MapGroupPointers (see data/maps/maps.asm)
; - MapGroupRoofs (see data/maps/roofs.asm)
; - OutdoorSprites (see data/maps/outdoor_sprites.asm)
; - RoofPals (see gfx/tilesets/roofs.pal)
; `map_const` indexes are for the sub-tables of MapGroupPointers (see data/maps/maps.asm)
; Each map also has associated data:
; - attributes (see data/maps/attributes.asm)
; - blocks (see data/maps/blocks.asm)
; - scripts and events (see data/maps/scripts.asm)
	const_def

	newgroup                                                      ;  1
	
	map_const WEST_CITY,                                   20, 18 ;  1
	map_const WEST_POKECENTER_1F,                           5,  4 ;  2
	map_const WEST_ROCKET_HOUSE,                            5,  4 ;  3
	map_const WEST_HOUSE_1,                                 5,  4 ;  4
	map_const WEST_HOUSE_2,                                 5,  4 ;  5
	map_const WEST_DEPT_1F,                                 8,  4 ;  6
	map_const WEST_DEPT_2F,                                 8,  4 ;  7
	map_const WEST_DEPT_3F,                                 8,  4 ;  8
	map_const WEST_DEPT_4F,                                 8,  4 ;  9
	map_const WEST_DEPT_5F,                                 8,  4 ; 10
	map_const WEST_DEPT_6F,                                 8,  4 ; 11
	map_const WEST_DEPT_ELEVATOR,                           2,  2 ; 12
	map_const WEST_PORT,                                   20, 18 ; 13

	newgroup                                                      ;  2
	
	map_const ROUTE_12_WEST_GATE,                           5,  4 ;  1
	map_const ROUTE_12_WEST_GATE_UPSTAIRS,                  4,  3 ;  2
	map_const BIRDON,                                      10,  9 ;  3
	map_const ROUTE_12,                                    10, 27 ;  4
	map_const ROUTE_13,                                    50,  9 ;  5
	map_const ROUTE_14,                                    10, 18 ;  6
	map_const BIRDON_POKECENTER_1F,                         5,  4 ;  7
	
	newgroup                                                      ;  3

	map_const SPROUT_TOWER,                                17, 10 ;  1
	map_const WEST_RADIO_1F,                                4,  4 ;  2
	map_const WEST_RADIO_2F,                                4,  4 ;  3
	map_const WEST_RADIO_3F,                                4,  4 ;  4
	map_const WEST_RADIO_4F,                                4,  4 ;  5
	map_const WEST_RADIO_5F,                                4,  4 ;  6
	map_const FAST_SHIP_EXT,                               13, 23 ;  7
	map_const FAST_SHIP_DECK,                              11,  8 ;  8
	map_const FAST_SHIP_INT_1,                             28, 11 ;  9
	map_const RUINS_OF_ALPH_ENTRANCE,                       5,  5 ; 10
	map_const RUINS_OF_ALPH,                               24, 26 ; 11

	newgroup                                                      ;  4
	
	map_const HITEKK_CITY,                                 20, 18 ;  1
	map_const HITEKK_PORT,                                 10, 18 ;  2
	map_const ROUTE_3,                                     25,  9 ;  3
	map_const ROUTE_4,                                     11, 27 ;  4
	map_const ROUTE_5,                                     10, 23 ;  6
	map_const HITEKK_POKECENTER_1F,                         5,  4 ;  7
	map_const HITEKK_MART,                                  8,  4 ;  8

	newgroup                                                      ;  5
	
	map_const ROUTE_10,                                    25,  9 ;  1
	map_const ROUTE_11,                                    10, 18 ;  2
	map_const FOUNT_TOWN,                                  10,  9 ;  3
	map_const ROUTE_9,                                     35, 10 ;  4
	map_const ROUTE_6,                                     10, 23 ;  5
	map_const ROUTE_7,                                     35,  9 ;  6
	map_const ROUTE_8,                                     10, 18 ;  7
	map_const FOUNT_POKECENTER_1F,                          5,  4 ;  8

	newgroup                                                      ;  6
	
	map_const SOUTH_CITY,                                  20, 18 ;  1
	map_const ROUTE_4_SOUTH_GATE,                           5,  4 ;  2
	map_const ROUTE_4_SOUTH_GATE_UPSTAIRS,                  4,  3 ;  3
	map_const ROUTE_9_SOUTH_GATE,                           5,  4 ;  4
	map_const ROUTE_9_SOUTH_GATE_UPSTAIRS,                  4,  3 ;  5
	map_const SOUTH_POKECENTER_1F,                          5,  4 ;  6

	newgroup                                                      ;  7

	map_const ROUTE_15,                                    15,  9 ;  1
	map_const NEWTYPE_CITY,                                20, 18 ;  2
	map_const ROUTE_15_POKECENTER_1F,                       5,  4 ;  3
	map_const NEWTYPE_POKECENTER_1F,                        5,  4 ;  4
	map_const ROUTE_14_TO_15_GATE,                          5,  4 ;  5
	map_const ROUTE_14_TO_15_GATE_UPSTAIRS,                 4,  3 ;  6
	map_const ROUTE_17,                                    15,  9 ;  7
	map_const ROUTE_18,                                    10, 45 ;  8
	map_const ROUTE_18_POKECENTER_1F,                       5,  4 ; 11

	newgroup                                                      ;  8

	map_const SUGAR_ISLAND,                                10,  9 ;  1
	map_const ROUTE_16,                                    10, 27 ;  2
	map_const SUGAR_POKECENTER_1F,                          5,  4 ;  3

	newgroup                                                      ;  9

	map_const ROUTE_23,                                    25,  9 ;  1
	map_const BLUE_FOREST,                                 20, 18 ;  2
	map_const ROUTE_24,                                    10, 27 ;  3
	map_const ROUTE_22,                                    10, 27 ;  4
	map_const NORTH_TOWN,                                  14, 10 ;  5
	map_const BLUE_POKECENTER_1F,                           5,  4 ;  6
	map_const NORTH_POKECENTER_1F,                          5,  4 ;  7

	newgroup                                                      ; 10

	map_const ROUTE_2,                                     15,  9 ;  1
	map_const OLD_CITY,                                    20, 18 ;  2
	map_const OLD_MART,                                     8,  4 ;  3
	map_const OLD_GYM,                                      5,  8 ;  4
	map_const OLD_MUSEUM,                                   8,  4 ;  5
	map_const OLD_KURTS_HOUSE,                              8,  4 ;  6
	map_const OLD_POKECENTER_1F,                            5,  4 ;  7
	map_const OLD_BILLS_HOUSE,                              4,  4 ;  8
	map_const OLD_HOUSE,                                    4,  4 ;  9
	map_const OLD_SCHOOL,                                   4,  8 ; 10
	map_const ROUTE_2_WEST_GATE,                            5,  4 ; 11
	map_const ROUTE_2_WEST_GATE_UPSTAIRS,                   4,  3 ; 12
	map_const ROUTE_2_GAME_HOUSE,                           4,  4 ; 13

	newgroup                                                      ; 11

	map_const STAND_CITY,                                  20, 18 ;  1
	map_const ROUTE_21,                                    10, 27 ;  2
	map_const STAND_POKECENTER_1F,                          5,  4 ;  3

	newgroup                                                      ; 12

	map_const PEWTER_CITY,                                 20, 18 ;  1
	map_const ROUTE_K3,                                    35,  9 ;  2
	map_const ROUTE_K2_NORTH,                              10, 19 ;  3

	newgroup                                                      ; 13
	
	map_const PALLET_TOWN,                                 10,  9 ;  1
	map_const ROUTE_K1,                                    10, 18 ;  2
	map_const VIRIDIAN_CITY,                               20, 18 ;  3
	map_const ROUTE_K22,                                   22,  9 ;  4
	map_const ROUTE_K2_SOUTH,                              10, 17 ;  5
	map_const ROUTE_K20,                                   40,  9 ;  6
	map_const CINNABAR_ISLAND,                             20, 18 ;  7
	map_const ROUTE_K21,                                   10, 36 ;  8
	map_const PALLET_DUMMY,                                10,  9 ;  9

	newgroup                                                      ; 14
	
	map_const ROUTE_K4,                                    45,  9 ;  1
	map_const CERULEAN_CITY,                               20, 18 ;  2
	map_const ROUTE_K24,                                   11, 18 ;  3
	map_const ROUTE_K25,                                   29,  9 ;  4
	map_const ROUTE_K9,                                    30,  9 ;  5
	map_const ROUTE_K10_NORTH,                             10, 25 ;  6
	map_const ROUTE_K5,                                    10, 16 ;  7

	newgroup                                                      ; 15
	
	map_const ROUTE_K10_SOUTH,                             10, 11 ;  1
	map_const LAVENDER_TOWN,                               10,  9 ;  2
	map_const ROUTE_20,                                    30,  9 ;  3
	map_const ROUTE_K8,                                    28,  9 ;  4
	map_const ROUTE_K12,                                   10, 54 ;  5
	map_const ROUTE_K13,                                   30,  9 ;  6
	map_const ROUTE_K14,                                   10, 27 ;  7
	map_const ROUTE_K15,                                   30,  9 ;  8

	newgroup                                                      ; 16
	
	map_const ROUTE_K6,                                    10, 17 ;  1
	map_const VERMILION_CITY,                              20, 18 ;  2
	map_const ROUTE_K11,                                   30,  9 ;  3

	newgroup                                                      ; 17

	newgroup                                                      ; 18
	
	map_const SAFFRON_CITY,                                23, 21 ;  1
	map_const ROUTE_K23,                                   10, 72 ;  2
	map_const INDIGO_PLATEAU,                              10,  9 ;  3

	newgroup                                                      ; 19
	
	map_const ROUTE_K7,                                     9,  9 ;  1
	map_const CELADON_CITY,                                25, 18 ;  2
	map_const ROUTE_K16,                                   20,  9 ;  3
	map_const ROUTE_K17,                                   10, 72 ;  4

	newgroup                                                      ; 20

	map_const POKECENTER_2F,                                8,  4 ;  1
	map_const TRADE_CENTER,                                 5,  4 ;  2
	map_const COLOSSEUM,                                    5,  4 ;  3
	map_const TIME_CAPSULE,                                 5,  4 ;  4
	map_const MOBILE_TRADE_ROOM,                            5,  4 ;  5
	map_const MOBILE_BATTLE_ROOM,                           5,  4 ;  6

	newgroup                                                      ; 21
	
	map_const ROUTE_K18,                                   25,  9 ;  1
	map_const FUCHSIA_CITY,                                20, 18 ;  2

	newgroup                                                      ; 22
	
	map_const ROUTE_K19,                                   10, 27 ;  1

	newgroup                                                      ; 23

	newgroup                                                      ; 24
	
	map_const ROUTE_1,                                     25, 18 ;  1
	map_const SILENT_HILL,                                 10,  9 ;  2
	map_const OAKS_LAB,                                     4,  8 ;  3
	map_const PLAYERS_HOUSE_1F,                             5,  4 ;  4
	map_const PLAYERS_HOUSE_2F,                             4,  3 ;  5
	map_const SILENT_POKECENTER_1F,                         5,  4 ;  6
	map_const SILVERS_HOUSE,                                5,  4 ;  7
	map_const OAKS_LAB_BACK,                                4,  4 ;  8
	map_const PRINCE_TOWN,                                 10, 14 ;  9
	map_const ROUTE_19,                                    31,  9 ; 10
	map_const ROUTE_K22_GATE,                              15,  9 ; 11s

	newgroup                                                      ; 25

	newgroup                                                      ; 26

	map_const QUIET_HILL,                                  25, 18 ;  1
	map_const ROUTE_1_OLD_CITY_GATE,                        5,  4 ;  2
	map_const ROUTE_1_OLD_CITY_GATE_UPSTAIRS,               4,  3 ;  3
