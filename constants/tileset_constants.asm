; Tilesets indexes (see data/tilesets.asm)
	const_def 1
	const TILESET_PLAYERS_HOUSE        ; 01
	const TILESET_POKECENTER           ; 02
	const TILESET_GATE                 ; 03
	const TILESET_PORT                 ; 04
	const TILESET_LAB                  ; 05
	const TILESET_MART                 ; 06
	const TILESET_MANSION              ; 07
	const TILESET_ELITE_FOUR_ROOM      ; 08
	const TILESET_TRADITIONAL_HOUSE    ; 09
	const TILESET_LIGHTHOUSE           ; 0a
	const TILESET_PLAYERS_ROOM         ; 0b
	const TILESET_RADIO_TOWER          ; 0c
	const TILESET_FOREST               ; 0d
	const TILESET_WEST_CITY            ; 0e
	const TILESET_SILENT_HILL          ; 0f
	const TILESET_OLD_CITY             ; 10
	const TILESET_BIRDON               ; 11
	const TILESET_HIGH_TECH            ; 12
	const TILESET_FOUNT_TOWN           ; 13
	const TILESET_SOUTH_CITY           ; 14
	const TILESET_BLUE_FOREST          ; 15
	const TILESET_RUINS_OF_ALPH        ; 16
	const TILESET_KANTO                ; 17

; bg palette values (see gfx/tilesets/*_palette_map.asm)
; TilesetBGPalette indexes (see gfx/tilesets/bg_tiles.pal)
	const_def
	const PAL_BG_GRAY   ; 0
	const PAL_BG_RED    ; 1
	const PAL_BG_GREEN  ; 2
	const PAL_BG_WATER  ; 3
	const PAL_BG_YELLOW ; 4
	const PAL_BG_BROWN  ; 5
	const PAL_BG_ROOF   ; 6
	const PAL_BG_TEXT   ; 7
