; See data/maps/scenes.asm for which maps have scene variables.
; Each scene_script and coord_event is associated with a current scene ID.

; Scene variables default to 0.
SCENE_DEFAULT  EQU 0
; Often a map will have a one-time default event for scene 0, and switch to a
; do-nothing scene 1 when the event finishes.
SCENE_FINISHED EQU 1

; wPokecenter2FSceneID
	const_def 1
	const SCENE_POKECENTER2F_LEAVE_TRADE_CENTER       ; 1
	const SCENE_POKECENTER2F_LEAVE_COLOSSEUM          ; 2
	const SCENE_POKECENTER2F_LEAVE_TIME_CAPSULE       ; 3
	const SCENE_POKECENTER2F_LEAVE_MOBILE_TRADE_ROOM  ; 4
	const SCENE_POKECENTER2F_LEAVE_MOBILE_BATTLE_ROOM ; 5

; wOaksLabSceneID
	const_def 1
	const SCENE_SILENT_LAB_MEET_OAK           ; 1
	const SCENE_SILENT_LAB_GOT_POKE           ; 2
	const SCENE_SILENT_LAB_GOT_DEX            ; 3
	const SCENE_SILENT_LAB_FOUGHT_RIVAL       ; 4
	const SCENE_SILENT_LAB_NOTHING            ; 5

; wOaksLabBackSceneID
	const_def 1
	const SCENE_SILENTLABBACK_GETTING_POKE ; 1
	const SCENE_SILENTLABBACK_GOT_POKE     ; 2

; wFastShipExtSceneID
	const_def 1
	const SCENE_FAST_SHIP_EXT_ENTER ; 1
	
; wSilentHillSceneID
	const_def 1
	const SCENE_SILENT_MET_RIVAL   ; 1
	const SCENE_SILENT_HAS_STARTER ; 2
	const SCENE_SILENT_CAN_LEAVE   ; 3
	
; wWestPortSceneID
	const_def 1
	const SCENE_EXIT_SHIP_1 ; 1
	const SCENE_EXIT_SHIP_2 ; 2
	const SCENE_EXIT_SHIP_3 ; 3
	
; wHitekkPortSceneID
	const_def 1
	const SCENE_HIGH_TECH_PORT_EXIT ; 1
