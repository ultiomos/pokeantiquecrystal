; Landmarks indexes (see data/maps/landmarks.asm)
	const_def

; Johto landmarks
	const SPECIAL_MAP       ; 00
	const SILENT_HILL       ; 01
	const ROUTE_1           ; 02
	const QUIET_HILL        ; 03
	const OLD_CITY          ; 04
	const SPROUT_TOWER      ; 05
	const ROUTE_2           ; 06
	const WEST_CITY         ; 08
	const RADIO_TOWER       ; 09
	const OPEN_OCEAN        ; 0c
	const HITEKK_CITY       ; 0d
	const ROUTE_3           ; 0e
	const ROUTE_4           ; 0f
	const ROUTE_5           ; 15
	const FOUNT_TOWN        ; 12
	const RUINS_OF_ALPH
	const ROUTE_6
	const ROUTE_7
	const ROUTE_8
	const SOUTH_CITY        ; 13
	const ROUTE_9           ; 14
	const ROUTE_10          ; 10
	const ROUTE_11          ; 11
	const BIRDON            ; 0b
	const ROUTE_12          ; 0a
	const ROUTE_13
	const ROUTE_14
	const ROUTE_15
	const NEWTYPE_CITY
	const ROUTE_16
	const SUGAR_ISLAND
	const ROUTE_17
	const ROUTE_18
	const ROUTE_19
	const KANTO_WORLDMAP
	const ROUTE_20
	const ROUTE_21
	const STAND_CITY
	const ROUTE_22
	const BLUE_FOREST
	const ROUTE_23
	const ROUTE_24
	const NORTH_TOWN
	const ROUTE_25
	const PRINCE_TOWN       ; 07

KANTO_LANDMARK EQU const_value

	const DUMMY_RETURN
	const PALLET_TOWN
	const ROUTE_K1
	const VIRIDIAN_CITY
	const ROUTE_K2
	const VIRIDIAN_FOREST
	const PEWTER_CITY
	const ROUTE_K3
	const MT_MOON
	const ROUTE_K4
	const CERULEAN_CITY
	const ROUTE_K24
	const ROUTE_K25
	const ROUTE_K5
	const ROUTE_K6
	const VERMILION_CITY
	const CELADON_CITY
	const ROUTE_K7
	const ROUTE_K8
	const LAVENDER_TOWN
	const POKEMON_TOWER
	const DUMMY_ROUTE_20
	const SAFFRON_CITY
	const ROUTE_K9
	const ROUTE_K10
	const ROCK_TUNNEL
	const KANTO_POWER_PLANT
	const ROUTE_K11
	const DIGLETTS_CAVE
	const ROUTE_K12
	const ROUTE_K13
	const ROUTE_K14
	const ROUTE_K15
	const FUCHSIA_CITY
	const ROUTE_K16
	const ROUTE_K17
	const ROUTE_K18
	const ROUTE_K19
	const ROUTE_K20
	const SEAFOAM_ISLANDS
	const CINNABAR_ISLAND
	const ROUTE_K21
	const ROUTE_K22
	const DUMMY_ROUTE_19
	const ROUTE_K23
	const VICTORY_ROAD
	const INDIGO_PLATEAU

; used in CaughtData
GIFT_LOCATION  EQU $7e
EVENT_LOCATION EQU $7f


; Regions
	const_def
	const JOHTO_REGION ; 0
	const KANTO_REGION ; 1
NUM_REGIONS EQU const_value
