; item ids
; indexes for:
; - ItemNames (see data/items/names.asm)
; - ItemDescriptions (see data/items/descriptions.asm)
; - ItemAttributes (see data/items/attributes.asm)
; - ItemEffects (see engine/items/item_effects.asm)
	const_def
	const NO_ITEM      ; 00
	const MASTER_BALL  ; 01
	const ULTRA_BALL   ; 02
	const ITEM_03      ; 03 - Catch Rate Item
	const GREAT_BALL   ; 04
	const POKE_BALL    ; 05
	const TOWN_MAP     ; 06
	const BICYCLE      ; 07
	const MOON_STONE   ; 08
	const ANTIDOTE     ; 09 - Catch Rate Item (Not actually obtainable)
	const BURN_HEAL    ; 0a
	const ICE_HEAL     ; 0b
	const AWAKENING    ; 0c
	const PARLYZ_HEAL  ; 0d
	const FULL_RESTORE ; 0e
	const MAX_POTION   ; 0f
	const HYPER_POTION ; 10
	const SUPER_POTION ; 11
	const POTION       ; 12
	const ESCAPE_ROPE  ; 13
	const REPEL        ; 14
	const MAX_ELIXER   ; 15
	const FIRE_STONE   ; 16
	const THUNDERSTONE ; 17
	const WATER_STONE  ; 18
	const ITEM_19      ; 19 - Catch Rate Item
	const HP_UP        ; 1a
	const PROTEIN      ; 1b - Catch Rate Item
	const IRON         ; 1c
	const CARBOS       ; 1d
	const ITEM_1E      ; 1e - Catch Rate Item
	const CALCIUM      ; 1f
	const RARE_CANDY   ; 20
	const X_ACCURACY   ; 21
	const LEAF_STONE   ; 22
	const ITEM_23      ; 23 - Catch Rate Item
	const NUGGET       ; 24
	const POKE_DOLL    ; 25
	const FULL_HEAL    ; 26
	const REVIVE       ; 27
	const MAX_REVIVE   ; 28
	const GUARD_SPEC   ; 29
	const SUPER_REPEL  ; 2a
	const MAX_REPEL    ; 2b
	const DIRE_HIT     ; 2c
	const ITEM_2D      ; 2d - Catch Rate Item
	const FRESH_WATER  ; 2e
	const SODA_POP     ; 2f
	const LEMONADE     ; 30
	const X_ATTACK     ; 31
	const ITEM_32      ; 32 - Catch Rate Item
	const X_DEFEND     ; 33
	const X_SPEED      ; 34
	const X_SPECIAL    ; 35
	const COIN_CASE    ; 36
	const ITEMFINDER   ; 37
	const POKE_FLUTE   ; 38
	const EXP_SHARE    ; 39
	const OLD_ROD      ; 3a
	const GOOD_ROD     ; 3b
	const ITEM_3C      ; 3c - Catch Rate Item
	const SUPER_ROD    ; 3d
	const PP_UP        ; 3e
	const ETHER        ; 3f
	const MAX_ETHER    ; 40
	const ELIXER       ; 41
	
	const MYSTIC_PETAL ; 42
	const WHITEFEATHER ; 43
	const CONFUSE_CLAW ; 44
	const WISDOM_ORB   ; 45
	const STEEL_SHELL  ; 46
	const BLACKGLASSES ; 47
	const ODD_THREAD   ; 48
	const BIG_LEAF     ; 49
	const QUICK_NEEDLE ; 4a
	const ITEM_4B      ; 4b - Catch Rate Item
	const SHARP_STONE  ; 4c
	const BLACKFEATHER ; 4d
	const SHARP_FANG   ; 4e
	const SNAKESKIN    ; 4f
	const ELEC__POUCH  ; 50
	const TOXIC_NEEDLE ; 51
	const KINGS_ROCK   ; 52
	const STRANGEPOWER ; 53
	const LIFE_TAG     ; 54
	const METAL_CLAW   ; 55
	const CORDYCEPS    ; 56
	const PRETTY_TAIL  ; 57
	const SILVERPOWDER ; 58
	const DIGGING_CLAW ; 59
	const ITEM_5A      ; 5a - Catch Rate Item
	const AMULET_COIN  ; 5b
	const MIGRAINESEED ; 5c
	const COUNTER_CUFF ; 5d
	const TALISMAN_TAG ; 5e
	const STRANGEWATER ; 5f
	const TWISTEDSPOON ; 60 - Catch Rate Item
	const ATTACKNEEDLE ; 61
	const POWER_BRACER ; 62
	const SHARP_HORN   ; 63
	const ITEM_64      ; 64 - Catch Rate Item
	const BOUNCBALLOON ; 65
	const FIRE_MANE    ; 66
	const SLOWPOKETAIL ; 67
	const EARTH        ; 68
	const STICK        ; 69
	const FLEE_FEATHER ; 6a
	const ICE_FANG     ; 6b
	const FOSSIL_SHARD ; 6c
	const GROSSGARBAGE ; 6d
	const CHAMPIONBELT ; 6e
	const BIG_PEARL    ; 6f
	const EVERSTONE    ; 70
	const SPELL_TAG    ; 71
	const FIVEYEN_COIN ; 72
	const MYTHRIL_CAPE ; 73
	const STIMULUS_ORB ; 74
	const CALM_BERRY   ; 75
	const THICK_CLUB   ; 76
	const FOCUS_ORB    ; 77
	const ITEM_78      ; 78 - Catch Rate Item
	const DETECT_ORB   ; 79
	const LONG_TONGUE  ; 7a
	const LOTTO_TICKET ; 7b
	const TAG          ; 7c
	const HARD_STONE   ; 7d
	const LUCKY_EGG    ; 7e
	const LONG_VINE    ; 7f
	const MOMS_LOVE    ; 80
	const SMOKESCREEN_I; 81
	const WET_HORN     ; 82
	const SKATEBOARD   ; 83
	const CRIMSONJEWEL ; 84
	const INVIS__WALL  ; 85
	const SHARP_SCYTHE ; 86
	const NIGHTLIGHT   ; 87
	const ICE_BIKINI   ; 88
	const THUNDER_FANG ; 89
	const FIRE_CLAW    ; 8a
	const TWIN_HORNS   ; 8b
	const SPIKE        ; 8c
	const ITEM_8D      ; 8d
	const BETA_DISK    ; 8e
	const METAL_COAT   ; 8f
	const DRAGON_FANG  ; 90
	const WATER_TAIL   ; 91
	const LEFTOVERS    ; 92
	const ICE_WING     ; 93
	const THUNDER_WING ; 94
	const FIRE_WING    ; 95
	const ITEM_96      ; 96 - Catch Rate Item
	const DRAGON_SCALE ; 97
	const BERSERK_GENE ; 98
	const HEART_STONE  ; 99
	const FIRE_TAIL    ; 9a
	const THUNDER_TAIL ; 9b
	const SACRED_ASH   ; 9c
	const HEAVY_BALL   ; 9d
	const ITEM_MAIL    ; 9e
	const LEVEL_BALL   ; 9f
	const LURE_BALL    ; a0
	const FAST_BALL    ; a1
	const POISON_STONE ; a2
	const LIGHT_BALL   ; a3 - Catch Rate Item
	const FRIEND_BALL  ; a4
	const MOON_BALL    ; a5
	const LOVE_BALL    ; a6
	const ITEM_A7      ; a7
	const ITEM_A8      ; a8
	const ITEM_A9      ; a9
	const EXPLOSIVES   ; aa - Catch Rate Item
	const SHIP_TICKET  ; ab
	const UP_GRADE     ; ac
	const BERRY        ; ad
	const APPLE        ; ae
	const CHERI_BERRY  ; af
	const CHESTO_BERRY ; b0
	const PECHA_BERRY  ; b1
	const RAWST_BERRY  ; b2
	const ASPEAR_BERRY ; b3
	const LEPPA_BERRY  ; b4 - Catch Rate Item
	const PERSIM_BERRY ; b5
	const LUM_BERRY    ; b6
	const BLK_APRICORN ; b7
	const RED_APRICORN ; b8
	const BLU_APRICORN ; b9
	const GRN_APRICORN ; ba
	const YLW_APRICORN ; bb
	const WHT_APRICORN ; bc
	const PNK_APRICORN ; bd
	const ITEM_BE      ; be - Catch Rate Item
	const ITEM_BF      ; bf
	const ITEM_C0      ; c0

add_tm: MACRO
if !DEF(TM01)
TM01 = const_value
	enum_start 1
endc
	define _\@_1, "TM_\1"
	const _\@_1
	enum \1_TMNUM
ENDM

; see data/moves/tmhm_moves.asm for moves
	add_tm DYNAMICPUNCH ; c1
	add_tm HEADBUTT     ; c2
	add_tm STRENGTH     ; c3
	add_tm ROLLOUT      ; c4
	add_tm ROAR         ; c5
	add_tm TOXIC        ; c6
	add_tm ZAP_CANNON   ; c7
	add_tm COIN_HURL    ; c8 - Catch Rate Item
	add_tm SURF         ; c9
	add_tm HIDDEN_POWER ; ca
	add_tm SUNNY_DAY    ; cb
	add_tm TEMPT        ; cc
	add_tm SNORE        ; cd
	add_tm BLIZZARD     ; ce
	add_tm HYPER_BEAM   ; cf
	add_tm CUT          ; d0
	add_tm PROTECT      ; d1
	add_tm RAIN_DANCE   ; d2
	add_tm GIGA_DRAIN   ; d3
	add_tm ENDURE       ; d4
	add_tm FRUSTRATION  ; d5
	add_tm SOLARBEAM    ; d6
	add_tm IRON_TAIL    ; d7
	add_tm DRAGONBREATH ; d8
	add_tm THUNDER      ; d9
	add_tm EARTHQUAKE   ; da
	add_tm RETURN       ; db
	add_tm DIG          ; dc
	add_tm PSYCHIC_M    ; dd
	add_tm FLY          ; de
	add_tm MUD_SLAP     ; df
	add_tm DOUBLE_TEAM  ; e0
	add_tm ICE_PUNCH    ; e1 - Catch Rate Item
	add_tm SWAGGER      ; e2
	add_tm SLEEP_TALK   ; e3
	add_tm SLUDGE_BOMB  ; e4
	add_tm SANDSTORM    ; e5
	add_tm FIRE_BLAST   ; e6
	add_tm SWIFT        ; e7
	add_tm DEFENSE_CURL ; e8
	add_tm THUNDERPUNCH ; e9
	add_tm DREAM_EATER  ; ea
	add_tm DETECT       ; eb - Catch Rate Item
	add_tm REST         ; ec
	add_tm ATTRACT      ; ed
	add_tm THIEF        ; ee
	add_tm STEEL_WING   ; ef
	add_tm FIRE_PUNCH   ; f0
	add_tm FURY_CUTTER  ; f1
	add_tm NIGHTMARE    ; f2
NUM_TMS = const_value - TM01

add_hm: MACRO
if !DEF(HM01)
HM01 = const_value
endc
	define _\@_1, "HM_\1"
	const _\@_1
	enum \1_TMNUM
ENDM

	add_hm UPROOT       ; f3
	add_hm WIND_RIDE    ; f4
	add_hm WATER_SPORT  ; f5
	add_hm STRONG_ARM   ; f6
	add_hm BRIGHT_MOSS  ; f7
	add_hm WHIRLPOOL    ; f8
	add_hm BOUNCE       ; f9
NUM_HMS = const_value - HM01

add_mt: MACRO
	enum \1_TMNUM
ENDM

	add_mt FLAMETHROWER
	add_mt THUNDERBOLT
	add_mt ICE_BEAM
NUM_TM_HM_TUTOR = __enum__ + -1

	const ITEM_FA       ; fa
	const ITEM_FB       ; fb
	const ITEM_FC       ; fc
	const ITEM_FD       ; fd
	const ITEM_FE       ; fe
	const ITEM_FF       ; ff

USE_SCRIPT_VAR EQU $00
ITEM_FROM_MEM  EQU $ff

; leftovers from red
SAFARI_BALL    EQU $08 ; MOON_STONE
MOON_STONE_RED EQU $0a ; BURN_HEAL
FULL_HEAL_RED  EQU $34 ; X_SPEED
