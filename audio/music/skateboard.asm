Music_Skateboard:
	musicheader 4, 1, Music_Skateboard_Ch1
	musicheader 4, 2, Music_Skateboard_Ch2
	musicheader 1, 3, Music_Skateboard_Ch3
	musicheader 1, 4, Music_Skateboard_Ch4

Music_Skateboard_Ch1:
	tempo $62
	volume $77
	stereopanning $77
	octave 4
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 4
	note A_, 2
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 6
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 4
	note __, 2
	note B_, 6
	note A_, 6
	note G#, 4
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 4
	note A_, 2
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 6
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 4
	note __, 2
	note B_, 6
	note A_, 6
	note B_, 4
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 4
	note A_, 2
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 6
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 4
	note __, 2
	note B_, 6
	note A_, 6
	note G#, 4
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 4
	note A_, 2
	notetype $F, $A6
	note A_, 2
	note G#, 2
	notetype $C, $A6
	note __, 1
	note E_, 4
	note E_, 2
	note G#, 4
	note A_, 6
	note B_, 6
	note G#, 4
	note A_, 6
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 10
	octave 3
	note A_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note A_, 2
	note G#, 4
	note E_, 4
	note A_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note A_, 2
	note E_, 4
	note G#, 4
	note E_, 2
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 10
	note A_, 2
	note G#, 2
	note E_, 2
	note A_, 2
	note G#, 2
	note E_, 2
	note A_, 2
	note G#, 2
	note E_, 2
	note A_, 2
	note G#, 2
	note E_, 2
	note B_, 2
	note B_, 2
	note A_, 2
	note G#, 2
	note A_, 2
	note G#, 2
	note E_, 2
	note A_, 2
	note G#, 2
	note E_, 2
	note B_, 2
	note B_, 2
	note A_, 2
	note G#, 2
	note E_, 2
	note A_, 2
	note G#, 2
	note E_, 2
	note F#, 2
	note F#, 2
	note G#, 2
	octave 4
	note A_, 6
	note G#, 6
	note E_, 8
	note C#, 4
	note D_, 4
	note E_, 4
	note E_, 12
	note F#, 2
	note E_, 16
	note __, 2
	note A_, 2
	note G#, 2
	note E_, 2
	note A_, 6
	note G#, 6
	note A_, 4
	note G#, 6
	note E_, 4
	note C#, 6
	octave 3
	note B_, 6
	note A_, 4
	note B_, 6
	octave 4
	note C#, 6
	note E_, 4
	note F#, 16
	note __, 10
	note A_, 4
	octave 5
	note E_, 16
	note __, 8
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 8
	octave 4
	note A_, 6
	octave 3
	note A_, 6
	octave 4
	note G#, 6
	octave 3
	note G#, 6
	octave 4
	notetype $F, $A6
	note E_, 16
	octave 3
	note E_, 16
	octave 4
	notetype $C, $A6
	note A_, 6
	octave 3
	note A_, 6
	octave 4
	note G#, 6
	octave 3
	note G#, 6
	octave 4
	notetype $F, $A6
	note E_, 16
	octave 3
	note E_, 16
	octave 4
	notetype $C, $A6
	note A_, 6
	octave 3
	note A_, 6
	octave 4
	note G#, 6
	octave 3
	note G#, 6
	octave 4
	note E_, 8
	octave 3
	note E_, 8
	octave 4
	note C#, 4
	octave 3
	note C#, 4
	octave 4
	note D_, 4
	octave 3
	note D_, 4
	octave 4
	note E_, 4
	octave 3
	note E_, 4
	octave 4
	note E_, 12
	octave 3
	note E_, 12
	octave 4
	note F#, 2
	octave 3
	note F#, 2
	octave 4
	note E_, 16
	note __, 2
	octave 3
	note E_, 16
	note __, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note A_, 2
	octave 3
	note A_, 2
	octave 4
	note B_, 3
	octave 3
	note B_, 3
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 2
	octave 3
	note G#, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note E_, 2
	octave 3
	note E_, 2
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 3
	octave 3
	note G#, 3
	octave 4
	note A_, 2
	octave 3
	note A_, 2
	octave 4
	note B_, 3
	octave 3
	note B_, 3
	octave 4
	note A_, 3
	octave 3
	note A_, 3
	octave 4
	note G#, 2
	octave 3
	note G#, 2
	octave 4
	notetype $F, $A6
	note A_, 16
	octave 3
	note A_, 16
	endchannel

Music_Skateboard_Ch2:
	notetype $C, $A6
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 12
	octave 3
	note E_, 4
	notetype $E, $A6
	note A_, 3
	note __, 7
	notetype $1, $A6
	note __, 4
	notetype $E, $A6
	notetype $C, $A6
	note E_, 4
	note A_, 4
	note A_, 2
	note A_, 4
	note B_, 4
	notetype $8, $A6
	note A_, 8
	note __, 13
	note E_, 6
	note A_, 6
	note A_, 3
	note A_, 6
	note G#, 6
	notetype $B, $A6
	note A_, 6
	note __, 9
	notetype $1, $A6
	note __, 3
	notetype $B, $A6
	notetype $C, $A6
	note E_, 4
	note A_, 4
	note A_, 2
	note A_, 4
	note B_, 4
	notetype $8, $A6
	note A_, 8
	note __, 13
	note E_, 6
	octave 4
	note C#, 6
	note D_, 6
	note C#, 6
	octave 3
	note B_, 6
	note A_, 5
	note __, 13
	note E_, 6
	note A_, 6
	note G#, 6
	note F#, 6
	note G#, 6
	note A_, 6
	note __, 12
	note A_, 3
	note G#, 3
	note G#, 6
	note A_, 6
	note B_, 6
	octave 4
	note C#, 6
	octave 3
	note A_, 6
	note __, 6
	octave 4
	note D_, 9
	note C#, 6
	octave 3
	note B_, 9
	note A_, 6
	notetype $F, $A6
	note A_, 1
	notetype $B, $A6
	note B_, 3
	notetype $C, $A6
	note G#, 6
	note A_, 2
	note A_, 11
	notetype $1, $A6
	note A_, 6
	notetype $C, $A6
	notetype $F, $A6
	note __, 10
	notetype $C, $A6
	note G#, 6
	note A_, 6
	note __, 4
	note B_, 6
	note A_, 6
	note A_, 2
	note B_, 2
	octave 4
	note C#, 4
	note D_, 4
	note C#, 4
	octave 3
	note B_, 4
	note B_, 2
	note A_, 2
	note G#, 2
	note A_, 6
	note __, 4
	note G#, 6
	note A_, 6
	note __, 4
	note B_, 6
	note A_, 6
	note B_, 4
	octave 4
	note C#, 9
	notetype $1, $A6
	note C#, 6
	notetype $C, $A6
	notetype $F, $A6
	note __, 2
	notetype $C, $A6
	note D_, 2
	note E_, 4
	notetype $6, $A6
	note __, 1
	note C#, 8
	octave 3
	note B_, 7
	notetype $1, $A6
	note B_, 5
	notetype $6, $A6
	notetype $1, $A6
	note __, 1
	notetype $C, $A6
	note A_, 4
	notetype $9, $A6
	note __, 2
	note G#, 8
	notetype $F, $A6
	note A_, 8
	notetype $C, $A6
	note B_, 6
	note A_, 6
	note A_, 2
	note B_, 2
	octave 4
	note C#, 4
	note D_, 4
	note C#, 4
	octave 3
	note B_, 4
	note B_, 2
	note A_, 2
	note G#, 2
	notetype $8, $A6
	note A_, 8
	note __, 7
	note G#, 9
	notetype $9, $A6
	note A_, 7
	note __, 6
	notetype $1, $A6
	note __, 3
	notetype $9, $A6
	note B_, 8
	note A_, 8
	notetype $C, $A6
	note A_, 2
	note B_, 2
	octave 4
	note C#, 16
	note C_, 16
	octave 3
	note B_, 16
	note __, 9
	notetype $1, $A6
	note __, 11
	notetype $C, $A6
	note __, 16
	note __, 12
	notetype $1, $A6
	note __, 1
	notetype $C, $A6
	note E_, 2
	note A_, 2
	note G#, 2
	notetype $1, $A6
	note __, 1
	notetype $C, $A6
	note A_, 2
	note B_, 2
	notetype $1, $A6
	note __, 1
	note A_, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 6
	notetype $C, $A6
	note __, 2
	note G#, 2
	note A_, 2
	note B_, 6
	octave 4
	note C#, 6
	octave 3
	note A_, 4
	note B_, 4
	octave 4
	note C#, 4
	note D_, 4
	note C#, 2
	octave 3
	note A_, 9
	note __, 3
	note F#, 2
	octave 4
	note C#, 2
	octave 3
	note B_, 2
	note A_, 4
	note A_, 4
	note A_, 2
	note G#, 4
	octave 4
	note C#, 4
	octave 3
	note B_, 4
	octave 4
	note D_, 4
	note C#, 4
	note __, 2
	octave 3
	note G#, 4
	note A_, 2
	note A_, 4
	note A_, 2
	note B_, 4
	octave 4
	note C#, 6
	note E_, 6
	note C#, 4
	octave 3
	notetype $F, $A6
	note A_, 6
	notetype $9, $A6
	note __, 6
	notetype $C, $A6
	note G#, 2
	note A_, 2
	note B_, 6
	octave 4
	note C#, 6
	octave 3
	note A_, 4
	note B_, 4
	octave 4
	note C#, 4
	note D_, 4
	notetype $9, $A6
	note C#, 3
	note E_, 11
	notetype $1, $A6
	note E_, 3
	notetype $9, $A6
	notetype $F, $A6
	note __, 1
	notetype $C, $A6
	note C#, 4
	note D_, 4
	note E_, 4
	note __, 4
	octave 3
	note A_, 4
	note E_, 4
	note A_, 4
	note G#, 4
	note A_, 4
	note B_, 4
	octave 4
	note C#, 6
	octave 3
	note A_, 2
	note A_, 6
	note A_, 2
	octave 4
	note C#, 8
	note D_, 2
	note C#, 4
	octave 3
	note A_, 14
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 16
	note __, 12
	note A_, 4
	note B_, 4
	notetype $E, $A6
	note A_, 3
	notetype $9, $A6
	note __, 6
	notetype $E, $A6
	note A_, 3
	notetype $6, $A6
	note __, 1
	note B_, 8
	note A_, 7
	note __, 9
	note A_, 8
	note E_, 8
	note A_, 8
	note A_, 8
	note B_, 4
	note A_, 12
	note __, 16
	note A_, 8
	note B_, 8
	note A_, 8
	note __, 8
	note A_, 8
	note E_, 8
	note A_, 8
	note __, 8
	note A_, 8
	note B_, 8
	octave 4
	note C#, 8
	note C#, 8
	note D_, 4
	note C#, 12
	note __, 16
	note C#, 8
	note D_, 8
	note E_, 8
	note __, 8
	octave 3
	note A_, 8
	note E_, 8
	note A_, 8
	note __, 8
	note A_, 8
	note B_, 8
	octave 4
	note C#, 8
	note C#, 8
	note D_, 4
	note C#, 11
	note __, 16
	notetype $1, $A6
	note __, 6
	notetype $6, $A6
	note C#, 8
	note D_, 8
	note E_, 8
	note __, 8
	octave 3
	note A_, 8
	note E_, 8
	note A_, 16
	note A_, 8
	note B_, 8
	octave 4
	note C#, 16
	octave 3
	note B_, 16
	notetype $D, $A6
	note A_, 12
	notetype $C, $A6
	note __, 9
	note E_, 2
	note A_, 2
	note G#, 2
	note A_, 2
	note B_, 2
	note A_, 9
	note __, 3
	note G#, 2
	note A_, 2
	note B_, 6
	octave 4
	note C#, 6
	octave 3
	notetype $E, $A6
	note A_, 3
	notetype $6, $A6
	note __, 1
	note B_, 8
	octave 4
	note C#, 6
	note __, 2
	note D_, 8
	note C#, 4
	octave 3
	notetype $C, $A6
	note A_, 9
	note __, 3
	note F#, 2
	octave 4
	note C#, 2
	octave 3
	note B_, 2
	note A_, 4
	note A_, 4
	note A_, 1
	notetype $1, $A6
	note A_, 11
	notetype $C, $A6
	notetype $1, $A6
	note __, 1
	notetype $C, $A6
	note G#, 4
	octave 4
	note C#, 4
	octave 3
	note B_, 4
	octave 4
	note D_, 4
	notetype $9, $A6
	note C#, 6
	note __, 2
	octave 3
	notetype $C, $A6
	note A_, 4
	note A_, 2
	note A_, 4
	note A_, 1
	notetype $1, $A6
	note A_, 11
	notetype $C, $A6
	notetype $1, $A6
	note __, 1
	notetype $C, $A6
	note B_, 4
	octave 4
	note C#, 6
	note E_, 6
	note C#, 4
	octave 3
	note A_, 8
	note __, 4
	note G#, 2
	note A_, 2
	note B_, 6
	octave 4
	note C#, 6
	octave 3
	note A_, 4
	note B_, 4
	octave 4
	note C#, 4
	note D_, 4
	note C#, 2
	octave 3
	note A_, 9
	note __, 3
	note F#, 2
	octave 4
	note C#, 2
	octave 3
	note B_, 2
	note A_, 4
	note A_, 4
	note A_, 2
	note G#, 4
	octave 4
	note C#, 4
	octave 3
	note B_, 2
	note __, 2
	octave 4
	note D_, 4
	note C#, 4
	note __, 2
	octave 3
	note G#, 4
	note A_, 2
	note A_, 4
	note A_, 2
	note B_, 4
	octave 4
	note C#, 6
	note E_, 6
	note F#, 4
	note E_, 8
	note __, 4
	octave 3
	note G#, 2
	note A_, 2
	note B_, 6
	octave 4
	note C#, 6
	octave 3
	note A_, 4
	note B_, 4
	octave 4
	note C#, 4
	note D_, 4
	note C#, 2
	note E_, 8
	notetype $1, $A6
	note E_, 6
	notetype $C, $A6
	notetype $9, $A6
	note __, 2
	notetype $C, $A6
	note C#, 4
	note D_, 4
	note E_, 4
	note __, 4
	octave 3
	note A_, 4
	note E_, 4
	note A_, 4
	note G#, 4
	note A_, 4
	note B_, 4
	octave 4
	note C#, 6
	octave 3
	note A_, 2
	note A_, 6
	note A_, 2
	octave 4
	note C#, 8
	note D_, 2
	note C#, 4
	note E_, 16
	note __, 16
	note __, 3
	endchannel

Music_Skateboard_Ch3:
	octave 3
	notetype $C, $A6
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note D_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note E_, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note F#, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note A_, 2
	octave 2
	note A_, 4
	octave 3
	note E_, 2
	note A_, 4
	note E_, 2
	note A_, 2
	note E_, 2
	octave 2
	note E_, 4
	note B_, 2
	octave 3
	note E_, 4
	note E_, 2
	octave 2
	note B_, 2
	note B_, 2
	note F#, 4
	octave 3
	note C#, 2
	note F#, 4
	note C#, 2
	note F#, 2
	note C#, 2
	octave 2
	note A_, 4
	octave 3
	note E_, 2
	note A_, 4
	note A_, 2
	note E_, 2
	note E_, 2
	octave 2
	note D_, 4
	note A_, 2
	octave 3
	note D_, 4
	octave 2
	note A_, 2
	octave 3
	note D_, 2
	octave 2
	note A_, 2
	note E_, 4
	note B_, 2
	octave 3
	note E_, 4
	octave 2
	note B_, 2
	octave 3
	note E_, 2
	octave 2
	note B_, 2
	note F#, 4
	octave 3
	note C#, 2
	note F#, 4
	note C#, 2
	note F#, 2
	note C#, 2
	octave 2
	note A_, 4
	octave 3
	note E_, 2
	note A_, 4
	note A_, 2
	note E_, 2
	note E_, 2
	octave 2
	note F#, 4
	octave 3
	note C#, 2
	note F#, 4
	note F#, 2
	note F#, 2
	note C#, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	note D_, 4
	note A_, 4
	note __, 2
	octave 4
	note D_, 2
	note __, 2
	octave 3
	note A_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	note D_, 6
	note D_, 6
	note D_, 4
	note E_, 6
	note E_, 6
	note E_, 4
	note F#, 6
	note F#, 6
	note F#, 4
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	note D_, 6
	note D_, 6
	note D_, 4
	note E_, 6
	note E_, 6
	note E_, 4
	octave 2
	note F#, 4
	octave 3
	note C#, 2
	note F#, 8
	note C#, 2
	note A_, 4
	note __, 8
	note F#, 4
	note D_, 6
	note D_, 6
	note D_, 4
	note E_, 6
	note E_, 6
	note E_, 4
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	note A_, 6
	note A_, 6
	note A_, 4
	note D_, 6
	note D_, 6
	note D_, 4
	note E_, 6
	note E_, 6
	note E_, 4
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note C#, 2
	octave 3
	note C#, 2
	octave 2
	note C#, 2
	octave 3
	note C#, 2
	octave 2
	note C#, 2
	octave 3
	note C#, 2
	octave 2
	note C#, 2
	octave 3
	note C#, 2
	octave 2
	note C_, 2
	octave 3
	note C_, 2
	octave 2
	note C_, 2
	octave 3
	note C_, 2
	octave 2
	note C_, 2
	octave 3
	note C_, 2
	octave 2
	note C_, 2
	octave 3
	note C_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	note __, 2
	octave 3
	note A_, 2
	note A_, 2
	note A_, 2
	note G#, 4
	note __, 2
	note A_, 2
	note A_, 2
	note A_, 2
	note G#, 4
	note __, 10
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note A_, 4
	note A_, 2
	octave 3
	note A_, 4
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 4
	octave 3
	note F#, 4
	octave 2
	note F#, 2
	octave 3
	note C#, 2
	note F#, 4
	octave 2
	note A_, 4
	octave 3
	note A_, 4
	octave 2
	note A_, 2
	octave 3
	note E_, 2
	note A_, 4
	octave 2
	note D_, 4
	octave 3
	note D_, 4
	octave 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 4
	octave 2
	note E_, 4
	octave 3
	note E_, 4
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 4
	octave 2
	note F#, 4
	octave 3
	note F#, 4
	octave 2
	note F#, 2
	octave 3
	note C#, 2
	note F#, 4
	octave 2
	note E_, 4
	octave 3
	note E_, 4
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 4
	octave 2
	note A_, 4
	octave 3
	note A_, 4
	octave 2
	note A_, 2
	octave 3
	note E_, 2
	note A_, 4
	octave 2
	note E_, 4
	octave 3
	note E_, 4
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 4
	octave 2
	note F#, 4
	octave 3
	note F#, 4
	octave 2
	note F#, 2
	octave 3
	note C#, 2
	note F#, 4
	octave 2
	note A_, 4
	octave 3
	note A_, 4
	octave 2
	note A_, 2
	octave 3
	note E_, 2
	note A_, 4
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 4
	octave 3
	note F#, 4
	octave 2
	note F#, 2
	octave 3
	note C#, 2
	note F#, 4
	octave 2
	note D_, 4
	octave 3
	note D_, 4
	octave 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 4
	octave 2
	note E_, 4
	octave 3
	note E_, 4
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 4
	octave 2
	note A_, 4
	octave 3
	note A_, 4
	octave 2
	note A_, 2
	octave 3
	note G#, 4
	note E_, 2
	octave 2
	note F#, 4
	octave 3
	note F#, 4
	octave 2
	note F#, 2
	octave 3
	note C#, 2
	note F#, 4
	octave 2
	note D_, 4
	octave 3
	note D_, 4
	octave 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 4
	octave 2
	note E_, 4
	octave 3
	note E_, 4
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 4
	octave 2
	note A_, 4
	octave 3
	note A_, 4
	octave 2
	note A_, 2
	octave 3
	note G#, 4
	note E_, 2
	octave 2
	note F#, 4
	octave 3
	note F#, 4
	octave 2
	note F#, 2
	octave 3
	note C#, 2
	note F#, 4
	octave 2
	note D_, 4
	octave 3
	note D_, 4
	octave 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 4
	octave 2
	note E_, 4
	octave 3
	note E_, 4
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 4
	octave 2
	note A_, 4
	octave 3
	note A_, 4
	octave 2
	note A_, 2
	octave 3
	note E_, 2
	note A_, 4
	octave 2
	note F#, 4
	octave 3
	note F#, 4
	octave 2
	note F#, 2
	octave 3
	note C#, 2
	note F#, 4
	octave 2
	note D_, 4
	octave 3
	note D_, 4
	octave 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 4
	octave 2
	note E_, 4
	octave 3
	note E_, 4
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 4
	octave 2
	note A_, 4
	octave 3
	note A_, 4
	octave 2
	note A_, 2
	octave 3
	note E_, 2
	note A_, 4
	octave 2
	note D_, 4
	octave 3
	note D_, 4
	octave 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 8
	note __, 12
	octave 4
	note D_, 16
	note E_, 16
	note F_, 16
	note F#, 16
	note D_, 16
	note E_, 16
	note F#, 16
	note __, 16
	octave 2
	note D_, 2
	octave 1
	note D_, 16
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 1
	note E_, 16
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 1
	note F#, 16
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note G#, 2
	octave 1
	note A_, 16
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note D_, 2
	octave 1
	note D_, 16
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 1
	note E_, 16
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 1
	note F#, 16
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 1
	note A_, 16
	octave 2
	note G#, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note D_, 2
	octave 1
	note D_, 16
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 1
	note E_, 16
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 1
	note F#, 16
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note G#, 2
	octave 1
	note A_, 16
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note D_, 2
	octave 1
	note D_, 16
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 1
	note E_, 16
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 1
	note D_, 16
	octave 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note A_, 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note A_, 2
	note E_, 2
	octave 1
	note E_, 16
	octave 2
	note B_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note B_, 2
	note D_, 2
	octave 1
	note D_, 16
	octave 2
	note A_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note A_, 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	note A_, 2
	octave 3
	note D_, 2
	octave 1
	note E_, 16
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note B_, 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	note B_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 2
	octave 2
	note D_, 3
	octave 3
	note D_, 3
	note D_, 3
	octave 2
	note D_, 3
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 3
	octave 3
	note D_, 3
	octave 2
	note D_, 3
	octave 3
	note D_, 3
	note D_, 2
	octave 2
	note D_, 2
	note E_, 3
	octave 3
	note E_, 3
	note E_, 3
	octave 2
	note E_, 3
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	note E_, 3
	octave 3
	note E_, 3
	octave 2
	note E_, 3
	octave 3
	note E_, 3
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 3
	octave 3
	note F#, 3
	octave 2
	note F#, 3
	octave 3
	note F#, 3
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	note F#, 3
	octave 2
	note F#, 3
	note F#, 3
	octave 3
	note F#, 3
	note F#, 2
	octave 2
	note F#, 2
	note A_, 3
	octave 3
	note A_, 3
	note A_, 3
	octave 2
	note A_, 3
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 3
	octave 2
	note A_, 3
	octave 3
	note A_, 3
	octave 2
	note A_, 3
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	note D_, 3
	octave 3
	note D_, 3
	note D_, 3
	octave 2
	note D_, 3
	note D_, 2
	octave 3
	note D_, 2
	octave 2
	note D_, 3
	octave 3
	note D_, 3
	octave 2
	note D_, 3
	octave 3
	note D_, 3
	note D_, 2
	octave 2
	note D_, 2
	note E_, 3
	octave 3
	note E_, 3
	note E_, 3
	octave 2
	note E_, 3
	octave 3
	note E_, 2
	octave 2
	note E_, 2
	note E_, 3
	octave 3
	note E_, 3
	octave 2
	note E_, 3
	octave 3
	note E_, 3
	octave 2
	note E_, 2
	octave 3
	note E_, 2
	octave 2
	note F#, 3
	octave 3
	note F#, 3
	octave 2
	note F#, 3
	octave 3
	note F#, 3
	octave 2
	note F#, 2
	octave 3
	note F#, 2
	note F#, 3
	octave 2
	note F#, 3
	note F#, 3
	octave 3
	note F#, 3
	note F#, 2
	octave 2
	note F#, 2
	note A_, 3
	octave 3
	note A_, 3
	note A_, 3
	octave 2
	note A_, 3
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 3
	octave 2
	note A_, 3
	octave 3
	note A_, 3
	octave 2
	note A_, 3
	octave 3
	note A_, 2
	octave 2
	note A_, 2
	octave 3
	note A_, 16
	note __, 8
	octave 2
	note A_, 16
	note __, 8
	endchannel

Music_Skateboard_Ch4:
	endchannel

