StdScripts::
; entries correspond to constants/std_constants.asm
	dba PokecenterNurseScript
	dba DifficultBookshelfScript
	dba PictureBookshelfScript
	dba MagazineBookshelfScript
	dba TeamRocketOathScript
	dba IncenseBurnerScript
	dba MerchandiseShelfScript
	dba TownMapScript
	dba WindowScript
	dba TVScript
	dba HomepageScript
	dba Radio1Script
	dba Radio2Script
	dba TrashCanScript
	dba StrengthBoulderScript
	dba SmashRockScript
	dba PokecenterSignScript
	dba MartSignScript
	dba GoldenrodRocketsScript
	dba RadioTowerRocketsScript
	dba ElevatorButtonScript
	dba DayToTextScript
	dba BugContestResultsWarpScript
	dba BugContestResultsScript
	dba InitializeEventsScript
	dba AskNumber1MScript
	dba AskNumber2MScript
	dba RegisteredNumberMScript
	dba NumberAcceptedMScript
	dba NumberDeclinedMScript
	dba PhoneFullMScript
	dba RematchMScript
	dba GiftMScript
	dba PackFullMScript
	dba RematchGiftMScript
	dba AskNumber1FScript
	dba AskNumber2FScript
	dba RegisteredNumberFScript
	dba NumberAcceptedFScript
	dba NumberDeclinedFScript
	dba PhoneFullFScript
	dba RematchFScript
	dba GiftFScript
	dba PackFullFScript
	dba RematchGiftFScript
	dba GymStatue1Script
	dba GymStatue2Script
	dba ReceiveItemScript
	dba ReceiveTogepiEggScript
	dba PCScript
	dba GameCornerCoinVendorScript
	dba HappinessCheckScript
	dba VirtualBoyScript

PokecenterNurseScript:
; EVENT_WELCOMED_TO_POKECOM_CENTER is never set

	opentext
	checktime MORN
	iftrue .morn
	checktime DAY
	iftrue .day
	checktime NITE
	iftrue .nite
	jump .ok

.morn
	farwritetext NurseMornText
	buttonsound
	jump .ok
.day
	farwritetext NurseDayText
	buttonsound
	jump .ok
.nite
	farwritetext NurseNiteText
	buttonsound
.ok
	; only do this once

	farwritetext NurseAskHealText
	yesorno
	iffalse .done

	farwritetext NurseTakePokemonText
	pause 20
	special StubbedTrainerRankings_Healings
	turnobject LAST_TALKED, LEFT
	pause 10
	special HealParty
	playmusic MUSIC_NONE
	writebyte HEALMACHINE_POKECENTER
	special HealMachineAnim
	pause 30
	special RestartMapMusic
	turnobject LAST_TALKED, DOWN
	pause 10

	checkphonecall ; elm already called about pokerus
	iftrue .no
	checkflag ENGINE_CAUGHT_POKERUS
	iftrue .no
	special CheckPokerus
	iftrue .pokerus
.no

	farwritetext NurseReturnPokemonText
	pause 20

.done
	farwritetext NurseGoodbyeText

	turnobject LAST_TALKED, UP
	pause 10
	turnobject LAST_TALKED, DOWN
	pause 10

	waitbutton
	closetext
	end

.pokerus
	farwritetext NursePokerusText
	waitbutton
	closetext
	setflag ENGINE_CAUGHT_POKERUS
	end

DifficultBookshelfScript:
	farjumptext DifficultBookshelfText

PictureBookshelfScript:
	farjumptext PictureBookshelfText

MagazineBookshelfScript:
	farjumptext MagazineBookshelfText

TeamRocketOathScript:
	farjumptext TeamRocketOathText

IncenseBurnerScript:
	farjumptext IncenseBurnerText

MerchandiseShelfScript:
	farjumptext MerchandiseShelfText

VirtualBoyScript:
	farjumptext VirtualBoyText

TownMapScript:
	opentext
	farwritetext TownMapText
	waitbutton
	special OverworldTownMap
	closetext
	end

WindowScript:
	farjumptext WindowText

TVScript:
	opentext
	farwritetext TVText
	waitbutton
	closetext
	end

HomepageScript:
	farjumptext HomepageText

Radio1Script:
	opentext
	writebyte MAPRADIO_POKEMON_CHANNEL
	special MapRadio
	closetext
	end

Radio2Script:
; Lucky Channel
	opentext
	writebyte MAPRADIO_LUCKY_CHANNEL
	special MapRadio
	closetext
	end

TrashCanScript: ; 0xbc1a5
	farjumptext TrashCanText

PCScript:
	opentext
	special PokemonCenterPC
	closetext
	end

ElevatorButtonScript:
	playsound SFX_READ_TEXT_2
	pause 15
	playsound SFX_ELEVATOR_END
	end

StrengthBoulderScript:
	farjump AskStrengthScript

SmashRockScript:
	farjump AskRockSmashScript

PokecenterSignScript:
	farjumptext PokecenterSignText

MartSignScript:
	farjumptext MartSignText

DayToTextScript:
	checkcode VAR_WEEKDAY
	ifequal MONDAY, .Monday
	ifequal TUESDAY, .Tuesday
	ifequal WEDNESDAY, .Wednesday
	ifequal THURSDAY, .Thursday
	ifequal FRIDAY, .Friday
	ifequal SATURDAY, .Saturday
	stringtotext .SundayText, MEM_BUFFER_0
	end
.Monday:
	stringtotext .MondayText, MEM_BUFFER_0
	end
.Tuesday:
	stringtotext .TuesdayText, MEM_BUFFER_0
	end
.Wednesday:
	stringtotext .WednesdayText, MEM_BUFFER_0
	end
.Thursday:
	stringtotext .ThursdayText, MEM_BUFFER_0
	end
.Friday:
	stringtotext .FridayText, MEM_BUFFER_0
	end
.Saturday:
	stringtotext .SaturdayText, MEM_BUFFER_0
	end
.SundayText:
	db "SUNDAY@"
.MondayText:
	db "MONDAY@"
.TuesdayText:
	db "TUESDAY@"
.WednesdayText:
	db "WEDNESDAY@"
.ThursdayText:
	db "THURSDAY@"
.FridayText:
	db "FRIDAY@"
.SaturdayText:
	db "SATURDAY@"

GoldenrodRocketsScript:
	clearevent EVENT_GOLDENROD_CITY_ROCKET_TAKEOVER
	end

RadioTowerRocketsScript:
	setflag ENGINE_ROCKETS_IN_RADIO_TOWER
	;~ setevent EVENT_GOLDENROD_CITY_CIVILIANS
	;~ setevent EVENT_RADIO_TOWER_BLACKBELT_BLOCKS_STAIRS
	;~ clearevent EVENT_RADIO_TOWER_ROCKET_TAKEOVER
	;~ clearevent EVENT_USED_THE_CARD_KEY_IN_THE_RADIO_TOWER
	;~ setevent EVENT_MAHOGANY_TOWN_POKEFAN_M_BLOCKS_EAST
	specialphonecall SPECIALCALL_WEIRDBROADCAST
	;~ setmapscene MAHOGANY_TOWN, SCENE_FINISHED
	end

BugContestResultsWarpScript:
	special ClearBGPalettes
	scall BugContestResults_CopyContestantsToResults
	setevent EVENT_ROUTE_36_NATIONAL_PARK_GATE_OFFICER_CONTEST_DAY
	clearevent EVENT_ROUTE_36_NATIONAL_PARK_GATE_OFFICER_NOT_CONTEST_DAY
	setevent EVENT_WARPED_FROM_ROUTE_35_NATIONAL_PARK_GATE
	;~ warp ROUTE_36_NATIONAL_PARK_GATE, 0, 4
	applymovement PLAYER, Movement_ContestResults_WalkAfterWarp

BugContestResultsScript:
	clearflag ENGINE_BUG_CONTEST_TIMER
	clearevent EVENT_WARPED_FROM_ROUTE_35_NATIONAL_PARK_GATE
	clearevent EVENT_CONTEST_OFFICER_HAS_SUN_STONE
	clearevent EVENT_CONTEST_OFFICER_HAS_EVERSTONE
	clearevent EVENT_CONTEST_OFFICER_HAS_GOLD_BERRY
	clearevent EVENT_CONTEST_OFFICER_HAS_BERRY
	opentext
	farwritetext ContestResults_ReadyToJudgeText
	waitbutton
	special BugContestJudging
	vartomem MEM_BUFFER_0
	ifequal 1, BugContestResults_FirstPlace
	ifequal 2, BugContestResults_SecondPlace
	ifequal 3, BugContestResults_ThirdPlace
	farwritetext ContestResults_ConsolationPrizeText
	buttonsound
	waitsfx
	verbosegiveitem BERRY
	iffalse BugContestResults_NoRoomForBerry

BugContestResults_DidNotWin:
	farwritetext ContestResults_DidNotWinText
	buttonsound
	jump BugContestResults_FinishUp
; 0xbc2b1

BugContestResults_ReturnAfterWinnersPrize: ; 0xbc2b1
	farwritetext ContestResults_JoinUsNextTimeText
	buttonsound

BugContestResults_FinishUp:
	;~ checkevent EVENT_LEFT_MONS_WITH_CONTEST_OFFICER
	iffalse BugContestResults_DidNotLeaveMons
	farwritetext ContestResults_ReturnPartyText
	waitbutton
	special ContestReturnMons
BugContestResults_DidNotLeaveMons:
	special CheckPartyFullAfterContest
	ifequal BUGCONTEST_CAUGHT_MON, BugContestResults_CleanUp
	ifequal BUGCONTEST_NO_CATCH, BugContestResults_CleanUp
	; BUGCONTEST_BOXED_MON
	farwritetext ContestResults_PartyFullText
	waitbutton
BugContestResults_CleanUp:
	closetext
	;~ setscene SCENE_ROUTE36NATIONALPARKGATE_NOTHING
	;~ setmapscene ROUTE_35_NATIONAL_PARK_GATE, SCENE_ROUTE35NATIONALPARKGATE_NOTHING
	setevent EVENT_BUG_CATCHING_CONTESTANT_1A
	setevent EVENT_BUG_CATCHING_CONTESTANT_2A
	setevent EVENT_BUG_CATCHING_CONTESTANT_3A
	setevent EVENT_BUG_CATCHING_CONTESTANT_4A
	setevent EVENT_BUG_CATCHING_CONTESTANT_5A
	setevent EVENT_BUG_CATCHING_CONTESTANT_6A
	setevent EVENT_BUG_CATCHING_CONTESTANT_7A
	setevent EVENT_BUG_CATCHING_CONTESTANT_8A
	setevent EVENT_BUG_CATCHING_CONTESTANT_9A
	setevent EVENT_BUG_CATCHING_CONTESTANT_10A
	setevent EVENT_BUG_CATCHING_CONTESTANT_1B
	setevent EVENT_BUG_CATCHING_CONTESTANT_2B
	setevent EVENT_BUG_CATCHING_CONTESTANT_3B
	setevent EVENT_BUG_CATCHING_CONTESTANT_4B
	setevent EVENT_BUG_CATCHING_CONTESTANT_5B
	setevent EVENT_BUG_CATCHING_CONTESTANT_6B
	setevent EVENT_BUG_CATCHING_CONTESTANT_7B
	setevent EVENT_BUG_CATCHING_CONTESTANT_8B
	setevent EVENT_BUG_CATCHING_CONTESTANT_9B
	setevent EVENT_BUG_CATCHING_CONTESTANT_10B
	setflag ENGINE_DAILY_BUG_CONTEST
	special PlayMapMusic
	end
; 0xbc31e

BugContestResults_FirstPlace: ; 0xbc31e
	setevent EVENT_TEMPORARY_UNTIL_MAP_RELOAD_1
	itemtotext HEART_STONE, MEM_BUFFER_1
	farwritetext ContestResults_PlayerWonAPrizeText
	waitbutton
	verbosegiveitem HEART_STONE
	iffalse BugContestResults_NoRoomForSunStone
	jump BugContestResults_ReturnAfterWinnersPrize
; 0xbc332

BugContestResults_SecondPlace: ; 0xbc332
	itemtotext EVERSTONE, MEM_BUFFER_1
	farwritetext ContestResults_PlayerWonAPrizeText
	waitbutton
	verbosegiveitem EVERSTONE
	iffalse BugContestResults_NoRoomForEverstone
	jump BugContestResults_ReturnAfterWinnersPrize
; 0xbc343

BugContestResults_ThirdPlace: ; 0xbc343
	itemtotext APPLE, MEM_BUFFER_1
	farwritetext ContestResults_PlayerWonAPrizeText
	waitbutton
	verbosegiveitem APPLE
	iffalse BugContestResults_NoRoomForGoldBerry
	jump BugContestResults_ReturnAfterWinnersPrize
; 0xbc354

BugContestResults_NoRoomForSunStone: ; 0xbc354
	farwritetext BugContestPrizeNoRoomText
	buttonsound
	setevent EVENT_CONTEST_OFFICER_HAS_SUN_STONE
	jump BugContestResults_ReturnAfterWinnersPrize
; 0xbc35f

BugContestResults_NoRoomForEverstone: ; 0xbc35f
	farwritetext BugContestPrizeNoRoomText
	buttonsound
	setevent EVENT_CONTEST_OFFICER_HAS_EVERSTONE
	jump BugContestResults_ReturnAfterWinnersPrize
; 0xbc36a

BugContestResults_NoRoomForGoldBerry: ; 0xbc36a
	farwritetext BugContestPrizeNoRoomText
	buttonsound
	setevent EVENT_CONTEST_OFFICER_HAS_GOLD_BERRY
	jump BugContestResults_ReturnAfterWinnersPrize
; 0xbc375

BugContestResults_NoRoomForBerry: ; 0xbc375
	farwritetext BugContestPrizeNoRoomText
	buttonsound
	setevent EVENT_CONTEST_OFFICER_HAS_BERRY
	jump BugContestResults_DidNotWin
; 0xbc380

BugContestResults_CopyContestantsToResults: ; 0xbc380
	checkevent EVENT_BUG_CATCHING_CONTESTANT_1A
	iftrue .skip1
	clearevent EVENT_BUG_CATCHING_CONTESTANT_1B
.skip1
	checkevent EVENT_BUG_CATCHING_CONTESTANT_2A
	iftrue .skip2
	clearevent EVENT_BUG_CATCHING_CONTESTANT_2B
.skip2
	checkevent EVENT_BUG_CATCHING_CONTESTANT_3A
	iftrue .skip3
	clearevent EVENT_BUG_CATCHING_CONTESTANT_3B
.skip3
	checkevent EVENT_BUG_CATCHING_CONTESTANT_4A
	iftrue .skip4
	clearevent EVENT_BUG_CATCHING_CONTESTANT_4B
.skip4
	checkevent EVENT_BUG_CATCHING_CONTESTANT_5A
	iftrue .skip5
	clearevent EVENT_BUG_CATCHING_CONTESTANT_5B
.skip5
	checkevent EVENT_BUG_CATCHING_CONTESTANT_6A
	iftrue .skip6
	clearevent EVENT_BUG_CATCHING_CONTESTANT_6B
.skip6
	checkevent EVENT_BUG_CATCHING_CONTESTANT_7A
	iftrue .skip7
	clearevent EVENT_BUG_CATCHING_CONTESTANT_7B
.skip7
	checkevent EVENT_BUG_CATCHING_CONTESTANT_8A
	iftrue .skip8
	clearevent EVENT_BUG_CATCHING_CONTESTANT_8B
.skip8
	checkevent EVENT_BUG_CATCHING_CONTESTANT_9A
	iftrue .skip9
	clearevent EVENT_BUG_CATCHING_CONTESTANT_9B
.skip9
	checkevent EVENT_BUG_CATCHING_CONTESTANT_10A
	iftrue .skip10
	clearevent EVENT_BUG_CATCHING_CONTESTANT_10B
.skip10
	end

InitializeEventsScript:
	setevent EVENT_BUG_CATCHING_CONTESTANT_1A
	setevent EVENT_BUG_CATCHING_CONTESTANT_2A
	setevent EVENT_BUG_CATCHING_CONTESTANT_3A
	setevent EVENT_BUG_CATCHING_CONTESTANT_4A
	setevent EVENT_BUG_CATCHING_CONTESTANT_5A
	setevent EVENT_BUG_CATCHING_CONTESTANT_6A
	setevent EVENT_BUG_CATCHING_CONTESTANT_7A
	setevent EVENT_BUG_CATCHING_CONTESTANT_8A
	setevent EVENT_BUG_CATCHING_CONTESTANT_9A
	setevent EVENT_BUG_CATCHING_CONTESTANT_10A
	setevent EVENT_BUG_CATCHING_CONTESTANT_1B
	setevent EVENT_BUG_CATCHING_CONTESTANT_2B
	setevent EVENT_BUG_CATCHING_CONTESTANT_3B
	setevent EVENT_BUG_CATCHING_CONTESTANT_4B
	setevent EVENT_BUG_CATCHING_CONTESTANT_5B
	setevent EVENT_BUG_CATCHING_CONTESTANT_6B
	setevent EVENT_BUG_CATCHING_CONTESTANT_7B
	setevent EVENT_BUG_CATCHING_CONTESTANT_8B
	setevent EVENT_BUG_CATCHING_CONTESTANT_9B
	setevent EVENT_BUG_CATCHING_CONTESTANT_10B
	setevent EVENT_PLAYERS_HOUSE_2F_CONSOLE
	setevent EVENT_PLAYERS_HOUSE_2F_DOLL_1
	setevent EVENT_PLAYERS_HOUSE_2F_DOLL_2
	setevent EVENT_PLAYERS_HOUSE_2F_BIG_DOLL
	setevent EVENT_DECO_BED_1
	setevent EVENT_DECO_PLANT_4
	setevent EVENT_MYSTERY_GIFT_DELIVERY_GUY
	setflag ENGINE_ROCKET_SIGNAL_ON_CH20
	setflag ENGINE_ROCKETS_IN_MAHOGANY
	variablesprite SPRITE_WEIRD_TREE, SPRITE_SUDOWOODO
	variablesprite SPRITE_OLIVINE_RIVAL, SPRITE_SILVER
	variablesprite SPRITE_AZALEA_ROCKET, SPRITE_ROCKET
	variablesprite SPRITE_FUCHSIA_GYM_1, SPRITE_JANINE
	variablesprite SPRITE_FUCHSIA_GYM_2, SPRITE_JANINE
	variablesprite SPRITE_FUCHSIA_GYM_3, SPRITE_JANINE
	variablesprite SPRITE_FUCHSIA_GYM_4, SPRITE_JANINE
	variablesprite SPRITE_COPYCAT, SPRITE_LASS
	variablesprite SPRITE_JANINE_IMPERSONATOR, SPRITE_LASS
	setevent EVENT_SILENTLAB_GREEN
	setevent EVENT_SILENTLAB_AIDE
	setevent EVENT_SILENTLAB_OAK
	setevent EVENT_PLAYERS_HOUSE_MOM
	setevent EVENT_SILENT_HILL_GREEN
	setevent EVENT_SILENT_HILL_MOM
	setevent EVENT_SILENTLAB_DOOR_LOCKED
	setevent EVENT_CINNABAR_BLAINE
	setflag ENGINE_FLYPOINT_SILENT
	setflag ENGINE_FLYPOINT_DUMMY
	setflag ENGINE_FLYPOINT_KANTO
if DEF(_DEBUG)
	setevent EVENT_GOT_A_POKEMON_FROM_OAK
	setevent EVENT_PLAYERS_HOUSE_SET_TIME
	setevent EVENT_PLAYERS_HOUSE_1F_KEN
	setevent EVENT_GOT_DEX
	setevent EVENT_SILENT_HILL_SILVER
	setevent EVENT_SILENTLAB_STARTUP_DONE
	clearevent EVENT_PLAYERS_HOUSE_MOM
	clearevent EVENT_SILENTLAB_AIDE
	clearevent EVENT_SILENTLAB_OAK
	clearevent EVENT_SILENTLAB_GREEN
	setmapscene SILENT_HILL, SCENE_SILENT_CAN_LEAVE
	setmapscene PLAYERS_HOUSE_1F, SCENE_FINISHED
	setmapscene OAKS_LAB, SCENE_SILENT_LAB_NOTHING
	setmapscene OAKS_LAB_BACK, SCENE_SILENTLABBACK_GOT_POKE
	setflag ENGINE_POKEGEAR
	setflag ENGINE_PHONE_CARD
	setflag ENGINE_BAG
	setflag ENGINE_POKEDEX
	giveitem HM_WIND_RIDE, 1
	giveitem SKATEBOARD, 1
	callasm .DebugStarter
	setevent EVENT_CRUZ_BALL
	setevent EVENT_CHIKORITA_BALL
	setevent EVENT_GOT_CRUZ_FROM_OAK
endc
	setevent EVENT_INITIALIZED_EVENTS
	return
	
if DEF(_DEBUG)
.DebugStarter:
	ld a, $ff
	ld [wBadges], a
	ld a, MEW
	ld [wCurPartySpecies], a
	ld a, 100
	ld [wCurPartyLevel], a
	xor a
	ld [wCurItem], a
	ld b, 1
	ld de, .PokeDatTable
	farcall GivePoke
	ld a, $fc
	ld hl, wVisitedSpawns
	ldi [hl], a
	ld a, -1
	ldi [hl], a
	ldi [hl], a
	ld [hl], a
	ret
	
.PokeDatTable:
	dw .PokeName
	dw .PokeOt
.PokeName:
	db "I@"
.PokeOt:
	db "I@"
endc

AskNumber1MScript:
	special RandomPhoneMon
	checkcode VAR_CALLERID
	;~ ifequal PHONE_SCHOOLBOY_JACK, .Jack
	;~ ifequal PHONE_SAILOR_HUEY, .Huey
	;~ ifequal PHONE_COOLTRAINERM_GAVEN, .Gaven
	;~ ifequal PHONE_BIRDKEEPER_JOSE, .Jose
	;~ ifequal PHONE_YOUNGSTER_JOEY, .Joey
	;~ ifequal PHONE_BUG_CATCHER_WADE, .Wade
	;~ ifequal PHONE_FISHER_RALPH, .Ralph
	;~ ifequal PHONE_HIKER_ANTHONY, .Anthony
	;~ ifequal PHONE_CAMPER_TODD, .Todd
	;~ ifequal PHONE_JUGGLER_IRWIN, .Irwin
	;~ ifequal PHONE_BUG_CATCHER_ARNIE, .Arnie
	;~ ifequal PHONE_SCHOOLBOY_ALAN, .Alan
	;~ ifequal PHONE_SCHOOLBOY_CHAD, .Chad
	;~ ifequal PHONE_POKEFANM_DEREK, .Derek
	;~ ifequal PHONE_FISHER_TULLY, .Tully
	;~ ifequal PHONE_POKEMANIAC_BRENT, .Brent
	;~ ifequal PHONE_BIRDKEEPER_VANCE, .Vance
	;~ ifequal PHONE_FISHER_WILTON, .Wilton
	;~ ifequal PHONE_BLACKBELT_KENJI, .Kenji
	;~ ifequal PHONE_HIKER_PARRY, .Parry

.Jack:
	farwritetext JackAskNumber1Text
	end
.Huey:
	farwritetext HueyAskNumber1Text
	end
.Gaven:
	farwritetext GavenAskNumber1Text
	end
.Jose:
	farwritetext JoseAskNumber1Text
	end
.Joey:
	farwritetext JoeyAskNumber1Text
	end
.Wade:
	farwritetext WadeAskNumber1Text
	end
.Ralph:
	farwritetext RalphAskNumber1Text
	end
.Anthony:
	farwritetext AnthonyAskNumber1Text
	end
.Todd:
	farwritetext ToddAskNumber1Text
	end
.Irwin:
	farwritetext IrwinAskNumber1Text
	end
.Arnie:
	farwritetext ArnieAskNumber1Text
	end
.Alan:
	farwritetext AlanAskNumber1Text
	end
.Chad:
	farwritetext ChadAskNumber1Text
	end
.Derek:
	farwritetext DerekAskNumber1Text
	end
.Tully:
	farwritetext TullyAskNumber1Text
	end
.Brent:
	farwritetext BrentAskNumber1Text
	end
.Vance:
	farwritetext VanceAskNumber1Text
	end
.Wilton:
	farwritetext WiltonAskNumber1Text
	end
.Kenji:
	farwritetext KenjiAskNumber1Text
	end
.Parry:
	farwritetext ParryAskNumber1Text
	end

AskNumber2MScript:
	special RandomPhoneMon
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_SCHOOLBOY_JACK, .Jack
	;~ ifequal PHONE_SAILOR_HUEY, .Huey
	;~ ifequal PHONE_COOLTRAINERM_GAVEN, .Gaven
	;~ ifequal PHONE_BIRDKEEPER_JOSE, .Jose
	;~ ifequal PHONE_YOUNGSTER_JOEY, .Joey
	;~ ifequal PHONE_BUG_CATCHER_WADE, .Wade
	;~ ifequal PHONE_FISHER_RALPH, .Ralph
	;~ ifequal PHONE_HIKER_ANTHONY, .Anthony
	;~ ifequal PHONE_CAMPER_TODD, .Todd
	;~ ifequal PHONE_JUGGLER_IRWIN, .Irwin
	;~ ifequal PHONE_BUG_CATCHER_ARNIE, .Arnie
	;~ ifequal PHONE_SCHOOLBOY_ALAN, .Alan
	;~ ifequal PHONE_SCHOOLBOY_CHAD, .Chad
	;~ ifequal PHONE_POKEFANM_DEREK, .Derek
	;~ ifequal PHONE_FISHER_TULLY, .Tully
	;~ ifequal PHONE_POKEMANIAC_BRENT, .Brent
	;~ ifequal PHONE_BIRDKEEPER_VANCE, .Vance
	;~ ifequal PHONE_FISHER_WILTON, .Wilton
	;~ ifequal PHONE_BLACKBELT_KENJI, .Kenji
	;~ ifequal PHONE_HIKER_PARRY, .Parry

.Jack:
	farwritetext JackAskNumber2Text
	end
.Huey:
	farwritetext HueyAskNumber2Text
	end
.Gaven:
	farwritetext GavenAskNumber2Text
	end
.Jose:
	farwritetext JoseAskNumber2Text
	end
.Joey:
	farwritetext JoeyAskNumber2Text
	end
.Wade:
	farwritetext WadeAskNumber2Text
	end
.Ralph:
	farwritetext RalphAskNumber2Text
	end
.Anthony:
	farwritetext AnthonyAskNumber2Text
	end
.Todd:
	farwritetext ToddAskNumber2Text
	end
.Irwin:
	farwritetext IrwinAskNumber2Text
	end
.Arnie:
	farwritetext ArnieAskNumber2Text
	end
.Alan:
	farwritetext AlanAskNumber2Text
	end
.Chad:
	farwritetext ChadAskNumber2Text
	end
.Derek:
	farwritetext DerekAskNumber2Text
	end
.Tully:
	farwritetext TullyAskNumber2Text
	end
.Brent:
	farwritetext BrentAskNumber2Text
	end
.Vance:
	farwritetext VanceAskNumber2Text
	end
.Wilton:
	farwritetext WiltonAskNumber2Text
	end
.Kenji:
	farwritetext KenjiAskNumber2Text
	end
.Parry:
	farwritetext ParryAskNumber2Text
	end

RegisteredNumberMScript:
	farwritetext RegisteredNumber1Text
	playsound SFX_REGISTER_PHONE_NUMBER
	waitsfx
	buttonsound
	end

NumberAcceptedMScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_SCHOOLBOY_JACK, .Jack
	;~ ifequal PHONE_SAILOR_HUEY, .Huey
	;~ ifequal PHONE_COOLTRAINERM_GAVEN, .Gaven
	;~ ifequal PHONE_BIRDKEEPER_JOSE, .Jose
	;~ ifequal PHONE_YOUNGSTER_JOEY, .Joey
	;~ ifequal PHONE_BUG_CATCHER_WADE, .Wade
	;~ ifequal PHONE_FISHER_RALPH, .Ralph
	;~ ifequal PHONE_HIKER_ANTHONY, .Anthony
	;~ ifequal PHONE_CAMPER_TODD, .Todd
	;~ ifequal PHONE_JUGGLER_IRWIN, .Irwin
	;~ ifequal PHONE_BUG_CATCHER_ARNIE, .Arnie
	;~ ifequal PHONE_SCHOOLBOY_ALAN, .Alan
	;~ ifequal PHONE_SCHOOLBOY_CHAD, .Chad
	;~ ifequal PHONE_POKEFANM_DEREK, .Derek
	;~ ifequal PHONE_FISHER_TULLY, .Tully
	;~ ifequal PHONE_POKEMANIAC_BRENT, .Brent
	;~ ifequal PHONE_BIRDKEEPER_VANCE, .Vance
	;~ ifequal PHONE_FISHER_WILTON, .Wilton
	;~ ifequal PHONE_BLACKBELT_KENJI, .Kenji
	;~ ifequal PHONE_HIKER_PARRY, .Parry

.Jack:
	farwritetext JackNumberAcceptedText
	waitbutton
	closetext
	end
.Huey:
	farwritetext HueyNumberAcceptedText
	waitbutton
	closetext
	end
.Gaven:
	farwritetext GavenNumberAcceptedText
	waitbutton
	closetext
	end
.Jose:
	farwritetext JoseNumberAcceptedText
	waitbutton
	closetext
	end
.Joey:
	farwritetext JoeyNumberAcceptedText
	waitbutton
	closetext
	end
.Wade:
	farwritetext WadeNumberAcceptedText
	waitbutton
	closetext
	end
.Ralph:
	farwritetext RalphNumberAcceptedText
	waitbutton
	closetext
	end
.Anthony:
	farwritetext AnthonyNumberAcceptedText
	waitbutton
	closetext
	end
.Todd:
	farwritetext ToddNumberAcceptedText
	waitbutton
	closetext
	end
.Irwin:
	farwritetext IrwinNumberAcceptedText
	waitbutton
	closetext
	end
.Arnie:
	farwritetext ArnieNumberAcceptedText
	waitbutton
	closetext
	end
.Alan:
	farwritetext AlanNumberAcceptedText
	waitbutton
	closetext
	end
.Chad:
	farwritetext ChadNumberAcceptedText
	waitbutton
	closetext
	end
.Derek:
	farwritetext DerekNumberAcceptedText
	waitbutton
	closetext
	end
.Tully:
	farwritetext TullyNumberAcceptedText
	waitbutton
	closetext
	end
.Brent:
	farwritetext BrentNumberAcceptedText
	waitbutton
	closetext
	end
.Vance:
	farwritetext VanceNumberAcceptedText
	waitbutton
	closetext
	end
.Wilton:
	farwritetext WiltonNumberAcceptedText
	waitbutton
	closetext
	end
.Kenji:
	farwritetext KenjiNumberAcceptedText
	waitbutton
	closetext
	end
.Parry:
	farwritetext ParryNumberAcceptedText
	waitbutton
	closetext
	end

NumberDeclinedMScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_SCHOOLBOY_JACK, .Jack
	;~ ifequal PHONE_SAILOR_HUEY, .Huey
	;~ ifequal PHONE_COOLTRAINERM_GAVEN, .Gaven
	;~ ifequal PHONE_BIRDKEEPER_JOSE, .Jose
	;~ ifequal PHONE_YOUNGSTER_JOEY, .Joey
	;~ ifequal PHONE_BUG_CATCHER_WADE, .Wade
	;~ ifequal PHONE_FISHER_RALPH, .Ralph
	;~ ifequal PHONE_HIKER_ANTHONY, .Anthony
	;~ ifequal PHONE_CAMPER_TODD, .Todd
	;~ ifequal PHONE_JUGGLER_IRWIN, .Irwin
	;~ ifequal PHONE_BUG_CATCHER_ARNIE, .Arnie
	;~ ifequal PHONE_SCHOOLBOY_ALAN, .Alan
	;~ ifequal PHONE_SCHOOLBOY_CHAD, .Chad
	;~ ifequal PHONE_POKEFANM_DEREK, .Derek
	;~ ifequal PHONE_FISHER_TULLY, .Tully
	;~ ifequal PHONE_POKEMANIAC_BRENT, .Brent
	;~ ifequal PHONE_BIRDKEEPER_VANCE, .Vance
	;~ ifequal PHONE_FISHER_WILTON, .Wilton
	;~ ifequal PHONE_BLACKBELT_KENJI, .Kenji
	;~ ifequal PHONE_HIKER_PARRY, .Parry

.Jack:
	farwritetext JackNumberDeclinedText
	waitbutton
	closetext
	end
.Huey:
	farwritetext HueyNumberDeclinedText
	waitbutton
	closetext
	end
.Gaven:
	farwritetext GavenNumberDeclinedText
	waitbutton
	closetext
	end
.Jose:
	farwritetext JoseNumberDeclinedText
	waitbutton
	closetext
	end
.Joey:
	farwritetext JoeyNumberDeclinedText
	waitbutton
	closetext
	end
.Wade:
	farwritetext WadeNumberDeclinedText
	waitbutton
	closetext
	end
.Ralph:
	farwritetext RalphNumberDeclinedText
	waitbutton
	closetext
	end
.Anthony:
	farwritetext AnthonyNumberDeclinedText
	waitbutton
	closetext
	end
.Todd:
	farwritetext ToddNumberDeclinedText
	waitbutton
	closetext
	end
.Irwin:
	farwritetext IrwinNumberDeclinedText
	waitbutton
	closetext
	end
.Arnie:
	farwritetext ArnieNumberDeclinedText
	waitbutton
	closetext
	end
.Alan:
	farwritetext AlanNumberDeclinedText
	waitbutton
	closetext
	end
.Chad:
	farwritetext ChadNumberDeclinedText
	waitbutton
	closetext
	end
.Derek:
	farwritetext DerekNumberDeclinedText
	waitbutton
	closetext
	end
.Tully:
	farwritetext TullyNumberDeclinedText
	waitbutton
	closetext
	end
.Brent:
	farwritetext BrentNumberDeclinedText
	waitbutton
	closetext
	end
.Vance:
	farwritetext VanceNumberDeclinedText
	waitbutton
	closetext
	end
.Wilton:
	farwritetext WiltonNumberDeclinedText
	waitbutton
	closetext
	end
.Kenji:
	farwritetext KenjiNumberDeclinedText
	waitbutton
	closetext
	end
.Parry:
	farwritetext ParryNumberDeclinedText
	waitbutton
	closetext
	end

PhoneFullMScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_SCHOOLBOY_JACK, .Jack
	;~ ifequal PHONE_SAILOR_HUEY, .Huey
	;~ ifequal PHONE_COOLTRAINERM_GAVEN, .Gaven
	;~ ifequal PHONE_BIRDKEEPER_JOSE, .Jose
	;~ ifequal PHONE_YOUNGSTER_JOEY, .Joey
	;~ ifequal PHONE_BUG_CATCHER_WADE, .Wade
	;~ ifequal PHONE_FISHER_RALPH, .Ralph
	;~ ifequal PHONE_HIKER_ANTHONY, .Anthony
	;~ ifequal PHONE_CAMPER_TODD, .Todd
	;~ ifequal PHONE_JUGGLER_IRWIN, .Irwin
	;~ ifequal PHONE_BUG_CATCHER_ARNIE, .Arnie
	;~ ifequal PHONE_SCHOOLBOY_ALAN, .Alan
	;~ ifequal PHONE_SCHOOLBOY_CHAD, .Chad
	;~ ifequal PHONE_POKEFANM_DEREK, .Derek
	;~ ifequal PHONE_FISHER_TULLY, .Tully
	;~ ifequal PHONE_POKEMANIAC_BRENT, .Brent
	;~ ifequal PHONE_BIRDKEEPER_VANCE, .Vance
	;~ ifequal PHONE_FISHER_WILTON, .Wilton
	;~ ifequal PHONE_BLACKBELT_KENJI, .Kenji
	;~ ifequal PHONE_HIKER_PARRY, .Parry

.Jack:
	farwritetext JackPhoneFullText
	waitbutton
	closetext
	end
.Huey:
	farwritetext HueyPhoneFullText
	waitbutton
	closetext
	end
.Gaven:
	farwritetext GavenPhoneFullText
	waitbutton
	closetext
	end
.Jose:
	farwritetext JosePhoneFullText
	waitbutton
	closetext
	end
.Joey:
	farwritetext JoeyPhoneFullText
	waitbutton
	closetext
	end
.Wade:
	farwritetext WadePhoneFullText
	waitbutton
	closetext
	end
.Ralph:
	farwritetext RalphPhoneFullText
	waitbutton
	closetext
	end
.Anthony:
	farwritetext AnthonyPhoneFullText
	waitbutton
	closetext
	end
.Todd:
	farwritetext ToddPhoneFullText
	waitbutton
	closetext
	end
.Irwin:
	farwritetext IrwinPhoneFullText
	waitbutton
	closetext
	end
.Arnie:
	farwritetext ArniePhoneFullText
	waitbutton
	closetext
	end
.Alan:
	farwritetext AlanPhoneFullText
	waitbutton
	closetext
	end
.Chad:
	farwritetext ChadPhoneFullText
	waitbutton
	closetext
	end
.Derek:
	farwritetext DerekPhoneFullText
	waitbutton
	closetext
	end
.Tully:
	farwritetext TullyPhoneFullText
	waitbutton
	closetext
	end
.Brent:
	farwritetext BrentPhoneFullText
	waitbutton
	closetext
	end
.Vance:
	farwritetext VancePhoneFullText
	waitbutton
	closetext
	end
.Wilton:
	farwritetext WiltonPhoneFullText
	waitbutton
	closetext
	end
.Kenji:
	farwritetext KenjiPhoneFullText
	waitbutton
	closetext
	end
.Parry:
	farwritetext ParryPhoneFullText
	waitbutton
	closetext
	end

RematchMScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_SCHOOLBOY_JACK, .Jack
	;~ ifequal PHONE_SAILOR_HUEY, .Huey
	;~ ifequal PHONE_COOLTRAINERM_GAVEN, .Gaven
	;~ ifequal PHONE_BIRDKEEPER_JOSE, .Jose
	;~ ifequal PHONE_YOUNGSTER_JOEY, .Joey
	;~ ifequal PHONE_BUG_CATCHER_WADE, .Wade
	;~ ifequal PHONE_FISHER_RALPH, .Ralph
	;~ ifequal PHONE_HIKER_ANTHONY, .Anthony
	;~ ifequal PHONE_CAMPER_TODD, .Todd
	;~ ifequal PHONE_BUG_CATCHER_ARNIE, .Arnie
	;~ ifequal PHONE_SCHOOLBOY_ALAN, .Alan
	;~ ifequal PHONE_SCHOOLBOY_CHAD, .Chad
	;~ ifequal PHONE_FISHER_TULLY, .Tully
	;~ ifequal PHONE_POKEMANIAC_BRENT, .Brent
	;~ ifequal PHONE_BIRDKEEPER_VANCE, .Vance
	;~ ifequal PHONE_FISHER_WILTON, .Wilton
	;~ ifequal PHONE_HIKER_PARRY, .Parry

.Jack:
	farwritetext JackRematchText
	waitbutton
	closetext
	end
.Huey:
	farwritetext HueyRematchText
	waitbutton
	closetext
	end
.Gaven:
	farwritetext GavenRematchText
	waitbutton
	closetext
	end
.Jose:
	farwritetext JoseRematchText
	waitbutton
	closetext
	end
.Joey:
	farwritetext JoeyRematchText
	waitbutton
	closetext
	end
.Wade:
	farwritetext WadeRematchText
	waitbutton
	closetext
	end
.Ralph:
	farwritetext RalphRematchText
	waitbutton
	closetext
	end
.Anthony:
	farwritetext AnthonyRematchText
	waitbutton
	closetext
	end
.Todd:
	farwritetext ToddRematchText
	waitbutton
	closetext
	end
.Arnie:
	farwritetext ArnieRematchText
	waitbutton
	closetext
	end
.Alan:
	farwritetext AlanRematchText
	waitbutton
	closetext
	end
.Chad:
	farwritetext ChadRematchText
	waitbutton
	closetext
	end
.Tully:
	farwritetext TullyRematchText
	waitbutton
	closetext
	end
.Brent:
	farwritetext BrentRematchText
	waitbutton
	closetext
	end
.Vance:
	farwritetext VanceRematchText
	waitbutton
	closetext
	end
.Wilton:
	farwritetext WiltonRematchText
	waitbutton
	closetext
	end
.Parry:
	farwritetext ParryRematchText
	waitbutton
	closetext
	end

GiftMScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_BIRDKEEPER_JOSE, .Jose
	;~ ifequal PHONE_BUG_CATCHER_WADE, .Wade
	;~ ifequal PHONE_SCHOOLBOY_ALAN, .Alan
	;~ ifequal PHONE_POKEFANM_DEREK, .Derek
	;~ ifequal PHONE_FISHER_TULLY, .Tully
	;~ ifequal PHONE_FISHER_WILTON, .Wilton
	;~ ifequal PHONE_BLACKBELT_KENJI, .Kenji

.Jose:
	farwritetext JoseGiftText
	buttonsound
	end
.Wade:
	farwritetext WadeGiftText
	buttonsound
	end
.Alan:
	farwritetext AlanGiftText
	buttonsound
	end
.Derek:
	farwritetext DerekGiftText
	buttonsound
	end
.Tully:
	farwritetext TullyGiftText
	buttonsound
	end
.Wilton:
	farwritetext WiltonGiftText
	buttonsound
	end
.Kenji:
	farwritetext KenjiGiftText
	buttonsound
	end

PackFullMScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_SAILOR_HUEY, .Huey
	;~ ifequal PHONE_BIRDKEEPER_JOSE, .Jose
	;~ ifequal PHONE_YOUNGSTER_JOEY, .Joey
	;~ ifequal PHONE_BUG_CATCHER_WADE, .Wade
	;~ ifequal PHONE_SCHOOLBOY_ALAN, .Alan
	;~ ifequal PHONE_POKEFANM_DEREK, .Derek
	;~ ifequal PHONE_FISHER_TULLY, .Tully
	;~ ifequal PHONE_BIRDKEEPER_VANCE, .Vance
	;~ ifequal PHONE_FISHER_WILTON, .Wilton
	;~ ifequal PHONE_BLACKBELT_KENJI, .Kenji
	;~ ifequal PHONE_HIKER_PARRY, .Parry

.Huey:
	farwritetext HueyPackFullText
	waitbutton
	closetext
	end
.Jose:
	farwritetext JosePackFullText
	waitbutton
	closetext
	end
.Joey:
	farwritetext JoeyPackFullText
	waitbutton
	closetext
	end
.Wade:
	farwritetext WadePackFullText
	waitbutton
	closetext
	end
.Alan:
	farwritetext AlanPackFullText
	waitbutton
	closetext
	end
.Derek:
	farwritetext DerekPackFullText
	waitbutton
	closetext
	end
.Tully:
	farwritetext TullyPackFullText
	waitbutton
	closetext
	end
.Vance:
	farwritetext VancePackFullText
	waitbutton
	closetext
	end
.Wilton:
	farwritetext WiltonPackFullText
	waitbutton
	closetext
	end
.Kenji:
	farwritetext KenjiPackFullText
	waitbutton
	closetext
	end
.Parry:
	farwritetext ParryPackFullText
	waitbutton
	closetext
	end

RematchGiftMScript:
	opentext
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_SAILOR_HUEY, .Huey
	;~ ifequal PHONE_YOUNGSTER_JOEY, .Joey
	;~ ifequal PHONE_BIRDKEEPER_VANCE, .Vance
	;~ ifequal PHONE_HIKER_PARRY, .Parry

.Huey:
	farwritetext HueyRematchGiftText
	buttonsound
	end
.Joey:
	farwritetext JoeyRematchGiftText
	buttonsound
	end
.Vance:
	farwritetext VanceRematchGiftText
	buttonsound
	end
.Parry:
	farwritetext ParryRematchGiftText
	buttonsound
	end

AskNumber1FScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_POKEFAN_BEVERLY, .Beverly
	;~ ifequal PHONE_COOLTRAINERF_BETH, .Beth
	;~ ifequal PHONE_COOLTRAINERF_REENA, .Reena
	;~ ifequal PHONE_PICNICKER_LIZ, .Liz
	;~ ifequal PHONE_PICNICKER_GINA, .Gina
	;~ ifequal PHONE_LASS_DANA, .Dana
	;~ ifequal PHONE_PICNICKER_TIFFANY, .Tiffany
	;~ ifequal PHONE_PICNICKER_ERIN, .Erin

.Beverly:
	farwritetext BeverlyAskNumber1Text
	end
.Beth:
	farwritetext BethAskNumber1Text
	end
.Reena:
	farwritetext ReenaAskNumber1Text
	end
.Liz:
	farwritetext LizAskNumber1Text
	end
.Gina:
	farwritetext GinaAskNumber1Text
	end
.Dana:
	farwritetext DanaAskNumber1Text
	end
.Tiffany:
	farwritetext TiffanyAskNumber1Text
	end
.Erin:
	farwritetext ErinAskNumber1Text
	end

AskNumber2FScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_POKEFAN_BEVERLY, .Beverly
	;~ ifequal PHONE_COOLTRAINERF_BETH, .Beth
	;~ ifequal PHONE_COOLTRAINERF_REENA, .Reena
	;~ ifequal PHONE_PICNICKER_LIZ, .Liz
	;~ ifequal PHONE_PICNICKER_GINA, .Gina
	;~ ifequal PHONE_LASS_DANA, .Dana
	;~ ifequal PHONE_PICNICKER_TIFFANY, .Tiffany
	;~ ifequal PHONE_PICNICKER_ERIN, .Erin

.Beverly:
	farwritetext BeverlyAskNumber2Text
	end
.Beth:
	farwritetext BethAskNumber2Text
	end
.Reena:
	farwritetext ReenaAskNumber2Text
	end
.Liz:
	farwritetext LizAskNumber2Text
	end
.Gina:
	farwritetext GinaAskNumber2Text
	end
.Dana:
	farwritetext DanaAskNumber2Text
	end
.Tiffany:
	farwritetext TiffanyAskNumber2Text
	end
.Erin:
	farwritetext ErinAskNumber2Text
	end

RegisteredNumberFScript:
	farwritetext RegisteredNumber2Text
	playsound SFX_REGISTER_PHONE_NUMBER
	waitsfx
	buttonsound
	end

NumberAcceptedFScript: ; 0xbcbd3
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_POKEFAN_BEVERLY, .Beverly
	;~ ifequal PHONE_COOLTRAINERF_BETH, .Beth
	;~ ifequal PHONE_COOLTRAINERF_REENA, .Reena
	;~ ifequal PHONE_PICNICKER_LIZ, .Liz
	;~ ifequal PHONE_PICNICKER_GINA, .Gina
	;~ ifequal PHONE_LASS_DANA, .Dana
	;~ ifequal PHONE_PICNICKER_TIFFANY, .Tiffany
	;~ ifequal PHONE_PICNICKER_ERIN, .Erin

.Beverly:
	farwritetext BeverlyNumberAcceptedText
	waitbutton
	closetext
	end
.Beth:
	farwritetext BethNumberAcceptedText
	waitbutton
	closetext
	end
.Reena:
	farwritetext ReenaNumberAcceptedText
	waitbutton
	closetext
	end
.Liz:
	farwritetext LizNumberAcceptedText
	waitbutton
	closetext
	end
.Gina:
	farwritetext GinaNumberAcceptedText
	waitbutton
	closetext
	end
.Dana:
	farwritetext DanaNumberAcceptedText
	waitbutton
	closetext
	end
.Tiffany:
	farwritetext TiffanyNumberAcceptedText
	waitbutton
	closetext
	end
.Erin:
	farwritetext ErinNumberAcceptedText
	waitbutton
	closetext
	end

NumberDeclinedFScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_POKEFAN_BEVERLY, .Beverly
	;~ ifequal PHONE_COOLTRAINERF_BETH, .Beth
	;~ ifequal PHONE_COOLTRAINERF_REENA, .Reena
	;~ ifequal PHONE_PICNICKER_LIZ, .Liz
	;~ ifequal PHONE_PICNICKER_GINA, .Gina
	;~ ifequal PHONE_LASS_DANA, .Dana
	;~ ifequal PHONE_PICNICKER_TIFFANY, .Tiffany
	;~ ifequal PHONE_PICNICKER_ERIN, .Erin

.Beverly:
	farwritetext BeverlyNumberDeclinedText
	waitbutton
	closetext
	end
.Beth:
	farwritetext BethNumberDeclinedText
	waitbutton
	closetext
	end
.Reena:
	farwritetext ReenaNumberDeclinedText
	waitbutton
	closetext
	end
.Liz:
	farwritetext LizNumberDeclinedText
	waitbutton
	closetext
	end
.Gina:
	farwritetext GinaNumberDeclinedText
	waitbutton
	closetext
	end
.Dana:
	farwritetext DanaNumberDeclinedText
	waitbutton
	closetext
	end
.Tiffany:
	farwritetext TiffanyNumberDeclinedText
	waitbutton
	closetext
	end
.Erin:
	farwritetext ErinNumberDeclinedText
	waitbutton
	closetext
	end

PhoneFullFScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_POKEFAN_BEVERLY, .Beverly
	;~ ifequal PHONE_COOLTRAINERF_BETH, .Beth
	;~ ifequal PHONE_COOLTRAINERF_REENA, .Reena
	;~ ifequal PHONE_PICNICKER_LIZ, .Liz
	;~ ifequal PHONE_PICNICKER_GINA, .Gina
	;~ ifequal PHONE_LASS_DANA, .Dana
	;~ ifequal PHONE_PICNICKER_TIFFANY, .Tiffany
	;~ ifequal PHONE_PICNICKER_ERIN, .Erin

.Beverly:
	farwritetext BeverlyPhoneFullText
	waitbutton
	closetext
	end
.Beth:
	farwritetext BethPhoneFullText
	waitbutton
	closetext
	end
.Reena:
	farwritetext ReenaPhoneFullText
	waitbutton
	closetext
	end
.Liz:
	farwritetext LizPhoneFullText
	waitbutton
	closetext
	end
.Gina:
	farwritetext GinaPhoneFullText
	waitbutton
	closetext
	end
.Dana:
	farwritetext DanaPhoneFullText
	waitbutton
	closetext
	end
.Tiffany:
	farwritetext TiffanyPhoneFullText
	waitbutton
	closetext
	end
.Erin:
	farwritetext ErinPhoneFullText
	waitbutton
	closetext
	end

RematchFScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_COOLTRAINERF_BETH, .Beth
	;~ ifequal PHONE_COOLTRAINERF_REENA, .Reena
	;~ ifequal PHONE_PICNICKER_LIZ, .Liz
	;~ ifequal PHONE_PICNICKER_GINA, .Gina
	;~ ifequal PHONE_LASS_DANA, .Dana
	;~ ifequal PHONE_PICNICKER_TIFFANY, .Tiffany
	;~ ifequal PHONE_PICNICKER_ERIN, .Erin

.Beth:
	farwritetext BethRematchText
	waitbutton
	closetext
	end
.Reena:
	farwritetext ReenaRematchText
	waitbutton
	closetext
	end
.Liz:
	farwritetext LizRematchText
	waitbutton
	closetext
	end
.Gina:
	farwritetext GinaRematchText
	waitbutton
	closetext
	end
.Dana:
	farwritetext DanaRematchText
	waitbutton
	closetext
	end
.Tiffany:
	farwritetext TiffanyRematchText
	waitbutton
	closetext
	end
.Erin:
	farwritetext ErinRematchText
	waitbutton
	closetext
	end

GiftFScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_POKEFAN_BEVERLY, .Beverly
	;~ ifequal PHONE_PICNICKER_GINA, .Gina
	;~ ifequal PHONE_LASS_DANA, .Dana
	;~ ifequal PHONE_PICNICKER_TIFFANY, .Tiffany

.Beverly:
	farwritetext BeverlyGiftText
	buttonsound
	end
.Gina:
	farwritetext GinaGiftText
	buttonsound
	end
.Dana:
	farwritetext DanaGiftText
	buttonsound
	end
.Tiffany:
	farwritetext TiffanyGiftText
	buttonsound
	end

PackFullFScript:
	;~ checkcode VAR_CALLERID
	;~ ifequal PHONE_POKEFAN_BEVERLY, .Beverly
	;~ ifequal PHONE_PICNICKER_GINA, .Gina
	;~ ifequal PHONE_LASS_DANA, .Dana
	;~ ifequal PHONE_PICNICKER_TIFFANY, .Tiffany
	;~ ifequal PHONE_PICNICKER_ERIN, .Erin

.Beverly:
	farwritetext BeverlyPackFullText
	waitbutton
	closetext
	end
.Gina:
	farwritetext GinaPackFullText
	waitbutton
	closetext
	end
.Dana:
	farwritetext DanaPackFullText
	waitbutton
	closetext
	end
.Tiffany:
	farwritetext TiffanyPackFullText
	waitbutton
	closetext
	end
.Erin:
	farwritetext ErinPackFullText
	waitbutton
	closetext
	end

RematchGiftFScript:
	checkcode VAR_CALLERID
	;~ ifequal PHONE_PICNICKER_ERIN, .Erin

.Erin:
	opentext
	farwritetext ErinRematchGiftText
	buttonsound
	end

GymStatue1Script:
	mapnametotext MEM_BUFFER_0
	opentext
	farwritetext GymStatue_CityGymText
	waitbutton
	closetext
	end

GymStatue2Script:
	mapnametotext MEM_BUFFER_0
	opentext
	farwritetext GymStatue_CityGymText
	buttonsound
	farwritetext GymStatue_WinningTrainersText
	waitbutton
	closetext
	end

ReceiveItemScript: ; 0xbcdb9
	waitsfx
	farwritetext ReceivedItemText
	playsound SFX_ITEM
	waitsfx
	end
; 0xbcdc3

ReceiveTogepiEggScript: ; 0xbcdc3
	waitsfx
	farwritetext ReceivedItemText
	playsound SFX_GET_EGG_FROM_DAY_CARE_LADY
	waitsfx
	end
; 0xbcdcd

GameCornerCoinVendorScript: ; 0xbcdcd
	faceplayer
	opentext
	farwritetext CoinVendor_WelcomeText
	buttonsound
	checkitem COIN_CASE
	iftrue CoinVendor_IntroScript
	farwritetext CoinVendor_NoCoinCaseText
	waitbutton
	closetext
	end
; 0xbcde0

CoinVendor_IntroScript: ; 0xbcde0
	farwritetext CoinVendor_IntroText

.loop ; 0xbcde4
	special DisplayMoneyAndCoinBalance
	loadmenu .MenuHeader
	verticalmenu
	closewindow
	ifequal 1, .Buy50
	ifequal 2, .Buy500
	jump .Cancel
; 0xbcdf7

.Buy50: ; 0xbcdf7
	checkcoins MAX_COINS - 50
	ifequal HAVE_MORE, .CoinCaseFull
	checkmoney YOUR_MONEY, 1000
	ifequal HAVE_LESS, .NotEnoughMoney
	givecoins 50
	takemoney YOUR_MONEY, 1000
	waitsfx
	playsound SFX_TRANSACTION
	farwritetext CoinVendor_Buy50CoinsText
	waitbutton
	jump .loop
; 0xbce1b

.Buy500: ; 0xbce1b
	checkcoins MAX_COINS - 500
	ifequal HAVE_MORE, .CoinCaseFull
	checkmoney YOUR_MONEY, 10000
	ifequal HAVE_LESS, .NotEnoughMoney
	givecoins 500
	takemoney YOUR_MONEY, 10000
	waitsfx
	playsound SFX_TRANSACTION
	farwritetext CoinVendor_Buy500CoinsText
	waitbutton
	jump .loop
; 0xbce3f

.NotEnoughMoney: ; 0xbce3f
	farwritetext CoinVendor_NotEnoughMoneyText
	waitbutton
	closetext
	end
; 0xbce46

.CoinCaseFull: ; 0xbce46
	farwritetext CoinVendor_CoinCaseFullText
	waitbutton
	closetext
	end
; 0xbce4d

.Cancel: ; 0xbce4d
	farwritetext CoinVendor_CancelText
	waitbutton
	closetext
	end
; 0xbce54


.MenuHeader:
	db MENU_BACKUP_TILES ; flags
	menu_coords 0, 4, 15, TEXTBOX_Y - 1
	dw .MenuData
	db 1 ; default option

.MenuData:
	db STATICMENU_CURSOR ; flags
	db 3 ; items
	db " 50 :  ¥1000@"
	db "500 : ¥10000@"
	db "CANCEL@"
; 0xbce7f


HappinessCheckScript:
	faceplayer
	opentext
	special GetFirstPokemonHappiness
	ifless 50, .Unhappy
	ifless 150, .KindaHappy
	farwritetext HappinessText3
	waitbutton
	closetext
	end

.KindaHappy:
	farwritetext HappinessText2
	waitbutton
	closetext
	end

.Unhappy:
	farwritetext HappinessText1
	waitbutton
	closetext
	end

Movement_ContestResults_WalkAfterWarp: ; bcea1
	step RIGHT
	step DOWN
	turn_head UP
	step_end
; bcea5
