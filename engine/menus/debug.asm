	const_def
	const DEBUGMENU_PICTURES
	const DEBUGMENU_WARP
	const DEBUGMENU_GIVEMON
	const DEBUGMENU_GIVEITEM
	const DEBUGMENU_SOUNDTEST
	const DEBUGMENU_HEXEDIT
	const DEBUGMENU_TIMECHANGE
	const DEBUGMENU_POKER
DEBUGMENU_LENGTH EQU const_value
	
DebugMenu::
	xor a
	ld [wcd53], a
	call ClearWindowData
	ld de, SFX_MENU
	call PlaySFX
	farcall ReanchorBGMap_NoOAMUpdate
	ld hl, .MenuHeader
	call LoadMenuHeader
	call .SetUpMenuItems
	ld a, [wBattleMenuCursorBuffer]
	ld [wMenuCursorBuffer], a
	call DrawVariableLengthMenuBox
	call SafeUpdateSprites
	call _OpenAndCloseMenu_HDMATransferTileMapAndAttrMap
	farcall LoadFonts_NoOAMUpdate
	call UpdateTimePals
	call .GetInput
	jr c, .Exit
	ld a, [wMenuCursorBuffer]
	ld [wBattleMenuCursorBuffer], a
	call PlayClickSFX
	call PlaceHollowCursor
	call .OpenMenu
.Exit:
	ld a, [hOAMUpdate]
	push af
	ld a, 1
	ld [hOAMUpdate], a
	call LoadFontsExtra
	pop af
	ld [hOAMUpdate], a
	call ExitMenu
	call CloseText
	call UpdateTimePals
	ld a, [wcd53]
	and a
	jr z, .justexit
	xor a
	ld [wBattleScriptFlags], a
	ld a, MAPSETUP_RELOADMAP
	ld [hMapEntryMethod], a
	ld a, $1
	call LoadMapStatus
.justexit
	ret
	
.GetInput:
	xor a
	ld [hBGMapMode], a
	call SetUpMenu
	ld a, $ff
	ld [wMenuSelection], a
.loop
	call GetScrollingMenuJoypad
	ld a, [wMenuJoypad]
	cp B_BUTTON
	jr z, .b
	cp A_BUTTON
	jr z, .a
	jr .loop
.a
	call PlayClickSFX
	and a
	ret
.b
	scf
	ret
.MenuHeader:
	db MENU_BACKUP_TILES
	;~ menu_coords 0, 0, 12, SCREEN_HEIGHT - 1
	menu_coords 0, 0, SCREEN_WIDTH - 8, SCREEN_HEIGHT
	dw .MenuData
	db 1
.MenuData:
	db STATICMENU_CURSOR | STATICMENU_WRAP | STATICMENU_ENABLE_START
	dn 0, 0
	dw wMenuItemsList
	dw .MenuString
	dw .Items
.Items:
	dw DebugMenu_Pictures,   .PicturesString
	dw DebugMenu_Warp,       .WarpString
	dw DebugMenu_GiveMon,    .GiveMonString
	dw DebugMenu_GiveItem,   .GiveItemString
	dw DebugMenu_SoundTest,  .SoundTestString
	dw DebugMenu_HexEdit,    .HexEditString
	dw DebugMenu_TimeChange, .TimeChangeString
	dw DebugMenu_Poker,      .PokerString
.PicturesString:   db "Pictures@"
.WarpString:       db "Warp@"
.GiveMonString:    db "Give #@"
.GiveItemString:   db "Give Item@"
.SoundTestString:  db "Sound Test@"
.HexEditString:    db "Hex Edit@"
.TimeChangeString: db "Set Time@"
.PokerString:      db "#r Game@"

.OpenMenu:
	ld bc, .Items
	ld a, [wMenuSelection]
	ld l, a
	ld h, 0
	add hl, hl
	add hl, hl
	add hl, bc
	ldi a, [hl]
	ld h, [hl]
	ld l, a
	jp hl

.MenuString:
	ld bc, .Items
	ld a, [wMenuSelection]
	ld l, a
	ld h, 0
	add hl, hl
	add hl, hl
	add hl, bc
	inc hl
	inc hl
	push de
	ldi a, [hl]
	ld d, [hl]
	ld e, a
	pop hl
	call PlaceString
	ret
	
.SetUpMenuItems:
	ld hl, wMenuItemsList
	ld a, DEBUGMENU_LENGTH
	ldi [hl], a
	ld a, DEBUGMENU_PICTURES
	ldi [hl], a
	ld a, DEBUGMENU_WARP
	ldi [hl], a
	ld a, DEBUGMENU_GIVEMON
	ldi [hl], a
	ld a, DEBUGMENU_GIVEITEM
	ldi [hl], a
	ld a, DEBUGMENU_SOUNDTEST
	ldi [hl], a
	ld a, DEBUGMENU_HEXEDIT
	ldi [hl], a
	ld a, DEBUGMENU_TIMECHANGE
	ldi [hl], a
	ld a, DEBUGMENU_POKER
	ldi [hl], a
	ld a, -1
	ld [hl], a
	ret

DebugMenu_SoundTest:
	xor a
	ld hl, wDebugMenuGivePokeParams
	ldi [hl], a
	ld [hl], a
	hlcoord 0, 0
	lb bc, 10, 18
	call TextBox
	call _OpenAndCloseMenu_HDMATransferTileMapAndAttrMap
	hlcoord 1, 7
	ld de, .helptext1
	call PlaceString
	hlcoord 1, 8
	ld de, .helptext2
	call PlaceString
	hlcoord 1, 9
	ld de, .helptext3
	call PlaceString
	hlcoord 1, 10
	ld de, .helptext4
	call PlaceString
	call DebugSoundTest_Draw
.loop
	call DelayFrame
	call JoyTextDelay
	call DebugSoundTest_GetInput
	jr nc, .loop
	ret
	
.helptext1
	db "U/D : Select Music@"
.helptext2
	db "L/R : Select SFX@"
.helptext3
	db "A : Play Music@"
.helptext4
	db "Start : Play SFX@"
	
DebugSoundTest_GetInput:
	ld a, [hJoyDown]
	bit SELECT_F, a
	jr nz, .select
	ld a, [hJoyPressed]
.select
	bit B_BUTTON_F, a
	jr nz, .b_button
	ld hl, wDebugMenuGivePokeParams
	bit D_UP_F, a
	jr nz, .d_up
	bit D_DOWN_F, a
	jr nz, .d_down
	bit A_BUTTON_F, a
	jr nz, .a_button
	inc hl
	bit D_LEFT_F, a
	jr nz, .d_down
	bit D_RIGHT_F, a
	jr nz, .d_up
	bit START_F, a
	jr nz, .start
	xor a
	ret
	
.b_button
	scf
	ret
	
.a_button
	ld de, MUSIC_NONE
	call PlayMusic
	xor a
	ld [wMusicFade], a
	call MaxVolume
	ld a, [wDebugMenuGivePokeParams]
	ld e, a
	ld d, 0
	call PlayMusic
	ret
	
.start
	ld a, [wDebugMenuGivePokeParams+1]
	ld e, a
	ld d, 0
	call PlaySFX
	ret
	
.d_up
	inc [hl]
	jr .done
.d_down
	dec [hl]
.done
	call DebugSoundTest_Draw
	ret
	
DebugSoundTest_Draw:
	hlcoord 1, 1
	lb bc, 5, 18
	call ClearBox
	hlcoord 1, 1
	ld de, .musStr
	call PlaceString
	hlcoord 8, 1
	ld de, wDebugMenuGivePokeParams
	ld bc, $0103
	call PrintNum
	hlcoord 1, 2
	ld de, .sfxStr
	call PlaceString
	hlcoord 8, 2
	ld de, wDebugMenuGivePokeParams+1
	ld bc, $0103
	call PrintNum
	hlcoord 1, 4
	ld de, .nameStr
	call PlaceString
	call .MusicNameString
	ret
	
.musStr
	db "Music: @"
.sfxStr
	db "SFX: @"
.nameStr
	db "Music Name:@"
	
.MusicNameString:
	ld a, [wDebugMenuGivePokeParams]
	ld c, a
	ld hl, MusicNames
	and a
	jr z, .alreadyzero
.loop
	ldi a, [hl]
	cp "@"
	jr nz, .loop
	dec c
	jr nz, .loop
.alreadyzero
	ld d, h
	ld e, l
	hlcoord 1, 5
	call PlaceString
	ret
	
INCLUDE "data/music_names.asm"
	
DebugMenu_TimeChange:
	farcall SetTimeFromEvent
	farcall SetDayOfWeek
	farcall InitialSetDSTFlag
	ret

DebugMenu_Warp:
	xor a
	ld [wDebugWarpMenu], a
	ld [wDebugWarpX], a
	ld [wDebugWarpY], a
	inc a
	ld [wDebugWarpBank], a
	ld [wDebugWarpMap], a
	hlcoord 0, 3
	lb bc, 1, 18
	call TextBox
	hlcoord 0, 0
	lb bc, 2, 18
	call TextBox
	call _OpenAndCloseMenu_HDMATransferTileMapAndAttrMap
	hlcoord 0, 4
	ld bc, 20
	ld a, " "
	call ByteFill
	ld c, 20
	ld a, "─"
	call ByteFill
	lb bc, 1, 1
	call DebugWarp_FindMapName
	ld h, d
	ld l, e
	ld de, wStringBuffer1
	ld bc, 20
	call CopyBytes
	call DebugWarp_DrawMapName
.loop
	call DelayFrame
	call JoyTextDelay
	call DebugMenuWarp_GetInput
	jr nc, .loop
	ret
	
DebugWarp_DrawMapName:
	hlcoord 1, 1
	ld de, .BankMapStr
	call PlaceString
	hlcoord 1, 2
	ld de, .CoordStr
	call PlaceString
	hlcoord 7, 1
	ld de, wDebugWarpBank
	ld bc, $0102
	call PrintNum
	hlcoord 14, 1
	inc de
	inc c
	call PrintNum
	hlcoord 4, 2
	inc de
	call PrintNum
	hlcoord 12, 2
	inc de
	call PrintNum
	hlcoord 0, 4
	ld bc, 20
	ld a, " "
	call ByteFill
	hlcoord 0, 4
	ld de, wStringBuffer1
	call PlaceString
	ld a, "│"
	hlcoord 0, 1
	ld [hl], a
	hlcoord 0, 2
	ld [hl], a
	ld a, [wDebugWarpMenu]
	dec a
	jr nz, .skip
	hlcoord 0, 1
.skip
	ld a, "→"
	ld [hl], a
	ret
	
.BankMapStr:
	db "Bank:    Map:    @"
.CoordStr:
	db "X:       Y:    @"
	
DebugMenuWarp_GetInput:
	ld a, [hJoyDown]
	bit A_BUTTON_F, a
	jr nz, .a_button
	ld a, [hJoyPressed]
.a_button
	bit B_BUTTON_F, a
	jr nz, .b_button
	bit START_F, a
	jr nz, .start_button
	bit SELECT_F, a
	jr nz, .select_button
	push af
	ld a, [wDebugWarpMenu]
	ld b, a
	pop af
	dec b
	jr z, DebugWarp_InputM1
	jr DebugWarp_InputM0
	
.b_button
	scf
	ret
	
.start_button
	ld a, [wDebugWarpBank]
	ld [wMapGroup], a
	ld a, [wDebugWarpMap]
	ld [wMapNumber], a
	ld a, [wDebugWarpX]
	ld [wXCoord], a
	ld a, [wDebugWarpY]
	ld [wYCoord], a
	ld a, -1
	ld [wDefaultSpawnpoint], a
	ld a, MAPSETUP_WARP
	ld [hMapEntryMethod], a
	ld a, 1
	call LoadMapStatus
	scf
	ret
	
.select_button
	ld a, [wDebugWarpMenu]
	dec a
	jr z, .skip
	inc a
	inc a
.skip
	ld [wDebugWarpMenu], a
	jr DebugWarp_FinishInput
	
NAME_BANK_TABLE_SIZE EQU 26
DebugWarp_InputM0:
	bit D_UP_F, a
	jr nz, .d_up
	bit D_DOWN_F, a
	jr nz, .d_down
	bit D_LEFT_F, a
	jr nz, .d_left
	bit D_RIGHT_F, a
	jr nz, .d_right
	ret
.d_up
	ld a, [wDebugWarpY]
	dec a
	ld [wDebugWarpY], a
	jr DebugWarp_FinishInput
	
.d_down
	ld a, [wDebugWarpY]
	inc a
	ld [wDebugWarpY], a
	jr DebugWarp_FinishInput
	
.d_left
	ld a, [wDebugWarpX]
	dec a
	ld [wDebugWarpX], a
	jr DebugWarp_FinishInput
	
.d_right
	ld a, [wDebugWarpX]
	inc a
	ld [wDebugWarpX], a
	jr DebugWarp_FinishInput

DebugWarp_InputM1:
	bit D_UP_F, a
	jr nz, .d_up
	bit D_DOWN_F, a
	jr nz, .d_down
	bit D_LEFT_F, a
	jr nz, .d_left
	bit D_RIGHT_F, a
	jr nz, .d_right
	ret
.d_up
	ld a, [wDebugWarpBank]
	cp NAME_BANK_TABLE_SIZE
	jr c, .bank_inc
	xor a
.bank_inc
	inc a
	ld [wDebugWarpBank], a
	jr DebugWarp_FinishInput
	
.d_down
	ld a, [wDebugWarpBank]
	dec a
	jr nz, .bank_dec
	ld a, NAME_BANK_TABLE_SIZE
.bank_dec
	ld [wDebugWarpBank], a
	jr DebugWarp_FinishInput
	
.d_left
	ld a, [wDebugWarpMap]
	dec a
	ld [wDebugWarpMap], a
	jr DebugWarp_FinishInput
	
.d_right
	ld a, [wDebugWarpMap]
	inc a
	ld [wDebugWarpMap], a
	jr DebugWarp_FinishInput
	
DebugWarp_FinishInput:
	ld a, [wDebugWarpBank]
	ld b, a
	ld a, [wDebugWarpMap]
	ld c, a
	call DebugWarp_FindMapName
	ld h, d
	ld l, e
	ld de, wStringBuffer1
	ld bc, 21
	call CopyBytes
	call DebugWarp_DrawMapName
	xor a
	ret
	
DebugWarp_FindMapName:
	; takes bank b, map c and returns a pointer to its name
	dec b
	dec c
	push bc
	ld c, b
	ld b, 0
	ld hl, NameBankTable
	add hl, bc
	add hl, bc
	ldi a, [hl]
	ld h, [hl]
	ld l, a
	pop bc
	ld a, c
	and a
	jr z, .found
.loop
	ldi a, [hl]
	cp "@"
	jr nz, .loop
	dec c
	jr nz, .loop
.found
	ld d, h
	ld e, l
	ret

DebugMenu_GiveMon:
	ld hl, wDebugMenuGivePokeParams
	xor a
	inc a
	ldi [hl], a
	ldi [hl], a
	dec a
	ldi [hl], a
	ldi [hl], a
	ldi [hl], a
	ldi [hl], a
	ldi [hl], a
	hlcoord 0, 0
	lb bc, 16, 18
	call TextBox
	call _OpenAndCloseMenu_HDMATransferTileMapAndAttrMap
	call DebugGiveMon_DrawStrings
.loop
	call DelayFrame
	call JoyTextDelay
	call DebugGiveMon_GetInput
	jr nc, .loop
	ret
	
DebugGiveMon_DrawStrings:
	lb bc, 16, 18
	hlcoord 1, 1
	call ClearBox
	call DebugGiveMon_DrawMonName
	call DebugGiveMon_DrawMonLevel
	call DebugGiveMon_DrawItemName
	;~ call DebugGiveMon_DrawMoveNames
	call DebugGiveMon_DrawCursor
	ret
	
DebugGiveMon_DrawMonName:
	ld a, [wDebugMenuGivePokeParams]
	ld [wNamedObjectIndexBuffer], a
	call GetPokemonName
	ld de, wStringBuffer1
	hlcoord 2, 1
	call PlaceString
	hlcoord 16, 1
	ld de, wDebugMenuGivePokeParams
	ld bc, $0103
	call PrintNum
	ret
	
DebugGiveMon_DrawMonLevel:
	hlcoord 2, 2
	ld de, .levelStr
	call PlaceString
	hlcoord 9, 2
	ld de, wDebugMenuGivePokeParams+1
	ld bc, $0103
	call PrintNum
	ret
	
.levelStr:
	db "Level: @"
	
DebugGiveMon_DrawItemName:
	hlcoord 2, 3
	ld de, .itemStr
	call PlaceString
	ld a, [wDebugMenuGivePokeParams+2]
	ld [wNamedObjectIndexBuffer], a
	call GetItemName
	ld de, wStringBuffer1
	hlcoord 7, 3
	call PlaceString
	ret
	
.itemStr:
	db "Itm: @"
	
DebugGiveMon_DrawMoveNames:
	hlcoord 2, 4
	ld a, 1
	call .PlaceMove
	hlcoord 2, 5
	ld a, 2
	call .PlaceMove
	hlcoord 2, 6
	ld a, 3
	call .PlaceMove
	hlcoord 2, 7
	ld a, 4
	call .PlaceMove
	ret
	
.PlaceMove:
	push hl
	ld hl, wDebugMenuGivePokeParams+2
.loop
	inc hl
	dec a
	jr nz, .loop
	ld a, [hl]
	and a
	jr z, .isNull
	cp a, BOUNCE+1
	jr nc, .notValid
	ld [wNamedObjectIndexBuffer], a
	call GetMoveName
	ld hl, wStringBuffer1+20
	ld a, "@"
	ld [hl], a
	ld de, wStringBuffer1
	pop hl
	call PlaceString
	ret
	
.isNull:
	pop hl
	ld de, .noMoveStr
	call PlaceString
	ret
	
.noMoveStr:
	db "-@"
	
.notValid:
	pop hl
	ld de, .notValidStr
	call PlaceString
	ret
	
.notValidStr:
	db "INVALID MOVE@"
	
DebugGiveMon_DrawCursor:
	hlcoord 1, 1
	ld a, [wDebugWarpMenu]
	ld c, a
	ld b, 0
	ld d, 20
.loop
	add hl, bc
	dec d
	jr nz, .loop
	ld a, "→"
	ld [hl], a
	ret
	
DebugGiveMon_GetInput:
	ld a, [hJoyPressed]
	bit B_BUTTON_F, a
	jr nz, .b_button
	bit START_F, a
	jr nz, .start_button
	push af
	ld a, [wDebugWarpMenu]
	ld c, a
	ld b, 0
	ld hl, wDebugMenuGivePokeParams
	add hl, bc
	pop af
	bit D_UP_F, a
	jr nz, .d_up
	bit D_DOWN_F, a
	jr nz, .d_down
	bit D_LEFT_F, a
	jr nz, .d_left
	bit D_RIGHT_F, a
	jr nz, .d_right
	ret
	
.b_button
	scf
	ret
	
.start_button
	; give poke
	ld hl, wDebugMenuGivePokeParams
	ldi a, [hl]
	ld [wCurPartySpecies], a
	ldi a, [hl]
	ld [wCurPartyLevel], a
	ld a, [hl]
	ld [wCurItem], a
	farcall GivePoke
	scf
	ret
	
GIVE_POKE_PARAM_COUNT EQU 2
.d_down
	ld a, [wDebugWarpMenu]
	cp a, GIVE_POKE_PARAM_COUNT
	jr c, .inc
	ld a, -1
.inc
	inc a
	ld [wDebugWarpMenu], a
	jr .finish
.d_up
	ld a, [wDebugWarpMenu]
	and a
	jr nz, .dec
	ld a, GIVE_POKE_PARAM_COUNT+1
.dec
	dec a
	ld [wDebugWarpMenu], a
	jr .finish
.d_left
	ld c, 1
	ld a, [hJoyDown]
	and SELECT
	jr z, .dec_loop
	ld c, 10
.dec_loop
	dec [hl]
	dec c
	jr nz, .dec_loop
	jr .finish
.d_right
	ld c, 1
	ld a, [hJoyDown]
	and SELECT
	jr z, .inc_loop
	ld c, 10
.inc_loop
	inc [hl]
	dec c
	jr nz, .inc_loop
.finish
	call DebugGiveMon_DrawStrings
	xor a
	ret
	
DebugMenu_HexEdit:
	hlcoord 0, 0
	lb bc, 16, 18
	call TextBox
	call _OpenAndCloseMenu_HDMATransferTileMapAndAttrMap
	ld hl, wDebugMenuGivePokeParams
	ld a, $d0
	ldi [hl], a
	ld a, $00
	ldi [hl], a
	ld [hl], a
	hlcoord 0, 0
	ld bc, 360
	ld a, " "
	call ByteFill
.DisplayLoop:
	hlcoord 0, 8
	ld a, "→"
	ld [hl], a
	ld hl, wDebugMenuGivePokeParams
	ldi a, [hl]
	ld l, [hl]
	ld h, a
	push hl
	push hl
	pop de
	hlcoord 0, 17
	ld a, d
	call .DrawHex
	ld a, e
	call .DrawHex
	inc hl
	ld a, [wDebugMenuGivePokeParams+2]
	call .DrawHex
	pop hl
	ld bc, -64
	add hl, bc
	ld d, h
	ld e, l
	ld b, 16
	hlcoord 0, 0
.drawLoop
	ld c, 4
.drawLoop2
	inc hl
	ld a, [de]
	call .DrawHex
	inc de
	ld a, [de]
	call .DrawHex
	inc de
	dec c
	jr nz, .drawLoop2
	dec b
	jr nz, .drawLoop
	call GetJoypad
	ld a, [wDebugMenuGivePokeParams+2]
	and a
	jr z, .SeekMode
	call .UpdateInputEdit
	jr .finishup
.SeekMode
	call .UpdateInputSeek
.finishup
	ld d, h
	ld e, l
	ld hl, wDebugMenuGivePokeParams
	ld [hl], d
	inc hl
	ld [hl], e
	call DelayFrame
	jr .DisplayLoop
	
.DrawHex:
	push af
	and $f0
	swap a
	add "0"
	jr nc, .ok
	add "A"
.ok
	ldi [hl], a
	pop af
	and $0f
	add "0"
	jr nc, .ok2
	add "A"
.ok2
	ldi [hl], a
	ret
	
.UpdateInputSeek:
	ld hl, wDebugMenuGivePokeParams
	ldi a, [hl]
	ld l, [hl]
	ld h, a
	ld a, [hJoyDown]
	bit A_BUTTON_F, a
	jr nz, .skipSeek
	ld a, [hJoyPressed]
.skipSeek
	bit D_UP_F, a
	jr nz, .d_up_s
	bit D_DOWN_F, a
	jr nz, .d_down_s
	bit D_LEFT_F, a
	jr nz, .d_left_s
	bit D_RIGHT_F, a
	jr nz, .d_right_s
	bit SELECT_F, a
	jr nz, .select
	bit B_BUTTON_F, a
	jr nz, .b_button
	ret
	
.d_up_s:
	ld c, 8
.d_up_s_loop
	dec hl
	dec c
	jr nz, .d_up_s_loop
	ret
	
.d_down_s:
	ld c, 8
.d_down_s_loop
	inc hl
	dec c
	jr nz, .d_down_s_loop
	ret
	
.d_left_s:
	dec hl
	ret
	
.d_right_s:
	inc hl
	ret
	
.UpdateInputEdit:
	ld hl, wDebugMenuGivePokeParams
	ldi a, [hl]
	ld l, [hl]
	ld h, a
	ld a, [hJoyDown]
	bit A_BUTTON_F, a
	jr nz, .skipEdit
	ld a, [hJoyPressed]
.skipEdit
	bit D_UP_F, a
	jr nz, .d_up_e
	bit D_DOWN_F, a
	jr nz, .d_down_e
	bit D_LEFT_F, a
	jr nz, .d_left_e
	bit D_RIGHT_F, a
	jr nz, .d_right_e
	bit SELECT_F, a
	jr nz, .select
	bit B_BUTTON_F, a
	jr nz, .b_button
	ret
	
.d_up_e:
	ld c, 8
.d_up_e_loop
	inc [hl]
	dec c
	jr nz, .d_up_e_loop
	ret
	
.d_down_e:
	ld c, 8
.d_down_e_loop
	dec [hl]
	dec c
	jr nz, .d_down_e_loop
	ret
	
.d_left_e:
	dec [hl]
	ret
	
.d_right_e:
	inc [hl]
	ret
	
.select:
	ld a, [wDebugMenuGivePokeParams+2]
	dec a
	jr z, .ok3
	inc a
	inc a
.ok3
	ld [wDebugMenuGivePokeParams+2], a
	ret
	
.b_button:
	pop hl
	ret

DebugMenu_Pictures:
	jp ColorTest
	
DebugMenu_GiveItem:
	ld de, ENGINE_BAG
	ld b, SET_FLAG
	farcall EngineFlagAction
	ld hl, wDebugMenuGivePokeParams
	xor a
	inc a
	ldi [hl], a
	ldi [hl], a
	hlcoord 0, 0
	lb bc, 3, 18
	call TextBox
	call _OpenAndCloseMenu_HDMATransferTileMapAndAttrMap
	call DebugGiveItem_DrawStrings
.loop
	call DelayFrame
	call JoyTextDelay
	call DebugGiveItem_GetInput
	jr nc, .loop
	ret
	
DebugGiveItem_DrawStrings:
	lb bc, 3, 18
	hlcoord 1, 1
	call ClearBox
	ld a, [wDebugMenuGivePokeParams]
	ld [wNamedObjectIndexBuffer], a
	call GetItemName
	ld de, wStringBuffer1
	hlcoord 3, 1
	call PlaceString
	ld de, wDebugMenuGivePokeParams
	ld bc, $0103
	hlcoord 16, 1
	call PrintNum
	hlcoord 3, 2
	ld de, .amtText
	call PlaceString
	ld de, wDebugMenuGivePokeParams+1
	ld bc, $0103
	hlcoord 9, 2
	call PrintNum
	hlcoord 1, 1
	ld a, [wDebugWarpMenu]
	and a
	jr z, .skip
	hlcoord 1, 2
.skip
	ld a, "→"
	ld [hl], a
	ret
	
.amtText:
	db "Amount: @"
	
DebugGiveItem_GetInput:
	ld a, [hJoyPressed]
	bit B_BUTTON_F, a
	jr nz, .b_button
	bit START_F, a
	jr nz, .start_button
	push af
	ld a, [wDebugWarpMenu]
	ld c, a
	ld b, 0
	ld hl, wDebugMenuGivePokeParams
	add hl, bc
	pop af
	bit D_UP_F, a
	jr nz, .d_up
	bit D_DOWN_F, a
	jr nz, .d_down
	bit D_LEFT_F, a
	jr nz, .d_left
	bit D_RIGHT_F, a
	jr nz, .d_right
	ret
	
.b_button
	scf
	ret
	
.start_button
	ld hl, wDebugMenuGivePokeParams
	ldi a, [hl]
	ld [wCurItem], a
	ld a, [hl]
	ld [wItemQuantityChangeBuffer], a
	ld hl, wNumItems
	call ReceiveItem
	jr nc, .full
	scf
	ret
.full
	hlcoord 1, 3
	ld de, .PackFullStr
	call PlaceString
	ret
	
.PackFullStr:
	db "Pack is full!@"
	
.d_down
	ld a, [wDebugWarpMenu]
	cp a, 1
	jr c, .inc
	ld a, -1
.inc
	inc a
	ld [wDebugWarpMenu], a
	jr .finish
.d_up
	ld a, [wDebugWarpMenu]
	and a
	jr nz, .dec
	ld a, 2
.dec
	dec a
	ld [wDebugWarpMenu], a
	jr .finish
.d_left
	ld c, 1
	ld a, [hJoyDown]
	and SELECT
	jr z, .dec_loop
	ld c, 10
.dec_loop
	dec [hl]
	dec c
	jr nz, .dec_loop
	jr .finish
.d_right
	ld c, 1
	ld a, [hJoyDown]
	and SELECT
	jr z, .inc_loop
	ld c, 10
.inc_loop
	inc [hl]
	dec c
	jr nz, .inc_loop
.finish
	call DebugGiveItem_DrawStrings
	xor a
	ret
	
DebugMenu_Poker:
	farcall PokerGame
	ret
