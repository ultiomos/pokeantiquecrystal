BattleCommand_MoneyPower:
	push bc
	ld hl, wBattleMonItem
	ld a, [hBattleTurn]
	and a
	jr z, .ok
	ld hl, wEnemyMonItem
.ok
	ld a, [hl]
	and a
	jr z, .failed
	ld [wCurItem], a
	farcall GetItemPrice
	ld a, d
	or e
	jr z, .failed
	ld a, d
	ld [hDividend+1], a
	ld a, e
	ld [hDividend+2], a
	ld a, 125
	ld [hDivisor], a
	call Divide
	ld a, [hQuotient+2]
	ld d, a
	pop bc
	ret
	
.failed
	pop bc
	call ResetDamage
	ld a, $1
	ld [wAttackMissed], a
	call FailMove
	jp EndMoveEffect
