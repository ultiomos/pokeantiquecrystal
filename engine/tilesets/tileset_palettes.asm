LoadSpecialMapPalette: ; 494ac
	ld a, [wMapTileset]
	;~ cp TILESET_POKECOM_CENTER
	;~ jr z, .pokecom_2f
	cp TILESET_BIRDON
	jr z, .birdon
	cp TILESET_SOUTH_CITY
	jr z, .birdon
	;~ cp TILESET_ICE_PATH
	;~ jr z, .ice_path
	cp TILESET_HIGH_TECH
	jr z, .hightech
	cp TILESET_RADIO_TOWER
	jr z, .radio_tower
	cp TILESET_MANSION
	jr z, .mansion_mobile
	cp TILESET_LIGHTHOUSE
	jr z, .lighthouse
	jr .do_nothing

.pokecom_2f
	call LoadPokeComPalette
	scf
	ret

.birdon
	call LoadBirdonPal
	scf
	ret
	
.lighthouse
	call LoadLighthousePal
	scf
	ret

.ice_path
	ld a, [wEnvironment]
	and $7
	cp INDOOR ; Hall of Fame
	jr z, .do_nothing
	call LoadIcePathPalette
	scf
	ret

.hightech
	call LoadHighTechPalette
	scf
	ret

.radio_tower
	call LoadRadioTowerPalette
	scf
	ret

.mansion_mobile
	call LoadMansionPalette
	scf
	ret

.do_nothing
	and a
	ret
; 494f2

LoadPokeComPalette: ; 494f2
	ld a, BANK(wBGPals1)
	ld de, wBGPals1
	ld hl, PokeComPalette
	ld bc, 8 palettes
	call FarCopyWRAM
	ret
; 49501

PokeComPalette: ; 49501
INCLUDE "gfx/tilesets/pokecom_center.pal"
; 49541

LoadBirdonPal: ; 49541
	ld a, [wTimeOfDayPal]
	maskbits NUM_DAYTIMES
	add a
	add a
	add a
	add a
	add a
	add a
	ld c, a
	ld b, 0
	ld hl, BattleTowerPalette
	add hl, bc
	ld a, BANK(wBGPals1)
	ld de, wBGPals1
	ld bc, 8 palettes
	call FarCopyWRAM
	ret
; 49550

BattleTowerPalette: ; 49550
INCLUDE "gfx/tilesets/birdon.pal"
; 49590

LoadLighthousePal:
	ld a, BANK(wBGPals1)
	ld de, wBGPals1
	ld hl, LighthousePalette
	ld bc, 8 palettes
	call FarCopyWRAM
	ret
	
LighthousePalette:
INCLUDE "gfx/tilesets/lighthouse.pal"

LoadIcePathPalette: ; 49590
	ld a, BANK(wBGPals1)
	ld de, wBGPals1
	ld hl, IcePathPalette
	ld bc, 8 palettes
	call FarCopyWRAM
	ret
; 4959f

IcePathPalette: ; 4959f
INCLUDE "gfx/tilesets/ice_path.pal"
; 495df

LoadHighTechPalette: ; 495df
	ld a, [wTimeOfDayPal]
	maskbits NUM_DAYTIMES
	add a
	add a
	add a
	add a
	add a
	add a
	ld c, a
	ld b, 0
	ld hl, HighTechPalette
	add hl, bc
	ld a, BANK(wBGPals1)
	ld de, wBGPals1
	ld bc, 8 palettes
	call FarCopyWRAM
	ret
; 495ee

HighTechPalette: ; 495ee
INCLUDE "gfx/tilesets/hightech.pal"
; 4962e

LoadRadioTowerPalette: ; 4962e
	ld a, BANK(wBGPals1)
	ld de, wBGPals1
	ld hl, RadioTowerPalette
	ld bc, 8 palettes
	call FarCopyWRAM
	ret
; 4963d

RadioTowerPalette: ; 4963d
INCLUDE "gfx/tilesets/radio_tower.pal"
; 4967d

MansionPalette1: ; 4967d
INCLUDE "gfx/tilesets/mansion_1.pal"
; 496c5

LoadMansionPalette: ; 496c5
	ld a, BANK(wBGPals1)
	ld de, wBGPals1
	ld hl, MansionPalette1
	ld bc, 8 palettes
	call FarCopyWRAM
	ld a, BANK(wBGPals1)
	ld de, wBGPals1 palette PAL_BG_YELLOW
	ld hl, MansionPalette2
	ld bc, 1 palettes
	call FarCopyWRAM
	ld a, BANK(wBGPals1)
	ld de, wBGPals1 palette PAL_BG_WATER
	ld hl, MansionPalette1 + 6 palettes
	ld bc, 1 palettes
	call FarCopyWRAM
	ld a, BANK(wBGPals1)
	ld de, wBGPals1 palette PAL_BG_ROOF
	ld hl, MansionPalette1 + 8 palettes
	ld bc, 1 palettes
	call FarCopyWRAM
	ret
; 496fe

MansionPalette2: ; 496fe
INCLUDE "gfx/tilesets/mansion_2.pal"
; 49706
