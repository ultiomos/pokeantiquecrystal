	const_def
	const POKER_ASK_PLAY
	const POKER_ASK_BET
	const POKER_DEDUCT_COINS
	const POKER_DEAL_CARDS_1
	const POKER_DRAW_FANCY
	const POKER_ASK_DRAW
	const POKER_CHOOSE_DISCARD
	const POKER_DO_DISCARD
	const POKER_DEAL_CARDS_2
	const POKER_DRAW_REGULAR
	const POKER_GET_RESULT
	const POKER_GIVE_RESULT
	const POKER_ASK_DOUBLE_NOTHING
	const POKER_DOUBLE_NOTHING
	const POKER_ASK_DOUBLE_NOTHING_AGAIN
	const POKER_GIVE_COINS
	const POKER_PLAY_AGAIN
	const POKER_QUIT
	
	const_def
	const POKER_NONE
	const POKER_PAIR
	const POKER_2_PAIR
	const POKER_3_OF_A_KIND
	const POKER_STRAIGHT
	const POKER_FLUSH
	const POKER_FULL_HOUSE
	const POKER_4_OF_A_KIND
	const POKER_STRAIGHT_FLUSH
	const POKER_ROYAL_FLUSH

_PokerGame:
	ld hl, wOptions
	set NO_TEXT_SCROLL, [hl]
	ld a, [hInMenu]
	push af
	ld a, 1
	ld [hInMenu], a
	call ClearBGPalettes
	call ClearTileMap
	call ClearSprites
	
	call DisableLCD
	call LoadStandardFont
	call LoadFontsExtra
	ld hl, PokerTileGFX
	ld de, vTiles2 tile $00
	call Decompress
	ld hl, PokerCardGFX
	ld de, vTiles2 tile $30
	call Decompress
	ld hl, PokerCursorGFX
	ld de, vTiles0 tile $00
	ld bc, 4 tiles
	call CopyBytes
	
	ld b, SCGB_BETA_POKER
	call GetSGBLayout
	call Poker_GetPals
	
	call EnableLCD
	call WaitBGMap2
	ld a, $e4
	call DmgToCgbBGPals
	ld de, $e4e4
	call DmgToCgbObjPals
	call DelayFrame
	
	xor a
	ld hl, wPokerHand
	ld bc, wPokerGameEnd - wPokerGame
	call ByteFill
	ld [wJumptableIndex], a
.MainLoop:
	ld a, [wJumptableIndex]
	bit 7, a
	jr nz, .ExitGame
	call .PokerGame
	jr .MainLoop
	
.ExitGame
	call WaitSFX
	ld de, SFX_QUIT_SLOTS
	call PlaySFX
	call WaitSFX
	call ClearBGPalettes
	ld hl, wOptions
	res NO_TEXT_SCROLL, [hl]
	pop af
	ld [hInMenu], a
	ret
	
.PokerGame:
	ld a, [wJumptableIndex]
	ld e, a
	ld d, 0
	ld hl, .Jumptable
	add hl, de
	add hl, de
	ldi a, [hl]
	ld h, [hl]
	ld l, a
	jp hl
	
.Jumptable:
	dw .AskPlay
	dw .AskBet
	dw .DeductCoins
	dw .DealCards
	dw .DrawFancy
	dw .AskDraw
	dw .ChooseDiscard
	dw .DoDiscard
	dw .DealCards
	dw .DrawRegular
	dw .CheckResult
	dw .GiveResult
	dw .AskDoubleOrNothing
	dw .DoubleOrNothing
	dw .AskDoubleOrNothingAgain
	dw .GiveCoins
	dw .PlayAgain
	dw .Quit
	
.Increment:
	ld hl, wJumptableIndex
	inc [hl]
	ret
	
.AskPlay:
	call Poker_AskPlay
	jr nc, .proceed
	ld a, POKER_QUIT
	ld [wJumptableIndex], a
	ret
	
.AskBet:
	call Poker_AskBet
	jr nc, .proceed
	ld a, POKER_QUIT
	ld [wJumptableIndex], a
	ret
	
.proceed
	call .Increment
	ret
	
.DeductCoins:
	call Poker_DeductCoins
	call nc, .Increment
	ret
	
.DealCards:
	call Poker_DealCards
	call .Increment
	ret
	
.DrawFancy:
	call Poker_DrawHandFancy
	call .Increment
	ret
	
.AskDraw:
	call Poker_AskDraw
	jr c, .dont_discard
	call .Increment
	ret
	
.dont_discard
	ld a, POKER_GET_RESULT
	ld [wJumptableIndex], a
	ret
	
.ChooseDiscard:
	call Poker_ChooseDiscard
	call ClearSprites
	call .Increment
	ret
	
.DoDiscard:
	call Poker_DoDiscard
	call .Increment
	ret
	
.DrawRegular:
	call Poker_DrawHandFancyFaceUp
	call .Increment
	ret
	
.CheckResult:
	call _Poker_CheckResult
	call .Increment
	ret
	
.GiveResult:
	call Poker_GiveResult
	ld a, [wPokerResult]
	and a
	jr z, .lose
	dec a
	jr z, .lose
	call .Increment
	ret
	
.lose
	ld a, POKER_PLAY_AGAIN
	ld [wJumptableIndex], a
	ret
	
.AskDoubleOrNothing:
	hlcoord 0, 12
	lb bc, 4, SCREEN_WIDTH - 2
	call TextBox
	ld hl, .double_or_nothing_text
	call PrintText
	call YesNoBox
	jr c, .dont_double
	ld hl, wPokerHand
	ld bc, 5
	xor a
	call ByteFill
	call Poker_DrawCardFromDeck
	ld a, d
	ld [wPokerDoubleUpChallenger], a
	call .Increment
	ret
	
.dont_double
	ld a, POKER_GIVE_COINS
	ld [wJumptableIndex], a
	ret
	
.double_or_nothing_text
	text_jump Poker_DoubleOrNothingText
	db "@"
	
.DoubleOrNothing:
	call Poker_DoubleOrNothing
	jr nc, .cant_continue
	call .Increment
	ret
	
.cant_continue
	ld a, POKER_PLAY_AGAIN
	ld [wJumptableIndex], a
	ret
	
.AskDoubleOrNothingAgain:
	hlcoord 0, 12
	lb bc, 4, SCREEN_WIDTH - 2
	call TextBox
	ld hl, .double_or_nothing_again_text
	call PrintText
	call YesNoBox
	jr c, .dont_double
	ld a, POKER_DOUBLE_NOTHING
	ld [wJumptableIndex], a
	ret
	
.double_or_nothing_again_text
	text_jump Poker_DoubleOrNothingAgainText
	db "@"
	
.GiveCoins:
	call Poker_GiveCoins
	call .Increment
	ret
	
.PlayAgain:
	hlcoord 0, 12
	lb bc, 4, SCREEN_WIDTH - 2
	call TextBox
	ld hl, .play_again_text
	call PrintText
	call YesNoBox
	jr nc, .play_again
	ld a, POKER_QUIT
	ld [wJumptableIndex], a
	ret
	
.play_again
	ld a, POKER_ASK_BET
	ld [wJumptableIndex], a
	ret
	
.Quit:
	ld hl, wJumptableIndex
	set 7, [hl]
	ret
	
.play_again_text:
	text_jump Poker_PlayAgainText
	db "@"
	
Poker_AskPlay:
	call Poker_InitTilemap
	call Poker_DrawCoinCounts
	hlcoord 0, 12
	lb bc, 4, SCREEN_WIDTH - 2
	call TextBox
	ld hl, .play_text
	call PrintText
	call YesNoBox
	ret
	
.play_text:
	text_jump Poker_PlayText
	db "@"
	
Poker_AskBet:
	ld hl, wPokerHand
	ld bc, 5
	xor a
	call ByteFill
	call Poker_ClearBackground
	hlcoord 0, 12
	lb bc, 4, SCREEN_WIDTH - 2
	call TextBox
	ld hl, .bet_text
	call PrintText
	ld hl, .BetMenuHeader
	call LoadMenuHeader
	call VerticalMenu
	call CloseWindow
	ret c
	ld a, [wMenuCursorY]
	dec a
	ld l, a
	ld h, 0
	ld de, .BetAmounts
	add hl, de
	ld a, [hl]
	ld [wPokerBet], a
	ret
	
.BetMenuHeader:
	db MENU_BACKUP_TILES
	menu_coords 13, 10, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1
	dw .BetMenuData
	db 1
	
.BetMenuData:
	db STATICMENU_CURSOR
	db 3
	db "10@"
	db " 5@"
	db " 1@"
	
.BetAmounts
	db 10
	db 5
	db 1
	
.bet_text:
	text_jump Poker_BetText
	db "@"
	
Poker_DeductCoins:
	ld a, [wPokerBet]
	ld b, a
	ld a, [wCoins]
	ld h, a
	ld a, [wCoins + 1]
	ld l, a
	ld a, h
	and a
	jr nz, .have_coins
	ld a, l
	cp b
	jr nc, .have_coins
	hlcoord 0, 12
	ld b, 4
	ld c, SCREEN_WIDTH - 2
	call TextBox
	ld hl, .NotEnoughCoinsText
	call PrintText
	ld a, POKER_QUIT
	ld [wJumptableIndex], a
	scf
	ret
	
.have_coins
	push hl
	ld hl, wPokerBet
	xor a
	sub [hl]
	pop hl
	ld c, a
	ld b, -1
	add hl, bc
	ld a, h
	ld [wCoins], a
	ld a, l
	ld [wCoins + 1], a
	call Poker_DrawCoinCounts
	and a
	ret
	
.NotEnoughCoinsText:
	text_jump UnknownText_0x1c57ab
	db "@"
	
Poker_DealCards:
	ld hl, wPokerHand
	ld b, 5
.loop
	ld a, [hl]
	and a
	jr nz, .already_card
	call Poker_DrawCardFromDeckToHand
.already_card
	inc hl
	dec b
	jr nz, .loop
	ret
	
Poker_AskDraw:
	hlcoord 0, 12
	lb bc, 4, SCREEN_WIDTH - 2
	call TextBox
	ld hl, .discard_text
	call PrintText
	ld hl, .yesno_header
	call LoadMenuHeader
	call VerticalMenu
	call CloseWindow
	ret c
	ld a, [wMenuCursorY]
	dec a
	jr nz, .no
	and a
	ret
	
.no
	scf
	ret
	
.discard_text:
	text_jump Poker_DiscardText
	db "@"
	
.yesno_header:
	db MENU_BACKUP_TILES
	menu_coords 14, 11, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 3
	dw .yesno_data
	db 1

.yesno_data:
	db STATICMENU_CURSOR | STATICMENU_NO_TOP_SPACING
	db 2
	db "Yes@"
	db "No@"
	
Poker_ChooseDiscard:
	call JoyTextDelay
	xor a
	ld [wPokerDiscardCursor], a
	ld [wPokerHandDiscard], a
.loop
	call Poker_DiscardInput
	ret c
	call JoyTextDelay
	call Poker_UpdateDiscardCursorOAM
	call DelayFrame
	jr .loop
	
Poker_DiscardInput:
	ld a, [hJoyPressed]
	bit D_LEFT_F, a
	jr nz, .d_left
	bit D_RIGHT_F, a
	jr nz, .d_right
	bit A_BUTTON_F, a
	jr nz, .a_button
	bit B_BUTTON_F, a
	jr nz, .b_button
	xor a
	ret
	
.b_button
	scf
	ret
	
.d_left
	ld a, [wPokerDiscardCursor]
	and a
	jr nz, .dec_ok
	ld a, 5
.dec_ok
	dec a
	ld [wPokerDiscardCursor], a
	jr .finish
	
.d_right
	ld a, [wPokerDiscardCursor]
	cp 4
	jr c, .inc_ok
	ld a, -1
.inc_ok
	inc a
	ld [wPokerDiscardCursor], a
	jr .finish

.a_button
	ld a, [wPokerDiscardCursor]
	ld b, a
	call .toggle_flag_b
	
.finish
	call Poker_DrawHand
	call Poker_UpdateDiscardCursorOAM
	and a
	ret
	
.toggle_flag_b
	ld hl, wPokerHandDiscard
	xor a
	inc a
	inc b
.loop
	dec b
	jr z, .got_a
	rla
	jr .loop
.got_a
	xor [hl]
	ld [hl], a
	ret
	
Poker_UpdateDiscardCursorOAM:
	call ClearSprites
	ld a, [hCGB]
	and a
	jr nz, .skip
	ld a, [hVBlankCounter]
	and $4
	ret nz
	
.skip
	ld a, [wPokerDiscardCursor]
	ld bc, .OAMData
	ld l, a
	ld h, 0
	add hl, hl
	add hl, hl
	add hl, bc
	call Poker_CopyOAMCoords
	ret
	
.OAMData
	db $28, $20, $28, $20
	db $40, $38, $40, $38
	db $58, $50, $58, $50
	db $70, $68, $70, $68
	db $88, $80, $88, $80
	
Poker_CopyOAMCoords:
	; y = $60 or $68
	ld de, wVirtualOAMSprite00
	ld c, 4
	ld b, $68
.loop
	ld a, b
	ld [de], a
	inc de
	ldi a, [hl]
	ld [de], a
	inc de
	ld a, c
	dec a
	ld [de], a
	inc de
	inc de
	dec c
	ret z
	ld a, c
	cp 2
	jr nz, .loop
	ld b, $60
	jr .loop
	
Poker_DoDiscard:
	ld hl, wPokerHand
	ld a, 1
	ld b, 5
.loop
	ld c, a
	ld a, [wPokerHandDiscard]
	and c
	ld a, [hl]
	jr z, .skip
	xor a
.skip
	ldi [hl], a
	ld a, c
	rla
	dec b
	jr nz, .loop
	ret
	
_Poker_CheckResult:
	call Poker_CheckResult
	push af
	call Poker_CalcPayout
	call Poker_WriteHand
	pop af
	ret
	
Poker_CheckResult:
	call Poker_CopyCheckNofaKindToBuffer
	xor a
	ld [wPokerResult], a
	call .check_royal_flush
	ret nc
	call .check_straight_flush
	ret nc
	call .check_4ofakind
	ret nc
	call .check_full_house
	ret nc
	call .check_flush
	ret nc
	call .check_straight
	ret nc
	call .check_3ofakind
	ret nc
	call .check_2pair
	ret nc
	call .check_pair
	ret
	
.check_royal_flush:
	call Poker_CheckStraightInHand
	ret c
	ld a, d
	cp 9
	ret c
	call Poker_CheckFlush
	ret c
	ld a, POKER_ROYAL_FLUSH
	ld [wPokerResult], a
	xor a
	ret
	
.check_straight_flush:
	call Poker_CheckStraightInHand
	ret c
	call Poker_CheckFlush
	ret c
	ld a, POKER_STRAIGHT_FLUSH
	ld [wPokerResult], a
	xor a
	ret
	
.check_4ofakind:
	call Poker_CheckNofaKind
	ret c
	ld a, d
	cp POKER_4_OF_A_KIND
	jr z, .nope
	ld [wPokerResult], a
	xor a
	ret
	
.check_full_house:
	call Poker_CheckNofaKind
	ret c
	ld a, d
	cp POKER_FULL_HOUSE
	jr z, .nope
	ld [wPokerResult], a
	xor a
	ret
	
.check_flush:
	call Poker_CheckFlush
	ret c
	ld a, POKER_FLUSH
	ld [wPokerResult], a
	xor a
	ret
	
.check_straight:
	call Poker_CheckStraightInHand
	ret c
	ld a, POKER_STRAIGHT
	ld [wPokerResult], a
	xor a
	ret
	
.check_3ofakind:
	call Poker_CheckNofaKind
	ret c
	ld a, d
	cp POKER_3_OF_A_KIND
	jr z, .nope
	ld [wPokerResult], a
	xor a
	ret
	
.check_2pair:
	call Poker_CheckNofaKind
	ret c
	ld a, d
	cp POKER_2_PAIR
	jr z, .nope
	ld [wPokerResult], a
	xor a
	ret
	
.check_pair:
	call Poker_CheckNofaKind
	ret c
	ld a, d
	cp POKER_PAIR
	jr z, .nope
	ld [wPokerResult], a
	xor a
	ret
	
.nope:
	scf
	ret
	
Poker_CalcPayout:
	ld hl, .payouts
	ld a, [wPokerResult]
	ld c, a
	ld b, 0
	add hl, bc
	ld a, [hl]
	ld c, a
	ld b, 0
	ld a, [wPokerBet]
	ld hl, 0
.add_loop
	add hl, bc
	dec a
	jr nz, .add_loop
	ld bc, wPokerPayout
	ld a, h
	ld [bc], a
	inc bc
	ld a, l
	ld [bc], a
	ret
	
.payouts
	db 0   ; none
	db 0   ; pair
	db 1   ; two pair
	db 2   ; 3 of a kind
	db 4   ; straight
	db 10  ; flush
	db 20  ; full house
	db 40  ; 4 of a kind
	db 80  ; straight flush
	db 100 ; royal flush
	
Poker_WriteHand:
	ld hl, .hand_names
	ld a, [wPokerResult]
	and a
	jr z, .skip
	ld b, a
.loop
	ldi a, [hl]
	cp "@"
	jr nz, .loop
	dec b
	jr nz, .loop
.skip
	push hl
	hlcoord 0, 12
	lb bc, 4, SCREEN_WIDTH - 2
	call TextBox
	pop de
	hlcoord 1, 14
	call PlaceString
	ret
	
.hand_names
	db "None<.>@"
	db "1 Pair@"
	db "2 Pair@"
	db "3 of a Kind@"
	db "Straight@"
	db "Flush@"
	db "Full House!@"
	db "4 of a Kind!@"
	db "Straight Flush!@"
	db "Royal Flush!@"
	
Poker_CheckFlush:
	; checks for a flush, returns nc for true
	ld a, 5
.loop
	ld e, a
	dec e
	call Poker_CountSuitInHand
	push af
	ld a, d
	cp 5
	jr z, .flush
	pop af
	dec a
	jr nz, .loop
	scf
	ret
	
.flush
	pop af
	and a
	ret
	
Poker_CountValueInHand:
	; counts the number of cards of value e in hand, returns in d
	push af
	push hl
	ld d, 0
	ld hl, wPokerHand
	ld a, 5
.loop
	push af
	ldi a, [hl]
	call Poker_ConvertCardToBC
	ld a, e
	cp c
	jr nz, .skip
	inc d
.skip
	pop af
	dec a
	jr nz, .loop
	pop hl
	pop af
	ret
	
Poker_CountSuitInHand:
	; counts the number of cards of suit e in hand, returns in d
	push af
	ld d, 0
	ld hl, wPokerHand
	ld a, 5
.loop
	push af
	ldi a, [hl]
	call Poker_ConvertCardToBC
	ld a, e
	cp b
	jr nz, .skip
	inc d
.skip
	pop af
	dec a
	jr nz, .loop
	pop af
	ret
	
Poker_CheckStraightInHand:
	; checks if there is a straight in the hand
	; if there is, returns lowest value card in d
	; otherwise, returns carry
	; ace is high
	ld l, 9
.loop1
	ld h, 5
	ld e, l
.loop2
	call Poker_CountValueInHand
	dec d
	jr nz, .not_a_straight
	inc e
	dec h
	jr nz, .loop2
	ld d, l
	xor a
	ret
	
.not_a_straight
	dec l
	jr nz, .loop1
	xor a
	scf
	ret
	
Poker_CopyCheckNofaKindToBuffer:
	ld hl, wPokerCheckBuffer
	ld a, 1
.loop
	ld e, a
	call Poker_CountValueInHand
	ld [hl], d
	inc hl
	inc a
	cp 14
	jr c, .loop
	ret
	
Poker_CheckNofaKind:
	; checks for pair, 2pair, 3oakind, 4oakind, full house
	; returns c for none, returns POKER_* in d if one is found
	; call Poker_CopyCheckNofaKindToBuffer first
	xor a
	ld [wPokerCheckPair], a
	ld hl, wPokerCheckBuffer
	ld b, 13
.loop
	ldi a, [hl]
	cp 4
	jr z, .found_4ofakind
	cp 3
	call z, .check_full_house
	cp 2
	call z, .found_pair
	dec b
	jr nz, .loop
	ld a, [wPokerCheckPair]
	and a
	jr z, .no_pair
	ld d, POKER_PAIR
	xor a
	ret
	
.no_pair
	xor a
	scf
	ret
	
.check_full_house
	push bc
	push hl
	ld b, 13
	ld hl, wPokerCheckBuffer
.full_house_loop
	ldi a, [hl]
	cp 2
	jr z, .found_full_house
	dec b
	jr nz, .full_house_loop
	pop hl
	pop bc
	pop hl
	ld d, POKER_3_OF_A_KIND
	xor a
	ret
	
.found_full_house
	pop hl
	pop bc
	pop hl
	ld d, POKER_FULL_HOUSE
	xor a
	ret
	
.found_pair
	ld a, [wPokerCheckPair]
	and a
	jr nz, .found_2pair
	ld a, 1
	ld [wPokerCheckPair], a
	ret
	
.found_2pair
	pop hl
	ld d, POKER_2_PAIR
	xor a
	ret
	
.found_4ofakind
	ld d, POKER_4_OF_A_KIND
	xor a
	ret
	
Poker_GiveResult:
	call Poker_DrawCoinCounts
	call WaitSFX
	ld a, [wPokerResult]
	and a
	jr z, .none
	dec a
	jr z, .pair
	dec a
	jr z, .twopair
	dec a
	jr z, .threeofakind
	dec a
	jr z, .straight
	dec a
	jr z, .flush
	dec a
	jr z, .fullhouse
	dec a
	jr z, .fourofakind
	dec a
	jr z, .straightflush
	ld de, SFX_DEX_FANFARE_230_PLUS
	jr .finish

.straightflush
	ld de, SFX_DEX_FANFARE_200_229
	jr .finish
	
.fourofakind
	ld de, SFX_DEX_FANFARE_170_199
	jr .finish
	
.fullhouse
	ld de, SFX_DEX_FANFARE_140_169
	jr .finish
	
.flush
	ld de, SFX_DEX_FANFARE_140_169
	jr .finish
	
.straight
	ld de, SFX_DEX_FANFARE_140_169
	jr .finish
	
.threeofakind
	ld de, SFX_CAUGHT_MON
	jr .finish
	
.twopair
	ld de, SFX_CAUGHT_MON
	jr .finish
	
.pair
	ld de, SFX_LEVEL_UP
	jr .finish
	
.none
	ld de, SFX_WRONG
.finish
	call PlaySFX
	call WaitSFX
	ret
	
Poker_GiveCoins:
	ld hl, wPokerPayout
	ldi a, [hl]
	ld e, [hl]
	ld d, a
	or e
	jr z, .skip
	ld a, d
	and a
	ld c, 2
	jr z, .low_payout
	ld c, 1
.low_payout
.loop
	ld a, [wPokerPayout]
	ld h, a
	ld a, [wPokerPayout + 1]
	ld l, a
	dec hl
	ld a, h
	ld [wPokerPayout], a
	ld a, l
	ld [wPokerPayout + 1], a
	call .is_full
	jr c, .full
	call .add_coin
.full
	push de
	push bc
	push bc
	call Poker_DrawCoinCounts
	pop bc
	call DelayFrames
	pop bc
	pop de
	dec de
	ld a, d
	or e
	jr nz, .loop
.skip
	ret
	
.add_coin:
	ld a, [wCoins]
	ld h, a
	ld a, [wCoins + 1]
	ld l, a
	inc hl
	ld a, h
	ld [wCoins], a
	ld a, l
	ld [wCoins + 1], a
	ld a, c
	dec a
	jr nz, .cont
	ld a, [wCoins + 1]
	and 1
	jr z, .skip_sfx
.cont
	push de
	ld de, SFX_PAY_DAY
	call PlaySFX
	pop de
.skip_sfx
	ret
	
.is_full:
	ld a, [wCoins]
	cp HIGH(MAX_COINS)
	jr c, .less
	jr z, .check_low
	jr .more
	
.check_low
	ld a, [wCoins + 1]
	cp LOW(MAX_COINS)
	jr c, .less
.more
	scf
	ret
	
.less
	and a
	ret
	
Poker_DoubleOrNothing:
	ld a, [wPokerDoubleUpChallenger]
	ld [wPokerDoubleUpCard], a
	call Poker_DrawCardFromDeck
	ld a, d
	ld [wPokerDoubleUpChallenger], a
	call Poker_ClearBackground
	hlcoord 5, 5
	ld a, [wPokerDoubleUpCard]
	call Poker_DrawCardFromA
	hlcoord 11, 5
	call Poker_DrawFacedown
	hlcoord 0, 12
	lb bc, 4, SCREEN_WIDTH - 2
	call TextBox
	ld hl, .double_or_nothing_text
	call PrintText
	ld hl, .double_up_menu_header
	call LoadMenuHeader
	call VerticalMenu
	call CloseWindow
	ld a, [wMenuCursorY]
	dec a
	push af
	call .dramatic_reveal
	hlcoord 0, 12
	lb bc, 4, SCREEN_WIDTH - 2
	call TextBox
	pop af
	jr z, .left
	ld a, [wPokerDoubleUpChallenger]
	and $0f
	ld b, a
	ld a, [wPokerDoubleUpCard]
	and $0f
	jr .check
.left
	ld a, [wPokerDoubleUpCard]
	and $0f
	ld b, a
	ld a, [wPokerDoubleUpChallenger]
	and $0f
.check
	cp b
	jr z, .equal
	jr c, .correct
.wrong
	call .zero_payout
	call WaitSFX
	ld de, SFX_WRONG
	call PlaySFX
	ld hl, .wrong_text
	jr .finish
.equal
	call WaitSFX
	ld hl, .same_text
	ld de, SFX_LEVEL_UP
	call PlaySFX
	jr .finish
.correct
	call .double_payout
	
	call WaitSFX
	ld de, SFX_3RD_PLACE
	ld hl, .correct_text
	call PlaySFX
.finish
	call PrintText
	call WaitSFX
	call Poker_DrawCoinCounts
	ld hl, wPokerPayout
	ldi a, [hl]
	or [hl]
	jr nz, .can_continue
	and a
	ret
	
.can_continue
	scf
	ret
	
.same_text
	text_jump Poker_SameText
	db "@"
	
.wrong_text
	text_jump Poker_WrongText
	db "@"
	
.correct_text
	text_jump Poker_CorrectText
	db "@"
	
.double_up_menu_header:
	db MENU_BACKUP_TILES
	menu_coords 12, 10, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 3
	dw .menu_data
	db 1
	
.menu_data:
	db STATICMENU_CURSOR | STATICMENU_DISABLE_B
	db 2
	db "Left@"
	db "Right@"
	
.double_or_nothing_text
	text_jump Poker_DoubleOrNothingGuessText
	db "@"
	
.dramatic_reveal
	call WaitSFX
	ld de, SFX_CHOOSE_A_CARD
	call PlaySFX
	call WaitSFX
	hlcoord 11, 5
	ld a, [wPokerDoubleUpChallenger]
	call Poker_DrawCardFromA
	ret
	
.zero_payout
	xor a
	ld [wPokerPayout], a
	ld [wPokerPayout + 1], a
	ret
	
.double_payout
	ld hl, wPokerPayout
	ldi a, [hl]
	ld l, [hl]
	ld h, a
	sla l
	rl h
	ld a, h
	cp HIGH(MAX_COINS)
	jr c, .low
	jr z, .check_low
	jr .more
.check_low
	ld a, l
	cp LOW(MAX_COINS)
	jr c, .low
.more
	ld a, HIGH(MAX_COINS)
	ld [wPokerPayout], a
	ld a, LOW(MAX_COINS)
	ld [wPokerPayout + 1], a
	ret
	
.low
	ld a, h
	ld [wPokerPayout], a
	ld a, l
	ld [wPokerPayout + 1], a
	ret
	
Poker_DrawCardFromA:
	call Poker_ConvertCardToBC
Poker_DrawCard:
	; draws card of suit b and value c to hl
	ld de, SCREEN_WIDTH - 4
	ld a, $0e
	add b
	ldi [hl], a
	ld a, $15
	ldi [hl], a
	ldi [hl], a
	inc a
	ldi [hl], a
	add hl, de
	xor a
	ldi [hl], a
	add c
	ldi [hl], a
	ld a, $7f
	ldi [hl], a
	ld a, $13
	ldi [hl], a
	add hl, de
	ld a, b
	swap a
	add $30
	ld b, 3
.row
	ld c, 4
.loop
	ldi [hl], a
	inc a
	dec c
	jr nz, .loop
	add hl, de
	dec b
	jr nz, .row
	ld a, $17
	ldi [hl], a
	inc a
	ldi [hl], a
	ldi [hl], a
	inc a
	ldi [hl], a
	ret
	
Poker_DrawFacedown:
	; draws a card face-down at hl
	ld de, SCREEN_WIDTH - 4
	ld a, $14
	ldi [hl], a
	inc a
	ldi [hl], a
	ldi [hl], a
	inc a
	ldi [hl], a
	add hl, de
	ld b, 4
	ld c, $3c
.loop
	call .check_for_1_2
	ld a, $12
	jr nz, .skip1
	ld a, $1a
.skip1
	ldi [hl], a
	ld a, c
	ldi [hl], a
	inc a
	ldi [hl], a
	add $0f
	ld c, a
	call .check_for_1_2
	ld a, $13
	jr nz, .skip2
	ld a, $1b
.skip2
	ldi [hl], a
	add hl, de
	dec b
	jr nz, .loop
	ld a, $17
	ldi [hl], a
	inc a
	ldi [hl], a
	ldi [hl], a
	inc a
	ldi [hl], a
	ret
	
.check_for_1_2
	ld a, b
	dec a
	or b
	cp 3
	ret
	
Poker_DrawHand:
	ld c, 0
	ld b, 0
	hlcoord 2, 5
	ld a, [wPokerHandDiscard]
	bit 0, a
	push bc
	call .draw
	pop bc
	inc c
	hlcoord 5, 5
	ld a, [wPokerHandDiscard]
	bit 1, a
	push bc
	call .draw
	pop bc
	inc c
	hlcoord 8, 5
	ld a, [wPokerHandDiscard]
	bit 2, a
	push bc
	call .draw
	pop bc
	inc c
	hlcoord 11, 5
	ld a, [wPokerHandDiscard]
	bit 3, a
	push bc
	call .draw
	pop bc
	inc c
	hlcoord 14, 5
	ld a, [wPokerHandDiscard]
	bit 4, a
	push bc
	call .draw
	pop bc
	ret
	
.draw
	jp nz, Poker_DrawFacedown
	push hl
	ld hl, wPokerHand
	add hl, bc
	ld a, [hl]
	pop hl
	jp Poker_DrawCardFromA
	
Poker_DrawHandFancyFaceUp:
	hlcoord 2, 5
	ld a, [wPokerHand]
	call Poker_DrawCardFromA
	call .wait
	hlcoord 5, 5
	ld a, [wPokerHand+1]
	call Poker_DrawCardFromA
	call .wait
	hlcoord 8, 5
	ld a, [wPokerHand+2]
	call Poker_DrawCardFromA
	call .wait
	hlcoord 11, 5
	ld a, [wPokerHand+3]
	call Poker_DrawCardFromA
	call .wait
	hlcoord 14, 5
	ld a, [wPokerHand+4]
	call Poker_DrawCardFromA
	call .wait
	ret
	
.wait
	ld c, 15
	call DelayFrames
	ret
	
Poker_DrawHandFancy:
	hlcoord 2, 5
	call Poker_DrawFacedown
	call .wait
	hlcoord 2, 5
	ld a, [wPokerHand]
	call Poker_DrawCardFromA
	hlcoord 5, 5
	call Poker_DrawFacedown
	call .wait
	hlcoord 5, 5
	ld a, [wPokerHand + 1]
	call Poker_DrawCardFromA
	hlcoord 8, 5
	call Poker_DrawFacedown
	call .wait
	hlcoord 8, 5
	ld a, [wPokerHand + 2]
	call Poker_DrawCardFromA
	hlcoord 11, 5
	call Poker_DrawFacedown
	call .wait
	hlcoord 11, 5
	ld a, [wPokerHand + 3]
	call Poker_DrawCardFromA
	hlcoord 14, 5
	call Poker_DrawFacedown
	call .wait
	hlcoord 14, 5
	ld a, [wPokerHand + 4]
	call Poker_DrawCardFromA
	call .wait
	ret
	
.wait
	ld c, 15
	call DelayFrames
	ret
	
Poker_InitTilemap:
	xor a
	ld [hBGMapMode], a
	call Poker_FillBackground
	call Poker_FillBorder
	hlcoord 0, 12
	lb bc, 4, 18
	call TextBox
	ret
	
Poker_FillBackground:
	hlcoord 0, 0
	ld a, $1c
	ld c, SCREEN_HEIGHT
.next_row
	ld b, SCREEN_WIDTH / 2
.loop_row
	ldi [hl], a
	inc a
	ldi [hl], a
	dec a
	dec b
	jr nz, .loop_row
	dec c
	ret z
	cp a, $1c
	jr z, .add
	add -4
.add
	add 2
	jr .next_row
	
Poker_FillBorder:
	hlcoord 0, 0
	ld a, $20
	ld bc, 20
	call ByteFill
	hlcoord 0, 1
	ld bc, SCREEN_WIDTH - 2
	ld d, 12
.loop
	ldi [hl], a
	add hl, bc
	ldi [hl], a
	dec d
	jr nz, .loop
	hlcoord 5, 0
	ld a, $3e
	ldi [hl], a
	inc a
	ldi [hl], a
	ld a, $4e
	ldi [hl], a
	inc a
	ldi [hl], a
	inc hl
	inc hl
	ld a, $5e
	ldi [hl], a
	inc a
	ldi [hl], a
	ld a, $6e
	ldi [hl], a
	inc a
	ld [hl], a
	ret
	
Poker_ClearBackground:
	hlcoord -1, 2
	ld a, $1d
	ld c, 10
.next_row
	ld b, SCREEN_WIDTH / 2 - 1
	inc hl
	inc hl
.loop_row
	ldi [hl], a
	dec a
	ldi [hl], a
	inc a
	dec b
	jr nz, .loop_row
	dec c
	ret z
	cp a, $1d
	jr z, .add
	add -4
.add
	add 2
	jr .next_row
	
Poker_CheckCardInDeck:
	; checks if card of suit b and value c is in the deck
	; returns z if card is still in the deck
	push bc
	xor a
	or b
	jr z, .suit_0
.add_loop
	add 12
	dec b
	jr nz, .add_loop
.suit_0
	add c
	dec a
	ld b, CHECK_FLAG
	ld hl, wPokerDeck
	ld e, a
	ld d, 0
	call FlagAction
	ld a, c
	and a
	pop bc
	ret
	
Poker_DrawCardFromDeck:
	; tries to draw a card from the deck
	; if the deck is empty, shuffle it, but don't shuffle hand in
	; returns the drawn card in d
	push bc
	call Poker_CheckEmptyDeck
	call c, .Reshuffle
.random_draw_loop
	call Random
	ld d, a
	cp $3d
	jr nc, .random_draw_loop
	and $0f
	cp 13
	jr nc, .random_draw_loop
	inc d
	ld a, d
	call Poker_ConvertCardToBC
	push de
	call Poker_CheckCardInDeck
	pop de
	jr nz, .random_draw_loop
	call .set_card
	pop bc
	ret
	
.Reshuffle
	call Poker_ShuffleDeck
	ld hl, wPokerHand
.hand_loop
	ldi a, [hl]
	and a
	ret z
	call Poker_ConvertCardToBC
	call .set_card
	jr .hand_loop
	
.set_card
	push de
	xor a
	or b
	jr z, .suit_0
.add_loop
	add 12
	dec b
	jr nz, .add_loop
.suit_0
	add c
	dec a
	ld b, SET_FLAG
	push hl
	ld hl, wPokerDeck
	ld e, a
	ld d, 0
	call FlagAction
	pop hl
	pop de
	ret
	
Poker_DrawCardFromDeckToHand:
	; draws a card and puts it into [hl]
	push hl
	call Poker_DrawCardFromDeck
	pop hl
	ld a, d
	ld [hl], a
	ret
	
Poker_CheckEmptyDeck:
	ld b, 4
.suit
	ld c, 13
.val
	dec b
	call Poker_CheckCardInDeck
	ret z
	inc b
	dec c
	jr nz, .val
	dec b
	jr nz, .suit
	scf
	ret
	
Poker_ShuffleDeck:
	; clears all deck flags
	xor a
	ld hl, wPokerDeck
	ld bc, wPokerDeckEnd - wPokerDeck
	call ByteFill
	ret
	
Poker_DrawHandFromDeck:
	xor a
	ld hl, wPokerHand
	ld bc, wPokerHandEnd - wPokerHand
	call ByteFill
	ld b, 5
	ld hl, wPokerHand
.draw_loop
	push hl
	call Poker_DrawCardFromDeck
	pop hl
	ld a, d
	ldi [hl], a
	dec b
	jr nz, .draw_loop
	ret
	
Poker_ConvertCardToBC:
	; takes the card in a and converts it to
	; suit in b, value in c
	push af
	push af
	and $0f
	ld c, a
	pop af
	swap a
	and $03
	ld b, a
	pop af
	ret
	
Poker_GetPals:
	ld a, [hCGB]
	and a
	ret z
	
	hlcoord 0, 0, wAttrMap
	ld bc, SCREEN_HEIGHT * SCREEN_WIDTH
	xor a
	call ByteFill
	
	ld a, [rSVBK]
	push af
	ld a, BANK(wBGPals1)
	ld [rSVBK], a
	ld hl, PokerPalettes
	ld de, wBGPals1
	ld bc, 4 palettes
	call CopyBytes
	pop af
	ld [rSVBK], a
	
	ret
	
Poker_DrawCoinCounts:
	hlcoord 5, 1
	lb bc, 2 | PRINTNUM_LEADINGZEROS, 4
	ld de, wCoins
	call PrintNum
	hlcoord 11, 1
	lb bc, 2 | PRINTNUM_LEADINGZEROS, 4
	ld de, wPokerPayout
	call PrintNum
	ret

PokerPalettes:
INCLUDE "gfx/poker/poker.pal"

PokerTileGFX:
INCBIN "gfx/poker/nums_and_hud.2bpp.lz"

PokerCardGFX:
INCBIN "gfx/poker/cards.2bpp.lz"

PokerCursorGFX:
INCBIN "gfx/poker/cursor.2bpp"
