tilepal: MACRO
; used in gfx/tilesets/*_palette_map.asm
; vram bank, pals
x = \1 << OAM_TILE_BANK
rept (_NARG + -1) / 2
	dn (x | PAL_BG_\3), (x | PAL_BG_\2)
	shift
	shift
endr
ENDM

Tileset0PalMap:
INCLUDE "gfx/tilesets/johto_palette_map.asm"

TilesetPlayersHousePalMap:
INCLUDE "gfx/tilesets/players_house_palette_map.asm"

TilesetPokecenterPalMap:
INCLUDE "gfx/tilesets/pokecenter_palette_map.asm"

TilesetGatePalMap:
INCLUDE "gfx/tilesets/gate_palette_map.asm"

TilesetPortPalMap:
INCLUDE "gfx/tilesets/port_palette_map.asm"

TilesetLabPalMap:
INCLUDE "gfx/tilesets/lab_palette_map.asm"

TilesetMartPalMap:
INCLUDE "gfx/tilesets/mart_palette_map.asm"

TilesetMansionPalMap:
INCLUDE "gfx/tilesets/mansion_palette_map.asm"

TilesetEliteFourRoomPalMap:
INCLUDE "gfx/tilesets/elite_four_room_palette_map.asm"

TilesetTraditionalHousePalMap:
INCLUDE "gfx/tilesets/traditional_house_palette_map.asm"

TilesetLighthousePalMap:
INCLUDE "gfx/tilesets/lighthouse_palette_map.asm"

TilesetPlayersRoomPalMap:
INCLUDE "gfx/tilesets/players_room_palette_map.asm"

TilesetRadioTowerPalMap:
INCLUDE "gfx/tilesets/radio_tower_palette_map.asm"

TilesetForestPalMap:
INCLUDE "gfx/tilesets/forest_palette_map.asm"

TilesetWestCityPalMap:
INCLUDE "gfx/tilesets/west_city_palette_map.asm"

TilesetSilentHillPalMap:
INCLUDE "gfx/tilesets/silent_hill_palette_map.asm"

TilesetOldCityPalMap:
INCLUDE "gfx/tilesets/old_city_palette_map.asm"

TilesetBirdonPalMap:
INCLUDE "gfx/tilesets/birdon_palette_map.asm"

TilesetHighTechPalMap:
INCLUDE "gfx/tilesets/high_tech_palette_map.asm"

TilesetFountTownPalMap:
INCLUDE "gfx/tilesets/fount_town_palette_map.asm"

TilesetSouthCityPalMap:
INCLUDE "gfx/tilesets/south_city_palette_map.asm"

TilesetBlueForestPalMap:
INCLUDE "gfx/tilesets/blue_forest_palette_map.asm"

TilesetRuinsOfAlphPalMap:
INCLUDE "gfx/tilesets/ruins_of_alph_palette_map.asm"

TilesetKantoPalMap:
INCLUDE "gfx/tilesets/kanto_palette_map.asm"
