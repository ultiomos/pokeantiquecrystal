tilecoll: MACRO
; used in data/tilesets/*_collision.asm
	db COLL_\1, COLL_\2, COLL_\3, COLL_\4
ENDM


SECTION "Tileset Data 1", ROMX

Tileset0GFX:
INCBIN "gfx/tilesets/johto.2bpp.lz"

Tileset0Meta:
INCBIN "data/tilesets/johto_metatiles.bin"

Tileset0Coll:
INCLUDE "data/tilesets/johto_collision.asm"

TilesetPlayersRoomGFX:
INCBIN "gfx/tilesets/players_room.2bpp.lz"

TilesetPlayersRoomMeta:
INCBIN "data/tilesets/players_room_metatiles.bin"

TilesetPlayersRoomColl:
INCLUDE "data/tilesets/players_room_collision.asm"

TilesetSilentHillColl:
INCLUDE "data/tilesets/silent_hill_collision.asm"

TilesetSilentHillMeta:
INCBIN "data/tilesets/silent_hill_metatiles.bin"

TilesetSilentHillGFX:
INCBIN "gfx/tilesets/silent_hill.2bpp.lz"

TilesetOldCityColl:
INCLUDE "data/tilesets/old_city_collision.asm"

TilesetOldCityMeta:
INCBIN "data/tilesets/old_city_metatiles.bin"

TilesetOldCityGFX:
INCBIN "gfx/tilesets/old_city.2bpp.lz"


SECTION "Tileset Data 2", ROMX

TilesetBirdonColl:
INCLUDE "data/tilesets/birdon_collision.asm"

TilesetBirdonMeta:
INCBIN "data/tilesets/birdon_metatiles.bin"

TilesetBirdonGFX:
INCBIN "gfx/tilesets/birdon.2bpp.lz"

TilesetPokecenterGFX:
INCBIN "gfx/tilesets/pokecenter.2bpp.lz"

TilesetPokecenterMeta:
INCBIN "data/tilesets/pokecenter_metatiles.bin"

TilesetPokecenterColl:
INCLUDE "data/tilesets/pokecenter_collision.asm"

TilesetPortGFX:
INCBIN "gfx/tilesets/port.2bpp.lz"

TilesetPortMeta:
INCBIN "data/tilesets/port_metatiles.bin"

TilesetPortColl:
INCLUDE "data/tilesets/port_collision.asm"

TilesetPlayersHouseGFX:
INCBIN "gfx/tilesets/players_house.2bpp.lz"

TilesetPlayersHouseMeta:
INCBIN "data/tilesets/players_house_metatiles.bin"

TilesetPlayersHouseColl:
INCLUDE "data/tilesets/players_house_collision.asm"

TilesetMansionGFX:
INCBIN "gfx/tilesets/mansion.2bpp.lz"

TilesetMansionMeta:
INCBIN "data/tilesets/mansion_metatiles.bin"

TilesetMansionColl:
INCLUDE "data/tilesets/mansion_collision.asm"


SECTION "Tileset Data 3", ROMX

TilesetLabGFX:
INCBIN "gfx/tilesets/lab.2bpp.lz"

TilesetLabMeta:
INCBIN "data/tilesets/lab_metatiles.bin"

TilesetLabColl:
INCLUDE "data/tilesets/lab_collision.asm"

TilesetMartGFX:
INCBIN "gfx/tilesets/mart.2bpp.lz"

TilesetMartMeta:
INCBIN "data/tilesets/mart_metatiles.bin"

TilesetMartColl:
INCLUDE "data/tilesets/mart_collision.asm"

TilesetForestMeta:
INCBIN "data/tilesets/forest_metatiles.bin"

TilesetHighTechColl:
INCLUDE "data/tilesets/high_tech_collision.asm"

TilesetHighTechMeta:
INCBIN "data/tilesets/high_tech_metatiles.bin"

TilesetHighTechGFX:
INCBIN "gfx/tilesets/high_tech.2bpp.lz"


SECTION "Tileset Data 4", ROMX

TilesetEliteFourRoomGFX:
INCBIN "gfx/tilesets/elite_four_room.2bpp.lz"

TilesetEliteFourRoomMeta:
INCBIN "data/tilesets/elite_four_room_metatiles.bin"

TilesetEliteFourRoomColl:
INCLUDE "data/tilesets/elite_four_room_collision.asm"

TilesetRadioTowerGFX:
INCBIN "gfx/tilesets/radio_tower.2bpp.lz"

TilesetRadioTowerMeta:
INCBIN "data/tilesets/radio_tower_metatiles.bin"

TilesetRadioTowerColl:
INCLUDE "data/tilesets/radio_tower_collision.asm"


SECTION "Tileset Data 5", ROMX

TilesetGateGFX:
INCBIN "gfx/tilesets/gate.2bpp.lz"

TilesetGateMeta:
INCBIN "data/tilesets/gate_metatiles.bin"

TilesetGateColl:
INCLUDE "data/tilesets/gate_collision.asm"

TilesetTraditionalHouseGFX:
INCBIN "gfx/tilesets/traditional_house.2bpp.lz"

TilesetTraditionalHouseMeta:
INCBIN "data/tilesets/traditional_house_metatiles.bin"

TilesetTraditionalHouseColl:
INCLUDE "data/tilesets/traditional_house_collision.asm"


SECTION "Tileset Data 6", ROMX

TilesetForestGFX:
INCBIN "gfx/tilesets/forest.2bpp.lz"

TilesetLighthouseGFX:
INCBIN "gfx/tilesets/lighthouse.2bpp.lz"

TilesetLighthouseMeta:
INCBIN "data/tilesets/lighthouse_metatiles.bin"

TilesetLighthouseColl:
INCLUDE "data/tilesets/lighthouse_collision.asm"

TilesetForestColl:
INCLUDE "data/tilesets/forest_collision.asm"


SECTION "Tileset Data 7", ROMX

TilesetSouthCityColl:
INCLUDE "data/tilesets/south_city_collision.asm"

TilesetSouthCityMeta:
INCBIN "data/tilesets/south_city_metatiles.bin"

TilesetSouthCityGFX:
INCBIN "gfx/tilesets/south_city.2bpp.lz"

TilesetBlueForestColl:
INCLUDE "data/tilesets/blue_forest_collision.asm"

TilesetBlueForestMeta:
INCBIN "data/tilesets/blue_forest_metatiles.bin"

TilesetBlueForestGFX:
INCBIN "gfx/tilesets/blue_forest.2bpp.lz"

TilesetKantoColl:
INCLUDE "data/tilesets/kanto_collision.asm"

TilesetKantoMeta:
INCBIN "data/tilesets/kanto_metatiles.bin"

TilesetKantoGFX:
INCBIN "gfx/tilesets/kanto.2bpp.lz"


SECTION "Tileset Data 8", ROMX

TilesetWestCityColl:
INCLUDE "data/tilesets/west_city_collision.asm"

TilesetWestCityMeta:
INCBIN "data/tilesets/west_city_metatiles.bin"

TilesetWestCityGFX:
INCBIN "gfx/tilesets/west_city.2bpp.lz"

TilesetFountTownColl:
INCLUDE "data/tilesets/fount_town_collision.asm"

TilesetFountTownMeta:
INCBIN "data/tilesets/fount_town_metatiles.bin"

TilesetFountTownGFX:
INCBIN "gfx/tilesets/fount_town.2bpp.lz"

TilesetRuinsOfAlphColl:
INCLUDE "data/tilesets/ruins_of_alph_collision.asm"

TilesetRuinsOfAlphMeta:
INCBIN "data/tilesets/ruins_of_alph_metatiles.bin"

TilesetRuinsOfAlphGFX:
INCBIN "gfx/tilesets/ruins_of_alph.2bpp.lz"
