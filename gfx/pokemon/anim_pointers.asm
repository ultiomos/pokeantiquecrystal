AnimationPointers:
	dw BulbasaurAnimation
	dw IvysaurAnimation
	dw VenusaurAnimation
	dw CharmanderAnimation
	dw CharmeleonAnimation
	dw CharizardAnimation
	dw SquirtleAnimation
	dw WartortleAnimation
	dw BlastoiseAnimation
	dw CaterpieAnimation
	dw MetapodAnimation
	dw ButterfreeAnimation
	dw WeedleAnimation
	dw KakunaAnimation
	dw BeedrillAnimation
	dw PidgeyAnimation
	dw PidgeottoAnimation
	dw PidgeotAnimation
	dw RattataAnimation
	dw RaticateAnimation
	dw SpearowAnimation
	dw FearowAnimation
	dw EkansAnimation
	dw ArbokAnimation
	dw PikachuAnimation
	dw RaichuAnimation
	dw SandshrewAnimation
	dw SandslashAnimation
	dw RayleepAnimation
	dw BorygonAnimation
	dw HoohooAnimation
	dw SunmolaAnimation
	dw AnchorageAnimation
	dw GurotesuAnimation
	dw ClefairyAnimation
	dw ClefableAnimation
	dw VulpixAnimation
	dw NinetalesAnimation
	dw JigglypuffAnimation
	dw WigglytuffAnimation
	dw ZubatAnimation
	dw GolbatAnimation
	dw OddishAnimation
	dw GloomAnimation
	dw VileplumeAnimation
	dw ParasAnimation
	dw ParasectAnimation
	dw VenonatAnimation
	dw VenomothAnimation
	dw TurbannAnimation
	dw TripstarAnimation
	dw MeowthAnimation
	dw PersianAnimation
	dw PsyduckAnimation
	dw GolduckAnimation
	dw MankeyAnimation
	dw PrimeapeAnimation
	dw GrowlitheAnimation
	dw ArcanineAnimation
	dw PoliwagAnimation
	dw PoliwhirlAnimation
	dw PoliwrathAnimation
	dw AbraAnimation
	dw KadabraAnimation
	dw AlakazamAnimation
	dw MachopAnimation
	dw MachokeAnimation
	dw MachampAnimation
	dw BellsproutAnimation
	dw WeepinbellAnimation
	dw VictreebelAnimation
	dw TentacoolAnimation
	dw TentacruelAnimation
	dw GeodudeAnimation
	dw GravelerAnimation
	dw GolemAnimation
	dw PonytaAnimation
	dw RapidashAnimation
	dw SlowpokeAnimation
	dw SlowbroAnimation
	dw MagnemiteAnimation
	dw MagnetonAnimation
	dw FarfetchDAnimation
	dw DoduoAnimation
	dw DodrioAnimation
	dw KurstrawAnimation
	dw PangshiAnimation
	dw GrimerAnimation
	dw MukAnimation
	dw ShellderAnimation
	dw CloysterAnimation
	dw GastlyAnimation
	dw HaunterAnimation
	dw GengarAnimation
	dw OnixAnimation
	dw DrowzeeAnimation
	dw HypnoAnimation
	dw KrabbyAnimation
	dw KinglerAnimation
	dw VoltorbAnimation
	dw ElectrodeAnimation
	dw ExeggcuteAnimation
	dw ExeggutorAnimation
	dw CuboneAnimation
	dw MarowakAnimation
	dw HitmonleeAnimation
	dw HitmonchanAnimation
	dw LickitungAnimation
	dw KoffingAnimation
	dw WeezingAnimation
	dw RhyhornAnimation
	dw RhydonAnimation
	dw ChanseyAnimation
	dw TangelaAnimation
	dw KangaskhanAnimation
	dw HorseaAnimation
	dw SeadraAnimation
	dw GoldeenAnimation
	dw SeakingAnimation
	dw StaryuAnimation
	dw StarmieAnimation
	dw MrMimeAnimation
	dw ScytherAnimation
	dw JynxAnimation
	dw ElectabuzzAnimation
	dw MagmarAnimation
	dw PinsirAnimation
	dw TaurosAnimation
	dw MagikarpAnimation
	dw GyaradosAnimation
	dw LaprasAnimation
	dw DittoAnimation
	dw EeveeAnimation
	dw VaporeonAnimation
	dw JolteonAnimation
	dw FlareonAnimation
	dw PorygonAnimation
	dw OmanyteAnimation
	dw OmastarAnimation
	dw KabutoAnimation
	dw KabutopsAnimation
	dw AerodactylAnimation
	dw SnorlaxAnimation
	dw ArticunoAnimation
	dw ZapdosAnimation
	dw MoltresAnimation
	dw DratiniAnimation
	dw DragonairAnimation
	dw DragoniteAnimation
	dw MewtwoAnimation
	dw MewAnimation
	dw ChikoritaAnimation
	dw BayleefAnimation
	dw MeganiumAnimation
	dw FlambearAnimation
	dw VolbearAnimation
	dw DynabearAnimation
	dw CruzAnimation
	dw AquaAnimation
	dw AquariaAnimation
	dw TangelAnimation
	dw GalanlaAnimation
	dw HoothootAnimation
	dw NoctowlAnimation
	dw LedybaAnimation
	dw LedianAnimation
	dw SpinarakAnimation
	dw AriadosAnimation
	dw CrobatAnimation
	dw TigretteAnimation
	dw ElectigerAnimation
	dw PichuAnimation
	dw CleffaAnimation
	dw IgglybuffAnimation
	dw TogepiAnimation
	dw TogeticAnimation
	dw NatuAnimation
	dw XatuAnimation
	dw MareepAnimation
	dw FlaaffyAnimation
	dw AmpharosAnimation
	dw BellossomAnimation
	dw MarillAnimation
	dw AzumarillAnimation
	dw BomseelAnimation
	dw PolitoedAnimation
	dw HoppipAnimation
	dw SkiploomAnimation
	dw JumpluffAnimation
	dw AipomAnimation
	dw SunkernAnimation
	dw SunfloraAnimation
	dw LikkAnimation
	dw WooperAnimation
	dw QuagsireAnimation
	dw EspeonAnimation
	dw UmbreonAnimation
	dw MurkrowAnimation
	dw SlowkingAnimation
	dw TwinzAnimation
	dw UnownAnimation
	dw WobbuffetAnimation
	dw GirafarigAnimation
	dw NumpuffAnimation
	dw AnimonAnimation
	dw TrifoxAnimation
	dw PuddiAnimation
	dw SteelixAnimation
	dw SnubbullAnimation
	dw GranbullAnimation
	dw QwilfishAnimation
	dw ScizorAnimation
	dw MadameAnimation
	dw PluxAnimation
	dw SneaselAnimation
	dw GolppyAnimation
	dw ParaAnimation
	dw ChiksAnimation
	dw MeowsyAnimation
	dw MinicornAnimation
	dw GrimeyAnimation
	dw BallerineAnimation
	dw RemoraidAnimation
	dw OctilleryAnimation
	dw DelibirdAnimation
	dw MantineAnimation
	dw SkarmoryAnimation
	dw HoundourAnimation
	dw HoundoomAnimation
	dw KingdraAnimation
	dw PhanpyAnimation
	dw DonphanAnimation
	dw Porygon2Animation
	dw BelmittAnimation
	dw SmeargleAnimation
	dw TyrogueAnimation
	dw HitmontopAnimation
	dw SmoochumAnimation
	dw ElekidAnimation
	dw MagbyAnimation
	dw MiltankAnimation
	dw BlisseyAnimation
	dw RaiAnimation
	dw EnAnimation
	dw SuiAnimation
	dw WolfmanAnimation
	dw WarwolfAnimation
	dw RinringAnimation
	dw BellboyantAnimation
	dw HoOhAnimation
	dw LeafeonAnimation
