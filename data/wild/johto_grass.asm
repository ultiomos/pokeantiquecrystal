; Johto Pokémon in grass

JohtoGrassWildMons: ; 0x2a5e9

	map_id ROUTE_1
	db 10 percent, 10 percent, 10 percent ; encounter rates: morn/day/nite
	; morn
	db 2, MAREEP
	db 3, RATTATA
	db 2, RATTATA
	db 3, HOPPIP
	db 4, PIDGEY
	db 3, PIDGEY
	db 5, HOPPIP
	; day
	db 3, RATTATA
	db 4, PIDGEY
	db 3, HOPPIP
	db 2, HOPPIP
	db 3, MAREEP
	db 2, SPEAROW
	db 3, SPEAROW
	; night
	db 4, RATTATA
	db 5, HOOTHOOT
	db 6, HOOTHOOT
	db 7, RATTATA
	db 5, MARILL
	db 4, MAREEP
	db 6, MAREEP
	
	map_id ROUTE_2
	db 10 percent, 10 percent, 10 percent
	; morn
	db 9, CATERPIE
	db 9, METAPOD
	db 8, CATERPIE
	db 7, METAPOD
	db 10, PIDGEY
	db 7, CATERPIE
	db 8, SPEAROW
	; day
	db 8, CATERPIE
	db 9, PIDGEY
	db 11, RATTATA
	db 9, RATTATA
	db 10, RATTATA
	db 8, CATERPIE
	db 9, METAPOD
	; night
	db 9, RATTATA
	db 10, HOOTHOOT
	db 11, ODDISH
	db 12, HOOTHOOT
	db 11, ODDISH
	db 13, ODDISH
	db 11, HOOTHOOT
	
	map_id ROUTE_12
	db 10 percent, 10 percent, 10 percent
	; morn
	db 19, DITTO
	db 20, FEAROW
	db 18, DITTO
	db 17, METAPOD
	db 21, FEAROW
	db 17, DITTO
	db 16, BUTTERFREE
	; day
	db 17, SPEAROW
	db 18, SPEAROW
	db 21, DODUO
	db 19, DODUO
	db 20, DODUO
	db 18, DITTO
	db 19, DITTO
	; night
	db 19, DROWZEE
	db 20, DROWZEE
	db 21, DROWZEE
	db 22, DROWZEE
	db 23, DROWZEE
	db 23, DROWZEE
	db 24, HYPNO
	
	map_id ROUTE_3
	db 10 percent, 10 percent, 10 percent
	; morn
	db 12, EXEGGCUTE
	db 12, VENONAT
	db 11, EXEGGCUTE
	db 10, VENONAT
	db 13, SPEAROW
	db 10, EXEGGCUTE
	db 11, SPEAROW
	; day
	db 11, SPEAROW
	db 12, SPEAROW
	db 14, GEODUDE
	db 12, GEODUDE
	db 13, GEODUDE
	db 11, EXEGGCUTE
	db 12, EXEGGCUTE
	; night
	db 12, EKANS
	db 13, EKANS
	db 14, EKANS
	db 15, EKANS
	db 16, EKANS
	db 16, EKANS
	db 14, EKANS
	
	map_id ROUTE_10
	db 10 percent, 10 percent, 10 percent
	; morn
	db 16, MACHOP
	db 16, MACHOP
	db 15, MACHOP
	db 14, MACHOP
	db 17, SPEAROW
	db 14, MACHOP
	db 15, SPEAROW
	; day
	db 15, SPEAROW
	db 16, SPEAROW
	db 18, RATTATA
	db 16, RATTATA
	db 17, RATTATA
	db 15, SPEAROW
	db 16, MACHOP
	; night
	db 16, DROWZEE
	db 17, DROWZEE
	db 18, DROWZEE
	db 19, DROWZEE
	db 20, DROWZEE
	db 20, DROWZEE
	db 17, DROWZEE
	
	map_id ROUTE_11
	db 10 percent, 10 percent, 10 percent
	; morn
	db 17, SPEAROW
	db 17, SPEAROW
	db 16, SPEAROW
	db 15, SPEAROW
	db 18, DODUO
	db 15, SPEAROW
	db 16, DODUO
	; day
	db 16, DODUO
	db 17, PHANPY
	db 19, SANDSHREW
	db 17, SANDSHREW
	db 18, SANDSHREW
	db 16, DODUO
	db 17, DODUO
	; night
	db 17, DODUO
	db 18, DODUO
	db 19, DODUO
	db 20, DODUO
	db 21, SNUBBULL
	db 21, SNUBBULL
	db 20, SNUBBULL
	
	map_id ROUTE_13
	db 10 percent, 10 percent, 10 percent
	; morn
	db 21, SPEAROW
	db 21, SPEAROW
	db 20, FEAROW
	db 19, FEAROW
	db 22, FEAROW
	db 19, SPEAROW
	db 20, FEAROW
	; day
	db 20, DODUO
	db 22, SANDSLASH
	db 21, PHANPY
	db 21, SANDSHREW
	db 23, SANDSHREW
	db 20, DODUO
	db 23, DONPHAN
	; night
	db 21, DODUO
	db 22, DODUO
	db 23, DODUO
	db 24, DODUO
	db 25, SNUBBULL
	db 25, SNUBBULL
	db 24, SNUBBULL
	
	map_id ROUTE_14
	db 10 percent, 10 percent, 10 percent
	; morn
	db 23, HOPPIP
	db 23, SKIPLOOM
	db 22, HOPPIP
	db 25, SKIPLOOM
	db 24, SANDSHREW
	db 21, HOPPIP
	db 23, SANDSHREW
	; day
	db 22, SANDSHREW
	db 23, SANDSLASH
	db 25, VULPIX
	db 23, VULPIX
	db 21, TRIFOX
	db 22, HOPPIP
	db 20, TRIFOX
	; night
	db 23, HOOTHOOT
	db 24, HOOTHOOT
	db 25, HOOTHOOT
	db 26, HOOTHOOT
	db 27, SNUBBULL
	db 27, SNUBBULL
	db 26, SNUBBULL
	
	map_id ROUTE_15
	db 10 percent, 10 percent, 10 percent
	; morn
	db 24, HOPPIP
	db 24, SKIPLOOM
	db 23, HOPPIP
	db 22, SKIPLOOM
	db 25, SKIPLOOM
	db 22, HOPPIP
	db 22, HOPPIP
	; day
	db 23, PIDGEY
	db 24, PIDGEOTTO
	db 26, VULPIX
	db 24, VULPIX
	db 25, VULPIX
	db 22, HOPPIP
	db 23, VULPIX
	; night
	db 24, HOOTHOOT
	db 25, HOOTHOOT
	db 26, DROWZEE
	db 27, DROWZEE
	db 28, HYPNO
	db 28, HYPNO
	db 27, HYPNO
	
	db -1 ; end
