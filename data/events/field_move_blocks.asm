CutTreeBlockPointers: ; c862
; tileset, block list pointer
	dbw TILESET_SILENT_HILL,  .common
	dbw TILESET_WEST_CITY,    .common
	dbw TILESET_OLD_CITY,     .old
	dbw TILESET_FOREST,       .forest
	dbw TILESET_BLUE_FOREST,  .common
	dbw TILESET_BIRDON,       .common
	dbw TILESET_FOUNT_TOWN,   .common
	dbw TILESET_HIGH_TECH,    .common
	dbw TILESET_SOUTH_CITY,   .south
	dbw TILESET_KANTO,        .kanto
	db -1 ; end
	
.old:
	db $6c, $74, 0
	db $6d, $75, 0
.south:
	db $43, $69, 0
.common:
	db $3b, $04, 1
	db $30, $25, 0
	db $31, $2a, 0
	db $32, $34, 0
	db $33, $35, 0
	db -1
	
.kanto:
	db $3b, $04, 1
	db $24, $51, 0
	db $2c, $59, 0
	db $2e, $56, 0
	db $30, $5d, 0
	db $31, $54, 0
	db -1
	
	
.forest:
; facing block, replacement block, animation
	db $0f, $17, 0
	db $3b, $01, 1
	db -1 ; end


WhirlpoolBlockPointers: ; c8a4
	dbw TILESET_BLUE_FOREST, .blueforest
	dbw TILESET_HIGH_TECH, .hightech
	db -1 ; end

.blueforest:
; facing block, replacement block, animation
	db $5b, $21, 0
	db $5f, $21, 0
	db $65, $21, 0
	db $6f, $21, 0
	db -1 ; end
	
.hightech:
	db $68, $21, 0
	db $72, $21, 0
	db -1 ; end
