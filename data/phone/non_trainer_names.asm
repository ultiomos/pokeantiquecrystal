NonTrainerCallerNames: ; 903d6
; entries correspond to PHONECONTACT_* constants
	dw .none
	dw .mom
	dw .bill
	dw .oak
	dw .green

.none:     db "----------@"         ;   |  length limit  | "---@"
.mom:      db "<MOM>:<LNBRK>",          "   Mother@"
.bill:     db "Bill:<LNBRK>",           "   #maniac@"
.oak:      db "Samuel Oak:<LNBRK>",     "   <PKMN> Professor@"
.green:    db "<GREEN> Oak:<LNBRK>",    "   <PKMN> Researcher@"
; 90423
