HealingHPAmounts: ; f3af
	dbw FRESH_WATER,   50
	dbw SODA_POP,      60
	dbw LEMONADE,      80
	dbw HYPER_POTION, 200
	dbw SUPER_POTION,  50
	dbw POTION,        20
	dbw MAX_POTION,   MAX_STAT_VALUE
	dbw FULL_RESTORE, MAX_STAT_VALUE
	dbw BERRY,         10
	dbw APPLE,         30
	dbw -1, 0 ; end
; f3df
