item_attribute: MACRO
; price, held effect, parameter, property, pocket, field menu, battle menu
	dw \1
	db \2, \3, \4, \5
	dn \6, \7
ENDM

ItemAttributes: ; 67c1
; entries correspond to item ids
; MASTER_BALL
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; ULTRA_BALL
	item_attribute 1200, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; ITEM_03
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; GREAT_BALL
	item_attribute 600, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; POKE_BALL
	item_attribute 200, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; TOWN_MAP
	item_attribute 0, HELD_NONE, 0, CANT_TOSS, KEY_ITEM, ITEMMENU_CLOSE, ITEMMENU_NOUSE
; BICYCLE
	item_attribute 0, HELD_NONE, 0, CANT_TOSS, KEY_ITEM, ITEMMENU_CLOSE, ITEMMENU_NOUSE
; MOON_STONE
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; ANTIDOTE
	item_attribute 100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; BURN_HEAL
	item_attribute 250, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; ICE_HEAL
	item_attribute 250, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; AWAKENING
	item_attribute 250, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; PARLYZ_HEAL
	item_attribute 200, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; FULL_RESTORE
	item_attribute 3000, HELD_NONE, -1, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; MAX_POTION
	item_attribute 2500, HELD_NONE, -1, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; HYPER_POTION
	item_attribute 1200, HELD_NONE, 200, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; SUPER_POTION
	item_attribute 700, HELD_NONE, 50, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; POTION
	item_attribute 300, HELD_NONE, 20, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; ESCAPE_ROPE
	item_attribute 550, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_NOUSE
; REPEL
	item_attribute 350, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CURRENT, ITEMMENU_NOUSE
; MAX_ELIXER
	item_attribute 4500, HELD_NONE, -1, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; FIRE_STONE
	item_attribute 2100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; THUNDERSTONE
	item_attribute 2100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; WATER_STONE
	item_attribute 2100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; ITEM_19
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; HP_UP
	item_attribute 9800, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; PROTEIN
	item_attribute 9800, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; IRON
	item_attribute 9800, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; CARBOS
	item_attribute 9800, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; ITEM_1E
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; CALCIUM
	item_attribute 9800, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; RARE_CANDY
	item_attribute 4800, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; X_ACCURACY
	item_attribute 950, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; LEAF_STONE
	item_attribute 2100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; ITEM_23
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; NUGGET
	item_attribute 10000, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; POKE_DOLL
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; FULL_HEAL
	item_attribute 600, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; REVIVE
	item_attribute 1500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; MAX_REVIVE
	item_attribute 4000, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; GUARD_SPEC
	item_attribute 700, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; SUPER_REPEL
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CURRENT, ITEMMENU_NOUSE
; MAX_REPEL
	item_attribute 700, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CURRENT, ITEMMENU_NOUSE
; DIRE_HIT
	item_attribute 650, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; ITEM_2D
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; FRESH_WATER
	item_attribute 200, HELD_NONE, 50, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; SODA_POP
	item_attribute 300, HELD_NONE, 60, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; LEMONADE
	item_attribute 350, HELD_NONE, 80, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; X_ATTACK
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; ITEM_32
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; X_DEFEND
	item_attribute 550, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; X_SPEED
	item_attribute 350, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; X_SPECIAL
	item_attribute 350, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; COIN_CASE
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, KEY_ITEM, ITEMMENU_CURRENT, ITEMMENU_NOUSE
; ITEMFINDER
	item_attribute 0, HELD_NONE, 0, CANT_TOSS, KEY_ITEM, ITEMMENU_CLOSE, ITEMMENU_NOUSE
; POKE_FLUTE
	item_attribute 0, HELD_NONE, 0, CANT_TOSS, KEY_ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; EXP_SHARE
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; OLD_ROD
	item_attribute 0, HELD_NONE, 0, CANT_TOSS, KEY_ITEM, ITEMMENU_CLOSE, ITEMMENU_NOUSE
; GOOD_ROD
	item_attribute 0, HELD_NONE, 0, CANT_TOSS, KEY_ITEM, ITEMMENU_CLOSE, ITEMMENU_NOUSE
; ITEM_3C
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; SUPER_ROD
	item_attribute 0, HELD_NONE, 0, CANT_TOSS, KEY_ITEM, ITEMMENU_CLOSE, ITEMMENU_NOUSE
; PP_UP
	item_attribute 9800, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; ETHER
	item_attribute 1200, HELD_NONE, 10, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; MAX_ETHER
	item_attribute 2000, HELD_NONE, -1, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; ELIXER
	item_attribute 3000, HELD_NONE, 10, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
	
; MYSTIC_PETAL
	item_attribute 100, HELD_GRASS_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; WHITEFEATHER
	item_attribute 100, HELD_FLYING_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; CONFUSE_CLAW
	item_attribute 100, HELD_PREEMPTIVE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; WISDOM_ORB
	item_attribute 100, HELD_SP_DEFENSE_UP, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; STEEL_SHELL
	item_attribute 100, HELD_DEFENSE_UP, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; BLACKGLASSES
	item_attribute 100, HELD_DARK_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ODD_THREAD
	item_attribute 100, HELD_BRIGHTPOWDER, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; BIG_LEAF
	item_attribute 100, HELD_GRASS_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; QUICK_NEEDLE
	item_attribute 100, HELD_QUICK_CLAW, 60, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ITEM_4B
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; SHARP_STONE
	item_attribute 100, HELD_ROCK_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; BLACK_FEATHER
	item_attribute 100, HELD_FLYING_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SHARP_FANG
	item_attribute 100, HELD_NORMAL_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SNAKESKIN
	item_attribute 100, HELD_PREVENT_POISON, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ELEC__POUCH
	item_attribute 100, HELD_ELECTRIC_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; TOXIC_NEEDLE
	item_attribute 100, HELD_POISON_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; KINGS_ROCK
	item_attribute 100, HELD_FLINCH, 30, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; STRANGE_POWER
	item_attribute 100, HELD_STRANGE_POWER, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; LIFE_TAG
	item_attribute 100, HELD_LIFE_TAG, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; METAL_CLAW
	item_attribute 100, HELD_STEEL_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; CORDYCEPS
	item_attribute 10000, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; PRETTY_TAIL
	item_attribute 100, HELD_PREVENT_POISON, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SILVERPOWDER
	item_attribute 100, HELD_BUG_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; DIGGING_CLAW
	item_attribute 100, HELD_CAN_DIG, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ITEM_5A
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; AMULET_COIN
	item_attribute 100, HELD_AMULET_COIN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; MIGRAINESEED
	item_attribute 100, HELD_PSYCHIC_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; COUNTER_CUFF
	item_attribute 100, HELD_COUNTER_CUFF, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; TALISMAN_TAG
	item_attribute 100, HELD_CLEANSE_TAG, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; STRANGEWATER
	item_attribute 100, HELD_WATER_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; TWISTEDSPOON
	item_attribute 100, HELD_PSYCHIC_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ATTACKNEEDLE
	item_attribute 100, HELD_BUG_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; POWER_BRACER
	item_attribute 100, HELD_FIGHTING_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SHARP_HORN
	item_attribute 100, HELD_ATTACK_UP, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ITEM_64
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; BOUNCBALLOON
	item_attribute 100, HELD_NORMAL_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; FIRE_MANE
	item_attribute 100, HELD_FIRE_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SLOWPOKETAIL
	item_attribute 10000, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; EARTH
	item_attribute 100, HELD_PREVENT_PARALYZE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; STICK
	item_attribute 100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; FLEE_FEATHER
	item_attribute 100, HELD_ESCAPE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ICE_FANG
	item_attribute 100, HELD_ICE_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; FOSSIL_SHARD
	item_attribute 100, HELD_GROUND_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; GROSSGARBAGE
	item_attribute 100, HELD_POISON_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; CHAMPIONBELT
	item_attribute 100, HELD_FIGHTING_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; BIG_PEARL
	item_attribute 15000, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; EVERSTONE
	item_attribute 100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SPELL_TAG
	item_attribute 100, HELD_GHOST_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; FIVEYEN_COIN
	item_attribute 100, HELD_GHOST_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; MYTHRIL_CAPE
	item_attribute 100, HELD_STEEL_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; STIMULUS_ORB
	item_attribute 100, HELD_PREVENT_SLEEP, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; CALM_BERRY
	item_attribute 100, HELD_PREVENT_CONFUSE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; THICK_CLUB
	item_attribute 100, HELD_GROUND_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; FOCUS_ORB
	item_attribute 100, HELD_FOCUS_BAND, 30, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ITEM_78
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; DETECT_ORB
	item_attribute 100, HELD_BRIGHTPOWDER, 20, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; LONG_TONGUE
	item_attribute 100, HELD_CATCH_CHANCE, 20, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; LOTTO_TICKET
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; TAG
	item_attribute 100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; HARD_STONE
	item_attribute 100, HELD_ROCK_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; LUCKY_EGG
	item_attribute 100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; LONG_VINE
	item_attribute 100, HELD_NONE, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; MOMS_LOVE
	item_attribute 100, HELD_STEP_RECOVER, 1, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SMOKESCREEN_I
	item_attribute 100, HELD_ESCAPE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; WET_HORN
	item_attribute 100, HELD_WATER_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SKATEBOARD
	item_attribute 0, HELD_NONE, 0, CANT_TOSS, KEY_ITEM, ITEMMENU_CLOSE, ITEMMENU_NOUSE
; CRIMSONJEWEL
	item_attribute 12000, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; INVIS__WALL
	item_attribute 100, HELD_HALF_SPECIAL, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SHARP_SCYTHE
	item_attribute 100, HELD_CRITICAL_UP, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; NIGHTLIGHT
	item_attribute 100, HELD_DARK_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ICE_BIKINI
	item_attribute 100, HELD_ICE_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; THUNDER_FANG
	item_attribute 100, HELD_ELECTRIC_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; FIRE_CLAW
	item_attribute 100, HELD_FIRE_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; TWIN_HORNS
	item_attribute 100, HELD_ATTACK_UP, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; SPIKE
	item_attribute 100, HELD_GHOST_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ITEM_8D
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; BETA_DISK
	item_attribute 100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; METAL_COAT
	item_attribute 100, HELD_HALF_PHYSICAL, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; DRAGON_FANG
	item_attribute 100, HELD_DRAGON_BOOST, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; WATER_TAIL
	item_attribute 100, HELD_PREVENT_BURN, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; LEFTOVERS
	item_attribute 100, HELD_LEFTOVERS, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ICE_WING
	item_attribute 100, HELD_SP_DEFENSE_UP, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; THUNDER_WING
	item_attribute 100, HELD_SPEED_UP, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; FIRE_WING
	item_attribute 100, HELD_SP_ATTACK_UP, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ITEM_96
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; DRAGON_SCALE
	item_attribute 100, HELD_DRAGON_DOWN, 10, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; BERSERK_GENE
	item_attribute 100, HELD_STAT_BOOST_10, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; HEART_STONE
	item_attribute 2100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; FIRE_TAIL
	item_attribute 100, HELD_PREVENT_FREEZE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; THUNDER_TAIL
	item_attribute 100, HELD_PREVENT_PARALYZE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SACRED_ASH
	item_attribute 200, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_NOUSE
; HEAVY_BALL
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; ITEM_MAIL
	item_attribute 50, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; LEVEL_BALL
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; LURE_BALL
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; FAST_BALL
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; POISON_STONE
	item_attribute 2100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; LIGHT_BALL
	item_attribute 100, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; FRIEND_BALL
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; MOON_BALL
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; LOVE_BALL
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, BALL, ITEMMENU_NOUSE, ITEMMENU_CLOSE
; ITEM_A7
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; ITEM_A8
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; ITEM_A9
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; EXPLOSIVES
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, KEY_ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; SHIP_TICKET
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, KEY_ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; UP_GRADE
	item_attribute 100, HELD_STAT_BOOST_5, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; BERRY
	item_attribute 10, HELD_BERRY, 10, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; APPLE
	item_attribute 10, HELD_BERRY, 30, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; CHERI_BERRY
	item_attribute 10, HELD_HEAL_PARALYZE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; CHESTO_BERRY
	item_attribute 10, HELD_HEAL_SLEEP, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; PECHA_BERRY
	item_attribute 10, HELD_HEAL_POISON, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; RAWST_BERRY
	item_attribute 10, HELD_HEAL_BURN, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; ASPEAR_BERRY
	item_attribute 10, HELD_HEAL_FREEZE, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; LEPPA_BERRY
	item_attribute 10, HELD_RESTORE_PP, -1, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; PERSIM_BERRY
	item_attribute 10, HELD_HEAL_CONFUSION, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_PARTY
; LUM_BERRY
	item_attribute 10, HELD_HEAL_STATUS, 0, CANT_SELECT, ITEM, ITEMMENU_PARTY, ITEMMENU_PARTY
; BLK_APRICORN
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; RED_APRICORN
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; BLU_APRICORN
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; GRN_APRICORN
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; YLW_APRICORN
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; WHT_APRICORN
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; PNK_APRICORN
	item_attribute 500, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; ITEM_BE
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; TM01
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM02
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM03
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM04
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; ITEM_C3
	item_attribute $9999, HELD_NONE, 0, NO_LIMITS, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; TM05
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM06
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM07
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM08
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM09
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM10
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM11
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM12
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM13
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM14
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM15
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM16
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM17
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM18
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM19
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM20
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM21
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM22
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM23
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM24
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM25
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM26
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM27
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM28
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; ITEM_DC
	item_attribute $9999, HELD_NONE, 0, NO_LIMITS, ITEM, ITEMMENU_NOUSE, ITEMMENU_NOUSE
; TM29
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM30
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM31
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM32
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM33
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM34
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM35
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM36
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM37
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM38
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM39
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM40
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM41
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM42
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM43
	item_attribute 1000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM44
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM45
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM46
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM47
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM48
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM49
	item_attribute 3000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; TM50
	item_attribute 2000, HELD_NONE, 0, CANT_SELECT, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; HM01
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; HM02
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; HM03
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; HM04
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; HM05
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; HM06
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; HM07
	item_attribute 0, HELD_NONE, 0, CANT_SELECT | CANT_TOSS, TM_HM, ITEMMENU_PARTY, ITEMMENU_NOUSE
; ITEM_FA
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; $fb
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; $fc
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; $fd
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; $fe
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; $ff
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
; $00
	item_attribute 0, HELD_NONE, 0, CANT_SELECT, ITEM, ITEMMENU_CLOSE, ITEMMENU_CLOSE
