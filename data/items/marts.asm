Marts: ; 160a9
; entries correspond to MART_* constants
	dw MartIndigoPlateau
	dw DefaultMart
.End
; 160ed

DefaultMart:
MartIndigoPlateau:
	db 7 ; # items
	db ULTRA_BALL
	db MAX_REPEL
	db HYPER_POTION
	db MAX_POTION
	db FULL_RESTORE
	db REVIVE
	db FULL_HEAL
	db -1 ; end
