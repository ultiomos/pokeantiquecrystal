; Pokémon traded from RBY do not have held items, so GSC usually interprets the
; catch rate as an item. However, if the catch rate appears in this table, the
; item associated with the table entry is used instead.

TimeCapsule_CatchRateItems: ; 28785
	db ITEM_03, RARE_CANDY
	db ITEM_19, LEFTOVERS
	db ITEM_1E, LUCKY_EGG
	db ITEM_23, METAL_COAT
	db ITEM_2D, APPLE
	db ITEM_32, APPLE
	db ITEM_3C, APPLE
	db ITEM_4B, APPLE
	db ITEM_5A, BERRY
	db ITEM_64, BERRY
	db ITEM_78, BERRY
	db ITEM_96, MOON_STONE
	db EXPLOSIVES, MOON_STONE
	db ITEM_BE, BERRY
	db ITEM_FA, BERRY
	db ITEM_FB, BERRY
	db ITEM_FC, BERRY
	db ITEM_FD, BERRY
	db ITEM_FE, BERRY
	db ITEM_FF, BERRY
	db 0 ; end
; 2879e
