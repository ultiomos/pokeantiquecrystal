ItemNames::
	db "Master Ball@"
	db "Ultra Ball@"
	db "?@" ; Bright Powder
	db "Great Ball@"
	db "# Ball@"
	db "Town Map@" ; Teru-Sama
	db "Bicycle@"
	db "Moon Stone@"
	db "Antidote@"
	db "Burn Heal@"
	db "Ice Heal@"
	db "Awakening@"
	db "Parlyz Heal@"
	db "Full Restore@"
	db "Max Potion@"
	db "Hyper Potion@"
	db "Super Potion@"
	db "Potion@"
	db "Escape Rope@"
	db "Repel@"
	db "Max Elixir@"
	db "Fire Stone@"
	db "Thunderstone@"
	db "Water Stone@"
	db "?@"
	db "HP Up@"
	db "Protein@"
	db "Iron@"
	db "Carbos@"
	db "?@" ; Lucky Punch
	db "Calcium@"
	db "Rare Candy@"
	db "X Accuracy@"
	db "Leaf Stone@"
	db "?@" ; Metal Powder
	db "Nugget@"
	db "# Doll@"
	db "Full Heal@"
	db "Revive@"
	db "Max Revive@"
	db "Guard Spec.@"
	db "Super Repel@"
	db "Max Repel@"
	db "Dire Hit@"
	db "?@"
	db "Fresh Water@"
	db "Soda Pop@"
	db "Lemonade@"
	db "X Attack@"
	db "?@"
	db "X Defend@"
	db "X Speed@"
	db "X Special@"
	db "Coin Case@"
	db "Itemfinder@"
	db "# Flute@" ; Teru-Sama
	db "Exp. Share@"
	db "Old Rod@"
	db "Good Rod@"
	db "?@" ; Silver Leaf
	db "Super Rod@"
	db "PP Up@"
	db "Ether@"
	db "Max Ether@"
	db "Elixir@"
	db "Mystic Petal@" ; Red Scale
	db "WhiteFeather@" ; Secret Potion
	db "Confuse Claw@" ; S.S. Ticket
	db "Wisdom Orb@" ; Mystery Egg
	db "Steel Shell@" ; Clear Bell
	db "Blackglasses@" ; Silver Wing
	db "Odd Thread@" ; Moomoo Milk
	db "Big Leaf@" ; Quick Claw
	db "Quick Needle@" ; Pecha Berry
	db "?@" ; Gold Leaf
	db "Sharp Stone@" ; Soft Sand
	db "BlackFeather@" ; Sharp Beak
	db "Sharp Fang@" ; Cheri Berry
	db "Snakeskin@" ; Aspear Berry
	db "Elec. Pouch@" ; Rawst Berry
	db "Toxic Needle@" ; Poison Barb
	db "King's Rock@"
	db "StrangePower@" ; Persim Berry
	db "Life Tag@" ; Chesto Berry
	db "Metal Claw@" ; Red Apricorn
	db "Cordyceps@" ; Tiny Mushroom
	db "Pretty Tail@" ; Big Mushroom
	db "SilverPowder@"
	db "Digging Claw@" ; Blue Apricorn
	db "?@"
	db "Amulet Coin@"
	db "MigraineSeed@" ; Yellow Apricorn
	db "Counter Cuff@" ; Green Apricorn
	db "Talisman Tag@" ; Cleanse Tag
	db "StrangeWater@" ; Mystic Water
	db "TwistedSpoon@"
	db "AttackNeedle@" ; White Apricorn
	db "Power Bracer@" ; Blackbelt
	db "Sharp Horn@" ; Black Apricorn
	db "?@"
	db "BouncBalloon@" ; Pink Apricorn
	db "Fire Mane@" ; Black Glasses
	db "SlowpokeTail@"
	db "Earth@" ; Pink Bow
	db "Stick@"
	db "Flee Feather@" ; Smoke Ball
	db "Ice Fang@" ; Never-melt-ice
	db "Fossil Shard@" ; Magnet
	db "GrossGarbage@" ; Lum Berry
	db "ChampionBelt@" ; Pearl
	db "Big Pearl@"
	db "Everstone@"
	db "Spell Tag@"
	db "5 Yen Coin@" ; Cake of Rage
	db "Mythril Cape@" ; GS Ball
	db "Stimulus Orb@" ; Blue Card
	db "Calm Berry@" ; Miracle Seed
	db "Thick Club@"
	db "Focus Orb@" ; Focus Band
	db "?@"
	db "Detect Orb@" ; Energy Powder
	db "Long Tongue@" ; Energy Root
	db "Lotto Ticket@" ; Heal Powder
	db "Tag@" ; Revival Herb
	db "Hard Stone@"
	db "Lucky Egg@"
	db "Long Vine@" ; Card Key
	db "Mom's Love@" ; Machine Part
	db "Smokescreen@" ; Egg Ticket
	db "Wet Horn@" ; Lost Item
	db "Skateboard@" ; Stardust
	db "CrimsonJewel@" ; Star Piece
	db "Invis. Wall@" ; Basement Key
	db "Sharp Scythe@" ; Pass
	db "Nightlight@" ; Teru-Sama
	db "Ice Bikini@" ; Teru-Sama
	db "Thunder Fang@" ; Teru-Sama
	db "Fire Claw@" ; Charcoal
	db "Twin Horns@" ; Berry Juice
	db "Spike@" ; Scope Lens
	db "?@"
	db "Beta Disk@" ; Teru-Sama
	db "Metal Coat@"
	db "Dragon Fang@"
	db "Water Tail@" ; Teru-Sama
	db "Leftovers@"
	db "Ice Wing@" ; Teru-Sama
	db "Thunder Wing@" ; Teru-Sama
	db "Fire Wing@" ; Teru-Sama
	db "?@" ; Leppa Berry
	db "Dragon Scale@"
	db "Berserk Gene@"
	db "Heart Stone@" ; Teru-Sama
	db "Fire Tail@" ; Teru-Sama
	db "Thunder Tail@" ; Teru-Sama
	db "Sacred Ash@"
	db "Heavy Ball@" ; Heavy Ball / TM Holder
	db "Mail@" ; Flower Mail
	db "Level Ball@" ; Level Ball / Ball Holder
	db "Lure Ball@" ; Lure Ball / Item Bag
	db "Fast Ball@" ; Fast Ball / Key Item Bag
	db "Poison Stone@" ; Teru-Sama
	db "Light Ball@"
	db "Friend Ball@" ; Friend Ball / ?
	db "Moon Ball@" ; Moon Ball / ?
	db "Love Ball@" ; Love Ball / ?
	db "?@" ; Normal Box
	db "?@" ; Gorgeous Box
	db "?@" ; Sun Stone
	db "Explosives@" ; Polkadot Pow
	db "Ship Ticket@" ; Teru-Sama
	db "Up-Grade@"
	db "Berry@"
	db "Apple@" ; Sitrus Berry
	db "Cheri Berry@" ; Squirt Bottle
	db "Chesto Berry@" ; Teru-Sama
	db "Pecha Berry@" ; Park Ball
	db "Rawst Berry@" ; Rainbow Wing
	db "Aspear Berry@" ; Teru-Sama
	db "Leppa Berry@" ; Brick Piece
	db "Persim Berry@" ; Surf Mail
	db "Lum Berry@" ; Light Blue Mail
	db "Blk Apricorn@" ; Portrait Mail
	db "Red Apricorn@" ; Lovely Mail
	db "Blu Apricorn@" ; Eon Mail
	db "Grn Apricorn@" ; Morph Mail
	db "Ylw Apricorn@" ; Blue Sky Mail
	db "Wht Apricorn@" ; Music Mail
	db "Pnk Apricorn@" ; Mirage Mail
	db "?@"
	db "TM01@"
	db "TM02@"
	db "TM03@"
	db "TM04@"
	db "?@"
	db "TM05@"
	db "TM06@"
	db "TM07@"
	db "TM08@"
	db "TM09@"
	db "TM10@"
	db "TM11@"
	db "TM12@"
	db "TM13@"
	db "TM14@"
	db "TM15@"
	db "TM16@"
	db "TM17@"
	db "TM18@"
	db "TM19@"
	db "TM20@"
	db "TM21@"
	db "TM22@"
	db "TM23@"
	db "TM24@"
	db "TM25@"
	db "TM26@"
	db "TM27@"
	db "TM28@"
	db "?@"
	db "TM29@"
	db "TM30@"
	db "TM31@"
	db "TM32@"
	db "TM33@"
	db "TM34@"
	db "TM35@"
	db "TM36@"
	db "TM37@"
	db "TM38@"
	db "TM39@"
	db "TM40@"
	db "TM41@"
	db "TM42@"
	db "TM43@"
	db "TM44@"
	db "TM45@"
	db "TM46@"
	db "TM47@"
	db "TM48@"
	db "TM49@"
	db "TM50@"
	db "HM01@"
	db "HM02@"
	db "HM03@"
	db "HM04@"
	db "HM05@"
	db "HM06@"
	db "HM07@"
	db "?@"
	db "?@"
	db "?@"
	db "?@"
	db "?@"
	db "?@"
	db "?@"
