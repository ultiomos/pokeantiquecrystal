INCLUDE "constants.asm"


SECTION "Evolutions and Attacks", ROMX


INCLUDE "data/pokemon/evos_attacks_pointers.asm"


EvosAttacks::
; Evos+attacks data structure:
; - Evolution methods:
;    * db EVOLVE_LEVEL, level, species
;    * db EVOLVE_ITEM, used item, species
;    * db EVOLVE_TRADE, held item (or -1 for none), species
;    * db EVOLVE_HAPPINESS, TR_* constant (ANYTIME, MORNDAY, NITE), species
;    * db EVOLVE_STAT, level, ATK_*_DEF constant (LT, GT, EQ), species
; - db 0 ; no more evolutions
; - Learnset (in increasing level order):
;    * db level, move
; - db 0 ; no more level-up moves


BulbasaurEvosAttacks:
	db EVOLVE_LEVEL, 16, IVYSAUR
	db 0 ; no more evolutions
	db 1, TACKLE
	db 2, GROWL
	db 4, STUN_SPORE
	db 7, LEECH_SEED
	db 11, VINE_WHIP
	db 16, POISONPOWDER
	db 22, GROWTH
	db 29, RAZOR_LEAF
	db 37, SLEEP_POWDER
	db 46, SYNTHESIS
	db 56, SOLARBEAM
	db 0 ; no more level-up moves

IvysaurEvosAttacks:
	db EVOLVE_LEVEL, 32, VENUSAUR
	db 0 ; no more evolutions
	db 1, TACKLE
	db 3, GROWL
	db 6, STUN_SPORE
	db 10, LEECH_SEED
	db 15, VINE_WHIP
	db 21, POISONPOWDER
	db 28, GROWTH
	db 36, RAZOR_LEAF
	db 45, SLEEP_POWDER
	db 55, SYNTHESIS
	db 66, SOLARBEAM
	db 0 ; no more level-up moves

VenusaurEvosAttacks:
	db 0 ; no more evolutions
	db 1, TACKLE
	db 4, GROWL
	db 8, STUN_SPORE
	db 13, LEECH_SEED
	db 19, VINE_WHIP
	db 26, POISONPOWDER
	db 34, GROWTH
	db 43, RAZOR_LEAF
	db 53, SLEEP_POWDER
	db 64, SYNTHESIS
	db 76, SOLARBEAM
	db 0 ; no more level-up moves

CharmanderEvosAttacks:
	db EVOLVE_LEVEL, 16, CHARMELEON
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 3, GROWL
	db 6, LEER
	db 10, EMBER
	db 15, RAGE
	db 21, FIRE_SPIN
	db 28, SLASH
	db 36, SCARY_FACE
	db 45, AGILITY
	db 55, FLAMETHROWER
	db 0 ; no more level-up moves

CharmeleonEvosAttacks:
	db EVOLVE_LEVEL, 36, CHARIZARD
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 4, GROWL
	db 8, LEER
	db 13, EMBER
	db 19, RAGE
	db 26, FIRE_SPIN
	db 34, SLASH
	db 43, SCARY_FACE
	db 55, AGILITY
	db 64, FLAMETHROWER
	db 0 ; no more level-up moves

CharizardEvosAttacks:
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 5, GROWL
	db 10, LEER
	db 16, EMBER
	db 23, RAGE
	db 31, FIRE_SPIN
	db 36, WING_ATTACK
	db 40, SLASH
	db 50, SCARY_FACE
	db 61, AGILITY
	db 73, FLAMETHROWER
	db 0 ; no more level-up moves

SquirtleEvosAttacks:
	db EVOLVE_LEVEL, 16, WARTORTLE
	db 0 ; no more evolutions
	db 1, TACKLE
	db 6, TAIL_WHIP
	db 11, BUBBLE
	db 16, WITHDRAW
	db 21, WATER_GUN
	db 26, BITE
	db 31, PROTECT
	db 36, MIST
	db 36, HAZE
	db 41, SKULL_BASH
	db 46, HYDRO_PUMP
	db 51, RAIN_DANCE
	db 0 ; no more level-up moves

WartortleEvosAttacks:
	db EVOLVE_LEVEL, 36, BLASTOISE
	db 0 ; no more evolutions
	db 1, TACKLE
	db 7, TAIL_WHIP
	db 13, BUBBLE
	db 19, WITHDRAW
	db 25, WATER_GUN
	db 31, BITE
	db 37, PROTECT
	db 43, MIST
	db 43, HAZE
	db 49, SKULL_BASH
	db 55, HYDRO_PUMP
	db 61, RAIN_DANCE
	db 0 ; no more level-up moves

BlastoiseEvosAttacks:
	db 0 ; no more evolutions
	db 1, TACKLE
	db 8, TAIL_WHIP
	db 15, BUBBLE
	db 22, WITHDRAW
	db 29, WATER_GUN
	db 36, BITE
	db 43, PROTECT
	db 50, MIST
	db 50, HAZE
	db 57, SKULL_BASH
	db 64, HYDRO_PUMP
	db 71, RAIN_DANCE
	db 0 ; no more level-up moves

CaterpieEvosAttacks:
	db EVOLVE_LEVEL, 7, METAPOD
	db 0 ; no more evolutions
	db 1, TACKLE
	db 1, STRING_SHOT
	db 0 ; no more level-up moves

MetapodEvosAttacks:
	db EVOLVE_LEVEL, 10, BUTTERFREE
	db 0 ; no more evolutions
	db 1, HARDEN
	db 7, HARDEN
	db 0 ; no more level-up moves

ButterfreeEvosAttacks:
	db 0 ; no more evolutions
	db 1, TACKLE
	db 1, STRING_SHOT
	db 10, CONFUSION
	db 12, GUST
	db 16, STUN_SPORE
	db 16, POISONPOWDER
	db 16, SLEEP_POWDER
	db 22, WHIRLWIND
	db 30, SUPERSONIC
	db 40, PSYBEAM
	db 0 ; no more level-up moves

WeedleEvosAttacks:
	db EVOLVE_LEVEL, 7, KAKUNA
	db 0 ; no more evolutions
	db 1, POISON_STING
	db 1, STRING_SHOT
	db 0 ; no more level-up moves

KakunaEvosAttacks:
	db EVOLVE_LEVEL, 10, BEEDRILL
	db 0 ; no more evolutions
	db 1, HARDEN
	db 7, HARDEN
	db 0 ; no more level-up moves

BeedrillEvosAttacks:
	db 0 ; no more evolutions
	db 1, POISON_STING
	db 1, STRING_SHOT
	db 10, PURSUIT
	db 15, FOCUS_ENERGY
	db 20, TWINEEDLE
	db 25, RAGE
	db 30, FURY_ATTACK
	db 35, AGILITY
	db 40, PIN_MISSILE
	db 0 ; no more level-up moves

PidgeyEvosAttacks:
	db EVOLVE_LEVEL, 18, PIDGEOTTO
	db 0 ; no more evolutions
	db 1, GUST
	db 6, SAND_ATTACK
	db 11, QUICK_ATTACK
	db 17, WHIRLWIND
	db 23, WING_ATTACK
	db 30, MUD_SLAP
	db 37, AGILITY
	db 45, MIRROR_MOVE
	db 0 ; no more level-up moves

PidgeottoEvosAttacks:
	db EVOLVE_LEVEL, 36, PIDGEOT
	db 0 ; no more evolutions
	db 1, GUST
	db 7, SAND_ATTACK
	db 13, QUICK_ATTACK
	db 20, WHIRLWIND
	db 27, WING_ATTACK
	db 35, MUD_SLAP
	db 43, AGILITY
	db 52, MIRROR_MOVE
	db 0 ; no more level-up moves

PidgeotEvosAttacks:
	db 0 ; no more evolutions
	db 1, GUST
	db 8, SAND_ATTACK
	db 15, QUICK_ATTACK
	db 23, WHIRLWIND
	db 31, WING_ATTACK
	db 40, MUD_SLAP
	db 49, AGILITY
	db 59, MIRROR_MOVE
	db 0 ; no more level-up moves

RattataEvosAttacks:
	db EVOLVE_LEVEL, 20, RATICATE
	db 0 ; no more evolutions
	db 1, TACKLE
	db 4, TAIL_WHIP
	db 8, QUICK_ATTACK
	db 13, LEER
	db 19, SUPER_FANG
	db 26, REVERSAL
	db 34, FOCUS_ENERGY
	db 43, HYPER_FANG
	db 0 ; no more level-up moves

RaticateEvosAttacks:
	db 0 ; no more evolutions
	db 1, TACKLE
	db 5, TAIL_WHIP
	db 10, QUICK_ATTACK
	db 16, LEER
	db 23, SUPER_FANG
	db 31, REVERSAL
	db 40, FOCUS_ENERGY
	db 50, HYPER_FANG
	db 0 ; no more level-up moves

SpearowEvosAttacks:
	db EVOLVE_LEVEL, 20, FEAROW
	db 0 ; no more evolutions
	db 1, PECK
	db 5, GROWL
	db 10, LEER
	db 16, PURSUIT
	db 23, FURY_ATTACK
	db 31, MIRROR_MOVE
	db 40, DRILL_PECK
	db 50, DOUBLE_TEAM
	db 61, AGILITY
	db 0 ; no more level-up moves

FearowEvosAttacks:
	db 0 ; no more evolutions
	db 1, PECK
	db 6, GROWL
	db 12, LEER
	db 19, PURSUIT
	db 27, FURY_ATTACK
	db 36, MIRROR_MOVE
	db 46, DRILL_PECK
	db 57, DOUBLE_TEAM
	db 69, AGILITY
	db 0 ; no more level-up moves

EkansEvosAttacks:
	db EVOLVE_LEVEL, 22, ARBOK
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ArbokEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PikachuEvosAttacks:
	db EVOLVE_ITEM, THUNDERSTONE, RAICHU
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

RaichuEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SandshrewEvosAttacks:
	db EVOLVE_LEVEL, 22, SANDSLASH
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SandslashEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

RayleepEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

BorygonEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HoohooEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SunmolaEvosAttacks:
	db EVOLVE_LEVEL, 19, ANCHORAGE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

AnchorageEvosAttacks:
	db EVOLVE_LEVEL, 38, GUROTESU
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GurotesuEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ClefairyEvosAttacks:
	db EVOLVE_ITEM, MOON_STONE, CLEFABLE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ClefableEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

VulpixEvosAttacks:
	db EVOLVE_ITEM, FIRE_STONE, NINETALES
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

NinetalesEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

JigglypuffEvosAttacks:
	db EVOLVE_ITEM, MOON_STONE, WIGGLYTUFF
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

WigglytuffEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ZubatEvosAttacks:
	db EVOLVE_LEVEL, 22, GOLBAT
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GolbatEvosAttacks:
	db EVOLVE_LEVEL, 44, CROBAT
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

OddishEvosAttacks:
	db EVOLVE_LEVEL, 21, GLOOM
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GloomEvosAttacks:
	db EVOLVE_ITEM, LEAF_STONE, VILEPLUME
	db EVOLVE_ITEM, POISON_STONE, BELLOSSOM
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

VileplumeEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ParasEvosAttacks:
	db EVOLVE_LEVEL, 24, PARASECT
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ParasectEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

VenonatEvosAttacks:
	db EVOLVE_LEVEL, 31, VENOMOTH
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

VenomothEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TurbannEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TripstarEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MeowthEvosAttacks:
	db EVOLVE_LEVEL, 28, PERSIAN
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PersianEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PsyduckEvosAttacks:
	db EVOLVE_LEVEL, 33, GOLDUCK
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GolduckEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MankeyEvosAttacks:
	db EVOLVE_LEVEL, 28, PRIMEAPE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PrimeapeEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GrowlitheEvosAttacks:
	db EVOLVE_ITEM, FIRE_STONE, ARCANINE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ArcanineEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PoliwagEvosAttacks:
	db EVOLVE_LEVEL, 25, POLIWHIRL
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PoliwhirlEvosAttacks:
	db EVOLVE_ITEM, WATER_STONE, POLIWRATH
	db EVOLVE_ITEM, HEART_STONE, POLITOED
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PoliwrathEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

AbraEvosAttacks:
	db EVOLVE_LEVEL, 16, KADABRA
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

KadabraEvosAttacks:
	db EVOLVE_TRADE, -1, ALAKAZAM
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

AlakazamEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MachopEvosAttacks:
	db EVOLVE_LEVEL, 28, MACHOKE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MachokeEvosAttacks:
	db EVOLVE_TRADE, -1, MACHAMP
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MachampEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

BellsproutEvosAttacks:
	db EVOLVE_LEVEL, 21, WEEPINBELL
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

WeepinbellEvosAttacks:
	db EVOLVE_ITEM, LEAF_STONE, VICTREEBEL
	db EVOLVE_ITEM, POISON_STONE, BELMITT
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

VictreebelEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TentacoolEvosAttacks:
	db EVOLVE_LEVEL, 30, TENTACRUEL
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TentacruelEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GeodudeEvosAttacks:
	db EVOLVE_LEVEL, 25, GRAVELER
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GravelerEvosAttacks:
	db EVOLVE_TRADE, -1, GOLEM
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GolemEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PonytaEvosAttacks:
	db EVOLVE_LEVEL, 40, RAPIDASH
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

RapidashEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SlowpokeEvosAttacks:
	db EVOLVE_LEVEL, 37, SLOWBRO
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SlowbroEvosAttacks:
	db EVOLVE_ITEM, KINGS_ROCK, SLOWKING
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MagnemiteEvosAttacks:
	db EVOLVE_LEVEL, 30, MAGNETON
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MagnetonEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

FarfetchDEvosAttacks:
	db EVOLVE_LEVEL, 24, MADAME
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

DoduoEvosAttacks:
	db EVOLVE_LEVEL, 31, DODRIO
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

DodrioEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

KurstrawEvosAttacks:
	db EVOLVE_LEVEL, 1, PANGSHI
	db 0 ; no more evolutions
	db 1, LEECH_LIFE
	db 8, DISABLE
	db 16, DESTINY_BOND
	db 25, SPITE
	db 35, CONFUSION
	db 46, NIGHT_SHADE
	db 58, SUBSTITUTE
	db 71, PSYCHIC_M
	db 85, PAIN_SPLIT
	db 100, NAIL
	db 0 ; no more level-up moves

PangshiEvosAttacks:
	db 0 ; no more evolutions
	db 1, LEECH_LIFE
	db 8, DISABLE
	db 16, SPLASH
	db 25, SPITE
	db 35, STOMP
	db 46, NIGHT_SHADE
	db 58, SUBSTITUTE
	db 71, BODY_SLAM
	db 85, PAIN_SPLIT
	db 100, CONFUSE_RAY
	db 0 ; no more level-up moves

GrimerEvosAttacks:
	db EVOLVE_LEVEL, 38, MUK
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MukEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ShellderEvosAttacks:
	db EVOLVE_ITEM, WATER_STONE, CLOYSTER
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

CloysterEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GastlyEvosAttacks:
	db EVOLVE_LEVEL, 25, HAUNTER
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HaunterEvosAttacks:
	db EVOLVE_TRADE, -1, GENGAR
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GengarEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

OnixEvosAttacks:
	db EVOLVE_LEVEL, 38, STEELIX
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

DrowzeeEvosAttacks:
	db EVOLVE_LEVEL, 26, HYPNO
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HypnoEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

KrabbyEvosAttacks:
	db EVOLVE_LEVEL, 28, KINGLER
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

KinglerEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

VoltorbEvosAttacks:
	db EVOLVE_LEVEL, 30, ELECTRODE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ElectrodeEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ExeggcuteEvosAttacks:
	db EVOLVE_ITEM, LEAF_STONE, EXEGGUTOR
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ExeggutorEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

CuboneEvosAttacks:
	db EVOLVE_LEVEL, 28, MAROWAK
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MarowakEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HitmonleeEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HitmonchanEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

LickitungEvosAttacks:
	db EVOLVE_LEVEL, 32, LIKK
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

KoffingEvosAttacks:
	db EVOLVE_LEVEL, 35, WEEZING
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

WeezingEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

RhyhornEvosAttacks:
	db EVOLVE_LEVEL, 42, RHYDON
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

RhydonEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ChanseyEvosAttacks:
	db EVOLVE_LEVEL, 45, BLISSEY
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TangelaEvosAttacks:
	db EVOLVE_LEVEL, 44, GALANLA
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

KangaskhanEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HorseaEvosAttacks:
	db EVOLVE_LEVEL, 32, SEADRA
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SeadraEvosAttacks:
	db EVOLVE_ITEM, DRAGON_SCALE, KINGDRA
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GoldeenEvosAttacks:
	db EVOLVE_LEVEL, 33, SEAKING
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SeakingEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

StaryuEvosAttacks:
	db EVOLVE_ITEM, WATER_STONE, STARMIE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

StarmieEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MrMimeEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ScytherEvosAttacks:
	db EVOLVE_LEVEL, 41, SCIZOR
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

JynxEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ElectabuzzEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MagmarEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PinsirEvosAttacks:
	db EVOLVE_LEVEL, 42, PLUX
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TaurosEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MagikarpEvosAttacks:
	db EVOLVE_LEVEL, 20, GYARADOS
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GyaradosEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

LaprasEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

DittoEvosAttacks:
	db EVOLVE_ITEM, METAL_COAT, ANIMON
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

EeveeEvosAttacks:
	db EVOLVE_ITEM, THUNDERSTONE, JOLTEON
	db EVOLVE_ITEM, WATER_STONE, VAPOREON
	db EVOLVE_ITEM, FIRE_STONE, FLAREON
	db EVOLVE_ITEM, HEART_STONE, ESPEON
	db EVOLVE_ITEM, POISON_STONE, UMBREON
	db EVOLVE_ITEM, LEAF_STONE, LEAFEON
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

VaporeonEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

JolteonEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

FlareonEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PorygonEvosAttacks:
	db EVOLVE_ITEM, UP_GRADE, PORYGON2
	db EVOLVE_ITEM, BETA_DISK, BORYGON
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

OmanyteEvosAttacks:
	db EVOLVE_LEVEL, 40, OMASTAR
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

OmastarEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

KabutoEvosAttacks:
	db EVOLVE_LEVEL, 40, KABUTOPS
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

KabutopsEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

AerodactylEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SnorlaxEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ArticunoEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ZapdosEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MoltresEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

DratiniEvosAttacks:
	db EVOLVE_LEVEL, 30, DRAGONAIR
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

DragonairEvosAttacks:
	db EVOLVE_LEVEL, 55, DRAGONITE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

DragoniteEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MewtwoEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MewEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ChikoritaEvosAttacks:
	db EVOLVE_LEVEL, 16, BAYLEEF
	db 0 ; no more evolutions
	db 1, TACKLE
	db 3, GROWTH
	db 6, LEECH_SEED
	db 10, RAZOR_LEAF
	db 15, STUN_SPORE
	db 21, SLEEP_POWDER
	db 28, SLAM
	db 36, POISONPOWDER
	db 45, MORNING_SUN
	db 55, SOLARBEAM
	db 0 ; no more level-up moves

BayleefEvosAttacks:
	db EVOLVE_LEVEL, 32, MEGANIUM
	db 0 ; no more evolutions
	db 1, TACKLE
	db 4, GROWTH
	db 8, LEECH_SEED
	db 13, RAZOR_LEAF
	db 19, STUN_SPORE
	db 26, SLEEP_POWDER
	db 34, SLAM
	db 43, POISONPOWDER
	db 53, MORNING_SUN
	db 64, SOLARBEAM
	db 0 ; no more level-up moves

MeganiumEvosAttacks:
	db 0 ; no more evolutions
	db 1, TACKLE
	db 5, GROWTH
	db 10, LEECH_SEED
	db 16, RAZOR_LEAF
	db 23, STUN_SPORE
	db 31, SLEEP_POWDER
	db 40, SLAM
	db 50, POISONPOWDER
	db 61, MORNING_SUN
	db 73, SOLARBEAM
	db 0 ; no more level-up moves

FlambearEvosAttacks:
	db EVOLVE_LEVEL, 16, VOLBEAR
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 5, LEER
	db 9, EMBER
	db 14, ROAR
	db 19, BITE
	db 25, REST
	db 31, FLAME_WHEEL
	db 38, SCARY_FACE
	db 45, FURY_SWIPES
	db 53, FLAMETHROWER
	db 0 ; no more level-up moves

VolbearEvosAttacks:
	db EVOLVE_LEVEL, 32, DYNABEAR
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 6, LEER
	db 11, EMBER
	db 17, ROAR
	db 23, BITE
	db 30, REST
	db 37, FLAME_WHEEL
	db 45, SCARY_FACE
	db 53, FURY_SWIPES
	db 62, FLAMETHROWER
	db 0 ; no more level-up moves

DynabearEvosAttacks:
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 7, LEER
	db 13, ROAR
	db 20, EMBER
	db 27, BITE
	db 35, REST
	db 43, FLAME_WHEEL
	db 52, SCARY_FACE
	db 61, FURY_SWIPES
	db 71, FLAMETHROWER
	db 0 ; no more level-up moves

CruzEvosAttacks:
	db EVOLVE_LEVEL, 16, AQUA
	db 0 ; no more evolutions
	db 1, TACKLE
	db 4, GROWL
	db 8, WATER_GUN
	db 13, BITE
	db 19, MIST
	db 26, AURORA_BEAM
	db 34, SAFEGUARD
	db 43, BODY_SLAM
	db 53, HYDRO_PUMP
	db 0 ; no more level-up moves

AquaEvosAttacks:
	db EVOLVE_LEVEL, 32, AQUARIA
	db 0 ; no more evolutions
	db 1, TACKLE
	db 5, GROWL
	db 10, WATER_GUN
	db 16, BITE
	db 23, MIST
	db 31, AURORA_BEAM
	db 40, SAFEGUARD
	db 50, BODY_SLAM
	db 61, HYDRO_PUMP
	db 0 ; no more level-up moves

AquariaEvosAttacks:
	db 0 ; no more evolutions
	db 1, TACKLE
	db 6, GROWL
	db 12, WATER_GUN
	db 19, BITE
	db 27, MIST
	db 36, AURORA_BEAM
	db 46, SAFEGUARD
	db 57, BODY_SLAM
	db 69, HYDRO_PUMP
	db 0 ; no more level-up moves

TangelEvosAttacks:
	db EVOLVE_LEVEL, 22, TANGELA
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GalanlaEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HoothootEvosAttacks:
	db EVOLVE_LEVEL, 20, NOCTOWL
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

NoctowlEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

LedybaEvosAttacks:
	db EVOLVE_LEVEL, 18, LEDIAN
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

LedianEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SpinarakEvosAttacks:
	db EVOLVE_LEVEL, 22, ARIADOS
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

AriadosEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

CrobatEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TigretteEvosAttacks:
	db EVOLVE_LEVEL, 35, ELECTIGER
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ElectigerEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PichuEvosAttacks:
	db EVOLVE_LEVEL, 12, PIKACHU
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

CleffaEvosAttacks:
	db EVOLVE_LEVEL, 12, CLEFAIRY
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

IgglybuffEvosAttacks:
	db EVOLVE_LEVEL, 12, JIGGLYPUFF
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TogepiEvosAttacks:
	db EVOLVE_HAPPINESS, TR_ANYTIME, TOGETIC
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TogeticEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

NatuEvosAttacks:
	db EVOLVE_ITEM, HEART_STONE, XATU
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

XatuEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MareepEvosAttacks:
	db EVOLVE_LEVEL, 16, FLAAFFY
	db 0 ; no more evolutions
	db 1, THUNDERSHOCK
	db 3, GROWL
	db 6, TAIL_WHIP
	db 10, HYPNOSIS
	db 15, SWIFT
	db 21, COTTON_SPORE
	db 28, SCREECH
	db 36, LIGHT_SCREEN
	db 45, THUNDERBOLT
	db 0 ; no more level-up moves

FlaaffyEvosAttacks:
	db EVOLVE_LEVEL, 32, AMPHAROS
	db 0 ; no more evolutions
	db 1, THUNDERSHOCK
	db 4, GROWL
	db 8, TAIL_WHIP
	db 13, HYPNOSIS
	db 19, SWIFT
	db 26, COTTON_SPORE
	db 34, SCREECH
	db 43, LIGHT_SCREEN
	db 53, THUNDERBOLT
	db 0 ; no more level-up moves

AmpharosEvosAttacks:
	db 0 ; no more evolutions
	db 1, THUNDERSHOCK
	db 5, GROWL
	db 10, TAIL_WHIP
	db 16, HYPNOSIS
	db 23, SWIFT
	db 31, COTTON_SPORE
	db 32, THUNDERPUNCH
	db 40, SCREECH
	db 50, LIGHT_SCREEN
	db 61, THUNDERBOLT
	db 0 ; no more level-up moves

BellossomEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MarillEvosAttacks:
	db EVOLVE_LEVEL, 18, AZUMARILL
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

AzumarillEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

BomseelEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PolitoedEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HoppipEvosAttacks:
	db EVOLVE_LEVEL, 18, SKIPLOOM
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SkiploomEvosAttacks:
	db EVOLVE_LEVEL, 40, JUMPLUFF
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

JumpluffEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

AipomEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SunkernEvosAttacks:
	db EVOLVE_ITEM, HEART_STONE, SUNFLORA
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SunfloraEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

LikkEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

WooperEvosAttacks:
	db EVOLVE_LEVEL, 20, QUAGSIRE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

QuagsireEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

EspeonEvosAttacks:
	db 0 ; no more evolutions
	db 1, TACKLE
	db 7, SAND_ATTACK
	db 14, QUICK_ATTACK
	db 21, TAIL_WHIP
	db 28, CONFUSION
	db 35, BITE
	db 42, AGILITY
	db 49, REFLECT
	db 56, LIGHT_SCREEN
	db 63, PSYCHIC_M
	db 0 ; no more level-up moves

UmbreonEvosAttacks:
	db 0 ; no more evolutions
	db 1, TACKLE
	db 7, SAND_ATTACK
	db 14, QUICK_ATTACK
	db 21, TAIL_WHIP
	db 28, SMOG
	db 35, ACID
	db 42, ACID_ARMOR
	db 49, SLUDGE
	db 56, SMOKESCREEN
	db 63, SLUDGE_BOMB
	db 0 ; no more level-up moves

MurkrowEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SlowkingEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

TwinzEvosAttacks:
	db EVOLVE_LEVEL, 29, GIRAFARIG
	db EVOLVE_ITEM, MOON_STONE, WOBBUFFET
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

UnownEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

WobbuffetEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GirafarigEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

NumpuffEvosAttacks:
	db 0 ; no more evolutions
	db 1, POISON_STING
	db 7, TAIL_WHIP
	db 14, WATER_GUN
	db 22, SELFDESTRUCT
	db 31, THUNDER_WAVE
	db 41, PIN_MISSILE
	db 52, SMOKESCREEN
	db 64, EXPLOSION
	db 0 ; no more level-up moves

AnimonEvosAttacks:
	db 0 ; no more evolutions
	db 1, TRANSFORM
	db 0 ; no more level-up moves

TrifoxEvosAttacks:
	db EVOLVE_LEVEL, 13, VULPIX
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PuddiEvosAttacks:
	db EVOLVE_LEVEL, 13, GROWLITHE
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SteelixEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SnubbullEvosAttacks:
	db EVOLVE_LEVEL, 23, GRANBULL
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GranbullEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

QwilfishEvosAttacks:
	db EVOLVE_LEVEL, 18, NUMPUFF
	db 0 ; no more evolutions
	db 1, POISON_STING
	db 6, TAIL_WHIP
	db 12, WATER_GUN
	db 19, SELFDESTRUCT
	db 27, THUNDER_WAVE
	db 36, PIN_MISSILE
	db 46, SMOKESCREEN
	db 57, EXPLOSION
	db 0 ; no more level-up moves

ScizorEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MadameEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PluxEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SneaselEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

GolppyEvosAttacks:
	db EVOLVE_LEVEL, 16, GOLDEEN
	db 0 ; no more evolutions
	db 1, PECK
	db 3, TAIL_WHIP
	db 6, SUPERSONIC
	db 10, FLAIL
	db 15, HORN_ATTACK
	db 21, FURY_ATTACK
	db 28, HORN_DRILL
	db 36, WATERFALL
	db 45, AGILITY
	db 0 ; no more level-up moves

ParaEvosAttacks:
	db EVOLVE_LEVEL, 12, PARAS
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 3, STUN_SPORE
	db 6, LEECH_LIFE
	db 10, POISONPOWDER
	db 15, GROWTH
	db 21, FURY_SWIPES
	db 28, SLASH
	db 36, SPORE
	db 0 ; no more level-up moves

ChiksEvosAttacks:
	db EVOLVE_LEVEL, 16, DODUO
	db 0 ; no more evolutions
	db 1, GROWL
	db 5, PECK
	db 9, QUICK_ATTACK
	db 14, RAGE
	db 19, PURSUIT
	db 25, FURY_ATTACK
	db 31, AGILITY
	db 38, TRI_ATTACK
	db 45, DRILL_PECK
	db 0 ; no more level-up moves

MeowsyEvosAttacks:
	db EVOLVE_LEVEL, 14, MEOWTH
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 4, GROWL
	db 7, TAIL_WHIP
	db 11, SAND_ATTACK
	db 15, PAY_DAY
	db 20, BITE
	db 25, FURY_SWIPES
	db 31, THIEF
	db 37, SCREECH
	db 44, SLASH
	db 51, COIN_HURL
	db 0 ; no more level-up moves

MinicornEvosAttacks:
	db EVOLVE_LEVEL, 20, PONYTA
	db 0 ; no more evolutions
	db 1, TAIL_WHIP
	db 7, EMBER
	db 14, QUICK_ATTACK
	db 21, GROWL
	db 28, STOMP
	db 35, FLAME_WHEEL
	db 42, AGILITY
	db 49, FIRE_SPIN
	db 56, TAKE_DOWN
	db 0 ; no more level-up moves

GrimeyEvosAttacks:
	db EVOLVE_LEVEL, 19, GRIMER
	db 0 ; no more evolutions
	db 1, POUND
	db 2, POISON_GAS
	db 4, DISABLE
	db 7, ACID
	db 11, MINIMIZE
	db 16, HARDEN
	db 22, SLUDGE
	db 29, HAZE
	db 37, SCREECH
	db 46, ACID_ARMOR
	db 56, SLUDGE_BOMB
	db 0 ; no more level-up moves

BallerineEvosAttacks:
	db EVOLVE_LEVEL, 15, MR__MIME
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

RemoraidEvosAttacks:
	db EVOLVE_LEVEL, 34, OCTILLERY
	db 0 ; no more evolutions
	db 1, WATER_GUN
	db 10, FOCUS_ENERGY
	db 31, AURORA_BEAM
	db 31, PSYBEAM
	db 31, BUBBLE_BEAM
	db 56, LOCK_ON
	db 70, ZAP_CANNON
	db 0 ; no more level-up moves

OctilleryEvosAttacks:
	db 0 ; no more evolutions
	db 1, WATER_GUN
	db 10, FOCUS_ENERGY
	db 20, OCTAZOOKA
	db 31, WRAP
	db 43, PROTECT
	db 56, LOCK_ON
	db 70, ZAP_CANNON
	db 0 ; no more level-up moves

DelibirdEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MantineEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SkarmoryEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HoundourEvosAttacks:
	db EVOLVE_LEVEL, 35, HOUNDOOM
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HoundoomEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

KingdraEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

PhanpyEvosAttacks:
	db EVOLVE_LEVEL, 33, DONPHAN
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

DonphanEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

Porygon2EvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

BelmittEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SmeargleEvosAttacks:
	db 0 ; no more evolutions
	db 1, SKETCH
	db 10, SKETCH
	db 20, SKETCH
	db 30, SKETCH
	db 40, SKETCH
	db 50, SKETCH
	db 60, SKETCH
	db 70, SKETCH
	db 80, SKETCH
	db 90, SKETCH
	db 100, SKETCH
	db 0 ; no more level-up moves

TyrogueEvosAttacks:
	db EVOLVE_STAT, 15, ATK_LT_DEF, HITMONCHAN
	db EVOLVE_STAT, 15, ATK_GT_DEF, HITMONLEE
	db EVOLVE_STAT, 15, ATK_EQ_DEF, HITMONTOP
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HitmontopEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

SmoochumEvosAttacks:
	db EVOLVE_LEVEL, 15, JYNX
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

ElekidEvosAttacks:
	db EVOLVE_LEVEL, 15, ELECTABUZZ
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MagbyEvosAttacks:
	db EVOLVE_LEVEL, 15, MAGMAR
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

MiltankEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

BlisseyEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 6, TAIL_WHIP
	db 12, GROWL
	db 19, DEFENSE_CURL
	db 27, DOUBLE_SLAP
	db 36, SING
	db 46, MINIMIZE
	db 57, LIGHT_SCREEN
	db 69, DOUBLE_EDGE
	db 82, PAIN_SPLIT
	db 0 ; no more level-up moves

RaiEvosAttacks:
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 10, THUNDERSHOCK
	db 19, QUICK_ATTACK
	db 29, FOCUS_ENERGY
	db 39, BITE
	db 50, FURY_SWIPES
	db 61, THUNDERBOLT
	db 73, DETECT
	db 85, THUNDER
	db 0 ; no more level-up moves

EnEvosAttacks:
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 10, EMBER
	db 19, QUICK_ATTACK
	db 29, FOCUS_ENERGY
	db 39, BITE
	db 50, FURY_SWIPES
	db 61, FLAMETHROWER
	db 73, ENDURE
	db 85, FIRE_BLAST
	db 0 ; no more level-up moves

SuiEvosAttacks:
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 10, WATER_GUN
	db 19, QUICK_ATTACK
	db 29, FOCUS_ENERGY
	db 39, BITE
	db 50, FURY_SWIPES
	db 61, BUBBLE_BEAM
	db 73, MIND_READER
	db 85, HYDRO_PUMP
	db 0 ; no more level-up moves

WolfmanEvosAttacks:
	db EVOLVE_LEVEL, 35, WARWOLF
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 7, POWDER_SNOW
	db 14, TAIL_WHIP
	db 21, SAFEGUARD
	db 28, FURY_SWIPES
	db 35, SLASH
	db 42, SCREECH
	db 49, CONFUSE_RAY
	db 56, BLIZZARD
	db 0 ; no more level-up moves

WarwolfEvosAttacks:
	db 0 ; no more evolutions
	db 1, SCRATCH
	db 8, POWDER_SNOW
	db 16, TAIL_WHIP
	db 24, SAFEGUARD
	db 32, FURY_SWIPES
	db 40, SLASH
	db 48, SCREECH
	db 56, CONFUSE_RAY
	db 64, BLIZZARD
	db 0 ; no more level-up moves

RinringEvosAttacks:
	db EVOLVE_LEVEL, 28, BELLBOYANT
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

BellboyantEvosAttacks:
	db 0 ; no more evolutions
	db 1, POUND
	db 0 ; no more level-up moves

HoOhEvosAttacks:
	db 0 ; no more evolutions
	db 1, WING_ATTACK
	db 9, LEER
	db 18, DETECT
	db 28, GUST
	db 39, LIGHT_SCREEN
	db 39, REFLECT
	db 39, SAFEGUARD
	db 51, SACRED_FIRE
	db 64, SCARY_FACE
	db 78, RECOVER
	db 93, SKY_ATTACK
	db 0 ; no more level-up moves

LeafeonEvosAttacks:
	db 0 ; no more evolutions
	db 1, TACKLE
	db 7, SAND_ATTACK
	db 14, QUICK_ATTACK
	db 21, TAIL_WHIP
	db 28, ABSORB
	db 35, RAZOR_LEAF
	db 42, GROWTH
	db 49, MORNING_SUN
	db 56, WRAP
	db 63, SOLARBEAM
	db 0 ; no more level-up moves

