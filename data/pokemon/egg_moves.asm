INCLUDE "constants.asm"


SECTION "Egg Moves", ROMX

; All instances of Charm, Steel Wing, Sweet Scent, and Lovely Kiss were
; removed from egg move lists in Crystal.
; Sweet Scent and Steel Wing were redundant since they're TMs, and
; Charm and Lovely Kiss were unobtainable.

; Staryu's egg moves were removed in Crystal, because Staryu is genderless
; and can only breed with Ditto.


INCLUDE "data/pokemon/egg_move_pointers.asm"


EggMoves::

BulbasaurEggMoves:
	db -1 ; end

CharmanderEggMoves:
	db -1 ; end

SquirtleEggMoves:
	db -1 ; end

PidgeyEggMoves:
	db -1 ; end

RattataEggMoves:
	db -1 ; end

SpearowEggMoves:
	db -1 ; end

EkansEggMoves:
	db -1 ; end

SandshrewEggMoves:
	db -1 ; end

NidoranFEggMoves:
	db -1 ; end

NidoranMEggMoves:
	db -1 ; end

VulpixEggMoves:
	db -1 ; end

ZubatEggMoves:
	db -1 ; end

OddishEggMoves:
	db -1 ; end

ParasEggMoves:
	db -1 ; end

VenonatEggMoves:
	db -1 ; end

DiglettEggMoves:
	db -1 ; end

MeowthEggMoves:
	db -1 ; end

PsyduckEggMoves:
	db -1 ; end

MankeyEggMoves:
	db -1 ; end

GrowlitheEggMoves:
	db -1 ; end

PoliwagEggMoves:
	db -1 ; end

AbraEggMoves:
	db -1 ; end

MachopEggMoves:
	db -1 ; end

BellsproutEggMoves:
	db -1 ; end

TentacoolEggMoves:
	db -1 ; end

GeodudeEggMoves:
	db -1 ; end

PonytaEggMoves:
	db -1 ; end

SlowpokeEggMoves:
	db -1 ; end

FarfetchDEggMoves:
	db -1 ; end

DoduoEggMoves:
	db -1 ; end

SeelEggMoves:
	db -1 ; end

GrimerEggMoves:
	db -1 ; end

ShellderEggMoves:
	db -1 ; end

GastlyEggMoves:
	db -1 ; end

OnixEggMoves:
	db -1 ; end

DrowzeeEggMoves:
	db -1 ; end

KrabbyEggMoves:
	db -1 ; end

ExeggcuteEggMoves:
	db -1 ; end

CuboneEggMoves:
	db -1 ; end

LickitungEggMoves:
	db -1 ; end

KoffingEggMoves:
	db -1 ; end

RhyhornEggMoves:
	db -1 ; end

ChanseyEggMoves:
	db -1 ; end

TangelaEggMoves:
	db -1 ; end

KangaskhanEggMoves:
	db -1 ; end

HorseaEggMoves:
	db -1 ; end

GoldeenEggMoves:
	db -1 ; end

StaryuEggMoves:
	db -1 ; end

MrMimeEggMoves:
	db -1 ; end

ScytherEggMoves:
	db -1 ; end

PinsirEggMoves:
	db -1 ; end

LaprasEggMoves:
	db -1 ; end

EeveeEggMoves:
	db -1 ; end

OmanyteEggMoves:
	db -1 ; end

KabutoEggMoves:
	db -1 ; end

AerodactylEggMoves:
	db -1 ; end

SnorlaxEggMoves:
	db -1 ; end

DratiniEggMoves:
	db -1 ; end

ChikoritaEggMoves:
	db -1 ; end

CyndaquilEggMoves:
	db -1 ; end

TotodileEggMoves:
	db -1 ; end

SentretEggMoves:
	db -1 ; end

HoothootEggMoves:
	db -1 ; end

LedybaEggMoves:
	db -1 ; end

SpinarakEggMoves:
	db -1 ; end

ChinchouEggMoves:
	db -1 ; end

PichuEggMoves:
	db -1 ; end

CleffaEggMoves:
	db -1 ; end

IgglybuffEggMoves:
	db -1 ; end

TogepiEggMoves:
	db -1 ; end

NatuEggMoves:
	db -1 ; end

MareepEggMoves:
	db -1 ; end

MarillEggMoves:
	db -1 ; end

SudowoodoEggMoves:
	db -1 ; end

HoppipEggMoves:
	db -1 ; end

AipomEggMoves:
	db -1 ; end

YanmaEggMoves:
	db -1 ; end

WooperEggMoves:
	db -1 ; end

MurkrowEggMoves:
	db -1 ; end

MisdreavusEggMoves:
	db -1 ; end

GirafarigEggMoves:
	db -1 ; end

PinecoEggMoves:
	db -1 ; end

DunsparceEggMoves:
	db -1 ; end

GligarEggMoves:
	db -1 ; end

SnubbullEggMoves:
	db -1 ; end

QwilfishEggMoves:
	db -1 ; end

ShuckleEggMoves:
	db -1 ; end

HeracrossEggMoves:
	db -1 ; end

SneaselEggMoves:
	db -1 ; end

TeddiursaEggMoves:
	db -1 ; end

SlugmaEggMoves:
	db -1 ; end

SwinubEggMoves:
	db -1 ; end

CorsolaEggMoves:
	db -1 ; end

RemoraidEggMoves:
	db -1 ; end

DelibirdEggMoves:
	db -1 ; end

MantineEggMoves:
	db -1 ; end

SkarmoryEggMoves:
	db -1 ; end

HoundourEggMoves:
	db -1 ; end

PhanpyEggMoves:
	db -1 ; end

StantlerEggMoves:
	db -1 ; end

TyrogueEggMoves:
	db -1 ; end

SmoochumEggMoves:
	db -1 ; end

ElekidEggMoves:
	db -1 ; end

MagbyEggMoves:
	db -1 ; end

MiltankEggMoves:
	db -1 ; end

LarvitarEggMoves:
	db -1 ; end

NoEggMoves:
	db -1 ; end

