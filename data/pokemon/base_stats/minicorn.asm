	db MINICORN ; 220
;~PH
	db  50,  50,  40,  50,  30,  30
	;   hp  atk  def  spd  sat  sdf

	db NORMAL, NORMAL ; type
	db 225 ; catch rate~PH
	db 78 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/minicorn/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_SLOW ; growth rate~PH
	dn EGG_GROUND, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





