	db TWINZ ; 200
;~PH
	db  60,  60,  60,  85,  85,  85
	;   hp  atk  def  spd  sat  sdf

	db DARK, NORMAL ; type
	db 45 ; catch rate~PH
	db 147 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 25 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/twinz/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_FAST ; growth rate~PH
	dn EGG_INDETERMINATE, EGG_INDETERMINATE ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





