	db PANGSHI ; 087
;~PH somewhat
	db  60,  50,  55,  55,  70,  50
	;   hp  atk  def  spd  sat  sdf

	db GHOST, GHOST ; type
	db 75 ; catch rate~PH
	db 176 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/pangshi/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_FAST ; growth rate~PH
	dn EGG_WATER_1, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





