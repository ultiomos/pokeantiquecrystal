	db BOMSEEL ; 185
;~PH
	db  70, 100, 115,  30,  30,  65
	;   hp  atk  def  spd  sat  sdf

	db WATER, FIRE ; type
	db 65 ; catch rate~PH
	db 135 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/bomseel/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_FAST ; growth rate~PH
	dn EGG_MINERAL, EGG_MINERAL ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





