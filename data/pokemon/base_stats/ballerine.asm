	db BALLERINE ; 222
;~PH
	db  55,  55,  85,  35,  65,  85
	;   hp  atk  def  spd  sat  sdf

	db NORMAL, NORMAL ; type
	db 60 ; catch rate~PH
	db 113 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F75 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/ballerine/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_FAST ; growth rate~PH
	dn EGG_WATER_1, EGG_WATER_3 ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





