	db RAYLEEP ; 029
;~PH
	db  55,  47,  52,  41,  40,  40
	;   hp  atk  def  spd  sat  sdf

	db WATER, FLYING ; type
	db 235 ; catch rate~PH
	db 59 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F100 ; gender ratio~L
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/rayleep/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_SLOW ; growth rate~PH
	dn EGG_MONSTER, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





