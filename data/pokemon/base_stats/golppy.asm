	db GOLPPY ; 216
;~PH
	db  60,  80,  50,  40,  50,  50
	;   hp  atk  def  spd  sat  sdf

	db WATER, WATER ; type
	db 120 ; catch rate~PH
	db 124 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/golppy/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_FAST ; growth rate~PH
	dn EGG_GROUND, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





