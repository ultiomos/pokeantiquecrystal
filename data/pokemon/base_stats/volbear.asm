	db VOLBEAR ; 156
;~PH somewhat
	db  60,  70,  50,  50,  60,  50
	;   hp  atk  def  spd  sat  sdf

	db FIRE, FIRE ; type
	db 45 ; catch rate~PH
	db 142 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F12_5 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/volbear/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_SLOW ; growth rate~PH
	dn EGG_GROUND, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





