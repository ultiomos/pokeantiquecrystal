	db NUMPUFF ; 204
;~PH
	db  50,  65,  90,  15,  35,  35
	;   hp  atk  def  spd  sat  sdf

	db WATER, POISON ; type (Executive Descision)
	db 190 ; catch rate~PH
	db 60 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/numpuff/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_FAST ; growth rate~PH
	dn EGG_BUG, EGG_BUG ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





