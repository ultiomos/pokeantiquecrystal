	db CHIKS ; 218
;~PH
	db  40,  40,  40,  20,  70,  40
	;   hp  atk  def  spd  sat  sdf

	db NORMAL, FLYING ; type
	db 190 ; catch rate~PH
	db 78 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/chiks/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_FAST ; growth rate~PH
	dn EGG_INDETERMINATE, EGG_INDETERMINATE ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





