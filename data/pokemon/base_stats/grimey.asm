	db GRIMEY ; 221
;~PH
	db 100, 100,  80,  50,  60,  60
	;   hp  atk  def  spd  sat  sdf

	db POISON, POISON ; type
	db 75 ; catch rate~PH
	db 160 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/grimey/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_SLOW ; growth rate~PH
	dn EGG_GROUND, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





