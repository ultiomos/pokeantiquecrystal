	db TRIPSTAR ; 051
;~PH
	db  35,  80,  50, 120,  50,  70
	;   hp  atk  def  spd  sat  sdf

	db BUG, FLYING ; type
	db 50 ; catch rate~PH
	db 153 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/tripstar/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_FAST ; growth rate~PH
	dn EGG_GROUND, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





