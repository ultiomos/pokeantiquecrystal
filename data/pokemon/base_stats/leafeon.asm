	db LEAFEON ; 251
; Set to actual ~Leafeon
	db 100, 100, 100, 100, 100, 100
	;   hp  atk  def  spd  sat  sdf

	db GRASS, GRASS ; type
	db 45 ; catch rate~L
	db 64 ; base exp~L
	db NO_ITEM, NO_ITEM ; items
	db GENDER_UNKNOWN ; gender ratio~L
	db 100 ; unknown 1
	db 120 ; step cycles to hatch~L
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/leafeon/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_SLOW ; growth rate~L
	dn EGG_NONE, EGG_NONE ; egg groups~L

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





