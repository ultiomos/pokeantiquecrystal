	db GUROTESU ; 034
;~PH kind of?
	db  60,  65,  60,  30,  80,  50
	;   hp  atk  def  spd  sat  sdf

	db WATER, STEEL ; type
	db 45 ; catch rate~PH
	db 195 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F0 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/gurotesu/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_SLOW ; growth rate~PH
	dn EGG_MONSTER, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





