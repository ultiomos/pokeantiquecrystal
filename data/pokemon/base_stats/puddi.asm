	db PUDDI ; 207
;~PH
	db  65,  75, 105,  85,  35,  65
	;   hp  atk  def  spd  sat  sdf

	db FIRE, FIRE ; type
	db 60 ; catch rate~PH
	db 108 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio~L
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/puddi/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_SLOW ; growth rate~PH
	dn EGG_BUG, EGG_BUG ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





