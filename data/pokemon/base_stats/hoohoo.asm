	db HOOHOO ; 031
;~PH
	db  90,  82,  87,  76,  75,  85
	;   hp  atk  def  spd  sat  sdf

	db FLYING, FLYING ; type
	db 45 ; catch rate~PH
	db 194 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F100 ; gender ratio~L
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/hoohoo/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_SLOW ; growth rate~PH
	dn EGG_NONE, EGG_NONE ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





