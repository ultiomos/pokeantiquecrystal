	db BELLBOYANT ; 249
;~PH
	db 106,  90, 130, 110,  90, 154
	;   hp  atk  def  spd  sat  sdf

	db DARK, DARK ; type
	db 3 ; catch rate~PH
	db 220 ; base exp~PH
	db NO_ITEM, NO_ITEM ; items
	db GENDER_UNKNOWN ; gender ratio
	db 100 ; unknown 1
	db 120 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/bellboyant/front.dimensions"

	db 0, 0, 0, 0 ; padding
	db GROWTH_SLOW ; growth rate~PH
	dn EGG_NONE, EGG_NONE ; egg groups

	; tm/hm learnset
	tmhm HIDDEN_POWER
	; end





