flypoint: MACRO
	const FLY_\1
	db \2, SPAWN_\1
ENDM

Flypoints: ; 91c5e
; landmark, spawn point
	const_def
; Johto
	flypoint SILENT,       SILENT_HILL
	flypoint OLD,          OLD_CITY
	flypoint WEST,         WEST_CITY
	flypoint HITEKK,       HITEKK_CITY
	flypoint FOUNT,        FOUNT_TOWN
	flypoint SOUTH,        SOUTH_CITY
	flypoint BIRDON,       BIRDON
	flypoint ROUTE_15,     ROUTE_15
	flypoint NEWTYPE,      NEWTYPE_CITY
	flypoint SUGAR,        SUGAR_ISLAND
	flypoint ROUTE_18,     ROUTE_18
	flypoint KANTO,        KANTO_WORLDMAP
	flypoint STAND,        STAND_CITY
	flypoint BLUE,         BLUE_FOREST
	flypoint NORTH,        NORTH_TOWN
	flypoint PRINCE,       PRINCE_TOWN
	
KANTO_FLYPOINT EQU const_value
	flypoint DUMMY,        DUMMY_RETURN
	flypoint PALLET,       PALLET_TOWN
	flypoint VIRIDIAN,     VIRIDIAN_CITY
	flypoint PEWTER,       PEWTER_CITY
	flypoint ROUTE_K4,     ROUTE_K4
	flypoint CERULEAN,     CERULEAN_CITY
	flypoint VERMILION,    VERMILION_CITY
	flypoint CELADON,      CELADON_CITY
	flypoint LAVENDER,     LAVENDER_TOWN
	flypoint ROUTE_K10,    ROUTE_K10
	flypoint SAFFRON,      SAFFRON_CITY
	flypoint FUCHSIA,      FUCHSIA_CITY
	flypoint CINNABAR,     CINNABAR_ISLAND
	flypoint INDIGO,       INDIGO_PLATEAU

; Kanto
	db -1
; 91c8f
