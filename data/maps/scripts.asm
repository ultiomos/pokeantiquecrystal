SECTION "Map Scripts 1", ROMX

INCLUDE "maps/FountPokecenter1F.asm"
INCLUDE "maps/SouthPokecenter1F.asm"
INCLUDE "maps/BirdonPokecenter1F.asm"
INCLUDE "maps/SugarPokecenter1F.asm"
INCLUDE "maps/Route18Pokecenter1F.asm"
INCLUDE "maps/StandPokecenter1F.asm"
INCLUDE "maps/BluePokecenter1F.asm"
INCLUDE "maps/NorthPokecenter1F.asm"


SECTION "Map Scripts 2", ROMX
SECTION "Map Scripts 3", ROMX
SECTION "Map Scripts 4", ROMX
SECTION "Map Scripts 5", ROMX

INCLUDE "maps/OldMart.asm"
INCLUDE "maps/OldGym.asm"
INCLUDE "maps/OldMuseum.asm"
INCLUDE "maps/OldKurtsHouse.asm"
INCLUDE "maps/OldPokecenter1F.asm"
INCLUDE "maps/OldBillsHouse.asm"
INCLUDE "maps/OldHouse.asm"
INCLUDE "maps/OldSchool.asm"
INCLUDE "maps/Route2GameHouse.asm"
INCLUDE "maps/HitekkPokecenter1F.asm"
INCLUDE "maps/HitekkMart.asm"


SECTION "Map Scripts 6", ROMX
SECTION "Map Scripts 7", ROMX
SECTION "Map Scripts 8", ROMX
SECTION "Map Scripts 9", ROMX

INCLUDE "maps/OaksLab.asm"
INCLUDE "maps/PlayersHouse1F.asm"
INCLUDE "maps/PlayersHouse2F.asm"
INCLUDE "maps/SilentPokecenter1F.asm"
INCLUDE "maps/SilversHouse.asm"
INCLUDE "maps/OaksLabBack.asm"


SECTION "Map Scripts 10", ROMX
SECTION "Map Scripts 11", ROMX
SECTION "Map Scripts 12", ROMX
SECTION "Map Scripts 13", ROMX
SECTION "Map Scripts 14", ROMX

INCLUDE "maps/SproutTower.asm"


SECTION "Map Scripts 15", ROMX
SECTION "Map Scripts 16", ROMX
SECTION "Map Scripts 17", ROMX

INCLUDE "maps/Pokecenter2F.asm"
INCLUDE "maps/TradeCenter.asm"
INCLUDE "maps/Colosseum.asm"
INCLUDE "maps/TimeCapsule.asm"
INCLUDE "maps/MobileTradeRoom.asm"
INCLUDE "maps/MobileBattleRoom.asm"


SECTION "Map Scripts 18", ROMX

INCLUDE "maps/Route1OldCityGate.asm"
INCLUDE "maps/Route1OldCityGateUpstairs.asm"
INCLUDE "maps/Route2WestCityGate.asm"
INCLUDE "maps/Route2WestCityGateUpstairs.asm"
INCLUDE "maps/Route12WestCityGate.asm"
INCLUDE "maps/Route12WestCityGateUpstairs.asm"
INCLUDE "maps/Route4SouthCityGate.asm"
INCLUDE "maps/Route4SouthCityGateUpstairs.asm"
INCLUDE "maps/Route9SouthCityGate.asm"
INCLUDE "maps/Route9SouthCityGateUpstairs.asm"


SECTION "Map Scripts 19", ROMX

INCLUDE "maps/WestPokecenter1F.asm"
INCLUDE "maps/WestRadio1F.asm"
INCLUDE "maps/WestRadio2F.asm"
INCLUDE "maps/WestRadio3F.asm"
INCLUDE "maps/WestRadio4F.asm"
INCLUDE "maps/WestRadio5F.asm"
INCLUDE "maps/WestRocketHouse.asm"
INCLUDE "maps/WestHouse1.asm"
INCLUDE "maps/WestHouse2.asm"
INCLUDE "maps/WestDept1F.asm"
INCLUDE "maps/WestDept2F.asm"
INCLUDE "maps/WestDept3F.asm"
INCLUDE "maps/WestDept4F.asm"
INCLUDE "maps/WestDept5F.asm"
INCLUDE "maps/WestDept6F.asm"
INCLUDE "maps/WestDeptElevator.asm"

SECTION "Map Scripts 20", ROMX
SECTION "Map Scripts 21", ROMX

INCLUDE "maps/Route1.asm"
INCLUDE "maps/Route2.asm"
INCLUDE "maps/Route3.asm"
INCLUDE "maps/Route4.asm"
INCLUDE "maps/Route5.asm"
INCLUDE "maps/Route6.asm"
INCLUDE "maps/Route7.asm"
INCLUDE "maps/Route8.asm"
INCLUDE "maps/Route9.asm"
INCLUDE "maps/Route10.asm"
INCLUDE "maps/Route11.asm"
INCLUDE "maps/Route12.asm"
INCLUDE "maps/Route13.asm"
INCLUDE "maps/Route14.asm"
INCLUDE "maps/Route15.asm"
INCLUDE "maps/Route16.asm"
INCLUDE "maps/Route17.asm"
INCLUDE "maps/Route18.asm"
INCLUDE "maps/Route19.asm"
INCLUDE "maps/Route20.asm"
INCLUDE "maps/Route21.asm"
INCLUDE "maps/Route22.asm"
INCLUDE "maps/Route23.asm"
INCLUDE "maps/Route24.asm"
INCLUDE "maps/RouteK1.asm"
INCLUDE "maps/RouteK2South.asm"
INCLUDE "maps/RouteK2North.asm"
INCLUDE "maps/RouteK3.asm"
INCLUDE "maps/RouteK4.asm"
INCLUDE "maps/RouteK5.asm"
INCLUDE "maps/RouteK6.asm"
INCLUDE "maps/RouteK7.asm"
INCLUDE "maps/RouteK8.asm"
INCLUDE "maps/RouteK9.asm"
INCLUDE "maps/RouteK10North.asm"
INCLUDE "maps/RouteK10South.asm"
INCLUDE "maps/RouteK11.asm"
INCLUDE "maps/RouteK12.asm"
INCLUDE "maps/RouteK13.asm"
INCLUDE "maps/RouteK14.asm"
INCLUDE "maps/RouteK15.asm"
INCLUDE "maps/RouteK16.asm"
INCLUDE "maps/RouteK17.asm"
INCLUDE "maps/RouteK18.asm"
INCLUDE "maps/RouteK19.asm"
INCLUDE "maps/RouteK20.asm"
INCLUDE "maps/RouteK21.asm"
INCLUDE "maps/RouteK22.asm"
INCLUDE "maps/RouteK23.asm"
INCLUDE "maps/RouteK24.asm"
INCLUDE "maps/RouteK25.asm"
INCLUDE "maps/QuietHill.asm"
INCLUDE "maps/WestPort.asm"
INCLUDE "maps/FastShipExt.asm"
INCLUDE "maps/FastShipDeck.asm"
INCLUDE "maps/FastShipInt1.asm"
INCLUDE "maps/HitekkPort.asm"
INCLUDE "maps/Route15Pokecenter1F.asm"
INCLUDE "maps/NewtypePokecenter1F.asm"
INCLUDE "maps/Route14to15Gate.asm"
INCLUDE "maps/Route14to15GateUpstairs.asm"
INCLUDE "maps/RuinsOfAlphEntrance.asm"
INCLUDE "maps/RuinsOfAlph.asm"
INCLUDE "maps/RouteK22Gate.asm"


SECTION "Map Scripts 22", ROMX
SECTION "Map Scripts 23", ROMX

INCLUDE "maps/SilentHill.asm"
INCLUDE "maps/OldCity.asm"
INCLUDE "maps/WestCity.asm"
INCLUDE "maps/Birdon.asm"
INCLUDE "maps/HitekkCity.asm"
INCLUDE "maps/FountTown.asm"
INCLUDE "maps/SouthCity.asm"
INCLUDE "maps/NewtypeCity.asm"
INCLUDE "maps/SugarIsland.asm"
INCLUDE "maps/BlueForest.asm"
INCLUDE "maps/NorthTown.asm"
INCLUDE "maps/StandCity.asm"
INCLUDE "maps/PrinceTown.asm"

INCLUDE "maps/PalletTown.asm"
INCLUDE "maps/ViridianCity.asm"
INCLUDE "maps/PewterCity.asm"
INCLUDE "maps/CeruleanCity.asm"
INCLUDE "maps/VermilionCity.asm"
INCLUDE "maps/CeladonCity.asm"
INCLUDE "maps/LavenderTown.asm"
INCLUDE "maps/SaffronCity.asm"
INCLUDE "maps/FuchsiaCity.asm"
INCLUDE "maps/CinnabarIsland.asm"
INCLUDE "maps/IndigoPlateau.asm"

INCLUDE "maps/PalletDummy.asm"


SECTION "Map Scripts 24", ROMX
SECTION "Map Scripts 25", ROMX
