spawn: MACRO
; map, x, y
	map_id \1
	db \2, \3
ENDM

SpawnPoints: ; 0x152ab
; entries correspond to SPAWN_* constants

	spawn PLAYERS_HOUSE_2F,            3,  3
	spawn PLAYERS_HOUSE_2F,            3,  3
	spawn SILENT_HILL,                 5,  5
	spawn OLD_CITY,                   27, 29
	spawn WEST_CITY,                  25, 15
	spawn HITEKK_CITY,                31, 11
	spawn FOUNT_TOWN,                  3, 13
	spawn SOUTH_CITY,                 33, 15
	spawn BIRDON,                     15,  5
	spawn NEWTYPE_CITY,                7,  9
	spawn SUGAR_ISLAND,                9, 11
	spawn PLAYERS_HOUSE_2F,            3,  3 ; Dummy for Map Change
	spawn STAND_CITY,                 33, 21
	spawn BLUE_FOREST,                13, 19
	spawn NORTH_TOWN,                 17, 13
	spawn PLAYERS_HOUSE_2F,            3,  3
	
	spawn PLAYERS_HOUSE_2F,            3,  3 ; Dummy for Map Change
	
	spawn PALLET_TOWN,                 5,  6
	spawn VIRIDIAN_CITY,              23, 26
	spawn PEWTER_CITY,                13, 26
	spawn CERULEAN_CITY,              19, 18
	spawn VERMILION_CITY,             11,  4
	spawn CELADON_CITY,               41, 10
	spawn LAVENDER_TOWN,               3,  6
	spawn SAFFRON_CITY,               11, 34
	spawn FUCHSIA_CITY,               19, 28
	spawn CINNABAR_ISLAND,            13, 20
	spawn INDIGO_PLATEAU,              9,  6
	
	spawn ROUTE_15,                    9, 11
	spawn ROUTE_18,                   13, 29
	spawn ROUTE_K4,                    0,  0
	spawn ROUTE_K10_NORTH,             0,  0
	
	spawn N_A,                        -1, -1
; 1531f
