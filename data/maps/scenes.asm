scene_var: MACRO
; map, variable
	map_id \1
	dw \2
ENDM

MapScenes:: ; 4d01e
	scene_var POKECENTER_2F,                wPokecenter2FSceneID
	scene_var TRADE_CENTER,                 wTradeCenterSceneID
	scene_var COLOSSEUM,                    wColosseumSceneID
	scene_var TIME_CAPSULE,                 wTimeCapsuleSceneID
	scene_var SILENT_HILL,                  wSilentHillSceneID
	scene_var OAKS_LAB,                     wOaksLabSceneID
	scene_var PLAYERS_HOUSE_1F,             wPlayersHouse1FSceneID
	scene_var OAKS_LAB_BACK,                wOaksLabBackSceneID
	scene_var PLAYERS_HOUSE_2F,             wPlayersHouse2FSceneID
	scene_var FAST_SHIP_EXT,                wFastShipExtSceneID
	scene_var WEST_PORT,                    wWestPortSceneID
	scene_var HITEKK_PORT,                  wHitekkPortSceneID
	scene_var CINNABAR_ISLAND,              wCinnabarIslandSceneID
	scene_var PALLET_DUMMY,                 wPalletDummySceneID
	db -1
; 4d15b
