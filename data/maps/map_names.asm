NameBankTable::
	dw MapNames1
	dw MapNames2
	dw MapNames3
	dw MapNames4
	dw MapNames5
	dw MapNames6
	dw MapNames7
	dw MapNames8
	dw MapNames9
	dw MapNames10
	dw MapNames11
	dw MapNames12
	dw MapNames13
	dw MapNames14
	dw MapNames15
	dw MapNames16
	dw MapNames17
	dw MapNames18
	dw MapNames19
	dw MapNames20
	dw MapNames21
	dw MapNames22
	dw MapNames23
	dw MapNames24
	dw MapNames25
	dw MapNames26

MapNames::
MapNames1:: ; bank 1
	db "West City@"
	db "West City PMC@"
	db "West City Rkt. House@"
	db "West City House 1@"
	db "West City House 2@"
	db "West City Dept. 1F@"
	db "West City Dept. 2F@"
	db "West City Dept. 3F@"
	db "West City Dept. 4F@"
	db "West City Dept. 5F@"
	db "West City Dept. 6F@"
	db "West Dept. Elevator@"
	db "West City Port@"
MapNames2:: ; bank 2
	db "R.12 -West City Gate@"
	db "R.12 -West Gate Upst@"
	db "Birdon@"
	db "Route 12@"
	db "Route 13@"
	db "Route 14@"
	db "Birdon PMC@"
MapNames3:: ; bank 3
	db "Sprout Tower@"
	db "Radio Tower 1F@"
	db "Radio Tower 2F@"
	db "Radio Tower 3F@"
	db "Radio Tower 4F@"
	db "Radio Tower 5F@"
	db "Fast Ship Exteriors@"
	db "Fast Ship Deck@"
	db "FastShip Interiors 1@"
	db "Ruins Of Alph entr.@"
	db "Ruins Of Alph@"
MapNames4:: ; bank 4
	db "Hitekk City@"
	db "Hitekk Port@"
	db "Route 3@"
	db "Route 4@"
	db "Route 5@"
	db "Hitekk City PMC@"
	db "Hitekk City Mart@"
MapNames5:: ; bank 5
	db "Route 10@"
	db "Route 11@"
	db "Fount Town@"
	db "Route 9@"
	db "Route 6@"
	db "Route 7@"
	db "Route 8@"
	db "Fount Town PMC@"
MapNames6:: ; bank 6
	db "South City@"
	db "R.4-South City Gate@"
	db "R.4-South Gate Upst@"
	db "R.9-South City Gate@"
	db "R.9-South Gate Upst@"
	db "South City PMC@"
MapNames7:: ; bank 7
	db "Route 15@"
	db "Newtype City@"
	db "Route 15 PMC@"
	db "Newtype City PMC@"
	db "R.14 - R.15 Gate@"
	db "R.14 - R.15 Gate Up@"
	db "Route 17@"
	db "Route 18@"
	db "Route 18 PMC@"
MapNames8:: ; bank 8
	db "Sugar Island@"
	db "Route 16@"
	db "Sugar Island PMC@"
MapNames9:: ; bank 9
	db "Route 23@"
	db "Blue Forest@"
	db "Route 24@"
	db "Route 22@"
	db "North Town@"
	db "Blue Forest PMC@"
	db "North Town PMC@"
MapNames10:: ; bank 10
	db "Route 2@"
	db "Old City@"
	db "Old City Mart@"
	db "Old City Gym@"
	db "Old City Museum@"
	db "Old City Kurt's House@"
	db "Old City PMC@"
	db "Old City Bill's House@"
	db "Old City House@"
	db "Old City School@"
	db "R.2 - West City Gate@"
	db "R.2 - West Gate Upst@"
	db "Route 2 Game House@"
MapNames11:: ; bank 11
	db "Stand City@"
	db "Route 21@"
	db "Stand City PMC@"
MapNames12:: ; bank 12
	db "Pewter City@"
	db "Kanto Route 3@"
	db "Kanto Route 2 North@"
MapNames13:: ; bank 13
	db "Pallet Town@"
	db "Kanto Route 1@"
	db "Viridian City@"
	db "Kanto Route 22@"
	db "Kanto Route 2 South@"
	db "Kanto Route 20@"
	db "Cinnabar Island@"
	db "Kanto Route 21@"
	db "Pallet After Evac.@"
MapNames14:: ; bank 14
	db "Kanto Route 4@"
	db "Cerulean City@"
	db "Kanto Route 24@"
	db "Kanto Route 25@"
	db "Kanto Route 9@"
	db "Kanto Route 10 North@"
	db "Kanto Route 5@"
MapNames15:: ; bank 15
	db "Kanto Route 10 South@"
	db "Lavender Town@"
	db "Route 20@"
	db "Kanto Route 8@"
	db "Kanto Route 12@"
	db "Kanto Route 13@"
	db "Kanto Route 14@"
	db "Kanto Route 15@"
MapNames16:: ; bank 16
	db "Kanto Route 6@"
	db "Vermilion City@"
	db "Kanto Route 11@"
MapNames17:: ; bank 17
MapNames18:: ; bank 18
	db "Saffron City@"
	db "Kanto Route 23@"
	db "Indigo Plateau@"
MapNames19:: ; bank 19
	db "Kanto Route 7@"
	db "Celadon City@"
	db "Kanto Route 16@"
	db "Kanto Route 17@"
MapNames20:: ; bank 20
	db "Pokemon Center 2F@"
	db "Trade Center@"
	db "Colosseum@"
	db "Time Capsule@"
	db "Mobile Trade Room@"
	db "Mobile Battle Room@"
MapNames21:: ; bank 21
	db "Kanto Route 18@"
	db "Fuchsia City@"
MapNames22:: ; bank 22
	db "Kanto Route 19@"
MapNames23:: ; bank 23
MapNames24:: ; bank 24
	db "Route 1@"
	db "Silent Hill@"
	db "Silent Hill Lab@"
	db "<PLAYER>'s House 1F@"
	db "<PLAYER>'s House 2F@"
	db "Silent Hill PMC@"
	db "<RIVAL>'s House@"
	db "Silent Hill Lab Back@"
	db "Prince Town@"
	db "Route 19@"
	db "Kanto Route 22 Gate@"
MapNames25:: ; bank 25
MapNames26:: ; bank 26
	db "Quiet Hill@"
	db "R.1 - Old City Gate@"
	db "R.1 - Old City Upstr@"
