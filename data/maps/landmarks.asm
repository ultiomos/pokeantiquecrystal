landmark: MACRO
; x, y, name
	db \1, \2
	dw \3
ENDM

Landmarks: ; 0x1ca8c3
; entries correspond to constants/landmark_constants.asm
	landmark   0,   0, SpecialMapName
	landmark 108, 132, SilentHillName
	landmark 100, 132, Route1Name
	landmark  92, 132, QuietHillName
	landmark  92, 116, OldCityName
	landmark  94, 114, SproutTowerName
	landmark  80, 116, Route2Name
	landmark  68, 116, WestCityName
	landmark  70, 114, RadioTowerName
	landmark   0,   0, OpenOceanName
	landmark  44, 108, HitekkCityName
	landmark  36, 108, Route3Name
	landmark  20, 100, Route4Name
	landmark  44,  96, Route5Name
	landmark  44,  84, FountTownName
	landmark  46,  82, RuinsOfAlphName
	landmark  44,  72, Route6Name
	landmark  32,  68, Route7Name
	landmark  20,  72, Route8Name
	landmark  20,  84, SouthCityName
	landmark  32,  84, Route9Name
	landmark  56,  84, Route10Name
	landmark  68,  84, Route11Name
	landmark  68,  92, BirdonName
	landmark  68, 104, Route12Name
	landmark  84,  92, Route13Name
	landmark 108,  96, Route14Name
	landmark 116, 100, Route15Name
	landmark 124, 100, NewtypeCityName
	landmark 124,  88, Route16Name
	landmark 124,  76, SugarIslandName
	landmark 132, 100, Route17Name
	landmark 140,  96, Route18Name
	landmark 120, 132, Route19Name
	landmark 132, 132, KantoName
	landmark 144, 132, Route20Name
	landmark 156, 120, Route21Name
	landmark 156, 100, StandCityName
	landmark 156,  88, Route22Name
	landmark 156,  76, BlueForestName
	landmark 144,  76, Route23Name
	landmark 156,  64, Route24Name
	landmark 156,  52, NorthTownName
	landmark 108, 124, Route25Name
	landmark 108, 116, PrinceTownName
	
	landmark  24,  72, MapReturnName
	landmark  60, 124, PalletTownName
	landmark  60, 108, RouteK1Name
	landmark  60,  92, ViridianCityName
	landmark  60,  80, RouteK2Name
	landmark  62,  78, ViridianForestName
	landmark  60,  68, PewterCityName
	landmark  72,  68, RouteK3Name
	landmark  84,  68, MtMoonName
	landmark  96,  68, RouteK4Name
	landmark 108,  68, CeruleanCityName
	landmark 108,  56, RouteK24Name
	landmark 120,  52, RouteK25Name
	landmark 108,  76, RouteK5Name
	landmark 108,  92, RouteK6Name
	landmark 108, 100, VermilionCityName
	landmark  84,  84, CeladonCityName
	landmark  96,  84, RouteK7Name
	landmark 124,  84, RouteK8Name
	landmark 140,  84, LavenderTownName
	landmark 142,  82, PokemonTowerName
	landmark 158,  84, Route20Name
	landmark 108,  84, SaffronCityName
	landmark 124,  68, RouteK9Name
	landmark 140,  72, RouteK10Name
	landmark 140,  68, RockTunnelName
	landmark 140,  76, KantoPowerPlantName
	landmark 124, 100, RouteK11Name
	landmark 116, 100, DiglettsCaveName
	landmark 140, 100, RouteK12Name
	landmark 132, 116, RouteK13Name
	landmark 124, 128, RouteK14Name
	landmark 112, 132, RouteK15Name
	landmark 100, 132, FuchsiaCityName
	landmark  76,  84, RouteK16Name
	landmark  76, 108, RouteK17Name
	landmark  84, 132, RouteK18Name
	landmark 100, 144, RouteK19Name
	landmark  84, 148, RouteK20Name
	landmark  76, 148, SeafoamIslandsName
	landmark  60, 148, CinnabarIslandName
	landmark  60, 136, RouteK21Name
	landmark  48,  84, RouteK22Name
	landmark  20,  84, Route19Name
	landmark  36,  76, RouteK23Name
	landmark  36,  68, VictoryRoadName
	landmark  36,  52, IndigoPlateauName
; 0x1caa43

SilentHillName:      db "Silent Hill@"
QuietHillName:       db "Quiet Hill@"
OldCityName:         db "Old City@"
WestCityName:        db "West City@"
OpenOceanName:       db "Open Ocean@"
HitekkCityName:      db "Hitekk City@"
FountTownName:       db "Fount Town@"
SouthCityName:       db "South City@"
BirdonName:          db "Birdon@"
NewtypeCityName:     db "Newtype¯City@"
SugarIslandName:     db "Sugar¯Island@"
KantoName:           db "Kanto¯Region@"
StandCityName:       db "Stand City@"
BlueForestName:      db "Blue Forest@"
NorthTownName:       db "North Town@"
PrinceTownName:      db "Prince Town@"
Route1Name:          db "Route 1@"
Route2Name:          db "Route 2@"
Route3Name:          db "Route 3@"
Route4Name:          db "Route 4@"
Route5Name:          db "Route 5@"
Route6Name:          db "Route 6@"
Route7Name:          db "Route 7@"
Route8Name:          db "Route 8@"
Route9Name:          db "Route 9@"
Route10Name:         db "Route 10@"
Route11Name:         db "Route 11@"
Route12Name:         db "Route 12@"
Route13Name:         db "Route 13@"
Route14Name:         db "Route 14@"
Route15Name:         db "Route 15@"
Route16Name:         db "Route 16@"
Route17Name:         db "Route 17@"
Route18Name:         db "Route 18@"
Route19Name:         db "Route 19@"
Route20Name:         db "Route 20@"
Route21Name:         db "Route 21@"
Route22Name:         db "Route 22@"
Route23Name:         db "Route 23@"
Route24Name:         db "Route 24@"
Route25Name:         db "Route 25@"
SproutTowerName:     db "Sprout¯Tower@"
RadioTowerName:      db "Radio Tower@"
RuinsOfAlphName:     db "Ruins of¯Alph@"

PalletTownName:      db "Pallet Town@"
ViridianCityName:    db "Viridian¯City@"
PewterCityName:      db "Pewter City@"
CeruleanCityName:    db "Cerulean¯City@"
VermilionCityName:   db "Vermilion¯City@"
CeladonCityName:     db "Celadon¯City@"
LavenderTownName:    db "Lavender¯Town@"
SaffronCityName:     db "Saffron¯City@"
FuchsiaCityName:     db "Fuchsia¯City@"
CinnabarIslandName:  db "Cinnabar¯Island@"
IndigoPlateauName:   db "Indigo¯Plateau@"
RouteK1Name:         db "Kanto¯Route 1@"
RouteK2Name:         db "Kanto¯Route 2@"
RouteK3Name:         db "Kanto¯Route 3@"
RouteK4Name:         db "Kanto¯Route 4@"
RouteK5Name:         db "Kanto¯Route 5@"
RouteK6Name:         db "Kanto¯Route 6@"
RouteK7Name:         db "Kanto¯Route 7@"
RouteK8Name:         db "Kanto¯Route 8@"
RouteK9Name:         db "Kanto¯Route 9@"
RouteK10Name:        db "Kanto¯Route 10@"
RouteK11Name:        db "Kanto¯Route 11@"
RouteK12Name:        db "Kanto¯Route 12@"
RouteK13Name:        db "Kanto¯Route 13@"
RouteK14Name:        db "Kanto¯Route 14@"
RouteK15Name:        db "Kanto¯Route 15@"
RouteK16Name:        db "Kanto¯Route 16@"
RouteK17Name:        db "Kanto¯Route 17@"
RouteK18Name:        db "Kanto¯Route 18@"
RouteK19Name:        db "Kanto¯Route 19@"
RouteK20Name:        db "Kanto¯Route 20@"
RouteK21Name:        db "Kanto¯Route 21@"
RouteK22Name:        db "Kanto¯Route 22@"
RouteK23Name:        db "Kanto¯Route 23@"
RouteK24Name:        db "Kanto¯Route 24@"
RouteK25Name:        db "Kanto¯Route 25@"
ViridianForestName:  db "Viridian¯Forest@"
MtMoonName:          db "Mt. Moon@"
PokemonTowerName:    db "#mon¯Tower@"
RockTunnelName:      db "Rock Tunnel@"
KantoPowerPlantName: db "Kanto Power¯Plant@"
DiglettsCaveName:    db "Diglett's¯Cave@"
SeafoamIslandsName:  db "Seafoam¯Islands@"
VictoryRoadName:     db "Victory¯Road@"
MapReturnName:       db "Return to¯Nihon@"
SpecialMapName:      db "SPECIAL@"
