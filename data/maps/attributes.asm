map_attributes: MACRO
;\1: map name
;\2: map id
;\3: border block
;\4: connections: combo of NORTH, SOUTH, WEST, and/or EAST, or 0 for none
CURRENT_MAP_WIDTH = \2_WIDTH
CURRENT_MAP_HEIGHT = \2_HEIGHT
\1_MapAttributes::
	db \3
	db CURRENT_MAP_HEIGHT, CURRENT_MAP_WIDTH
	db BANK(\1_Blocks)
	dw \1_Blocks
	db BANK(\1_MapScripts) ; BANK(\1_MapEvents)
	dw \1_MapScripts
	dw \1_MapEvents
	db \4
ENDM

; Connections go in order: north, south, west, east
connection: MACRO
;\1: direction
;\2: map name
;\3: map id
;\4: x offset for east/west, y offset for north/south
;\5: distance offset?
;\6: strip length
if "\1" == "north"
	map_id \3
	dw \2_Blocks + \3_WIDTH * (\3_HEIGHT - 3) + \5
	dw wOverworldMapBlocks + \4 + 3
	db \6
	db \3_WIDTH
	db \3_HEIGHT * 2 - 1
	db (\4 - \5) * -2
	dw wOverworldMapBlocks + \3_HEIGHT * (\3_WIDTH + 6) + 1
elif "\1" == "south"
	map_id \3
	dw \2_Blocks + \5
	dw wOverworldMapBlocks + (CURRENT_MAP_HEIGHT + 3) * (CURRENT_MAP_WIDTH + 6) + \4 + 3
	db \6
	db \3_WIDTH
	db 0
	db (\4 - \5) * -2
	dw wOverworldMapBlocks + \3_WIDTH + 7
elif "\1" == "west"
	map_id \3
	dw \2_Blocks + (\3_WIDTH * \5) + \3_WIDTH - 3
	dw wOverworldMapBlocks + (CURRENT_MAP_WIDTH + 6) * (\4 + 3)
	db \6
	db \3_WIDTH
	db (\4 - \5) * -2
	db \3_WIDTH * 2 - 1
	dw wOverworldMapBlocks + \3_WIDTH * 2 + 6
elif "\1" == "east"
	map_id \3
	dw \2_Blocks + (\3_WIDTH * \5)
	dw wOverworldMapBlocks + (CURRENT_MAP_WIDTH + 6) * (\4 + 3 + 1) - 3
	db \6
	db \3_WIDTH
	db (\4 - \5) * -2
	db 0
	dw wOverworldMapBlocks + \3_WIDTH + 7
endc
ENDM


	map_attributes SilentHill, SILENT_HILL, $2f, NORTH | WEST | EAST
	connection north, PrinceTown, PRINCE_TOWN, 0, 0, 10
	connection west, Route1, ROUTE_1, -3, 6, 12
	connection east, Route19, ROUTE_19, 0, 0, 9

	map_attributes OldCity, OLD_CITY, $2f, SOUTH | WEST
	connection south, Route1, ROUTE_1, 5, 0, 15
	connection west, Route2, ROUTE_2, 5, 0, 9

	map_attributes QuietHill, QUIET_HILL, $3f, 0

	map_attributes Route2, ROUTE_2, $2f, WEST | EAST
	connection west, WestCity, WEST_CITY, -5, 0, 12
	connection east, OldCity, OLD_CITY, -5, 0, 18

	map_attributes Route1, ROUTE_1, $2f, NORTH | EAST
	connection north, OldCity, OLD_CITY, 0, 5, 15
	connection east, SilentHill, SILENT_HILL, 9, 0, 9
	
	map_attributes PrinceTown, PRINCE_TOWN, $2f, SOUTH
	connection south, SilentHill, SILENT_HILL, 0, 0, 10
	
	map_attributes WestCity, WEST_CITY, $2f, NORTH | EAST
	connection north, Route12, ROUTE_12, 5, 0, 10
	connection east, Route2, ROUTE_2, 5, 0, 9
	
	map_attributes Birdon, BIRDON, $2f, NORTH | SOUTH | EAST
	connection north, Route11, ROUTE_11, 0, 0, 10
	connection south, Route12, ROUTE_12, 0, 0, 10
	connection east, Route13, ROUTE_13, 0, 0, 9
	
	map_attributes Route13, ROUTE_13, $2f, WEST | EAST
	connection west, Birdon, BIRDON, 0, 0, 9
	connection east, Route14, ROUTE_14, 0, 0, 9
	
	map_attributes Route14, ROUTE_14, $2f, SOUTH | WEST
	connection south, Route15, ROUTE_15, 0, 0, 10
	connection west, Route13, ROUTE_13, 0, 0, 9
	
	map_attributes Route15, ROUTE_15, $2f, NORTH | EAST
	connection north, Route14, ROUTE_14, 0, 0, 10
	connection east, NewtypeCity, NEWTYPE_CITY, -1, 0, 12
	
	map_attributes Route16, ROUTE_16, $21, NORTH | SOUTH
	connection north, SugarIsland, SUGAR_ISLAND, 0, 0, 10
	connection south, NewtypeCity, NEWTYPE_CITY, 0, 5, 10
	
	map_attributes Route12, ROUTE_12, $2f, NORTH | SOUTH
	connection north, Birdon, BIRDON, 0, 0, 10
	connection south, WestCity, WEST_CITY, 0, 5, 10
	
	map_attributes HitekkCity, HITEKK_CITY, $2f, NORTH | WEST
	connection north, Route5, ROUTE_5, 9, 0, 10
	connection west, Route3, ROUTE_3, 0, 0, 9
	
	map_attributes Route3, ROUTE_3, $2f, WEST | EAST
	connection west, Route4, ROUTE_4, 0, 19, 8
	connection east, HitekkCity, HITEKK_CITY, 0, 0, 9
	
	map_attributes Route4, ROUTE_4, $2f, NORTH | EAST
	connection north, SouthCity, SOUTH_CITY, 0, 10, 10
	connection east, Route3, ROUTE_3, 19, 0, 9
	
	map_attributes Route10, ROUTE_10, $2f, WEST | EAST
	connection west, FountTown, FOUNT_TOWN, 0, 0, 9
	connection east, Route11, ROUTE_11, 0, 0, 9
	
	map_attributes Route11, ROUTE_11, $2f, SOUTH | WEST
	connection south, Birdon, BIRDON, 0, 0, 10
	connection west, Route10, ROUTE_10, 0, 0, 9
	
	map_attributes FountTown, FOUNT_TOWN, $2f, NORTH | SOUTH | WEST | EAST
	connection north, Route6, ROUTE_6, 0, 0, 10
	connection south, Route5, ROUTE_5, 0, 0, 10
	connection west, Route9, ROUTE_9, 0, 0, 10
	connection east, Route10, ROUTE_10, 0, 0, 9
	
	map_attributes Route6, ROUTE_6, $21, SOUTH | WEST
	connection south, FountTown, FOUNT_TOWN, 0, 0, 10
	connection west, Route7, ROUTE_7, 0, 0, 9
	
	map_attributes Route7, ROUTE_7, $21, WEST | EAST
	connection west, Route8, ROUTE_8, 0, 0, 12
	connection east, Route6, ROUTE_6, 0, 0, 12
	
	map_attributes Route8, ROUTE_8, $21, SOUTH | EAST
	connection south, SouthCity, SOUTH_CITY, 0, 10, 10
	connection east, Route7, ROUTE_7, 0, 0, 9
	
	map_attributes SouthCity, SOUTH_CITY, $2f, NORTH | SOUTH | EAST
	connection north, Route8, ROUTE_8, 10, 0, 10
	connection south, Route4, ROUTE_4, 10, 0, 10
	connection east, Route9, ROUTE_9, 5, 0, 9
	
	map_attributes Route9, ROUTE_9, $2f, WEST | EAST
	connection west, SouthCity, SOUTH_CITY, 0, 5, 9
	connection east, FountTown, FOUNT_TOWN, 0, 0, 9
	
	map_attributes Route5, ROUTE_5, $21, NORTH | SOUTH
	connection north, FountTown, FOUNT_TOWN, 0, 0, 9
	connection south, HitekkCity, HITEKK_CITY, 0, 9, 9
	
	map_attributes SugarIsland, SUGAR_ISLAND, $21, SOUTH
	connection south, Route16, ROUTE_16, 0, 0, 10

	map_attributes NewtypeCity, NEWTYPE_CITY, $2f, NORTH | WEST | EAST
	connection north, Route16, ROUTE_16, 5, 0, 10
	connection west, Route15, ROUTE_15, 1, 0, 9
	connection east, Route17, ROUTE_17, 9, 0, 9
	
	map_attributes Route17, ROUTE_17, $2f, WEST | EAST
	connection west, NewtypeCity, NEWTYPE_CITY, 0, 9, 9
	connection east, Route18, ROUTE_18, 0, 36, 9
	
	map_attributes Route18, ROUTE_18, $7a, NORTH | WEST
	connection north, Route23, ROUTE_23, 0, 0, 10
	connection west, Route17, ROUTE_17, 36, 0, 9
	
	map_attributes Route23, ROUTE_23, $2f, SOUTH | EAST
	connection south, Route18, ROUTE_18, 0, 0, 10
	connection east, BlueForest, BLUE_FOREST, 0, 9, 9
	
	map_attributes BlueForest, BLUE_FOREST, $2f, NORTH | SOUTH | WEST
	connection north, Route24, ROUTE_24, 5, 0, 10
	connection south, Route22, ROUTE_22, 5, 0, 10
	connection west, Route23, ROUTE_23, 9, 0, 9
	
	map_attributes Route24, ROUTE_24, $21, NORTH | SOUTH
	connection north, NorthTown, NORTH_TOWN, 0, 2, 12
	connection south, BlueForest, BLUE_FOREST, 0, 5, 10
	
	map_attributes NorthTown, NORTH_TOWN, $21, SOUTH
	connection south, Route24, ROUTE_24, 2, 0, 10
	
	map_attributes Route22, ROUTE_22, $2f, NORTH | SOUTH
	connection north, BlueForest, BLUE_FOREST, 0, 5, 10
	connection south, StandCity, STAND_CITY, 0, 10, 10
	
	map_attributes StandCity, STAND_CITY, $2f, NORTH | SOUTH
	connection north, Route22, ROUTE_22, 10, 0, 10
	connection south, Route21, ROUTE_21, 10, 0, 10
	
	map_attributes Route21, ROUTE_21, $2f, NORTH | SOUTH
	connection north, StandCity, STAND_CITY, 0, 10, 10
	connection south, Route20, ROUTE_20, 0, 21, 10
	
	map_attributes Route19, ROUTE_19, $2f, WEST | EAST
	connection west, SilentHill, SILENT_HILL, 0, 0, 9
	connection east, RouteK22, ROUTE_K22, 2, 0, 9
	
	map_attributes PalletTown, PALLET_TOWN, $2f, NORTH | SOUTH
	connection north, RouteK1, ROUTE_K1, 0, 0, 10
	connection south, RouteK21, ROUTE_K21, 0, 0, 10
	
	map_attributes RouteK1, ROUTE_K1, $2f, NORTH | SOUTH
	connection north, ViridianCity, VIRIDIAN_CITY, 0, 5, 10
	connection south, PalletTown, PALLET_TOWN, 0, 0, 10
	
	map_attributes ViridianCity, VIRIDIAN_CITY, $2f, NORTH | SOUTH | WEST
	connection north, RouteK2South, ROUTE_K2_SOUTH, 5, 0, 10
	connection south, RouteK1, ROUTE_K1, 5, 0, 10
	connection west, RouteK22, ROUTE_K22, 4, 0, 9
	
	map_attributes RouteK22, ROUTE_K22, $76, NORTH | WEST | EAST
	connection north, RouteK23, ROUTE_K23, 2, 0, 10
	connection west, Route19, ROUTE_19, 0, 2, 9
	connection east, ViridianCity, VIRIDIAN_CITY, 0, 4, 9
	
	map_attributes RouteK23, ROUTE_K23, $2f, NORTH | SOUTH
	connection north, IndigoPlateau, INDIGO_PLATEAU, 0, 0, 10
	connection south, RouteK22, ROUTE_K22, 0, 2, 10
	
	map_attributes IndigoPlateau, INDIGO_PLATEAU, $2f, SOUTH
	connection south, RouteK23, ROUTE_K23, 0, 0, 10
	
	map_attributes RouteK2South, ROUTE_K2_SOUTH, $2f, NORTH | SOUTH
	connection north, RouteK2North, ROUTE_K2_NORTH, 0, 0, 10
	connection south, ViridianCity, VIRIDIAN_CITY, 0, 5, 10
	
	map_attributes RouteK2North, ROUTE_K2_NORTH, $2f, NORTH | SOUTH
	connection north, PewterCity, PEWTER_CITY, 0, 5, 10
	connection south, RouteK2South, ROUTE_K2_SOUTH, 0, 0, 10
	
	map_attributes PewterCity, PEWTER_CITY, $2f, SOUTH | EAST
	connection south, RouteK2North, ROUTE_K2_NORTH, 5, 0, 10
	connection east, RouteK3, ROUTE_K3, 4, 0, 9
	
	map_attributes RouteK3, ROUTE_K3, $76, NORTH | WEST
	connection north, RouteK4, ROUTE_K4, 25, 0, 10
	connection west, PewterCity, PEWTER_CITY, 0, 4, 9
	
	map_attributes RouteK4, ROUTE_K4, $76, SOUTH | EAST
	connection south, RouteK3, ROUTE_K3, 0, 25, 10
	connection east, CeruleanCity, CERULEAN_CITY, 0, 4, 9
	
	map_attributes CeruleanCity, CERULEAN_CITY, $2f, NORTH | SOUTH | WEST | EAST
	connection north, RouteK24, ROUTE_K24, 5, 0, 11
	connection south, RouteK5, ROUTE_K5, 5, 0, 10
	connection west, RouteK4, ROUTE_K4, 4, 0, 9
	connection east, RouteK9, ROUTE_K9, 4, 0, 9
	
	map_attributes RouteK5, ROUTE_K5, $2f, NORTH | SOUTH
	connection north, CeruleanCity, CERULEAN_CITY, 0, 5, 10
	connection south, SaffronCity, SAFFRON_CITY, 0, 6, 10
	
	map_attributes RouteK24, ROUTE_K24, $76, SOUTH | EAST
	connection south, CeruleanCity, CERULEAN_CITY, 0, 5, 11
	connection east, RouteK25, ROUTE_K25, 0, 0, 9
	
	map_attributes RouteK25, ROUTE_K25, $76, WEST
	connection west, RouteK24, ROUTE_K24, 0, 0, 9
	
	map_attributes RouteK9, ROUTE_K9, $76, WEST | EAST
	connection west, CeruleanCity, CERULEAN_CITY, 0, 4, 9
	connection east, RouteK10North, ROUTE_K10_NORTH, 0, 0, 9
	
	map_attributes RouteK10North, ROUTE_K10_NORTH, $76, SOUTH | WEST
	connection south, RouteK10South, ROUTE_K10_SOUTH, 0, 0, 10
	connection west, RouteK9, ROUTE_K9, 0, 0, 9
	
	map_attributes RouteK10South, ROUTE_K10_SOUTH, $76, NORTH | SOUTH
	connection north, RouteK10North, ROUTE_K10_NORTH, 0, 0, 10
	connection south, LavenderTown, LAVENDER_TOWN, 0, 0, 10
	
	map_attributes LavenderTown, LAVENDER_TOWN, $76, NORTH | SOUTH | WEST | EAST
	connection north, RouteK10South, ROUTE_K10_SOUTH, 0, 0, 10
	connection south, RouteK12, ROUTE_K12, 0, 0, 10
	connection west, RouteK8, ROUTE_K8, 0, 0, 9
	connection east, Route20, ROUTE_20, 0, 0, 9
	
	map_attributes Route20, ROUTE_20, $76, NORTH | WEST
	connection north, Route21, ROUTE_21, 21, 0, 10
	connection west, LavenderTown, LAVENDER_TOWN, 0, 0, 9
	
	map_attributes RouteK8, ROUTE_K8, $76, WEST | EAST
	connection west, SaffronCity, SAFFRON_CITY, 0, 6, 9
	connection east, LavenderTown, LAVENDER_TOWN, 0, 0, 9
	
	map_attributes RouteK6, ROUTE_K6, $2f, NORTH | SOUTH
	connection north, SaffronCity, SAFFRON_CITY, 0, 6, 10
	connection south, VermilionCity, VERMILION_CITY, 0, 5, 10
	
	map_attributes VermilionCity, VERMILION_CITY, $21, NORTH | EAST
	connection north, RouteK6, ROUTE_K6, 5, 0, 10
	connection east, RouteK11, ROUTE_K11, 4, 0, 9
	
	map_attributes RouteK11, ROUTE_K11, $2f, WEST | EAST
	connection west, VermilionCity, VERMILION_CITY, 0, 4, 9
	connection east, RouteK12, ROUTE_K12, 0, 27, 9
	
	map_attributes RouteK12, ROUTE_K12, $21, NORTH | SOUTH | WEST
	connection north, LavenderTown, LAVENDER_TOWN, 0, 0, 10
	connection south, RouteK13, ROUTE_K13, 0, 20, 10
	connection west, RouteK11, ROUTE_K11, 27, 0, 9
	
	map_attributes RouteK7, ROUTE_K7, $2f, WEST | EAST
	connection west, CeladonCity, CELADON_CITY, 0, 4, 9
	connection east, SaffronCity, SAFFRON_CITY, 0, 6, 9
	
	map_attributes CeladonCity, CELADON_CITY, $2f, WEST | EAST
	connection west, RouteK16, ROUTE_K16, 4, 0, 9
	connection east, RouteK7, ROUTE_K7, 4, 0, 9
	
	map_attributes RouteK16, ROUTE_K16, $2f, SOUTH | EAST
	connection south, RouteK17, ROUTE_K17, 0, 0, 10
	connection east, CeladonCity, CELADON_CITY, 0, 4, 9
	
	map_attributes RouteK17, ROUTE_K17, $21, NORTH | SOUTH
	connection north, RouteK16, ROUTE_K16, 0, 0, 10
	connection south, RouteK18, ROUTE_K18, 0, 0, 10
	
	map_attributes RouteK18, ROUTE_K18, $21, NORTH | EAST
	connection north, RouteK17, ROUTE_K17, 0, 0, 10
	connection east, FuchsiaCity, FUCHSIA_CITY, 0, 4, 9
	
	map_attributes FuchsiaCity, FUCHSIA_CITY, $2f, SOUTH | WEST | EAST
	connection south, RouteK19, ROUTE_K19, 5, 0, 10
	connection west, RouteK18, ROUTE_K18, 4, 0, 9
	connection east, RouteK15, ROUTE_K15, 4, 0, 9
	
	map_attributes RouteK13, ROUTE_K13, $21, NORTH | WEST
	connection north, RouteK12, ROUTE_K12, 20, 0, 10
	connection west, RouteK14, ROUTE_K14, 0, 0, 9
	
	map_attributes RouteK14, ROUTE_K14, $21, WEST | EAST
	connection west, RouteK15, ROUTE_K15, 18, 0, 9
	connection east, RouteK13, ROUTE_K13, 0, 0, 9
	
	map_attributes RouteK15, ROUTE_K15, $21, WEST | EAST
	connection west, FuchsiaCity, FUCHSIA_CITY, 0, 4, 9
	connection east, RouteK14, ROUTE_K14, 0, 18, 9
	
	map_attributes RouteK19, ROUTE_K19, $21, NORTH | WEST
	connection north, FuchsiaCity, FUCHSIA_CITY, 0, 5, 10
	connection west, RouteK20, ROUTE_K20, 18, 0, 9
	
	map_attributes RouteK20, ROUTE_K20, $21, WEST | EAST
	connection west, CinnabarIsland, CINNABAR_ISLAND, 0, 9, 12
	connection east, RouteK19, ROUTE_K19, 0, 18, 9
	
	map_attributes CinnabarIsland, CINNABAR_ISLAND, $21, NORTH | EAST
	connection north, RouteK21, ROUTE_K21, 0, 0, 10
	connection east, RouteK20, ROUTE_K20, 9, 0, 9
	
	map_attributes RouteK21, ROUTE_K21, $21, NORTH | SOUTH
	connection north, PalletTown, PALLET_TOWN, 0, 0, 10
	connection south, CinnabarIsland, CINNABAR_ISLAND, 0, 0, 10
	
	map_attributes SaffronCity, SAFFRON_CITY, $2f, NORTH | SOUTH | WEST | EAST
	connection north, RouteK5, ROUTE_K5, 6, 0, 10
	connection south, RouteK6, ROUTE_K6, 6, 0, 10
	connection west, RouteK7, ROUTE_K7, 6, 0, 9
	connection east, RouteK8, ROUTE_K8, 6, 0, 9
	
	map_attributes SproutTower, SPROUT_TOWER, $00, 0
	map_attributes OldMart, OLD_MART, $00, 0
	map_attributes OldGym, OLD_GYM, $00, 0
	map_attributes OldMuseum, OLD_MUSEUM, $00, 0
	map_attributes OldKurtsHouse, OLD_KURTS_HOUSE, $00, 0
	map_attributes OldPokecenter1F, OLD_POKECENTER_1F, $00, 0
	map_attributes OldBillsHouse, OLD_BILLS_HOUSE, $00, 0
	map_attributes OldHouse, OLD_HOUSE, $00, 0
	map_attributes OldSchool, OLD_SCHOOL, $00, 0
	map_attributes Pokecenter2F, POKECENTER_2F, $00, 0
	map_attributes TradeCenter, TRADE_CENTER, $00, 0
	map_attributes Colosseum, COLOSSEUM, $00, 0
	map_attributes TimeCapsule, TIME_CAPSULE, $00, 0
	map_attributes MobileTradeRoom, MOBILE_TRADE_ROOM, $00, 0
	map_attributes MobileBattleRoom, MOBILE_BATTLE_ROOM, $00, 0
	map_attributes OaksLab, OAKS_LAB, $00, 0
	map_attributes PlayersHouse1F, PLAYERS_HOUSE_1F, $00, 0
	map_attributes PlayersHouse2F, PLAYERS_HOUSE_2F, $00, 0
	map_attributes SilentPokecenter1F, SILENT_POKECENTER_1F, $00, 0
	map_attributes SilversHouse, SILVERS_HOUSE, $00, 0
	map_attributes OaksLabBack, OAKS_LAB_BACK, $00, 0
	map_attributes Route1OldCityGate, ROUTE_1_OLD_CITY_GATE, $00, 0
	map_attributes Route1OldCityGateUpstairs, ROUTE_1_OLD_CITY_GATE_UPSTAIRS, $00, 0
	map_attributes Route2WestCityGate, ROUTE_2_WEST_GATE, $00, 0
	map_attributes Route2WestCityGateUpstairs, ROUTE_2_WEST_GATE_UPSTAIRS, $00, 0
	map_attributes Route12WestCityGate, ROUTE_12_WEST_GATE, $00, 0
	map_attributes Route12WestCityGateUpstairs, ROUTE_12_WEST_GATE_UPSTAIRS, $00, 0
	map_attributes Route2GameHouse, ROUTE_2_GAME_HOUSE, $00, 0
	map_attributes WestPokecenter1F, WEST_POKECENTER_1F, $00, 0
	map_attributes HitekkPokecenter1F, HITEKK_POKECENTER_1F, $00, 0
	map_attributes Route15Pokecenter1F, ROUTE_15_POKECENTER_1F, $00, 0
	map_attributes NewtypePokecenter1F, NEWTYPE_POKECENTER_1F, $00, 0
	map_attributes HitekkMart, HITEKK_MART, $00, 0
	map_attributes WestRadio1F, WEST_RADIO_1F, $00, 0
	map_attributes WestRadio2F, WEST_RADIO_2F, $00, 0
	map_attributes WestRadio3F, WEST_RADIO_3F, $00, 0
	map_attributes WestRadio4F, WEST_RADIO_4F, $00, 0
	map_attributes WestRadio5F, WEST_RADIO_5F, $00, 0
	map_attributes WestRocketHouse, WEST_ROCKET_HOUSE, $00, 0
	map_attributes WestHouse1, WEST_HOUSE_1, $00, 0
	map_attributes WestHouse2, WEST_HOUSE_2, $00, 0
	map_attributes WestDept1F, WEST_DEPT_1F, $00, 0
	map_attributes WestDept2F, WEST_DEPT_2F, $00, 0
	map_attributes WestDept3F, WEST_DEPT_3F, $00, 0
	map_attributes WestDept4F, WEST_DEPT_4F, $00, 0
	map_attributes WestDept5F, WEST_DEPT_5F, $00, 0
	map_attributes WestDept6F, WEST_DEPT_6F, $00, 0
	map_attributes WestDeptElevator, WEST_DEPT_ELEVATOR, $00, 0
	map_attributes WestPort, WEST_PORT, $0a, 0
	map_attributes HitekkPort, HITEKK_PORT, $0a, 0
	map_attributes FastShipExt, FAST_SHIP_EXT, $14, 0
	map_attributes FastShipDeck, FAST_SHIP_DECK, $16, 0
	map_attributes FastShipInt1, FAST_SHIP_INT_1, $14, 0
	map_attributes Route4SouthCityGate, ROUTE_4_SOUTH_GATE, $00, 0
	map_attributes Route4SouthCityGateUpstairs, ROUTE_4_SOUTH_GATE_UPSTAIRS, $00, 0
	map_attributes Route9SouthCityGate, ROUTE_9_SOUTH_GATE, $00, 0
	map_attributes Route9SouthCityGateUpstairs, ROUTE_9_SOUTH_GATE_UPSTAIRS, $00, 0
	map_attributes Route14to15Gate, ROUTE_14_TO_15_GATE, $00, 0
	map_attributes Route14to15GateUpstairs, ROUTE_14_TO_15_GATE_UPSTAIRS, $00, 0
	map_attributes RuinsOfAlphEntrance, RUINS_OF_ALPH_ENTRANCE, $00, 0
	map_attributes RuinsOfAlph, RUINS_OF_ALPH, $00, 0
	map_attributes RouteK22Gate, ROUTE_K22_GATE, $00, 0
	map_attributes PalletDummy, PALLET_DUMMY, $00, 0
	map_attributes FountPokecenter1F, FOUNT_POKECENTER_1F, $00, 0
	map_attributes SouthPokecenter1F, SOUTH_POKECENTER_1F, $00, 0
	map_attributes BirdonPokecenter1F, BIRDON_POKECENTER_1F, $00, 0
	map_attributes SugarPokecenter1F, SUGAR_POKECENTER_1F, $00, 0
	map_attributes Route18Pokecenter1F, ROUTE_18_POKECENTER_1F, $00, 0
	map_attributes StandPokecenter1F, STAND_POKECENTER_1F, $00, 0
	map_attributes BluePokecenter1F, BLUE_POKECENTER_1F, $00, 0
	map_attributes NorthPokecenter1F, NORTH_POKECENTER_1F, $00, 0
