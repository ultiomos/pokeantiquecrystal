SECTION "Map Blocks 1", ROMX

Route10_Blocks:             INCBIN "maps/Route10.blk"
Route11_Blocks:             INCBIN "maps/Route11.blk"
HitekkCity_Blocks:          INCBIN "maps/HitekkCity.blk"
HitekkPort_Blocks:          INCBIN "maps/HitekkPort.blk"
PrinceTown_Blocks:          INCBIN "maps/PrinceTown.blk"
Route2_Blocks:              INCBIN "maps/Route2.blk"
SilversHouse_Blocks:        INCBIN "maps/SilversHouse.blk"
Pokecenter2F_Blocks:        INCBIN "maps/Pokecenter2F.blk"
Route1_Blocks:              INCBIN "maps/Route1.blk"
QuietHill_Blocks:           INCBIN "maps/QuietHill.blk"
Birdon_Blocks:              INCBIN "maps/Birdon.blk"
Route12_Blocks:             INCBIN "maps/Route12.blk"
WestPort_Blocks:            INCBIN "maps/WestPort.blk"
FastShipExt_Blocks:         INCBIN "maps/FastShipExt.blk"
FastShipDeck_Blocks:        INCBIN "maps/FastShipDeck.blk"
FastShipInt1_Blocks:        INCBIN "maps/FastShipInt1.blk"
PlayersHouse1F_Blocks:      INCBIN "maps/PlayersHouse1F.blk"
WestCity_Blocks:            INCBIN "maps/WestCity.blk"
WestDept1F_Blocks:          INCBIN "maps/WestDept1F.blk"
WestDept2F_Blocks:          INCBIN "maps/WestDept2F.blk"
WestDept3F_Blocks:          INCBIN "maps/WestDept3F.blk"
WestDept4F_Blocks:          INCBIN "maps/WestDept4F.blk"
WestDept5F_Blocks:          INCBIN "maps/WestDept5F.blk"
WestDept6F_Blocks:          INCBIN "maps/WestDept6F.blk"
WestDeptElevator_Blocks:    INCBIN "maps/WestDeptElevator.blk"
FountTown_Blocks:           INCBIN "maps/FountTown.blk"
SouthCity_Blocks:           INCBIN "maps/SouthCity.blk"
Route9_Blocks:              INCBIN "maps/Route9.blk"
Route5_Blocks:              INCBIN "maps/Route5.blk"
Route24_Blocks:             INCBIN "maps/Route24.blk"
Route22_Blocks:             INCBIN "maps/Route22.blk"
NewtypeCity_Blocks:         INCBIN "maps/NewtypeCity.blk"
BlueForest_Blocks:          INCBIN "maps/BlueForest.blk"
NorthTown_Blocks:           INCBIN "maps/NorthTown.blk"
RuinsOfAlphEntrance_Blocks: INCBIN "maps/RuinsOfAlphEntrance.blk"
RuinsOfAlph_Blocks:         INCBIN "maps/RuinsOfAlph.blk"

PalletDummy_Blocks:
PalletTown_Blocks:
	INCBIN "maps/PalletTown.blk"
	
Route2WestCityGate_Blocks:
Route9SouthCityGate_Blocks:
	INCBIN "maps/EastWestGate.blk"
Route1OldCityGate_Blocks:
Route12WestCityGate_Blocks:
Route4SouthCityGate_Blocks:
Route14to15Gate_Blocks:
	INCBIN "maps/NorthSouthGate.blk"
WestHouse1_Blocks:
WestHouse2_Blocks:
	INCBIN "maps/House1.blk"
	INCBIN "maps/House2.blk"

SECTION "Map Blocks 2", ROMX

OaksLab_Blocks:             INCBIN "maps/OaksLab.blk"
OaksLabBack_Blocks:         INCBIN "maps/OaksLabBack.blk"
OldMuseum_Blocks:           INCBIN "maps/OldMuseum.blk"
Colosseum_Blocks:           INCBIN "maps/Colosseum.blk"
OldKurtsHouse_Blocks:       INCBIN "maps/OldKurtsHouse.blk"
OldBillsHouse_Blocks:       INCBIN "maps/OldBillsHouse.blk"
SproutTower_Blocks:         INCBIN "maps/SproutTower.blk"
SilentHill_Blocks:          INCBIN "maps/SilentHill.blk"
OldCity_Blocks:             INCBIN "maps/OldCity.blk"
OldGym_Blocks:              INCBIN "maps/OldGym.blk"
OldHouse_Blocks:            INCBIN "maps/OldHouse.blk"
OldSchool_Blocks:           INCBIN "maps/OldSchool.blk"
Route2GameHouse_Blocks:     INCBIN "maps/Route2GameHouse.blk"
StandCity_Blocks:           INCBIN "maps/StandCity.blk"
ViridianCity_Blocks:        INCBIN "maps/ViridianCity.blk"
CeruleanCity_Blocks:        INCBIN "maps/CeruleanCity.blk"
Route3_Blocks:              INCBIN "maps/Route3.blk"
Route4_Blocks:              INCBIN "maps/Route4.blk"
Route13_Blocks:             INCBIN "maps/Route13.blk"
Route14_Blocks:             INCBIN "maps/Route14.blk"
Route15_Blocks:             INCBIN "maps/Route15.blk"
Route16_Blocks:             INCBIN "maps/Route16.blk"
Route17_Blocks:             INCBIN "maps/Route17.blk"
Route18_Blocks:             INCBIN "maps/Route18.blk"
Route20_Blocks:             INCBIN "maps/Route20.blk"
Route23_Blocks:             INCBIN "maps/Route23.blk"
Route6_Blocks:              INCBIN "maps/Route6.blk"
Route7_Blocks:              INCBIN "maps/Route7.blk"
Route8_Blocks:              INCBIN "maps/Route8.blk"
Route19_Blocks:             INCBIN "maps/Route19.blk"
Route21_Blocks:             INCBIN "maps/Route21.blk"
RouteK1_Blocks:             INCBIN "maps/RouteK1.blk"
RouteK5_Blocks:             INCBIN "maps/RouteK5.blk"
RouteK6_Blocks:             INCBIN "maps/RouteK6.blk"
RouteK7_Blocks:             INCBIN "maps/RouteK7.blk"
RouteK2South_Blocks:        INCBIN "maps/RouteK2South.blk"
RouteK2North_Blocks:        INCBIN "maps/RouteK2North.blk"
RouteK22_Blocks:            INCBIN "maps/RouteK22.blk"
RouteK3_Blocks:             INCBIN "maps/RouteK3.blk"
RouteK4_Blocks:             INCBIN "maps/RouteK4.blk"
RouteK9_Blocks:             INCBIN "maps/RouteK9.blk"
RouteK8_Blocks:             INCBIN "maps/RouteK8.blk"
RouteK24_Blocks:            INCBIN "maps/RouteK24.blk"
RouteK25_Blocks:            INCBIN "maps/RouteK25.blk"
RouteK11_Blocks:            INCBIN "maps/RouteK11.blk"
RouteK12_Blocks:            INCBIN "maps/RouteK12.blk"
RouteK16_Blocks:            INCBIN "maps/RouteK16.blk"
RouteK17_Blocks:            INCBIN "maps/RouteK17.blk"
RouteK18_Blocks:            INCBIN "maps/RouteK18.blk"
RouteK23_Blocks:            INCBIN "maps/RouteK23.blk"
RouteK20_Blocks:            INCBIN "maps/RouteK20.blk"
RouteK19_Blocks:            INCBIN "maps/RouteK19.blk"
RouteK10North_Blocks:       INCBIN "maps/RouteK10North.blk"
RouteK10South_Blocks:       INCBIN "maps/RouteK10South.blk"
RouteK13_Blocks:            INCBIN "maps/RouteK13.blk"
RouteK14_Blocks:            INCBIN "maps/RouteK14.blk"
RouteK15_Blocks:            INCBIN "maps/RouteK15.blk"
RouteK21_Blocks:            INCBIN "maps/RouteK21.blk"
	
OldMart_Blocks:
HitekkMart_Blocks:
	INCBIN "maps/Mart.blk"
OldPokecenter1F_Blocks:
SilentPokecenter1F_Blocks:
WestPokecenter1F_Blocks:
HitekkPokecenter1F_Blocks:
Route15Pokecenter1F_Blocks:
NewtypePokecenter1F_Blocks:
FountPokecenter1F_Blocks:
SouthPokecenter1F_Blocks:
BirdonPokecenter1F_Blocks:
SugarPokecenter1F_Blocks:
Route18Pokecenter1F_Blocks:
StandPokecenter1F_Blocks:
BluePokecenter1F_Blocks:
NorthPokecenter1F_Blocks:
	INCBIN "maps/Pokecenter1F.blk"
TradeCenter_Blocks:
TimeCapsule_Blocks:
	INCBIN "maps/TradeCenter.blk"

SECTION "Map Blocks 3", ROMX

CinnabarIsland_Blocks:      INCBIN "maps/CinnabarIsland.blk"
CinnabarPreErupt_Blocks:    INCBIN "maps/CinnabarPreErupt.blk"
FuchsiaCity_Blocks:         INCBIN "maps/FuchsiaCity.blk"
CeladonCity_Blocks:         INCBIN "maps/CeladonCity.blk"
PewterCity_Blocks:          INCBIN "maps/PewterCity.blk"
IndigoPlateau_Blocks:       INCBIN "maps/IndigoPlateau.blk"
SugarIsland_Blocks:         INCBIN "maps/SugarIsland.blk"
WestRocketHouse_Blocks:     INCBIN "maps/WestRocketHouse.blk"
WestRadio1F_Blocks:         INCBIN "maps/WestRadio1F.blk"
WestRadio2F_Blocks:         INCBIN "maps/WestRadio2F.blk"
WestRadio3F_Blocks:         INCBIN "maps/WestRadio3F.blk"
WestRadio4F_Blocks:         INCBIN "maps/WestRadio4F.blk"
WestRadio5F_Blocks:         INCBIN "maps/WestRadio5F.blk"
PlayersHouse2F_Blocks:      INCBIN "maps/PlayersHouse2F.blk"
MobileTradeRoom_Blocks:     INCBIN "maps/MobileTradeRoom.blk"
MobileBattleRoom_Blocks:    INCBIN "maps/MobileBattleRoom.blk"
RouteK22Gate_Blocks:        INCBIN "maps/RouteK22Gate.blk"
LavenderTown_Blocks:        INCBIN "maps/LavenderTown.blk"
SaffronCity_Blocks:         INCBIN "maps/SaffronCity.blk"
VermilionCity_Blocks:       INCBIN "maps/VermilionCity.blk"

Route1OldCityGateUpstairs_Blocks:
Route2WestCityGateUpstairs_Blocks:
Route12WestCityGateUpstairs_Blocks:
Route4SouthCityGateUpstairs_Blocks:
Route9SouthCityGateUpstairs_Blocks:
Route14to15GateUpstairs_Blocks:
	INCBIN "maps/GateUpstairs.blk"
